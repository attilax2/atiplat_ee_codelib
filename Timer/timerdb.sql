/*
Navicat MySQL Data Transfer

Source Server         : loc
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : timerdb

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-07-28 16:21:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `timer_tab`
-- ----------------------------
DROP TABLE IF EXISTS `timer_tab`;
CREATE TABLE `timer_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(255) NOT NULL,
  `timex` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of timer_tab
-- ----------------------------
INSERT INTO `timer_tab` VALUES ('1', '1', '0');
INSERT INTO `timer_tab` VALUES ('2', '2', '1510');
