package aaaAtiDslParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.attilax.io.pathx;
import com.attilax.lang.ParamX;
import com.attilax.net.websitex;
import com.attilax.text.regExpress;
import com.attilax.util.PropX;

public class JsX {

	public static void main(String[] args) throws NoSuchMethodException, ScriptException {
		
		//String rzt =new JsX(). get_timer_check_auth();
		String rzt =new JsX(). get_timer_check_auth();
		if(!rzt.trim().equals("ok"))
			throw new RuntimeException(rzt);
		
		System.out.println("--f:"+rzt);
		
	}

	public   String get_timer_check_auth() throws ScriptException,
			NoSuchMethodException {
		PropX px=new PropX(pathx.classPathParent()+"/cfg.properties");
		String apppath=px.getProperty("app-path");
		ScriptEngineManager manager = new ScriptEngineManager();   
		ScriptEngine engine = manager.getEngineByName("javascript");  
		
		
		Set<String> varNameList=px.loginprop.stringPropertyNames();
		Map<String,String> varList=getVarList(varNameList,px);
		
		
		List<String> stats=getStats(px);
		for (String stat : stats) {
			try {
				stat=new ParamX().sqlFmt_percentChar(stat, varList,  regExpress.ParamNameExpress_containByPercentChar);
				System.out.println(stat);
				engine.eval(stat);
			} catch (ScriptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		}
		Invocable invoke = (Invocable)engine; 
		String get_timer_check_auth_url =  (String) invoke.invokeFunction("get_timer_check_auth_url",apppath  ); 
		System.out.println( "get_timer_check_auth_url:"+get_timer_check_auth_url);
		String rzt=websitex.WebpageContent(get_timer_check_auth_url);
		return rzt;
	}

	private   Map<String,String> getVarList(Set<String> varNameList, PropX px) {
		 Map<String,String>  m=new HashMap<String, String>();
		 for (String varname : varNameList) {
			m.put(varname, px.getProperty(varname));
		}
		return m;
	}

	private   List<String> getStats(PropX px) {
		
		List<String> li=new ArrayList<String>();
		for(int i=0;i<1000;i++)
		{
		String stt=	px.getProperty("_stat_"+String.valueOf(i));
		 
			
		if(stt!=null)
		li.add(stt);
		}
		
		return li;
	}

}
