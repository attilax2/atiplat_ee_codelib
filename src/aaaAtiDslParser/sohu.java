package aaaAtiDslParser;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Aaa {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://attilax.blog.sohu.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAaa() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.linkText("登录")).click();
    driver.findElement(By.name("account")).clear();
    driver.findElement(By.name("account")).sendKeys("aaaaa");
    driver.findElement(By.name("account")).clear();
    driver.findElement(By.name("account")).sendKeys("attilax");
    driver.findElement(By.name("password")).clear();
    driver.findElement(By.name("password")).sendKeys("aaa000");
    driver.findElement(By.id("login_toolbar")).click();
    driver.findElement(By.cssSelector("div.entriesAdd > a")).click();
    driver.findElement(By.id("entrytitle")).clear();
    driver.findElement(By.id("entrytitle")).sendKeys("tittttttt");
    driver.findElement(By.id("save")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
