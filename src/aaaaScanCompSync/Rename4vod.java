/**
 * 
 */
package aaaaScanCompSync;

import java.util.function.Function;

import com.attilax.io.FileExt;
import com.attilax.io.filex;
import com.attilax.io.pathx;

import java.io.*;

import org.apache.commons.io.FileUtils;

/**aaaaScanCompSync.Rename4vod
 * @author attilax 2016年4月9日 下午3:23:13
 */
public class Rename4vod {

	/**
	 * attilax 2016年4月9日 下午3:23:13
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s = args[0];
		s = s.trim();
		Function<String, Object> fun = (fileItem) -> {
			try {
				String dir = new File(fileItem).getParentFile().getName();

				String dirPath = new File(fileItem).getParentFile().getPath();
				// String mainName=filex.getFileName_noExtName(fileItem);
				String extName = filex.getExtName(fileItem);
				String new_file = dirPath + "/" + dir + "." + extName;
				new_file = pathx.fixSlash(new_file);
				System.out.println(new_file);
				new File(fileItem).renameTo(new File(new_file));

				
			} catch (Throwable e) {
				e.printStackTrace();
			}
			return null;
		};
		new FileExt(s).trave(fun);

	}

}
