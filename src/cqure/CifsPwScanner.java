/* 
   Cifs Password Scanner
   Copyright (C) Patrik Karlsson 2004
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

package cqure;

import cqure.*;
import java.util.*;
import gnu.getopt.*;

public class CifsPwScanner {

    private static final String CIFS_SCANNER_VER = "1.0.5";
    private static final String CIFS_SCANNER_AUTHOR = "patrik@cqure.net";

    public static void usage() {
	System.err.println("");
	System.err.println("CifsPwScanner -t server -u users [options]");
	System.err.println("");
	System.err.println("options:");
	System.err.println("   -t <target>   - Server to scan");
	System.err.println("   -u <userfile> - File containing valid users");
	System.err.println("   -d <domain>   - Users domain");
	System.err.println("   -s <share>    - Share (default IPC$)"); 
	System.err.println("   -o <output>   - Output to file");
	System.err.println("   -p <passfile> - File containing passwords");
	System.err.println("   -v be verbose");
	System.err.println("");
	System.err.println("Version " + CIFS_SCANNER_VER + " by " + CIFS_SCANNER_AUTHOR );
	
    }


    public static void main (String args []) {

	FileListLoader oUsers = new FileListLoader();
	FileListLoader oPasswords = new FileListLoader();
	CifsGuesser oTh = null;
	AccountList oList = new AccountList();
	Logger oLogger = new Logger();
	String sUser = "", sServer = null, sDomain = "", sShare = null;
	String sUserfile = null, sPassfile = "passwords.default";
	String arg;
	long nStart = 0;
	Getopt oOpt = new Getopt("CifsPwScanner", args, "s:t:d:u:p:vo:h");
	int c;

	oLogger.setLogLevel( Logger.LOG_INFO );

	while ( ( c = oOpt.getopt() ) != -1 ) {

	    switch( c ) {

	    case 'd':
		arg = oOpt.getOptarg();
		sDomain = arg;
		break;

	    case 'h':
		usage();
		System.exit(1);

	    case 'o':
		arg = oOpt.getOptarg();
		oLogger.setLogFile( arg );
		break;

	    case 'p':
		arg = oOpt.getOptarg();
		sPassfile = arg;
		break;

	    case 's':
		arg = oOpt.getOptarg();
		sShare = arg;
		break;

	    case 't':
		arg = oOpt.getOptarg();
		sServer = arg;
		break;
		
	    case 'u':
		arg = oOpt.getOptarg();
		sUserfile = arg;
		break;

	    case 'v':
		oLogger.setLogLevel( Logger.LOG_DEBUG );
		break;

	    default:
		usage();
		System.exit(1);
	    }

	}

	if ( sUserfile == null || sServer == null ) {
	    usage();
	    System.exit(1);
	}

	oLogger.log( "CifsPwScanner version " + CIFS_SCANNER_VER + " by " +
		     CIFS_SCANNER_AUTHOR );
	oLogger.log( "-----------------------------------------------");

	if ( oUsers.loadFromFile(sUserfile) != FileListLoader.OK ) {
	    System.err.println("[x] Failed to open userfile");
	    System.exit(1);
	}

	if ( oPasswords.loadFromFile(sPassfile) != FileListLoader.OK ) {
	    System.err.println("[x] Failed to open passwordfile");
	    System.exit(1);
	}

	oLogger.log( "[-] Starting password scan against " + sServer );

	nStart = new GregorianCalendar().getTimeInMillis();

	/* Create a single guesser object */
	oTh = new CifsGuesser();

	while( ( sUser = oUsers.getNextItem() ) != null ) {
	    
	    oTh.setHost(sServer);
	    oTh.setUser(sUser);
	    oTh.setDomain(sDomain);

	    if ( sShare != null )
		oTh.setShare(sShare);

	    oTh.setPasswords(oPasswords);
	    oTh.setAccountList( oList );
	    oTh.setLogger( oLogger );
	    oTh.guessPasswords();
   
	}
   
	long l = (new GregorianCalendar().getTimeInMillis() - nStart ) / 1000;
	Long oL = new Long( l );

	oLogger.log("Total time = " + oL.toString() + " seconds" );

    }

}
