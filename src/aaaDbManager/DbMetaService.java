package aaaDbManager;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import aaaCfg.IocX4casher;
import aaaCfg.IocX4nodb;

import com.attilax.db.DBX;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;
import com.attilax.lang.MapX;
import com.attilax.web.ReqX;
import com.google.inject.Inject;

public class DbMetaService extends dbManager {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DbMetaService c = IocX4nodb.getBean(DbMetaService.class);

		IocX4nodb.db.set("shopedb");
		System.out.println(AtiJson.toJson(c.getPrimaryKeys(new HashMap() {
			{
				this.put("db", IocX4nodb.db.get());
				this.put("table", "底单申请表");

			}
		})));

	}

	protected List<Map> getColEx(Object reqx) {

		List<Map> li = new ArrayList<Map>();
		Map req = null;

		if (reqx instanceof HttpServletRequest)
			req = ReqX.toMap((HttpServletRequest) reqx);
		else
			req = (Map) reqx;
		String db = (String) req.get("db");
		String table = (String) req.get("table");
		String dir = pathx.appPath() + "/info_schema\\columns\\" + db + "\\"
				+ table + "";
		File f = new File(dir);
		File[] a = f.listFiles();
		for (File file : a) {
			String t = filex.read(file.getAbsolutePath());
			Map m = MapX.from(t);
			li.add(m);
		}

		return li;

	}

	@Inject
	DBX dbx;

	/**
	 * { "KEY_SEQ": 1, "TABLE_NAME": "底单申请表", "COLUMN_NAME": "id", "PK_NAME":
	 * "PRIMARY", "TABLE_CAT": "shopedb" }
	 * 
	 * @param req
	 * @return
	 */
	public List getPrimaryKeys(Map req) {

		// DBX dbx = IocX4casher.getBean(DBX.class);
		java.sql.DatabaseMetaData dbmd = null;

		try {
			try {
				dbmd = dbx.getConnection().getMetaData();
			} catch (com.attilax.biz.seo.getConnEx e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// databaseMetaData.getColumns(localCatalog, localSchema,
		// localTableName, null);
		String catalog = (String) req.get("db");
		ResultSet rs = null;
		try {
			Object tab = getTableName(req);
			
			rs = dbmd.getPrimaryKeys(catalog, "%", tab.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		List li = null;
		try {
			li = dbx.convertList(rs);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
		return li;
	}
	public String getPrimaryKeys(Connection conn,Object tab)
	{
		List<Map> li=getPrimaryKeys__all(conn,tab);
		return (String) li.get(0).get("COLUMN_NAME");
	}
	
	
	public List getPrimaryKeys__all(Connection conn,Object tab) {

		// DBX dbx = IocX4casher.getBean(DBX.class);
		java.sql.DatabaseMetaData dbmd = null;

		try {
			dbmd = conn.getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// databaseMetaData.getColumns(localCatalog, localSchema,
		// localTableName, null);
	//	String catalog = (String) req.get("db");
		ResultSet rs = null;
		try {
			// = getTableName(req);
			
			rs = dbmd.getPrimaryKeys(null, "%", tab.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		List li = null;
		try {
			li = dbx.convertList(rs);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
		return li;
	}


	private Object getTableName(Map req) {
		Object tab = req.get("$table");
		if(tab==null)
			tab = req.get("table");
		return tab;
	}

	public List getColumns(Map req) {

		DatabaseMetaData dbmd = null;

		try {
			dbmd = dbx.getConnection().getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (com.attilax.biz.seo.getConnEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);

		}
		// databaseMetaData.getColumns(localCatalog, localSchema,
		// localTableName, null);
		String catalog = "";
		if (req.get("catalog") == null)
			catalog = (String) req.get("db");
		else
			catalog = (String) req.get("catalog");
		ResultSet rs = null;
		try {
			Object tab = getTableName(req);
			rs = dbmd.getColumns(catalog, "%",tab.toString(),
					"%");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		List li = null;
		try {
			li = dbx.convertList(rs);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
		return li;
	}

	public String getPrimaryKey(Map req) {
	List<Map> li=getPrimaryKeys(req);
	 
		return (String) li.get(0).get("COLUMN_NAME");
	}

	public Set<String> getColumns_set(Map req) {
		
		List li=getColumns(req);
		
		return SetX.fromLi(li,"COLUMN_NAME");
	}

}
