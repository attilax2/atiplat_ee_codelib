package aaaPKg;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Map;

import com.attilax.auto.web.WebDriverUtil;
import com.attilax.io.pathx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.attilax.auto.web.WebDriverUtil;
import com.attilax.cmsPoster.BlogPubberMainform4bbs;
import com.attilax.coll.ListX;
import com.attilax.concur.TaskUtil;
import com.attilax.device.KeyboardUtil;
import com.attilax.exception.ExUtil;
import com.attilax.gui.WinUtil;
import com.attilax.html.htmlx;
import com.attilax.io.FileScript;
import com.attilax.io.SeriUtil;
import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Function;
import com.google.common.collect.Lists;

public class Doc360docPubber {
	private String rzt;
	private String title;

	FirefoxDriver driver = null;
	private String okli_file;

	public static Map<String, Object> propMap = new HashMap<String, Object>();

	public Doc360docPubber() {
		System.setProperty("webdriver.firefox.bin", pathx.classPathParent()
				+ "\\Mozilla Firefox\\firefox.exe");
		String f = "c:\\0seri\\okli.txt";
		iniOkLi(f);
	}

	private void iniOkLi(String f) {
		this.okli_file = f;
		List<String> li = null;
		try {
			li = filex.read2list(f);
		} catch (Exception e) {
			if (!new File(f).exists())
				li = Lists.newArrayList();
		}

		this.alreadyLi = li;

	}
	//ini
	MouseUtil mx = new MouseUtil();
	KeyboardUtil keyboardUtil = new KeyboardUtil(mx.robot);
	public static Logger logger = LoggerFactory
			.getLogger(Doc360docPubber.class);
	List<String> alreadyLi;

	public static void main(String[] args) {
		System.out.println("");
	 	TaskUtil.sleep_sec_withCountdown(3); //to open win
		String oriDir = "C:\\Users\\Administrator.ATTILAXPC188\\Documents";

		Doc360docPubber x = new Doc360docPubber();
		Function<String, Object> closure = new Function<String, Object>() {

			@Override
			public Object apply(String f) {
				if(f.contains("ati"))
				{
					System.out.println("dbg");
					
				}else
					return null;
				
			 	
				if(!isAtiDocFile(f))
					return null;
				
				logger.info("--now :"+f);
				System.out.println(x.alreadyLi.get(0).trim().length());
			 
				boolean contains = ListX.contain(x.alreadyLi,f);
						//x.alreadyLi.contains(f);
				logger.info("--alreadyLi contains rzt :"+contains+"  f:"+f);
				if( contains)
					return null;
				logger.info("--now will pub :"+f);
				x.publicDoc(f);
				
				// logger.info("--rzt " + String.valueOf(rezt) + " f:" + f);
				return null;
			}

			private boolean isAtiDocFile(String f) {
				String name = filex.getFileName_mainname(f);
				name = name.trim().toLowerCase();
				if (!name.contains("atitit"))
					return false;
				String ext = filex.getExtName(f);
				if (!ext.startsWith("doc"))
					return false;
				if(name.contains("acc"))
					return false;
				if(f.contains("/recy/"))
					return false;
				if(f.contains("\\recy\\"))
					return false;
				if(f.contains("notup"))
					return false;
				return true;
			}

		};
		new dirx().traveV3(oriDir, closure);
		System.out.println("--f");

	}

	protected void publicDoc(String f) {
		// open_mainPage("http://www.360doc.com/writedocument2.aspx",null);
		//active win 
		actWin(keyboardUtil);
	 	
	 	
		//---------log
		filex fx=filex.newFilex(this.okli_file);				
		fx.appendLine_flush_RE(f);
		fx.close_RE();

	 
		
	//	TaskUtil.sleep_sec_withCountdown(10);
		
		//goto pub page		
		gotoPubPage();
		
		//click up		
		clickUpBtn();
	 		
		selectFile(f);
	 
		submit();
		
		
		 
	
	}

	private void submit() {
		mx.mouseMove(1124, 704);
		mx.click();
		logger.info("--submit doc finish");
		TaskUtil.sleep_sec(5);  //submit
	}

	private void selectFile(String f) {
		keyboardUtil.keyPressString(f).keyPress_enter();
		TaskUtil.sleep_sec(3);  //submit
	}

	private void clickUpBtn() {
		mx.mouseMove(650, 361);			
		mx.click();
		// r.mousePress();
		TaskUtil.sleep_sec(2); //wait to open box
	}

	private void gotoPubPage() {
		mx.mouseMove(847,464);  
		mx.click();
		TaskUtil.sleep_sec(1);
		keyboardUtil.keyPressWithCtrl( KeyEvent.VK_TAB);
		TaskUtil.sleep_sec(3);
		logger.info("--goto pub page op finish");
	}

	private void actWin(KeyboardUtil keyboardUtil) {
		new WinUtil().WinActivate("我的图书馆 - 360安全浏览器");
	//	new WinUtil().WinActivate("我的图书馆 - 360安全浏览器");
	//	new WinUtil().WinActivate("上传文档 - 360安全浏览器");
	 	
	 	
	   TaskUtil.sleep_sec(1);
		//KeyboardUtil keyboardUtil = new KeyboardUtil();
		keyboardUtil.keyPress(KeyEvent.VK_F6);
		String string = "http://www.360doc.com/writedocument2.aspx";
		string="http://www.360doc.com/myfollow.aspx";
	 	//string="xx";
		keyboardUtil.keyPressString(string);
	 	keyboardUtil.keyPress_enter();
	    TaskUtil.sleep_sec(3);
	}

	public void open_mainPage(final String pubUrl, final Map blogMap) {
		System.out.println("");
		Runnable runnable = new Runnable() {

			@SuppressWarnings("static-access")
			@Override
			public void run() {
				WebDriverUtil wdu = WebDriverUtil.newx();

				driver = getDriverInstan(blogMap, wdu, driver);
				// try {
				// String baseUrl="http://write.blog.csdn.net/postedit";

				wdu.closeAlert(driver);
				// PropX px=(PropX) propMap.get(blogMap);
				// String pubUrl = (String) blogMap.get("pub_url");
				// String blog_index=px.getProperty("blog_index");

				driver.get(pubUrl);

				String sou = driver.getPageSource();
				String title = driver.getTitle();

				sou = driver.getPageSource();
				// JavascriptExecutor jse = (JavascriptExecutor) driver;
				// Object obj=
				// driver.executeScript("	return document.title; ",
				// driver.findElement(By.id("editor")));
				// title = (String) obj;
				// driver.getTitle();jeig yaosi manu op ..laoxsh outtime
				// value ..

				// publish(keyword, driver,blogMap);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }

			}

		};
		try {
			runnable.run();
		} catch (Exception e) {
			ExUtil.throwExV2(e);
		}
		// should ays first by blogger,,thern artis
		// core.execMeth_Ays(runnable, "threadName"+blogMap);

	}

	private FirefoxDriver getDriverInstan(final Map blogMap, WebDriverUtil wdu,
			FirefoxDriver driver) {
		try {
			// driver=(FirefoxDriver)
			// propMap.get(blogMap.get("pubber").toString()+"_driver");
			if (driver != null)
				if (wdu.isDisconn(driver))
					driver = null;
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (driver == null) {
			driver = new FirefoxDriver();
			// propMap.put(blogMap+"_driver",driver);
		}
		return driver;
	}

}
