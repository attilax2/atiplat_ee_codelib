package aaaPKg;
@FunctionalInterface
public interface ExCaptor {

	  public abstract void run(Exception e);
}
