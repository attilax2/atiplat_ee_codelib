package aaaAddr;

import java.lang.reflect.Type;
import java.util.Map;

import com.attilax.io.filex;
import com.google.gson.Gson;
/**
 * Java对象和JSON字符串相互转化工具类
 * @author penghuaiyi
 * @date 2013-08-10
 */
public final class JsonUtil {
	
	
	public static void main(String[] args) {
		String f="D:\\workspace 空格\\AtiPlatf\\info_schema\\columns\\shopedb\\exp\\img.txt";
		String t=filex.read(f);
	Map m=new 	Gson().fromJson(t, Map.class);
	System.out.println(m.toString());
	}
	
	private JsonUtil(){}
	
    /**
     * 对象转换成json字符串
     * @param obj 
     * @return 
     */
    public static String toJson(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    /**
     * json字符串转成对象
     * @param str  
     * @param type
     * @return 
     */
    public static <T> T fromJson(String str, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(str, type);
    }

    /**
     * json字符串转成对象
     * @param str  
     * @param type 
     * @return 
     */
    public static <T> T fromJson(String str, Class<T> type) {
        Gson gson = new Gson();
        return gson.fromJson(str, type);
    }

}
