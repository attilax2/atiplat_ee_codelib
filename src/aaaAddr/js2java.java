package aaaAddr;

import java.io.UnsupportedEncodingException;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

public class js2java {

	public static void main(String[] args) throws ScriptException, UnsupportedEncodingException {
		// Nashorn——在JDK 8中融合Java与JavaScript之力.htm
		// …或者你可以在Java中传递绑定，它们是可以从JavaScript引擎内部访问的全局变量
		byte[] a="本".getBytes("gbk");  //[-79, -66]
		byte[] a2="本".getBytes("unicode");
		byte[] a3="本".getBytes("utf8");
		  
		byte[] ai={44, 103, 51, 0, 51, 0, 77, 0};
		System.out.println( new String(ai,"UnicodeLittleUnmarked")  );
		
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");

		int valueIn = 10;
		SimpleBindings simpleBindings = new SimpleBindings();
		simpleBindings.put("globalValue", valueIn);
		simpleBindings.put("extObj", new Obj());
		Object o = nashorn.eval("print (globalValue)", simpleBindings);
		  o = nashorn.eval("print (extObj.play('aaa'))", simpleBindings);
		System.out.println(o);
	}
}