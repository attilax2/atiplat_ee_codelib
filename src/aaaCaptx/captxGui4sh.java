package aaaCaptx;

import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.SQLXML;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import net.sf.json.JSONObject;

import com.attilax.Closure;
import com.attilax.captcha.CaptchaSvr4Tesseract;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Base64;
import com.attilax.lang.IniEx;
import com.attilax.lang.ParamX;
import com.attilax.shbye.YejmX;
import com.attilax.ui.FileSelector;
import com.attilax.ui.HTML5Form;

/**
 * aaaCaptx aaaCaptx.captxGui
 * 
 * @author Administrator
 *
 */
@SuppressWarnings("all")
public class captxGui4sh {
	public static YejmX yjmx = new YejmX();
	
 

	public static void main(String[] args) throws IniEx {

		final HTML5Form ef = new HTML5Form(pathx.classPathParent_jensyegeor()
				+ "/capt/captch_index.html");

		ef.regJsCallbackMeth("save_localhost_pic", new Closure() {

			@Override
			public Object execute(Object arg0) throws Exception {

				List li = (List) arg0;
				String params = (String) (li.get(0));
				Map mp = new ParamX().urlParams2Map(params);

				String f = (String) mp.get("filex");
				File source = new File(f);
				File destFile = new File(pathx.classPathParent_jensyegeor()
						+ "/captch/img/" + source.getName());
				// FileUtils.copyFileToDirectory(source,);
				FileUtils.copyFile(source, destFile);
				String rzt = "img/" + source.getName();

				byte[] read4img = filex.read4img(destFile.getAbsolutePath());
				String base = Base64.encode(read4img, false);
				rzt = "data:image/jpeg;base64," + base;

				String call = (String) mp.get("$callback");
				String js2 = call + "('" + rzt + "');";
				boolean rzt2 = ef.atiBrowserExampleinstance.executeJsTxt(js2);
				return null;
			}
		});

		ef.regJsCallbackMeth("recog", new Closure() {

			@Override
			public Object execute(Object arg0) throws Exception {

			 
				List li = (List) arg0;
				String params = (String) (li.get(0));
				Map mp = new ParamX().urlParams2Map(params);

				String pathname = (String) mp
						.get("filex");

				String rzt =	new CaptchaSvr4Tesseract().getCaptch(pathname);
			 

				String call = (String) mp.get("$callback");
				String js2 = call + "('" + rzt + "');";
				boolean rzt2 = ef.atiBrowserExampleinstance.executeJsTxt(js2);
				return null;
			}
		});

		ef.setSize(550, 500);
		ef.show();

		ef.threadRecycle();

	}

}
