/**
 * 
 */
package aaaCfg;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.attilax.lang.Trigger;
import com.attilax.lang.YamlAtiX;

/**  aaaCfg.DataMapper4vod
 * @author attilax
 *2016年4月7日 下午4:25:59
 */
@SuppressWarnings("all") 
public class DataMapper4vod  extends Trigger {
	
	
	public Object exec(Object object) {
		return exe((List<Map>) object);
	}
	
	public List<Map>  exe(List<Map> li)
	{
		String s="data_id:title,title:title,thumb:posterPicString,bigpic:fanartPicString,desc:t_plot";
	Map mpr=	YamlAtiX.getMapReverse_fromTxt(s);
		for (Map m : li) {
			Set<String> keys=mpr.keySet();
			for (String k : keys) {
				//k==thumb:posterPicString;
				Object thumb = mpr.get(k);
				m.put(thumb.toString(), m.get(k));
			}
//			if(m.get("thumb")==null)
//				m.put("thumb", m.get("bigpic"));
 			if(m.get("bigpic")==null || m.get("bigpic").toString().trim().length()==0)
 				m.put("bigpic", m.get("thumb"));
			if(m.get("data_id")==null)
				m.put("data_id", m.get("title"));
			m.put( "thumb", "maindir/"+m.get("thumb"));			
			m.put("bigpic", "maindir/"+m.get("bigpic"));
			m.put( "playURL", "maindir/"+m.get("playURL")  );
			
		}
		return li;
		
		
	}
	

}
