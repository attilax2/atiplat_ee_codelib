/**
 * 
 */
package com.alipay.config;

import java.util.Map;

import com.attilax.json.AtiJson;
import com.google.common.collect.Maps;

/**
 * @author attilax
 *2016年11月28日 下午4:24:58
 */
public class MapSql {

	/**
	attilax    2016年11月28日  下午4:24:58
	 * @param args
	 */
	public static void main(String[] args) {
		Map m=Maps.newConcurrentMap();
		m.put("op","select");
		m.put("table", "tab1");
		m.put("where"," a=1");
		System.out.println(AtiJson.toJson(m));

	}

}
