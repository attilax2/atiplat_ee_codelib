package com.focusx.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;








import aaaCfg.IocX;

import com.attilax.core;
import com.attilax.jsonX;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Pluginx;
import com.sun.image.codec.jpeg.JPEGCodec;

/** 
 * com.focusx.util
 * EncodingFilter.java 
 * author:vincente  2013-8-19 
 */
public class EncodingFilter implements Filter {

	private String charset;  
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("all")
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		 
	
		try {
			
			new IocX().ini();
			Map m=new HashMap ();
			m.put("req", request);
			m.put("res", response);
			new Pluginx().do_action("chinese_filename_hook",m,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	//	org.apache.commons.logging.impl.Log4JLogger
			
		 
		core.log("--loadorderO9::EncodingFilter");
		//用init方法取得的charset覆盖被拦截下来的request对象的charset  
        request.setCharacterEncoding(this.charset);  
        //将请求移交给下一下过滤器，如果还有的情况下。  
        chain.doFilter(request, response);  
	}

	
//	private String getImgPath(String url2) {
//		String str;
//		try {
//			str = new String(url2.toString().getBytes("iso8859_1"), "GB2312");
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e);
//		}
//		String f = pathx.webAppPath() + File.separator + str;
//		File file = new File(f);
//		return f;
//	}
	public void init(FilterConfig config) throws ServletException {
		//从web.xml中的filter的配制信息中取得字符集  
        this.charset = config.getInitParameter("charset");  
	}

}
