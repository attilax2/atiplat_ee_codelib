package com.csmy.my.center.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesConfig {
	
	private static PropertiesConfig propertiesConfig;
	
	private Properties prop = null;
	
	private PropertiesConfig(){}
	
	public static PropertiesConfig getInstance(){
		if(propertiesConfig == null){
			propertiesConfig = new PropertiesConfig();
		}
		return propertiesConfig;
	}
	
	public void setFile(String fileUrl){
		InputStream ins = null;
		try {
			prop = new Properties();
			File file = new File(fileUrl);
			ins = new FileInputStream(file);
			prop.load(ins);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getProperties(String key){
		try {
			return prop.getProperty(key);
		} catch (Exception e) {
			return null;
		}
	}
}
