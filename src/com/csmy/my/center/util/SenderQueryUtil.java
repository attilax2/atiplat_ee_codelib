package com.csmy.my.center.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Random;

import com.csmy.my.center.util.dataconvert.json.JsonHelper;

/**
 * 获取物流单号
 * @author jackie
 *
 */
public class SenderQueryUtil {
	
	/**
     * 获取URL返回的字符串
     * @param callurl
     * @param charset
     * @return
     */
    private static String callUrlByGet(String callurl,String charset){   
        String result = "";   
        try {   
            URL url = new URL(callurl);   
            URLConnection connection = url.openConnection();   
            connection.connect();   
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),charset));   
            String line;   
            while((line = reader.readLine())!= null){    
                result += line;   
                result += "\n";
            }
        } catch (Exception e) {   
            e.printStackTrace();   
            return "";
        }
        if(CTUtils.isNotEmpty(result)){
        	result = result.replaceAll("wlxx", "");
        	result = result.replaceAll("\\(", "");
        	result = result.replaceAll("\\)", "");
        }
        return result;
    }
    
    /**
     * 物流查询
     * @param dh 快递单号
     * @param gs 快递公司
     * @return
     */
    public static String getWuliu1(String dh,String gs){
        if(CTUtils.isNotEmpty(dh)){
        	String enMailNo =UniqueID.getUniqueID(12, 2);//随机字符串
        	long cuttime = System.currentTimeMillis();
        	/*if(gs.indexOf("shunfeng")!=-1){
        		gs = "shunfeng";
        		enMailNo = "8f3ciffglidl";
        	}
			if(gs.indexOf("huitong")!=-1){
				gs = "huitong";
        		enMailNo = "cb9amblepejj";
        	}
			if(gs.indexOf("yuantong")!=-1){
				gs = "yuantong";
        		enMailNo = "cf5amfhepifj";
        	}
			if(gs.indexOf("zhongtong")!=-1){
				gs = "zhongtong";
        		enMailNo = "aecfkeojnhmo";
        	}
			if(gs.indexOf("shentong")!=-1){
				gs = "shentong";
        		enMailNo = "cd3amdfepgdj";
        	}
			if(gs.indexOf("yunda")!=-1){
				gs = "yunda";
        		enMailNo = "5f9affleiijj";
        	}
			if(gs.indexOf("ems")!=-1){
				gs = "ems";
        		enMailNo = "6j76gjjajmhf";
        	}
			if(gs.indexOf("tiantian")!=-1){
				gs = "tiantian";
        		enMailNo = "9e79jejdmhhi";
        	}*/
			        	
        	
            String url = "http://biz.trace.ickd.cn/"+gs+"/"+dh+"?ts="+cuttime+"&enMailNo="+enMailNo+"&callback=wlxx&_"+cuttime+"=";
            return callUrlByGet(url,"GBK");
        }else{
        	return null;
        }

    }
	
    /**
     * 查询物流信息
     */
    public static String getWuliu(String dh,String gs){
        if(CTUtils.isNotEmpty(dh)){
        	String queryKey ="";//查询key
        	long cuttime = System.currentTimeMillis();
        	//快递接口类型
        	String apiType = CTUtils.getParamValue("SENDER_API_TYPE");
        	//快递接口key
        	String apiKeys = CTUtils.getParamValue("SENDER_API_KEY");
        	
        	if(gs.contains("shunfeng")||gs.equalsIgnoreCase("SF")){
        		gs = "shunfeng";
        	}
			if(gs.indexOf("huitong")!=-1){
				gs = "huitongkuaidi";
        	}
			if(gs.contains("yuantong")||gs.equalsIgnoreCase("YTO")){
				gs = "yuantong";
        	}
			if(gs.indexOf("zhongtong")!=-1){
				gs = "zhongtong";
        	}
			if(gs.indexOf("shentong")!=-1||gs.equalsIgnoreCase("STO")){
				gs = "shentong";
        	}
			if(gs.contains("yunda")){
				gs = "yunda";
        	}
			if(gs.contains("ems")){
				gs = "ems";
        	}
			if(gs.contains("tiantian")){
				gs = "tiantian";
        	}
			
			if(gs.contains("youshuwuliu")||gs.equalsIgnoreCase("YS")){
				gs = "youshuwuliu";
        	}
			
			if(gs.contains("zhaijisong")||gs.equalsIgnoreCase("ZJS")){
				gs = "zhaijisong";
        	}
			
			String queryUrl = "";
			if(CTUtils.isEmpty(apiType)||CTUtils.isEmpty(apiKeys)){
				queryUrl = "http://www.kuaidi100.com/query?type="+gs+"&postid="+dh+"&id=1&valicode=&temp="+cuttime; 
			}else{//不为空
				if(apiType.equals("1")){
					//随机获取一个key
					String [] keys = null;
					Random rnd = new Random();
					if(apiKeys.contains("|")){
					   keys = apiKeys.split("\\|");
					   int rn = rnd.nextInt(keys.length);
					   queryKey = keys[rn];
					}
					StringUtil.xprint("查询获取的key为:【"+queryKey+"】");
					queryUrl = "http://api.kuaidi100.com/api?id="+queryKey+"&com="+gs+"&nu="+dh+"&show=0&muti=1&order=desc"; 
				}else{
					queryUrl = "http://www.kuaidi100.com/query?type="+gs+"&postid="+dh+"&id=1&valicode=&temp="+cuttime; 
				}
			}
        	
			StringUtil.xprint("url="+queryUrl);
            return callUrlByGet(queryUrl,"UTF-8");
        }else{
        	return null;
        }

    }
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//http://biz.trace.ickd.cn/huitong/280284827323?ts=1421165104619&enMailNo=cb9amblepejj&callback=_jqjsp&_1421165104634=
		//http://biz.trace.ickd.cn/yuantong/280284827323?ts=1421166244259&enMailNo=cf5amfhepifj&callback=_jqjsp&_1421166244259=
		//http://biz.trace.ickd.cn/zhongtong/280284827323?ts=1421166269947&enMailNo=aecfkeojnhmo&callback=_jqjsp&_1421166269947=
		//http://biz.trace.ickd.cn/shentong/280284827323?ts=1421166304039&enMailNo=cd3amdfepgdj&callback=_jqjsp&_1421166304039=
		//http://biz.trace.ickd.cn/yunda/280284827323?ts=1421166334652&enMailNo=5f9affleiijj&callback=_jqjsp&_1421166334652=
		//http://biz.trace.ickd.cn/shunfeng/280284827323?ts=1421166666055&enMailNo=8f3ciffglidl&callback=_jqjsp&_1421166666055=
		//http://biz.trace.ickd.cn/ems/280284827323?ts=1421166840493&enMailNo=6j76gjjajmhf&callback=_jqjsp&_1421166840508=
		//http://biz.trace.ickd.cn/tiantian/280284827323?ts=1421166963446&enMailNo=9e79jejdmhhi&callback=_jqjsp&_1421166963446=
		try {
			/*long cur = 280295323249L;
			Map cmap = null;
			for (int i = 0; i < 10; i++) {
				Thread.sleep(1000);
				cur = cur+i;
				//StringUtil.xprint("wuxx="+getWuliu(String.valueOf(cur), "huitong"));
				cmap = JsonHelper.toMap(getWuliu(String.valueOf(cur), "huitong"));
				StringUtil.xprint("cmap["+i+"]="+cmap);
				
			}*/
			
			StringUtil.xprint("wuxx="+getWuliu1("886325199202", "tiantian"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
