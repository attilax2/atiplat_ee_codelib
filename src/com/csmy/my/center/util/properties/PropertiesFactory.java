package com.csmy.my.center.util.properties;

import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.attilax.exception.ExUtil;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

/**
 * Properties文件静态工厂
 * 
 * @author wgp
 * @since 2012-08-2
 */
@SuppressWarnings("unchecked")
public class PropertiesFactory {
	private static Log log = LogFactory.getLog(PropertiesFactory.class);
	/**
	 * 属性文件实例容器
	 */
	private static Dto container = new BaseDto();
	
	static{
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			if (classLoader == null) {
				   classLoader = PropertiesFactory.class.getClassLoader();
				}
			//加载属性文件global.properties
			try {
				  InputStream is = classLoader.getResourceAsStream("global.ct.properties");
				  PropertiesHelper ph = new PropertiesHelper(is);
				  container.put(PropertiesFile.G4, ph);
			  } catch (Exception e1) {
			    log.error(CTConstants.Exception_Head + "加载属性文件global.ct.properties出错!");
			    e1.printStackTrace();
			  throw new RuntimeException("加载属性文件global.ct.properties出错");
			  }
		     //加载属性文件global.myconfig.properties
			 try {
				InputStream is = classLoader.getResourceAsStream("global.app.properties");
				PropertiesHelper ph = new PropertiesHelper(is);
				container.put(PropertiesFile.APP, ph);
			 } catch (Exception e1) {
				log.error(CTConstants.Exception_Head + "加载属性文件global.app.properties出错!");
				e1.printStackTrace();
			}
	}
	
    /**
     * 获取属性文件实例
     * @param pFile 文件类型
     * @return 返回属性文件实例
     */
	public static PropertiesHelper getPropertiesHelper(String pFile){
		PropertiesHelper ph = (PropertiesHelper)container.get(pFile);
		return ph;
	}
}
