package com.csmy.my.center.util.idgenerator;

import com.csmy.my.center.util.id.generator.DefaultIDGenerator;

/**
 * ID生成器 静态类解决多线程并发访问生成ID的问题
 * 此类第一次实例化会执行所有的static代码块，
 * 如果想按需加载这些ID生成器，则应该一个ID写一个静态类就可以
 * 
 * @author wgp
 * @since 2013-08-06
 */
public class IDHelper {

	/**
	 * CODEID
	 */
	private static DefaultIDGenerator defaultIDGenerator_codeid = null;
	
	/**
	 * PARAMID
	 */
	private static DefaultIDGenerator defaultIDGenerator_paramid = null;
	
	/**
	 * USERID
	 */
	private static DefaultIDGenerator defaultIDGenerator_userid = null;
	
	
	static {
		IdGenerator idGenerator_codeid = new IdGenerator();
		idGenerator_codeid.setFieldname("CODEID");
		defaultIDGenerator_codeid = idGenerator_codeid.getDefaultIDGenerator();
	}

	static {
		IdGenerator idGenerator_paramid = new IdGenerator();
		idGenerator_paramid.setFieldname("PARAMID");
		defaultIDGenerator_paramid = idGenerator_paramid.getDefaultIDGenerator();
	}
	
	static {
		IdGenerator idGenerator_userid = new IdGenerator();
		idGenerator_userid.setFieldname("USERID");
		defaultIDGenerator_userid = idGenerator_userid.getDefaultIDGenerator();
	}
	
	/**
	 * 返回CODEID
	 * 
	 * @return
	 */
	public static String getCodeID() {
		return defaultIDGenerator_codeid.create();
	}
	
	/**
	 * 返回PARAMID
	 * 
	 * @return
	 */
	public static String getParamID() {
		return defaultIDGenerator_paramid.create();
	}
	
	/**
	 * 返回USERID
	 * 
	 * @return
	 */
	public static String getUserID() {
		return defaultIDGenerator_userid.create();
	}
	
}
