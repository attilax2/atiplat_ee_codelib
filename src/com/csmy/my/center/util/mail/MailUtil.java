package com.csmy.my.center.util.mail;

import java.util.ArrayList;
import java.util.List;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

public class MailUtil {

	/**
	 * 发送html格式邮件
	 * @param orderDto
	 */
	public static void sendHtmlEmail(Dto orderDto){

		String user_mail =orderDto.getAsString("uemail");
		String cust_mail =orderDto.getAsString("cemail");
		List<String>rev_email = new ArrayList<String>();
		if(CTUtils.isNotEmpty(user_mail)){
			StringUtil.xprint("会员邮箱为："+user_mail);
			rev_email.add(user_mail);
		}
		if(CTUtils.isNotEmpty(cust_mail)){
			StringUtil.xprint("商户邮箱为："+cust_mail);
			rev_email.add(cust_mail);
		}
		
		String titleString="新订单详情提醒";
		String mailContent = "【订单编号】："+orderDto.getAsString("order_id")+"<br/>";
			   mailContent +="【商户名称】："+orderDto.getAsString("cust_name")+"<br/>";
			   mailContent +="【所属会员】："+orderDto.getAsString("user_acc")+"<br/>";
			   mailContent +="【订购产品】："+orderDto.getAsString("good_name")+"<br/>";
			   mailContent +="【订购数量】："+orderDto.getAsString("buy_num")+"<br/>";
			   mailContent +="【订单金额】：￥"+orderDto.getAsString("money")+"<br/>";
		       mailContent +="【下单时间】："+orderDto.getAsString("order_time")+"<br/>";
		       mailContent +="【下单人IP】："+orderDto.getAsString("ip")+"<br/>";
		       mailContent +="【收货姓名】："+orderDto.getAsString("buy_name")+"<br/>";
		       mailContent +="【所在地区】："+orderDto.getAsString("area")+"<br/>";
		       mailContent +="【订单类型】："+orderDto.getAsString("log")+"<br/>";
		       mailContent +="【详细地址】："+orderDto.getAsString("address");
		
		boolean bool = MailService.sendHtmlMail(rev_email, titleString, mailContent);
		if(bool){
			StringUtil.xprint("提醒邮件发送成功！！！");
		}else{
			StringUtil.xprint("提醒邮件发送失败！！！");
		}
	}
	

	/**
	 * 发送html格式邮件
	 * @param orderDto
	 */
	public static void sendRegEmail(Dto userDto){

		
		String titleString="新会员注册提醒";
		String mailContent = "【会员编号】："+userDto.getAsString("user_id")+"<br/>";
			   mailContent +="【会员账号】："+userDto.getAsString("account")+"<br/>";
			   mailContent +="【QQ号码】："+userDto.getAsString("qq_num")+"<br/>";
			   mailContent +="【电子邮箱】："+userDto.getAsString("email")+"<br/>";
			   mailContent +="【手机号码】："+userDto.getAsString("phone")+"<br/>";
			   mailContent +="【账号类型】："+userDto.getAsString("tp_name")+"<br/>";
		       mailContent +="【注册时间】："+userDto.getAsString("create_time");
		
		boolean bool = MailService.sendHtmlMail(null, titleString, mailContent);
		if(bool){
			StringUtil.xprint("提醒邮件发送成功！！！");
		}else{
			StringUtil.xprint("提醒邮件发送失败！！！");
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		sendHtmlEmail(new BaseDto());

	}

}
