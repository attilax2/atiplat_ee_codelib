package com.csmy.my.center.util.mail;

import javax.mail.*;   
/**
 * 自定义认证器类，通过用户名和密码认证
 * @author jackie
 * @version date:2014-2-1
 */
public class MyAuthenticator extends Authenticator{   
    String userName=null;   
    String password=null;   
        
    public MyAuthenticator(){   
    }   
    public MyAuthenticator(String username, String password) {    
        this.userName = username;    
        this.password = password;    
    }    
    protected PasswordAuthentication getPasswordAuthentication(){   
        return new PasswordAuthentication(userName, password);   
    }   
}   
