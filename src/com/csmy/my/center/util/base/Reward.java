package com.csmy.my.center.util.base;

import java.io.Serializable;

public class Reward implements Serializable{

	private static final long serialVersionUID = 6736579284044686962L;
	public String order_id;
	public String name;
	public String qq_num;
	public String good_name;
	public String state_name;
	public String code_desc;
	public String order_remark;
	public double sku_pmoney;
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQq_num() {
		return qq_num;
	}
	public void setQq_num(String qq_num) {
		this.qq_num = qq_num;
	}
	public String getGood_name() {
		return good_name;
	}
	public void setGood_name(String good_name) {
		this.good_name = good_name;
	}
	public String getCode_desc() {
		return code_desc;
	}
	public void setCode_desc(String code_desc) {
		this.code_desc = code_desc;
	}
	public String getOrder_remark() {
		return order_remark;
	}
	public void setOrder_remark(String order_remark) {
		this.order_remark = order_remark;
	}
	public double getSku_pmoney() {
		return sku_pmoney;
	}
	public void setSku_pmoney(double sku_pmoney) {
		this.sku_pmoney = sku_pmoney;
	}
	public String getState_name() {
		return state_name;
	}
	public void setState_name(String state_name) {
		this.state_name = state_name;
	}
	
	
	

}
