package com.csmy.my.center.util.sms;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.properties.PropertiesFactory;
import com.csmy.my.center.util.properties.PropertiesFile;
import com.csmy.my.center.util.properties.PropertiesHelper;

public class SMS_Sender {
	
	public static String userName;
	public static String passWord;
	public static String reqUrl;
	
	public static PropertiesHelper pHelper;
	    
	//初始化
	static{
		pHelper = PropertiesFactory.getPropertiesHelper(PropertiesFile.APP);
		//获取账号密码
		userName = pHelper.getValue("sms_username");
		passWord = pHelper.getValue("sms_password");
		reqUrl = pHelper.getValue("sms_url");
	}
	
	/**
	 * 发送短信内容
	 * @param mobile
	 * @param msgText
	 * @return
	 */
	public static boolean sendSms(String mobile,String msgText) {
		
		boolean temp = false;
		if(CTUtils.isNotEmpty(mobile) && CTUtils.isNotEmpty(msgText)){
			
			HttpClient client = new HttpClient(); 
			PostMethod method = new PostMethod(reqUrl);
			client.getParams().setContentCharset("UTF-8");
			method.setRequestHeader("ContentType","application/x-www-form-urlencoded;charset=UTF-8");
			NameValuePair[] data = {//提交短信
				    new NameValuePair("account", userName), 
				    new NameValuePair("password", passWord), //密码可以使用明文密码或使用32位MD5加密
				    new NameValuePair("mobile", mobile), 
				    new NameValuePair("content", msgText),
			};
			method.setRequestBody(data);
			
			try {
				
				client.executeMethod(method);	
				String SubmitResult =method.getResponseBodyAsString();
				System.out.println(SubmitResult);
				Document doc = DocumentHelper.parseText(SubmitResult); 
				Element root = doc.getRootElement();
				String code = root.elementText("code");	
				String msg = root.elementText("msg");	
				String smsid = root.elementText("smsid");
				
				System.out.println("code="+code);
				System.out.println("msg="+msg);
				System.out.println("smsid="+smsid);
				
				if(code.equals("2")){
					temp = true;
				}else{
					temp = false;
				}
				
			} catch (HttpException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}	
		}
		return temp;
		
	}
	
	//发送验证码
	@SuppressWarnings("unchecked")
	public static Dto getSmsCode(String mobile) {
		Dto reDto = new BaseDto();
		HttpClient client = new HttpClient(); 
		PostMethod method = new PostMethod(reqUrl);
		client.getParams().setContentCharset("UTF-8");
		method.setRequestHeader("ContentType","application/x-www-form-urlencoded;charset=UTF-8");
		
		int mobile_code = (int)((Math.random()*9+1)*100000);
	    String content = new String("您的验证码是：" + mobile_code + "。请不要把验证码泄露给其他人。"); 
		NameValuePair[] data = {//提交短信
			    new NameValuePair("account", userName), 
			    new NameValuePair("password", passWord), //密码可以使用明文密码或使用32位MD5加密
			    //new NameValuePair("password", util.StringUtil.MD5Encode("密码")),
			    new NameValuePair("mobile", mobile), 
			    new NameValuePair("content", content),
		};
		
		method.setRequestBody(data);		
		try {
			client.executeMethod(method);	
			String SubmitResult =method.getResponseBodyAsString();
			//System.out.println(SubmitResult);
			Document doc = DocumentHelper.parseText(SubmitResult); 
			Element root = doc.getRootElement();
			String code = root.elementText("code");	
			String msg = root.elementText("msg");	
			String smsid = root.elementText("smsid");	
			
			System.out.println(code);
			System.out.println(msg);
			System.out.println(smsid);
						
			if(code.equals("2")){
				reDto.put("msg", "短信验证码发送成功");
				reDto.put("rcode", code);
			}else{
				reDto.put("msg", msg);
				reDto.put("rcode", code);
			}
			reDto.put("vcode", mobile_code);
			
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}	
		
		return reDto;
	}

}
