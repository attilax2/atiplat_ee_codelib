package com.csmy.my.center.util.exception;

import com.csmy.my.center.util.CTConstants;

/**
 * 公共异常类<br>
 * 
 * @author wgp
 * @since 2013-01-15
 */
public class G4Exception extends RuntimeException {


	private static final long serialVersionUID = 1629401320050583609L;

	public G4Exception() {
		super();
	}

	public G4Exception(String msg) {
		super(CTConstants.Exception_Head + msg);
	}
}
