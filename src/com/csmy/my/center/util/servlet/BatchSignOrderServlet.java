package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.SenderQueryUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.dataconvert.json.JsonHelper;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class BatchSignOrderServlet extends HttpServlet {
	
	private static final long serialVersionUID = 5495350044984253386L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		boolean temp = false;
		int countNmu = 0;
		try {
			Dto dto =  RequestUtil.getPraramsAsDto(req);
			int max_num = dto.getAsInteger("max_num");
			dto.put("pageSize", "10000");
			List<Dto> orderList = JdbcTemplateTool.getCTOrderList(dto).getDatas();
			if(CTUtils.isNotEmpty(orderList)){
				
				String kddh = null;
				String wlgs = null;
				String result = null;
				
				int isChecked = 0;//默认值为未签收
				double totalMoney=0.0;
				double totalPMoney=0.0;
				double totalSMoney=0.0;
				
				Dto inDto = null;
				Dto intDto = null;
				Dto resultMap = null;
				
				for (Dto orderInfo : orderList) {
					//获取物流单号和物流公司
					kddh = orderInfo.getAsString("courier_id");
					wlgs = orderInfo.getAsString("sender_type");
				//	org.apache.commons.lang.exception.NestableRuntimeException
					//调用物流查询接口
					result = SenderQueryUtil.getWuliu(kddh, wlgs);
					if(CTUtils.isNotEmpty(result)){
						resultMap = JsonHelper.toMap(result);
						StringUtil.xprint("获取结果为>>>>"+resultMap);
						if(!resultMap.containsKey("ischeck")) break;
						//获取是否签收
						isChecked = resultMap.getAsInteger("ischeck");
						
						if(isChecked==1){//已经签收了则处理状态
							inDto = new BaseDto();
							intDto = new BaseDto();
							inDto.put("order_id", orderInfo.getAsString("order_id"));
							intDto.put("order_id", orderInfo.getAsString("order_id"));
							//处理金额
							String nowtime = CTUtils.getCurrentTime();
							totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
							totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
							totalSMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("service_money"));
							
							//结算金额
							inDto.put("optype", "order_count");
							inDto.put("ocid", UniqueID.getUniqueID(8, 0));
							inDto.put("scid", UniqueID.getUniqueID(8, 0));
							inDto.put("order_money", totalMoney);//订单金额
							inDto.put("order_pmoney", totalPMoney);//订单分成
							inDto.put("server_pmoney", totalSMoney);//客服提成
							
							inDto.put("state_id", "1");//待结算
							inDto.put("createtime", nowtime);
							inDto.put("remark", nowtime+"结算分成返款金额【"+totalPMoney+"】");
							//结算预计支付日期
							String exp_day = CTUtils.getParamValue("ALIPAY_DATE_PERIOD");
							String exp_date = CTUtils.getDateByUDay(Integer.valueOf(exp_day));
							String subTime = nowtime.split(" ")[1];
							inDto.put("exp_pay_date", exp_date+" "+subTime);
							
							StringUtil.xprint("预期付款日期==>"+exp_date+" "+subTime);
							JdbcTemplateTool.addOrderCount(inDto);
							//resourceInfoService.operOrderInfo(inDto);
							
							//增加客服流水
							if(totalSMoney!=0L){
								inDto.put("optype", "sorder_count");
								inDto.put("remark", nowtime+"结算客服分成返款金额【"+totalSMoney+"】");
								//resourceInfoService.operOrderInfo(inDto);
							}
							
							intDto.put("state", "7");//待结算
							intDto.put("log_txt", "已批量处理签收、待结算");
							intDto.put("order_remark", "批量处理签收");
							intDto.put("createtime", nowtime);
							intDto.put("optype", "edit_order");
							JdbcTemplateTool.updateOrderCountSate(intDto);
							
							countNmu++;
							if(max_num!=0 && countNmu>max_num){
								break;//超过最大值结束处理
							}
							
							//线程停一秒
							Thread.sleep(1000);
						}
					}
				}
				temp = true;
			}
			
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}finally{
			if(temp){
				RequestUtil.renderText("true#"+countNmu);
			}else{
				RequestUtil.renderText("false#0");
			}
		}
		
		
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}


}
