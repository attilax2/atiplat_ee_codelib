package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.SenderQueryUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.ZHToEN;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class OrderInfoServlet extends HttpServlet {

	private static final long serialVersionUID = -2131533036486709011L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 //获取参数
		 String reqPath = req.getRequestURI();
         Dto paramDto = RequestUtil.getPraramsAsDto(req);
		 String nowURl = req.getServletPath();
		 String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
         if(CTUtils.isEmpty(RequestUtil.getUser(req))){
        	CTUtils.getParamToReq(req);
 			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
 		 } else {
			
 			//处理列表数据
			 int pageSize = 0;
			 String page_size = CTUtils.getParamValue("PAGE_MSIZE");
			 if(CTUtils.isNotEmpty(page_size)){
				pageSize = Integer.valueOf(page_size);
			 }else{
				pageSize = CTConstants.PAGER_MSIZE;
			 }
			 
 			 if(reqPath.indexOf("list")!=-1){//列表信息
 				 //获取所有订单状态
 				 req.setAttribute("orderStateList", CTUtils.getCodeList("ORDER_STATE"));
 				 //获取所有时间类型
 				 req.setAttribute("timeTypeList", CTUtils.getCodeList("TIME_TYPE"));
 				 //获取所有商品列表
 				 List<Dto> goodList = JdbcTemplateTool.getTableList("wxb_good", "");
 				 req.setAttribute("goodList", goodList);
 				 //获取用户所有渠道
 				 List<Dto> channelList = JdbcTemplateTool.getTableList("wxb_channel", " t.channel_uid='"+RequestUtil.getUser(req).getMemeber_id()+"'");
 				req.setAttribute("channelList", channelList);
 				
 				//获取所有订单信息
 				paramDto.put("user_id", RequestUtil.getUser(req).getMemeber_id());
 				//paramDto.put("page_size", 15);
				//paramDto = RequestUtil.getPageDto2(paramDto);
 				paramDto.put("pageSize", pageSize);
 				PageModel pager = JdbcTemplateTool.getOrderList(paramDto);
				req.setAttribute("pm", pager);
				
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("status", paramDto.getAsString("status"));
				req.setAttribute("s_date", paramDto.getAsString("s_date"));
				req.setAttribute("e_date", paramDto.getAsString("e_date"));
				req.setAttribute("d_type", paramDto.getAsString("d_type"));
				req.setAttribute("goods_id", paramDto.getAsString("goods_id"));
				req.setAttribute("channel_id", paramDto.getAsString("channel_id"));
				req.setAttribute("courier_id", paramDto.getAsString("courier_id"));
				
 				req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "morder");
 				CTUtils.getParamToReq(req);
 				req.getRequestDispatcher("/wxb/myOrderList.jsp").forward(req, resp);
 				 
 			 }
 			 
 			if(reqPath.indexOf("detail")!=-1){//详细信息
				 String order_id = RequestUtil.getParamFromURl(reqPath, temp_path);
				 if(CTUtils.isNotEmpty(order_id)){
					 Dto paDto = new BaseDto();
					 paDto.put("order_id", order_id);
					 //paDto.put("page_size", 15);
					 //paDto = RequestUtil.getPageDto2(paDto);
					 paDto.put("pageSize", pageSize);
					 PageModel pager = JdbcTemplateTool.getOrderList(paDto);
					 List<Dto> orderList = pager.getDatas();
					 if(!StringUtil.checkListBlank(orderList)){
						 req.setAttribute("orderInfo", orderList.get(0));
					 }
					 //获取日志列表
					 req.setAttribute("logList", JdbcTemplateTool.queryOrderLogList(order_id));
				 }
				 req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "morder");
				CTUtils.getParamToReq(req);
 				req.getRequestDispatcher("/wxb/orderDetailInfo.jsp").forward(req, resp);
			 }
 			
 			
 			if(reqPath.indexOf("mark")!=-1){//添加备注
				 String order_id = req.getParameter("order_id");
				 String log_txt = req.getParameter("log_txt");
				 if(CTUtils.isNotEmpty(order_id)){
					 boolean temp = JdbcTemplateTool.addOrderLog(order_id, log_txt);
					if(temp){
				    	RequestUtil.renderText("true");
				    }else{
				    	RequestUtil.renderText("false");
				    }
				 }
			 }
 			
 			if(reqPath.indexOf("getexpress")!=-1){//获取物流详情
 				String gs = paramDto.getAsString("company");
 				String dh = paramDto.getAsString("number");
 				if(CTUtils.isNotEmpty(gs) && CTUtils.isNotEmpty(dh)){
 					//通过公司编号获取公司名称
 					String gsspell = null;
 					String gs_name = CTUtils.getCodeName("SENDER_TYPE", gs);
 					try {
 						gsspell = ZHToEN.getPingyin(gs_name).toLowerCase();
					} catch (BadHanyuPinyinOutputFormatCombination e) {
						e.printStackTrace();
					}
					StringUtil.xprint("物流简称为："+gsspell);
					StringUtil.xprint("物流单号为："+dh);
 				    String wlString = SenderQueryUtil.getWuliu(dh, gsspell);
 				    StringUtil.xprint("物流信息为："+wlString);
 				    if(CTUtils.isNotEmpty(wlString)){
 				    	RequestUtil.renderText(wlString);
 				    }else{
 				    	RequestUtil.renderText("");
 				    }
 				}
 			}
 			 
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
