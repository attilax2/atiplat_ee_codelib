package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class CustomReportServlet extends HttpServlet {
	
	private static final long serialVersionUID = -3229488552489072046L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
		if(CTUtils.isEmpty(customerInfo)){
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
			
			//处理商品统计列表
			if(reqPath.indexOf("good")!=-1){
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				Dto proDto = JdbcTemplateTool.getProductCountList(paramDto);
				req.setAttribute("goodDto", proDto);
				//回传参数
				req.setAttribute("alisaname", searchValue);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "greport");
				req.getRequestDispatcher("/customer/custom_good_report.jsp").forward(req, resp);
			}
			
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	

}
