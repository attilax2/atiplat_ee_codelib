package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class ChangePwdServlet extends HttpServlet {

	private static final long serialVersionUID = -6475648484429635060L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 //进入修改密码
		String returnMsg="";
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		String memeber_id = paramDto.getAsString("memeber_id");
		MemeberInfo memberInfo = RequestUtil.getUser(req);
		if(CTUtils.isEmpty(memberInfo)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		} else if(StringUtil.isNotEmpty(memeber_id)){

			if(paramDto.getAsString("old_password").equals(memberInfo.getEpassword())){
				//处理修改数据逻辑
				String epassword = CTUtils.encryptBasedDes(paramDto.getAsString("password"));
				paramDto.put("password", epassword);
				boolean bool = JdbcTemplateTool.updateMemeberInfo(paramDto);
				if(bool){
					returnMsg = "0#修改会员密码成功";
					//修改会员session信息
					memberInfo.setPassword(epassword);
					memberInfo.setEpassword(paramDto.getAsString("password"));
					req.getSession().removeAttribute(CTConstants.SESSION_MUSER);
					req.getSession().setAttribute(CTConstants.SESSION_MUSER, memberInfo);
					
				}else{
					returnMsg = "1#修改会员密码出错";
				}
			}else{
				returnMsg = "1#原始密码不正确";
			}
			
			RequestUtil.renderText(returnMsg);
			
		} else {

		   req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "reset");
		   CTUtils.getParamToReq(req);
		   req.getRequestDispatcher("/wxb/changePwd.jsp").forward(req, resp);
		
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
