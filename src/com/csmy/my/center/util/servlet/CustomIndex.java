package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.attilax.ioc.IocXq214;
import com.attilax.token.TokenService;
import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.google.inject.Inject;

public class CustomIndex extends HttpServlet {

	private static final long serialVersionUID = 8809365458573472177L;
	@Inject
	TokenService tokSvr;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
            
		//进入用户首页
		HttpSession session = req.getSession();
		String reqPath = req.getRequestURI();
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customer = (CustomerInfo)session.getAttribute(CTConstants.SESSION_CUSER); 
//		if(CTUtils.isEmpty(customer)){
//			
//			
//			
//		} 
		
		//q316
				if(tokSvr==null)
					tokSvr=IocXq214.getBean(TokenService.class);
				tokSvr.setModule("merMod");
				System.out.println("---");
			//	tokSvr.clrToken(resp);
				if(!tokSvr.isHasToken(req))
				{
					resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");return;
				}
				
			paramDto.put("customer_id",tokSvr.getuid());
			Dto totalCounDto = JdbcTemplateTool.getCTOrderCount(paramDto);
			req.setAttribute("totalCounDto", totalCounDto);
			
			String today = CTUtils.getCurDate();
			paramDto.put("start_date", today);
			paramDto.put("end_date", today);
			Dto todayDto = JdbcTemplateTool.getCTOrderCount(paramDto);
			req.setAttribute("todayData", todayDto);
			
			String last7Day = CTUtils.getDateByUDay(-7);
			paramDto.put("start_date", last7Day);
			paramDto.put("end_date", today);
			Dto last7Dto = JdbcTemplateTool.getCTOrderCount(paramDto);
			req.setAttribute("last7Data", last7Dto);
			
			String last30Day = CTUtils.getDateByUDay(-30);
			paramDto.put("start_date", last30Day);
			paramDto.put("end_date", today);
			Dto last30Dto = JdbcTemplateTool.getCTOrderCount(paramDto);
			req.setAttribute("last30Data", last30Dto);
			
			req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "chome");
		    req.getRequestDispatcher("/customer/custom_index.jsp").forward(req, resp);
		 
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
