package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.MobileLocationUtil;
import com.csmy.my.center.util.QueryIPAddress;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.csmy.my.center.util.sms.SMS_Sender;

public class OrderProcessServlet extends HttpServlet {

	private static final long serialVersionUID = -3256670949095988088L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
		if(CTUtils.isEmpty(customerInfo)){
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{//处理订单流程
			
			
			if(CTUtils.isNotEmpty(paramDto.getAsString("state_id"))){
			
					List<Dto> dList = null;
					double totalMoney=0.0;
					double totalPMoney=0.0;
					try {
						boolean temp = false;
						Dto orderInfo = null;
						Dto dto =  paramDto;
						Dto mDto = new BaseDto();
						Dto intDto = new BaseDto();
						intDto.put("order_id", dto.getAsString("order_id"));
						int stateID = dto.getAsInteger("state_id");
						//处理流程
						String nowtime = CTUtils.getCurrentTime();
						String remark = dto.getAsString("remark");
						switch (stateID) {
						
						case 1://进入审核流程
							if(dto.getAsString("check_state").equals("1")){//审核有效
								intDto.put("state", "2");//已审核
								intDto.put("sender_type", dto.getAsString("sender_type"));//选择快递公司
								if(CTUtils.isNotEmpty(remark)){
									intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已审核、待发货 ");
								}
							}else if(dto.getAsString("check_state").equals("2")){//待跟进
								intDto.put("state", "9");//待跟进
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "待跟进 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "待跟进 ");
								}
							}else{//无效取消
								intDto.put("state", "3");//已取消
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已取消 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已取消 ");
								}
							}
							
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);
							
							break;
						case 2://进入发货流程
							if(CTUtils.isNotEmpty(dto.getAsString("courier_id"))){//快递单号
								intDto.put("state", "4");//已发货
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已发货、待签收 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已发货、待签收 ");
								}
							}
							intDto.put("sender_type", dto.getAsString("sender_type"));
							intDto.put("courier_id", dto.getAsString("courier_id"));
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);

							/*************************发送短信开始*************************/
			        		String send_sms = CTUtils.getParamValue("SEND_DELIVERY_SMS");
			        		String send_sms_temp = CTUtils.getParamValue("SEND_SMS_TEMPLATE");
			        		if(CTUtils.isNotEmpty(send_sms) && send_sms.equals("1") && CTUtils.isNotEmpty(send_sms_temp)){
			        			String buyer_name = dto.getAsString("buyer_name");
			        			String buyer_phone = dto.getAsString("buyer_phone");
			        			String orderid = dto.getAsString("order_id");
			        			String order_money = dto.getAsString("omoney");
			        			
			        			if(send_sms_temp.contains("buyer_name")){
			        				send_sms_temp = send_sms_temp.replaceAll("\\$buyer_name", buyer_name);
			        			}
			        			if(send_sms_temp.contains("order_id")){
			        				send_sms_temp = send_sms_temp.replaceAll("\\$order_id", orderid);
			        			}
			        			if(send_sms_temp.contains("order_money")){
			        				send_sms_temp = send_sms_temp.replaceAll("\\$order_money", ""+order_money);
			        			}
			        			
			        			String smString = send_sms_temp;
			        			StringUtil.xprint("发送发货提示短信内容为>>>"+smString);
			        			if(SMS_Sender.sendSms(buyer_phone, smString)){
			    					StringUtil.xprint("发货提示发送成功!");
			    				}else{
			    					StringUtil.xprint("发货提示短信发送失败!");
			    				}
			        		}
			        		/*************************发短信件结束*************************/
			        		
							
							break;
						case 4://进入签收流程
							if(dto.getAsString("sign_state").equals("1")){//已签收
								//结算金额
								mDto.put("ocid", CTUtils.getCurDateNoSplitTime()+UniqueID.getUniqueID(8, 0));
								mDto.put("order_id", dto.getAsString("order_id"));
								mDto.put("order_money", dto.getAsString("omoney"));
								mDto.put("order_pmoney", dto.getAsString("pmoney"));
								mDto.put("state_id", "1");//待结算
								mDto.put("createtime", nowtime);
								mDto.put("remark", nowtime+"结算分成返款金额【"+dto.getAsString("pmoney")+"】");
								//结算预计支付日期
								String exp_day = CTUtils.getParamValue("ALIPAY_DATE_PERIOD");
								String exp_date = CTUtils.getDateByUDay(Integer.valueOf(exp_day));
								String subTime = nowtime.split(" ")[1];
								mDto.put("exp_pay_date", exp_date+" "+subTime);
								StringUtil.xprint("预期付款日期==>"+exp_date+" "+subTime);
								JdbcTemplateTool.addOrderCount(mDto);
								
								intDto.put("state", "7");//待结算
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已签收、待结算 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已签收、待结算 ");
								}
								intDto.put("order_remark", remark);
								intDto.put("createtime", nowtime);
								
								//intDto.put("state", "5");//已签收
								//intDto.put("log_txt", "已签收");
							}else{
								intDto.put("state", "6");//已拒收
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已拒收 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已拒收 ");
								}
							}
						
							intDto.put("courier_id", dto.getAsString("courier_id"));
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);
							
							break;
						case 5://进入结算流程
							//结算金额
							mDto.put("optype", "order_count");
							mDto.put("ocid", CTUtils.getCurDateNoSplitTime()+UniqueID.getUniqueID(8, 0));
							mDto.put("order_id", dto.getAsString("order_id"));
							mDto.put("order_money", dto.getAsString("omoney"));
							mDto.put("order_pmoney", dto.getAsString("pmoney"));
							mDto.put("state_id", "1");//待结算
							mDto.put("createtime", nowtime);
							mDto.put("remark", nowtime+"结算分成返款金额【"+dto.getAsString("pmoney")+"】");
							JdbcTemplateTool.addOrderCount(mDto);
							
							intDto.put("state", "7");//已结算
							if(CTUtils.isNotEmpty(remark)){
							    intDto.put("log_txt", "已结算、待支付 "+"("+remark+")");
							}else{
								intDto.put("log_txt", "已结算、待支付 ");
							}
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);
							break;
						case 7://进入退款流程
							//结算金额
							/*mDto.put("optype", "order_count");
							mDto.put("ocid", "LSH"+CTUtils.getCurDateNoSplitTime()+UniqueID.getUniqueID(8, 0));
							mDto.put("order_id", dto.getAsString("order_id"));
							mDto.put("back_money", "-"+dto.getAsString("back_money"));
							mDto.put("order_money", "-"+dto.getAsString("back_pmoney"));
							mDto.put("state_id", "1");//退款
							mDto.put("reback", "1");//退款标志
							mDto.put("createtime", nowtime);
							mDto.put("remark", nowtime+"扣除订单金额【"+dto.getAsString("back_money")+"】佣金【"+dto.getAsString("back_pmoney")+"】");
							JdbcTemplateTool.addOrderCount(mDto);*/
							
							//intDto.put("state", "8");//已退货
							//intDto.put("log_txt", "已退货");
							//intDto.put("order_remark", remark);
							//intDto.put("createtime", nowtime);
							
							intDto.put("state", "10");//已退货
							intDto.put("log_txt", "进入退货流程");
							intDto.put("order_remark", "进入退货流程");
							intDto.put("createtime", nowtime);
							//intDto.put("optype", "edit_order");
							break;
						case 9://进入跟进审核流程
							if(dto.getAsString("check_state").equals("1")){//审核有效
								intDto.put("state", "2");//已审核
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已审核、待发货 ");
								}
								intDto.put("sender_type", dto.getAsString("sender_type"));//选择快递公司
							}else{//无效取消
								intDto.put("state", "3");//已取消
								if(CTUtils.isNotEmpty(remark)){
								    intDto.put("log_txt", "已取消 "+"("+remark+")");
								}else{
									intDto.put("log_txt", "已取消 ");
								}
							}
							
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);
							intDto.put("optype", "edit_order");
							
							break;
						case 10://进入跟进审核流程
							intDto.put("state", "8");//已退货
							intDto.put("log_txt", "用户已退货");
							intDto.put("order_remark", remark);
							intDto.put("createtime", nowtime);
							intDto.put("optype", "edit_order");
							
							break;
							
						default:
		
							break;
						}
						
						//修改订单信息
						temp = JdbcTemplateTool.updateOrderCountSate(intDto);
						if(temp){
							intDto.put("optype", "add_log");
							JdbcTemplateTool.addOrderLog(intDto.getAsString("order_id"), intDto.getAsString("log_txt"));
						}
						//查询订单信息
						dto.put("state_id", "");
						dList = JdbcTemplateTool.getCOrderList(dto);
						if(!StringUtil.checkListBlank(dList)){
							orderInfo = dList.get(0);
							totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
							totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
							orderInfo.put("totalMoney", totalMoney);
							orderInfo.put("totalPMoney", totalPMoney);
							orderInfo.put("senderType", CTUtils.getCodeName("SENDER_TYPE", orderInfo.getAsString("sender_type")));
							
							//号码归属
//							String visit_ip = orderInfo.getAsString("visit_ip");
//							if(visit_ip.equals("0:0:0:0:0:0:0:1")){
//								orderInfo.put("visit_ip", "127.0.0.1");
//								orderInfo.put("iplocation", "本机IP地址");
//							}else{
//								orderInfo.put("iplocation", QueryIPAddress.queryIPAddress(visit_ip.trim()));
//							}
							String buyer_phone = orderInfo.getAsString("buyer_phone");
							if(CTUtils.isNotEmpty(buyer_phone)){
							   String mlocation = MobileLocationUtil.getMobileLocation(buyer_phone);
							   orderInfo.put("mlocation", mlocation);
							}
							
						    req.setAttribute("orderInfo", orderInfo);
						    req.setAttribute("senderList", CTUtils.getCodeList("SENDER_TYPE"));
						    req.setAttribute("remarkList", CTUtils.getCodeList("REMARK_TEXT"));
						    
						    //获取订单流水信息
		//				    if(stateID==7){//获取退款订单流水
		//				    	Dto pDto = new BaseDto();
		//				    	pDto.put("reback", "1");
		//				    	pDto.put("order_id", orderInfo.getAsString("order_id"));
		//				    	Dto bOrderInfo = (BaseDto)CTReader.queryForObject("order.getOrderCount",pDto);
		//				    	request.setAttribute("bOrderInfo", bOrderInfo);
		//				    }
						}
							
					} catch (Exception e) {
						e.printStackTrace();
						StringUtil.xprint("订单流程处理出错!");
					}
			
			}else{//默认进入
				
				Dto orderInfo = null;
				double totalMoney=0.0;
				double totalPMoney=0.0;
				List<Dto> dList = JdbcTemplateTool.getCOrderList(paramDto);
				if(!StringUtil.checkListBlank(dList)){
					orderInfo = dList.get(0);
					totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
					totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
					orderInfo.put("totalMoney", totalMoney);
					orderInfo.put("totalPMoney", totalPMoney);
					orderInfo.put("senderType", CTUtils.getCodeName("SENDER_TYPE", orderInfo.getAsString("sender_type")));
					
					//号码归属
//					String visit_ip = orderInfo.getAsString("visit_ip");
//					if(visit_ip.equals("0:0:0:0:0:0:0:1")){
//						orderInfo.put("visit_ip", "127.0.0.1");
//						orderInfo.put("iplocation", "本机IP地址");
//					}else{
//						orderInfo.put("iplocation", QueryIPAddress.queryIPAddress(visit_ip.trim()));
//					}
					String buyer_phone = orderInfo.getAsString("buyer_phone");
					if(CTUtils.isNotEmpty(buyer_phone)){
					    String mlocation;
						try {
							mlocation = MobileLocationUtil.getMobileLocation(buyer_phone);
							 orderInfo.put("mlocation", mlocation);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					req.setAttribute("senderList", CTUtils.getCodeList("SENDER_TYPE"));
					req.setAttribute("remarkList", CTUtils.getCodeList("REMARK_TEXT"));
					req.setAttribute("orderInfo", orderInfo);
					//获取订单流水信息
//					    if(orderInfo.getAsInteger("state")==8){//获取退款订单流水
//					    	Dto pDto = new BaseDto();
//					    	pDto.put("reback", "1");
//					    	pDto.put("order_id", orderInfo.getAsString("order_id"));
//					    	Dto bOrderInfo = (BaseDto)CTReader.queryForObject("order.getOrderCount",pDto);
//					    	request.setAttribute("bOrderInfo", bOrderInfo);
//					    }
				}
				
			}
			
			req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "order_0");
			req.getRequestDispatcher("/customer/showOrderProcess.jsp").forward(req, resp);
			
		}

		
		
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
