package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class CustomGoodServlet extends HttpServlet {

	private static final long serialVersionUID = -6841308213269492595L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		CustomerInfo customerInfo = RequestUtil.getCUser(req);
		if(CTUtils.isEmpty(customerInfo)){
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
			
			//处理列表
			if(reqPath.indexOf("list")!=-1){
				//获取分类信息
				req.setAttribute("typeList", JdbcTemplateTool.getGoodTypeList());
				//获取所有标签
				req.setAttribute("tagsList", CTUtils.getCodeList("GOOD_TAG"));
				
				//处理列表数据
				String searchValue = paramDto.getAsString("searchText");
				paramDto.put("alisaname", searchValue);
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				
				//paramDto.put("page_size", 20);
				//paramDto = RequestUtil.getPageDto2(paramDto);
				//处理列表数据
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				paramDto.put("pageSize", pageSize);
				PageModel pager = JdbcTemplateTool.getGoodList(paramDto);
				req.setAttribute("pm", pager);
				
				//回传参数
				req.setAttribute("alisaname", searchValue);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("tag_id", paramDto.getAsString("tag_id"));
				req.setAttribute("type_id", paramDto.getAsString("type_id"));
				req.setAttribute("customer_id", customerInfo.getCustomer_id());
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_list");
				req.getRequestDispatcher("/customer/custom_good.jsp").forward(req, resp);
			}
			
			//处理增加
			if(reqPath.indexOf("add")!=-1){
				
				//获取文案信息列表
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				List<Dto> copyList = JdbcTemplateTool.getCCopyList(paramDto);
				//处理文案信息
				List<Dto> clist1 = null;
				List<Dto> clist2 = null;
				List<Dto> clist3 = null;
				if(CTUtils.isNotEmpty(copyList)){
					clist1 = new ArrayList<Dto>();
					clist2 = new ArrayList<Dto>();
					clist3 = new ArrayList<Dto>();
					for (Dto dto : copyList) {
						if(dto.getAsString("type_id").equals("0")){
							clist1.add(dto);
						}
						if(dto.getAsString("type_id").equals("1")){
							clist2.add(dto);
						}
						if(dto.getAsString("type_id").equals("2")){
							clist3.add(dto);
						}
					}
				}
				req.setAttribute("copyList", clist1);
				req.setAttribute("copyList2", clist2);
				req.setAttribute("copyList3", clist3);
				List<Dto> typeList = JdbcTemplateTool.getGoodTypeList();
				req.setAttribute("typeList", typeList);
				req.setAttribute("optype", "add_good");
				
				String good_id = paramDto.getAsString("good_id");
				if(CTUtils.isNotEmpty(good_id)){
					//获取商品信息
					Dto goodInfo = JdbcTemplateTool.getGoodByID(paramDto.getAsString("good_id"));
					if (CTUtils.isNotEmpty(goodInfo)) {
						
						List<Dto> skuList = JdbcTemplateTool.getGoodSkuList(paramDto.getAsString("good_id"));
						if(!StringUtil.checkListBlank(skuList)){
							int cont = 0;
							StringBuffer sBuffer = new StringBuffer("");
							for (Dto skuDto : skuList) {
								cont++;
								String sku_id =  skuDto.getAsString("sku_id");
								String sku_name = skuDto.getAsString("sku_name");
								String sku_cost = skuDto.getAsString("sku_cost");
								String sku_price = skuDto.getAsString("sku_price");
								String sku_pmoney = skuDto.getAsString("sku_pmoney");
								String service_money = skuDto.getAsString("service_money");
								sBuffer.append("<tr>");
								sBuffer.append("<td><input id=\"sku" + cont
										+ "\" name=\"sku" + cont
										+ "\" type=\"text\" style=\"width:150px\" value=\""
										+ sku_name + "\" />");
								sBuffer.append("<input id=\"sku_id_" + cont
										+ "\" name=\"sku_id_" + cont
										+ "\" type=\"hidden\" value=\""
										+ sku_id + "\" /></td>");
								
								sBuffer.append("<td><input id=\"cb" + cont
										+ "\" name=\"cb" + cont
										+ "\" type=\"text\" style=\"width:150px\" value=\""
										+ sku_cost + "\" /></td>");
								sBuffer.append("<td><input id=\"jg" + cont
										+ "\" name=\"jg" + cont
										+ "\" type=\"text\" style=\"width:150px\" value=\""
										+ sku_price + "\" /></td>");
								sBuffer.append("<td><input id=\"fc" + cont
										+ "\" name=\"fc" + cont
										+ "\" type=\"text\" style=\"width:150px\" value=\""
										+ sku_pmoney + "\" /></td>");
								sBuffer.append("<td><input id=\"kffc" + cont
										+ "\" name=\"kffc" + cont
										+ "\" type=\"text\" style=\"width:150px\" value=\""
										+ service_money + "\" /></td>");
								sBuffer.append("</tr>");
								}
							   req.setAttribute("good_sku", sBuffer.toString());
							}
						}
					req.setAttribute("goodInfo", goodInfo);
					req.setAttribute("optype", "edit_good");
					req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_list");
				}else{
					req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_add");
				}
				
				req.getRequestDispatcher("/customer/add_cust_good.jsp").forward(req, resp);
			}
			
			//处理修改
			if(reqPath.indexOf("edit")!=-1){
				
				//获取文案信息列表
				paramDto.put("customer_id", customerInfo.getCustomer_id());
				List<Dto> copyList = JdbcTemplateTool.getCCopyList(paramDto);
				req.setAttribute("copyList", copyList);
				List<Dto> typeList = JdbcTemplateTool.getGoodTypeList();
				req.setAttribute("typeList", typeList);
				
				req.setAttribute("optype", "edit_good");
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "good_add");
				req.getRequestDispatcher("/customer/add_cust_good.jsp").forward(req, resp);
			}
			
			//验证商品名称
			if(reqPath.indexOf("check")!=-1){
				boolean temp = false;
				String good_id = paramDto.getAsString("good_id");
				String good_name = paramDto.getAsString("good_name");
                String customer_id = customerInfo.getCustomer_id();
                //good_name = CTUtils.CharsetFilter(good_name);
                if(CTUtils.isNotEmpty(good_id)){
                	Dto goodInfo = JdbcTemplateTool.getGoodByID(good_id);
                	if (!goodInfo.getAsString("good_name").equals(good_name)) {//修改名称
                		 temp = JdbcTemplateTool.checkGoodName(good_name, customer_id);
					}
                }else{
				   temp = JdbcTemplateTool.checkGoodName(good_name, customer_id);
                }
                
                if(temp){
                	RequestUtil.renderText("true");
                }else{
                	RequestUtil.renderText("false");
                }
				
			}
			
			//处理删除
			if(reqPath.indexOf("trash")!=-1){
				//处理添加逻辑
				boolean temp = false;
				String good_id = paramDto.getAsString("good_id");
				if(CTUtils.isNotEmpty(good_id)){//修改
					good_id = good_id.replaceAll(",", "','");
					temp = JdbcTemplateTool.deleteCustGood(good_id,customerInfo.getCustomer_id());
					if(temp){
						RequestUtil.renderText("true");
					}else {
						RequestUtil.renderText("false");
					}
				}
			}
			
			
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
