package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.attilax.sso.LoginX;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.StringUtil;

public class GoAdminPageServlet extends HttpServlet {
	
	public static void main(String[] args) {
		try {
			
		} catch (Throwable e) {
			// TODO: handle exception
		}
	}

	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
         //处理后台跳转
        StringUtil.xprint("进入后台");
        System.out.println("----");
        
     //   req.getSession().getAttribute(CTConstants.SESSION_USER) != null
    	LoginX lx=new LoginX();
		lx.setModule("bkd");
        if ( lx.isLogined(req) ) {
        	req.getRequestDispatcher("/admin/main/main.jsp").forward(req, resp);
        }else{
        	req.getRequestDispatcher("/admin/login/login.jsp").forward(req, resp);
        }
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
