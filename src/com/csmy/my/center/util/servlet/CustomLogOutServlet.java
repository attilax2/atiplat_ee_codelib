package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.attilax.ioc.IocXq214;
import com.attilax.token.TokenService;
import com.attilax.util.request;
import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.google.inject.Inject;

public class CustomLogOutServlet extends HttpServlet {

	private static final long serialVersionUID = -7885597285082874634L;
	@Inject
	TokenService tokSvr;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		//退出登录
		HttpSession session = req.getSession();
		CustomerInfo customer = (CustomerInfo) session.getAttribute(CTConstants.SESSION_CUSER);
		if (customer != null) {
			session.removeAttribute(CTConstants.SESSION_CUSER);
			session.invalidate();
			StringUtil.xprint("=======当前登录商户session成功！======");
		}
		CTUtils.getParamToReq(req);
		
		//q316
		if(tokSvr==null)
			tokSvr=IocXq214.getBean(TokenService.class);
		tokSvr.setModule("merMod");
		System.out.println("---");
		tokSvr.clrToken(resp);
		
		if( "cp".equals(req.getParameter("envi")))
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/userPhone4jobusImp/mer_login.html#right-link");
		else
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
