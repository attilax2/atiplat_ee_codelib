package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.attilax.Closure;
import com.attilax.core;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

/**
 * 
 * @author Administrator
 *
 */
public class VisitRewardServlet extends HttpServlet {

	private static final long serialVersionUID = -1139974988311078664L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("--");
		    //获取请求路径
			String reqPath = req.getRequestURI();
			String nowURl = req.getServletPath();
			String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
			MemeberInfo me= (MemeberInfo) req.getSession().getAttribute("SESSION_MUSER");
	        //处理列表
			if(CTUtils.isEmpty(me)){
				CTUtils.getParamToReq(req);
				resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
			}else{
				
				
				if(CTUtils.isEmpty(me.getUse_recom())||me.getUse_recom().equals("0")){
					
					req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "vlink");
					CTUtils.getParamToReq(req);
					req.getRequestDispatcher("/wxb/visit_nolink.jsp").forward(req, resp);
					
				}else{
					if(reqPath.indexOf("query")!=-1){
						
						 query2(req, resp, me);
					}
					
					if(reqPath.indexOf("people")!=-1){
						 people2(req, resp, me);
					}
				}
			}

	}


	private void query2(final HttpServletRequest req, final HttpServletResponse resp,
			final MemeberInfo me) throws ServletException, IOException {
		
		
		core.retry3(new Closure<Object, Object>() {

			@Override
			public Object execute(Object arg0) throws Exception {
				List<Dto> list= JdbcTemplateTool.queryRewardList(me.getVisit_code());
				 req.setAttribute("rm", list);
				 req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "rreward");
				 CTUtils.getParamToReq(req);
				 req.getRequestDispatcher("/wxb/visit_reward.jsp").forward(req, resp);
				return null;
			}
		}, null, "c:\\e_wxb");
		
		
	}


	private void people2(final HttpServletRequest req, final HttpServletResponse resp,
			final MemeberInfo me) throws ServletException, IOException {
		
		
		core.retry3(new Closure<Object, Object>() {

			@Override
			public Object execute(Object arg0) throws Exception {
				List<Dto> list= JdbcTemplateTool.queryRPeopleList(me.getVisit_code());
				 req.setAttribute("rpList", list);
				 req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "rpeople");
				 CTUtils.getParamToReq(req);
				 req.getRequestDispatcher("/wxb/visit_people.jsp").forward(req, resp);
				return null;
			}
		}, null, "c:\\e_wxb");
	
	}


	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		 
		doGet(req, resp);
	}

	
	public void init() throws ServletException {
		// Put your code here
	}

}
