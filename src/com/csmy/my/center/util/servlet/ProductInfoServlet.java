package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class ProductInfoServlet extends HttpServlet {

	private static final long serialVersionUID = -8115363832923536477L;


	@Override
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
  
		//处理商品信息
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		Dto paramDto = RequestUtil.getPraramsAsDto(req);
		MemeberInfo memeberInfo = RequestUtil.getUser(req);
		if(CTUtils.isEmpty(memeberInfo)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
	        //处理列表
			String memeberID = memeberInfo.getMemeber_id();
			if(reqPath.indexOf("list")!=-1){
				//获取分类信息
				req.setAttribute("typeList", JdbcTemplateTool.getGoodTypeList());
				//获取商户信息
				req.setAttribute("customerList", JdbcTemplateTool.getCustomerList());
				//获取所有标签
				req.setAttribute("tagsList", CTUtils.getCodeList("GOOD_TAG"));
				
				//处理列表数据
				int pageSize = 0;
				String page_size = CTUtils.getParamValue("PAGE_MSIZE");
				if(CTUtils.isNotEmpty(page_size)){
					pageSize = Integer.valueOf(page_size);
				}else{
					pageSize = CTConstants.PAGER_MSIZE;
				}
				String searchValue = paramDto.getAsString("alisaname");
				paramDto.put("alisaname", CTUtils.specialStrFilter(searchValue));
				paramDto.put("enabled", "1");
				paramDto.put("pageSize", pageSize);
				//paramDto = RequestUtil.getPageDto2(paramDto);
				PageModel pager = JdbcTemplateTool.getGoodList(paramDto);
				req.setAttribute("pm", pager);
				
				//回传参数
				req.setAttribute("alisaname", searchValue);
				req.setAttribute("pageSize", pageSize);
				req.setAttribute("tag_id", paramDto.getAsString("tag_id"));
				req.setAttribute("type_id", paramDto.getAsString("type_id"));
				req.setAttribute("sort_type", paramDto.getAsString("sort_type"));
				req.setAttribute("customer_id", paramDto.getAsString("customer_id"));
				
				req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "good");
				CTUtils.getParamToReq(req);
				req.getRequestDispatcher("/wxb/product_list.jsp").forward(req, resp);
			}
			  
			
			//处理详情
	        if(reqPath.indexOf("index")!=-1){
	        	param = getParamFromURl(reqPath, temp_path);
				StringUtil.xprint("获取的参数=="+param);
				Dto goodInfo = JdbcTemplateTool.getGoodByID(param);
				req.setAttribute("goodInfo", goodInfo);
				//获取渠道
				List<Dto> chList = JdbcTemplateTool.getChannelList(goodInfo.getAsString("good_id"),memeberID);
				req.setAttribute("chList", chList);
				//生成默认购买参数（商品ID-用户ID）
				String defualParam = CTUtils.encryptBased64(goodInfo.getAsString("good_id")+"-"+memeberID);
				//defualParam = URLEncoder.encode(defualParam);
				StringUtil.xprint("defualParam="+defualParam);
				
				req.setAttribute("defualParam", defualParam);
				req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "good");
				CTUtils.getParamToReq(req);
	        	req.getRequestDispatcher("/wxb/product_info.jsp").forward(req, resp);
			}

	        
	        if(reqPath.indexOf("buykey")!=-1){//请求唯一标志
	        	String good_id = paramDto.getAsString("goods_id");
	        	String channel_id = paramDto.getAsString("channel_id");
	        	String buyKey = CTUtils.encryptBased64(good_id+"-"+memeberID+"-"+channel_id);
	        	//buyKey = URLEncoder.encode(buyKey);
	        	StringUtil.xprint("key="+buyKey);
	        	//保存数据
	        	if(CTUtils.isNotEmpty(buyKey)){
	        	   RequestUtil.renderText("0#"+buyKey);      
	        	}else{
	        	   RequestUtil.renderText("0#生成渠道参数出错");  
	        	}
	        } 
		}
	}
	
	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
