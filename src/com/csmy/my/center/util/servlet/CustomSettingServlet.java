package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class CustomSettingServlet extends HttpServlet {

	private static final long serialVersionUID = 1913729715078619603L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {


		//进入用户首页
		HttpSession session = req.getSession();
		String reqPath = req.getRequestURI();
		CustomerInfo customer = RequestUtil.getCUser(req);
		if(CTUtils.isEmpty(customer)){
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
			
		}else{
			
			//修改信息
			if(reqPath.indexOf("setting")!=-1){
				boolean bool = false;
				String cus_id = req.getParameter("customer_id");
				if(CTUtils.isNotEmpty(cus_id)){
					Dto paramDto = RequestUtil.getPraramsAsDto(req);
					paramDto.put("wxh",paramDto.getAsString("wxh"));
					paramDto.put("customer_name",paramDto.getAsString("customer_name"));
					bool = JdbcTemplateTool.updateCustomerInfo(paramDto);
					if(bool){
					    req.setAttribute("eflag", "1");
					    req.setAttribute("ermsg", "修改商户信息成功");
					}else{
						req.setAttribute("eflag", "2");
						req.setAttribute("ermsg", "修改商户信息失败");
					}
					req.setAttribute("returl", "/cust/setting");
				}
				//获取商户信息
				CustomerInfo customerInfo = JdbcTemplateTool.getCustomerInfo(customer.getCustomer_id());
				req.setAttribute("customer", customerInfo);
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "user_info");
			    req.getRequestDispatcher("/customer/customer_info.jsp").forward(req, resp);
				
			}
			
			//修改密码
			if(reqPath.indexOf("chpwd")!=-1){
				req.setAttribute(CTConstants.CMENU_SELECTED_INDEX, "user_pwd");
			    req.getRequestDispatcher("/customer/change_cust_pwd.jsp").forward(req, resp);
			}
			
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

}
