package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class HomeIndex extends HttpServlet {
	
	private static final long serialVersionUID = -6470850865029201146L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

         //进入用户首页
		String reqPath = req.getRequestURI();
		HttpSession session = req.getSession();
		MemeberInfo memeberInfo = (MemeberInfo)session.getAttribute(CTConstants.SESSION_MUSER); 
		if(CTUtils.isEmpty(memeberInfo)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
			
		}else{
			
			Dto inDto = new BaseDto();
			//获取今天的结果
			inDto.put("user_id", RequestUtil.getUser(req).getMemeber_id());
			//获取全部订单
			Dto totalDto = JdbcTemplateTool.getHomeDateCount(inDto);
			req.setAttribute("totalData", totalDto);
			
			String today = CTUtils.getCurDate();
			inDto.put("start_date", today);
			inDto.put("end_date", today);
			Dto todayDto = JdbcTemplateTool.getHomeDateCount(inDto);
			req.setAttribute("todayData", todayDto);
			
			String last7Day = CTUtils.getDateByUDay(-7);
			inDto.put("start_date", last7Day);
			inDto.put("end_date", today);
			Dto last7Dto = JdbcTemplateTool.getHomeDateCount(inDto);
			req.setAttribute("last7Data", last7Dto);
			
			String last30Day = CTUtils.getDateByUDay(-30);
			inDto.put("start_date", last30Day);
			inDto.put("end_date", today);
			Dto last30Dto = JdbcTemplateTool.getHomeDateCount(inDto);
			req.setAttribute("last30Data", last30Dto);
			
			//获取结算返款统计
			Dto ownMoneyDto = JdbcTemplateTool.getUserMoneyCount(inDto);
			req.setAttribute("ownMoneyData", ownMoneyDto);
			
			//获取半个月的统计量
			Dto perDto = null;
			String perDate = null;
			StringBuffer sBuffer = new StringBuffer("");
			for(int i = 14; i >= 0; i--){
				if(i==0)perDate = CTUtils.getCurDate();
				perDate = CTUtils.getDateByUDay(-i);
				inDto.put("start_date", perDate);
				inDto.put("end_date", perDate);
				perDto = JdbcTemplateTool.getVisitCount(inDto);
				
				sBuffer.append("{");
				sBuffer.append("\"date\":\""+perDate+"\",");
				sBuffer.append("\"orders\":"+(perDto.getAsInteger("order_num")==null?0:perDto.getAsInteger("order_num"))+",");
				sBuffer.append("\"view_times\":"+(perDto.getAsInteger("vit_num")==null?0:perDto.getAsInteger("vit_num"))+",");
				sBuffer.append("\"ip_times\":"+(perDto.getAsInteger("ip_num")==null?0:perDto.getAsInteger("ip_num"))+"");
				sBuffer.append("}");
				if(i>0){
					sBuffer.append(",");
				}
			}
			StringUtil.xprint("qstj="+sBuffer.toString());
			req.setAttribute("qstj", sBuffer.toString());
			
			//获取订单统计
			//Dto orDto = JdbcTemplateTool.getOrderCountData(inDto);
			//req.setAttribute("orderDate",orDto);
			//获取统计邀请奖励
			inDto.put("visit_code", RequestUtil.getUser(req).getVisit_code());
			Dto rwDto = JdbcTemplateTool.getVisitRewardCount(inDto);
			req.setAttribute("rewardData",rwDto);
			
			//获取系统公告列表
			List<Dto> siteNewsList = JdbcTemplateTool.querySiteNewsList("1");
			req.setAttribute("siteNewsList",siteNewsList);
			if(CTUtils.isNotEmpty(siteNewsList)){
			   req.setAttribute("curNewsInfo",siteNewsList.get(0));
			}
			
			req.setAttribute("VURL","user/register/"+RequestUtil.getUser(req).getVisit_code());
			CTUtils.getParamToReq(req);
			req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "home");
		    req.getRequestDispatcher("/wxb/homePage.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	
	
	
	
	

}
