package com.csmy.my.center.util.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
/**
 * 渠道推广
 * @author Administrator
 *
 */
public class ChannelCountServlet extends HttpServlet {

	private static final long serialVersionUID = -5889362730151796692L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		//获取请求路径
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		Dto paraDto = RequestUtil.getPraramsAsDto(req);
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		
		 MemeberInfo me= (MemeberInfo) req.getSession().getAttribute("SESSION_MUSER");
  		 String channel_uid =me.getMemeber_id();
        //处理列表
		if(reqPath.indexOf("count")!=-1){
			
			CTUtils.getParamToReq(req);
			req.getRequestDispatcher("/wxb/channel_count.jsp").forward(req, resp);
		}
		  
	}

	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		 
		doGet(req, resp);
	}

}
