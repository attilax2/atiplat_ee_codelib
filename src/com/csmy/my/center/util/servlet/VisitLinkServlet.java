package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

@SuppressWarnings("serial")
public class VisitLinkServlet extends HttpServlet {


	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		        //获取请求路径
				String param = "";
				String reqPath = req.getRequestURI();
				String nowURl = req.getServletPath();
				String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
				MemeberInfo me= (MemeberInfo) req.getSession().getAttribute("SESSION_MUSER");
				if(CTUtils.isEmpty(me)){
					resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
				}else{
			        //处理列表
					if(reqPath.indexOf("link")!=-1){
						
						if(CTUtils.isEmpty(me.getUse_recom())||me.getUse_recom().equals("0")){
							
							req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "vlink");
							CTUtils.getParamToReq(req);
							req.getRequestDispatcher("/wxb/visit_nolink.jsp").forward(req, resp);
							
						}else{
							
				  			String visit_code =me.getVisit_code();
				  			List<Dto> rList = JdbcTemplateTool.queryRPeopleList(visit_code);//获取邀请总数
				  			if(CTUtils.isEmpty(rList)){
				  				req.setAttribute("rnum","0");
				  			}else{
				  				req.setAttribute("rnum",rList.size());
				  			}
							req.setAttribute("URL","user/register/"+visit_code);
							req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "vlink");
							CTUtils.getParamToReq(req);
							req.getRequestDispatcher("/wxb/visit_link.jsp").forward(req, resp);
							
						}
						
					}
					
					if(reqPath.indexOf("register")!=-1){
						param = getParamFromURl(reqPath, temp_path);
						req.setAttribute("recom_user",param);
						CTUtils.getParamToReq(req);
						req.getRequestDispatcher("/wxb/mregister.jsp").forward(req, resp);
					}
			   }
	}


	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		 
		doGet(req, resp);
	}

	
	public void init() throws ServletException {
		// Put your code here
	}

}
