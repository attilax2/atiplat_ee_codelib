package com.csmy.my.center.util.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.SystemPropertyUtils;

import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.base.Channel;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.db.JdbcTemplateTool;

public class ChannelServlet extends HttpServlet {

	private static final long serialVersionUID = 708845212506625075L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//req.setCharacterEncoding("UTF-8");
		   
		//获取请求路径
		String param = "";
		String reqPath = req.getRequestURI();
		String nowURl = req.getServletPath();
		Dto paraDto = RequestUtil.getPraramsAsDto(req);
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		
		MemeberInfo me = RequestUtil.getUser(req);
  		req.setAttribute(CTConstants.MENU_SELECTED_INDEX, "channel");
		if(CTUtils.isEmpty(me)){
			CTUtils.getParamToReq(req);
			resp.sendRedirect(RequestUtil.getBasePath(req)+"/user/login");
		}else{
        //处理列表
		String channel_uid =me.getMemeber_id();
		if(reqPath.indexOf("list")!=-1){
			
			List<Channel> list=JdbcTemplateTool.QueryList(channel_uid);
			req.setAttribute("pm", list);
			 CTUtils.getParamToReq(req);
			req.getRequestDispatcher("/wxb/channel.jsp").forward(req, resp);
		}
		  
		
		//处理增加
        if(reqPath.indexOf("add")!=-1){
        	param = getParamFromURl(reqPath, temp_path);
        	String name=paraDto.getAsString("name");
        	String remark=paraDto.getAsString("remark");
        	boolean init =JdbcTemplateTool.AddDate(channel_uid,name, remark);
        	List<Channel> list=JdbcTemplateTool.QueryList(channel_uid);
			
			req.setAttribute("pm", list);
			 CTUtils.getParamToReq(req);
			req.getRequestDispatcher("/wxb/channel.jsp").forward(req, resp);
			
		}
        
		//处理增加(Ajax)
        if(reqPath.indexOf("save")!=-1){
        	
        	String chname = paraDto.getAsString("name");
        	//验证渠道是否以及存在
        	if(!StringUtil.checkListBlank(JdbcTemplateTool.getTableList("wxb_channel", " t.channel_name='"+chname+"'"))){
        		RequestUtil.renderText("1#渠道["+chname+"]已经存在");
        	}else{
	        	int bkey = JdbcTemplateTool.addChannel(RequestUtil.getUser(req).getMemeber_id(), chname, chname);
	        	if(bkey>0){
	        		RequestUtil.renderText("0#"+String.valueOf(bkey));
	        	}else{
	        		RequestUtil.renderText("1#增加渠道出错");
	        	}
        	}
		}
        
      //处理增加
        if(reqPath.indexOf("into")!=-1){
           param = getParamFromURl(reqPath, temp_path);
           CTUtils.getParamToReq(req);
		   req.getRequestDispatcher("/wxb/channel_add.jsp").forward(req, resp);
		}
		
		
		//处理修改
        if(reqPath.indexOf("edit")!=-1){
        	param = getParamFromURl(reqPath, temp_path);
        	List<Channel> list=JdbcTemplateTool.QueryList(channel_uid,param);
			req.setAttribute("pm", list.get(0));
			CTUtils.getParamToReq(req);
        	req.getRequestDispatcher("/wxb/channel_edit.jsp").forward(req, resp);
		}
		
		
		//处理删除
        if(reqPath.indexOf("delete")!=-1){
        	param = getParamFromURl(reqPath, temp_path);
        	boolean init=JdbcTemplateTool.deleteDate(param);
        	List<Channel> list=JdbcTemplateTool.QueryList(channel_uid);
			req.setAttribute("pm", list);
			CTUtils.getParamToReq(req);
			req.getRequestDispatcher("/wxb/channel.jsp").forward(req, resp);
		}
        
        //修改
        if(reqPath.indexOf("update")!=-1){
        	//param = getParamFromURl(reqPath, temp_path);
        	String id=paraDto.getAsString("id");
        	String name=paraDto.getAsString("name");
        	String remark=paraDto.getAsString("remark");
        	boolean init=JdbcTemplateTool.UpdateDate(channel_uid, id, name, remark);
        	if(init){
        		List<Channel> list=JdbcTemplateTool.QueryList(channel_uid);
    			req.setAttribute("pm", list);
    			CTUtils.getParamToReq(req);
    			req.getRequestDispatcher("/wxb/channel.jsp").forward(req, resp);
        	}else{
        		CTUtils.getParamToReq(req);
        		req.getRequestDispatcher("/wxb/channel_edit.jsp").forward(req, resp);
        	}
		}
	  }
		
	}
	
	/**
	 * 获取参数
	 * @param url
	 * @return
	 */
	private String getParamFromURl(String url,String surl) {
		String param=null;
		if(StringUtil.isNotEmpty(url)){
			String tempUrl = url.substring(url.lastIndexOf(surl), url.length());
			String params[] = tempUrl.split("\\/");
			if(params.length==3){
			   param = params[2];
			}
		}
		StringUtil.xprint("param="+param);
		return param;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		 
		doGet(req, resp);
	}

}
