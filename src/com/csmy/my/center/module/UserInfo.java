package com.csmy.my.center.module;

/**
 * 用户信息实体类
 * 
 * @author wgp
 * 
 */
public class UserInfo implements java.io.Serializable {
	
	private static final long serialVersionUID = -6844903841649209611L;
	private String user_id;// 用户编号
	private String user_name;// 用户名称
	private String account;// 登录账号
	private String password;// 登录密码
	private String epassword;// 登录密码(未加密)
	private String locked;// 是否锁定
	private String enabled;// 是否有效
	private String remark;// 备注信息
	private String role_id;// 角色变换
	private String user_type;

	private UserExtendInfo userExtendInfo;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEpassword() {
		return epassword;
	}

	public void setEpassword(String epassword) {
		this.epassword = epassword;
	}

	public String getLocked() {
		return locked;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public UserExtendInfo getUserExtendInfo() {
		return userExtendInfo;
	}

	public void setUserExtendInfo(UserExtendInfo userExtendInfo) {
		this.userExtendInfo = userExtendInfo;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

}
