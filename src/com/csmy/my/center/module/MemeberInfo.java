package com.csmy.my.center.module;

public class MemeberInfo implements java.io.Serializable  {
	
	private static final long serialVersionUID = -2470926605648525681L;
	
	private String memeber_id;
	private String account;
	private String qq_num;
	private String email;
	private String phone;
	private String recom_user;
	private String register_time;
	private String name;
	private String pay_account;
	private String password;
	private String epassword;
	private String visit_code;
	private String level_code;
	private String use_recom;
	private String update_time;//最后登录时间

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getUse_recom() {
		return use_recom;
	}

	public void setUse_recom(String use_recom) {
		this.use_recom = use_recom;
	}

	public String getMemeber_id() {
		return memeber_id;
	}

	public void setMemeber_id(String memeber_id) {
		this.memeber_id = memeber_id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getQq_num() {
		return qq_num;
	}

	public void setQq_num(String qq_num) {
		this.qq_num = qq_num;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRecom_user() {
		return recom_user;
	}

	public void setRecom_user(String recom_user) {
		this.recom_user = recom_user;
	}

	public String getRegister_time() {
		return register_time;
	}

	public void setRegister_time(String register_time) {
		this.register_time = register_time;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPay_account() {
		return pay_account;
	}

	public void setPay_account(String pay_account) {
		this.pay_account = pay_account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEpassword() {
		return epassword;
	}

	public void setEpassword(String epassword) {
		this.epassword = epassword;
	}

	public String getVisit_code() {
		return visit_code;
	}

	public void setVisit_code(String visit_code) {
		this.visit_code = visit_code;
	}

	public String getLevel_code() {
		return level_code;
	}

	public void setLevel_code(String level_code) {
		this.level_code = level_code;
	}
	
	
	
}
