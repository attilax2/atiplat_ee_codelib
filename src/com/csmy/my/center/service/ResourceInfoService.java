package com.csmy.my.center.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.dao.ResourceInfoDao;
import com.csmy.my.center.util.dataconvert.Dto;

/**
 * 资源信息业务类
 * 
 * @author wgp
 * @since 2013-01-15
 * 
 */
@Controller
@Scope("prototype")
@SuppressWarnings("unchecked")
public class ResourceInfoService {
	@Autowired
	private ResourceInfoDao  resourceInfoDao;
	private Logger logger = Logger.getLogger(ResourceInfoService.class);
	
	/**
	 * 操作字典数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operCodeInfo(Dto pDto) {
		return resourceInfoDao.operCodeInfo(pDto);
	}
	
	
	/**
	 * 操作菜单数据
	 * 
	 * @param pDto
	 * @return
	 */
	public Dto operMenuInfo(Dto pDto) {
		return resourceInfoDao.operMenuInfo(pDto);
	}
	/**
	 * 操作订单数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operOrderInfo(Dto pDto){
		return resourceInfoDao.operOrderInfo(pDto);
	}
	
	/**
	 * 操作全局数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operParamInfo(Dto pDto) {
		return resourceInfoDao.operParamInfo(pDto);
	}
	
	
	/**
	 * 操作内容数据
	 * 
	 * @param pDto
	 * @return
	 */
	public boolean operContentInfo(Dto pDto) {
		return resourceInfoDao.operContentInfo(pDto);
	}
}
