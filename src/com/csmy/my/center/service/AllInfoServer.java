package com.csmy.my.center.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.dao.AllInfoDao;
import com.csmy.my.center.dao.UserInfoDao;
import com.csmy.my.center.util.dataconvert.Dto;

@Controller
@Scope("prototype")
@SuppressWarnings("unchecked")
public class AllInfoServer {

	
	
	@Autowired
	private AllInfoDao allInfoDao;
	private Logger logger = Logger.getLogger(AllInfoServer.class);
	/**
	 * 添加渠道
	 * @param pDto
	 * @return
	 */
	public boolean AddChannelInfo(Dto pDto) {
		return allInfoDao.AddChannelInfo(pDto);
	}
	
	/**
	 * 修改渠道
	 * @param pDto
	 * @return
	 */
	public boolean UpdateChannelInfo(Dto pDto) {
		return allInfoDao.UpdateChannelInfo(pDto);
	}
	
	/**
	 * 删除渠道
	 * @param pDto
	 * @return
	 */
	public boolean DeleteChannelInfo(Dto pDto) {
		return allInfoDao.DeleteChannelInfo(pDto);
	}
	
	
}
