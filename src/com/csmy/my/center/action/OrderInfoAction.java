package com.csmy.my.center.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.csmy.my.center.module.MyOrderInfo;
import com.csmy.my.center.module.OrderInfo;
import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.service.ResourceInfoService;
import com.csmy.my.center.util.AttachMentMethod;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.FileUtils;
import com.csmy.my.center.util.MobileLocationUtil;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.RequestUtil;
import com.csmy.my.center.util.SenderQueryUtil;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.ZHToEN;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.dataconvert.json.JsonHelper;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.csmy.my.center.util.report.excel.JxlsUtil;
import com.csmy.my.center.util.sms.SMS_Sender;

/**
 * 订单请求管理
 * @author jackie
 *
 */
@SuppressWarnings("unchecked")
public class OrderInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 1566354769069913086L;
	
	private File myOrder;//上传附件
	private String myOrderFileName;//上传附件名称
	
	public PageModel pager = new PageModel();
	//请求页面
	public static final String ORDER_LIST = "order_list";// 订单列表
	public static final String ORDER_EDIT = "edit_order";// 修改订单
	public static final String ORDER_CLIST = "order_clist";// 订单流水列表
	public static final String ORDER_MLIST = "order_mlist";// 订单返款单列表
	public static final String ORDER_SMLIST = "order_smlist";// 客服返款单列表
	public static final String PAYLOG_LIST = "paylog_list";// 付款日志列表
	public static final String SPAYLOG_LIST = "spaylog_list";// 付款日志列表
	public static final String SHOW_ORDER = "show_order";// 订单详情
	public static final String SHOW_SENDER = "show_sender";// 物流详情
	public static final String PAY_MONEY = "pay_money";// 确认打款
	public static final String PAY_SMONEY = "pay_smoney";// 客服确认打款
	public static final String ORDER_INIT = "order_init";// 客服订单初始化
	public static final String SORDER_TREE = "sorder_tree";// 客服订单树结构
	public static final String SORDER_LIST = "sorder_list";// 客服订单列表
	public static final String SCOUNT_CLIST = "scount_list";// 客服订单流水列表
	public static final String ORDER_IMPORT = "order_import";// 导入订单功能
	public static final String ORDER_SIGN = "order_sign";// 批量签收订单功能
	public static final String BATCH_CHECK = "batch_check";// 批量审核订单功能
	public static final String MCORDER_LIST = "mcorder_list";// 会员统计订单列表
	public static final String CTORDER_LIST = "ctorder_list";// 商户统计订单列表
	public static final String GDORDER_LIST = "gdorder_list";// 商品统计订单列表
	
	public static final String ERROR = "error";// 错误页面
	
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession session = request.getSession();

	@Autowired
	private ResourceInfoService resourceInfoService;
	
	/**
	 * 客服订单初始化页面
	 * @return
	 */
	public String sorderInit(){
		Dto dto  = getPraramsAsDto(request);
		request.setAttribute("user_id",dto.getAsString("user_id"));
		return ORDER_INIT;
	}
	
	/**
	 * 客服订单树结构页面
	 * @return
	 */
	public String sorderTree(){
		Dto dto  = getPraramsAsDto(request);
		request.setAttribute("user_id",dto.getAsString("user_id"));
		return SORDER_TREE;
	}
	
	/**
	 * 客服订单列表页面
	 * @return
	 */
	public String sorderList(){
		Dto dto  = getPraramsAsDto(request);
		request.setAttribute("user_id",dto.getAsString("user_id"));
		return SORDER_LIST;
	}
	
	/**
	 * 确认打款
	 * @return
	 * @throws ParseException 
	 */
	public String goPayMoney() throws ParseException {
		//获取所有参数信息
		int count = 0;
		int fcount = 0;
		List<Dto> umList = null;
		Dto payMoneyDto = null;
		String um_idString = "";
		
		double payMoney = 0d;
		double smoney = 0d;
		
		Dto dto =  getPraramsAsDto(request);
		String um_ids = dto.getAsString("um_id");
		String account = dto.getAsString("account");
		
		String pay_type = "";
		if(CTUtils.isNotEmpty(account)){//批量处理
			pay_type = "批量支付";
			dto.put("state", "0");//只处理未支付
			umList = CTReader.queryForList("order.getUserMoneyList", dto); 
			//处理结算日期
			int exp_date = Integer.valueOf(CTUtils.getParamValue("ALIPAY_DATE_PERIOD"));
			StringUtil.xprint("系统结算周期："+exp_date);
			if(!StringUtil.checkListBlank(umList)){//处理只能支付的数据
				int period = 0;
				String createtime = null;
				payMoneyDto = (BaseDto)umList.get(0);
				for (Dto dto2 : umList) {
					createtime = dto2.getAsString("count_time");
					period = CTUtils.getSubDay(createtime, CTUtils.getCurrentTime());
					if(period>=exp_date){//可以支付
						count++;
						if(count<umList.size()){
							um_idString +=dto2.getAsString("um_id")+",";
						}else{
							um_idString +=dto2.getAsString("um_id");
						}
						smoney = Double.valueOf(dto2.getAsString("own_money"));
						if(smoney<0)fcount++;
						payMoney += smoney;//实际应该打款数
					}
				}
			}
			
		}else{//合并付款
			pay_type = "合并支付";
			if(CTUtils.isNotEmpty(um_ids)){
				umList = CTReader.queryForList("order.getUserMoneyList", dto);
				if(!StringUtil.checkListBlank(umList)){//处理只能支付的数据
					payMoneyDto = (BaseDto)umList.get(0);
					for (Dto dto2 : umList) {
						count++;
						if(count<umList.size()){
							um_idString +=dto2.getAsString("um_id")+",";
						}else{
							um_idString +=dto2.getAsString("um_id");
						}
						smoney = Double.valueOf(dto2.getAsString("own_money"));
						if(smoney<0)fcount++;
						payMoney += smoney;//实际应该打款数
					}
			  }
			}
		}
		
		if(count>1){
			payMoneyDto.put("payNum", count);
			payMoneyDto.put("payType", "合并付款");
		}
		if(fcount>0){
			payMoneyDto.put("payFNum", fcount);
		}
		payMoneyDto.put("pay_type", pay_type);
		payMoneyDto.put("payMoney", payMoney);
		payMoneyDto.put("um_ids", um_idString);
		
		request.setAttribute("moneyInfo", payMoneyDto);
		
		return PAY_MONEY;
	}
	
	
	
	/**
	 * 确认客服打款
	 * @return
	 * @throws ParseException 
	 */
	public String goSPayMoney() throws ParseException {
		//获取所有参数信息
		int count = 0;
		int fcount = 0;
		List<Dto> umList = null;
		Dto payMoneyDto = null;
		String um_idString = "";
		
		double payMoney = 0d;
		double smoney = 0d;
		
		Dto dto =  getPraramsAsDto(request);
		String um_ids = dto.getAsString("sm_id");
		String user_id = dto.getAsString("user_id");
		
		String pay_type = "";
		if(CTUtils.isNotEmpty(user_id)){//批量处理
			pay_type = "批量支付";
			dto.put("state", "0");//只处理未支付
			umList = CTReader.queryForList("order.getServerMoneyList", dto); 
			if(!StringUtil.checkListBlank(umList)){//处理只能支付的数据
				payMoneyDto = (BaseDto)umList.get(0);
				for (Dto dto2 : umList) {
					count++;
					if(count<umList.size()){
						um_idString +=dto2.getAsString("sm_id")+",";
					}else{
						um_idString +=dto2.getAsString("sm_id");
					}
					smoney = Double.valueOf(dto2.getAsString("own_money"));
					if(smoney<0)fcount++;
					payMoney += smoney;//实际应该打款数
				}
			}
			
		}else{//合并付款
			pay_type = "合并支付";
			if(CTUtils.isNotEmpty(um_ids)){
				umList = CTReader.queryForList("order.getServerMoneyList", dto);
				if(!StringUtil.checkListBlank(umList)){//处理只能支付的数据
					payMoneyDto = (BaseDto)umList.get(0);
					for (Dto dto2 : umList) {
						count++;
						if(count<umList.size()){
							um_idString +=dto2.getAsString("sm_id")+",";
						}else{
							um_idString +=dto2.getAsString("sm_id");
						}
						smoney = Double.valueOf(dto2.getAsString("own_money"));
						if(smoney<0)fcount++;
						payMoney += smoney;//实际应该打款数
					}
			  }
			}
		}
		
		if(count>1){
			payMoneyDto.put("payNum", count);
			payMoneyDto.put("payType", "合并付款");
		}
		if(fcount>0){
			payMoneyDto.put("payFNum", fcount);
		}
		payMoneyDto.put("pay_type", pay_type);
		payMoneyDto.put("payMoney", payMoney);
		payMoneyDto.put("sm_ids", um_idString);
		
		request.setAttribute("moneyInfo", payMoneyDto);
		
		return PAY_SMONEY;
	}
	
	
	/**
	 * 获取订单列表数据
	 * @return
	 */
	public String getOrderList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			int orderCount = 0;
			dList = CTReader.queryForPage("order.getOrderInfo", dto);
			if(!StringUtil.checkListBlank(dList)){
				for (Dto dto2 : dList) {
					dto2.put("state_name", CTUtils.getCodeName("ORDER_STATE", dto2.getAsString("state")));
					dto2.put("sender_name", CTUtils.getCodeName("SENDER_TYPE", dto2.getAsString("sender_type")));
					dto2.put("type_name", CTUtils.getCodeName("ORDER_TYPE", dto2.getAsString("order_type")));
					
					orderCount = (Integer)CTReader.queryForObject("order.getOrderByTelePhone", dto2);
					dto2.put("telcount", orderCount);
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getOrderInfoCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//获取订单总数合计
			Dto ocountDto = (BaseDto)CTReader.queryForObject("order.getOrderTotal",dto);
			Dto countDto = (BaseDto)CTReader.queryForObject("order.getOrderTotal");
			
			//获取商品信息
			// 获取商户信息
			dto.put("enabled", "1");
			List<Dto> cList = CTReader.queryForList("resource.getCustomerInfo", dto);
			request.setAttribute("customerList", cList);
			//获取商品信息
			List<Dto> goodList = CTReader.queryForList("good.getGoodInfo", dto);
			request.setAttribute("goodList", goodList);
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("ocountDto", ocountDto); 
			request.setAttribute("counDto", countDto); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("flags", "0");
			request.setAttribute("user_id", dto.getAsString("user_id"));
			request.setAttribute("state_id", dto.getAsString("state_id"));
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			return ORDER_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 会员订单统计
	 * @return
	 */
	public String getUserOrderCount() {
		try {
			Dto dto =  getPraramsAsDto(request);
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			
			dto = getPageDto(dto);
			//获取所有会员列表
			List<Dto> mList = CTReader.queryForPage("order.getUserOrderCountList", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("order.getUserOrderCountListCount", dto);
			if(totalCount>0){
			   Dto totalDto = (BaseDto)CTReader.queryForObject("order.getTotalUserOrderCount", dto);
			   request.setAttribute("totalDto", totalDto); 
			}
			pager = getPageModel(mList, dto.getAsInteger("start"), totalCount);
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			
			return MCORDER_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 商户订单统计
	 * @return
	 */
	public String getCustOrderCount() {
		try {
			Dto dto =  getPraramsAsDto(request);
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			//获取所有会员列表
			List<Dto> mList = CTReader.queryForPage("order.getCustOrderCountList", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("order.getCustOrderCountListCount", dto);
			if(totalCount>0){
			   Dto totalDto = (BaseDto)CTReader.queryForObject("order.getTotalCustOrderCount", dto);
			   request.setAttribute("totalDto", totalDto); 
			}
			pager = getPageModel(mList, dto.getAsInteger("start"), totalCount);
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			
			return CTORDER_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 商户订单统计
	 * @return
	 */
	public String getGoodOrderCount() {
		try {
			Dto dto =  getPraramsAsDto(request);
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			//获取所有会员列表
			List<Dto> mList = CTReader.queryForPage("order.getGoodOrderCountList", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("order.getGoodOrderCountListCount", dto);
			if(totalCount>0){
			   Dto totalDto = (BaseDto)CTReader.queryForObject("order.getTotalGoodOrderCount", dto);
			   request.setAttribute("totalDto", totalDto); 
			}
			pager = getPageModel(mList, dto.getAsInteger("start"), totalCount);
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			
			return GDORDER_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 获取客服订单列表数据
	 * @return
	 */
	public String getSOrderList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			UserInfo userInfo = getUser(request);
			dto.put("user_id", userInfo.getUser_id());
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("order.getOrderInfo", dto);
			if(!StringUtil.checkListBlank(dList)){
				for (Dto dto2 : dList) {
					dto2.put("state_name", CTUtils.getCodeName("ORDER_STATE", dto2.getAsString("state")));
					dto2.put("type_name", CTUtils.getCodeName("ORDER_TYPE", dto2.getAsString("order_type")));
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getOrderInfoCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//获取订单总数合计
			Dto ocountDto = (BaseDto)CTReader.queryForObject("order.getOrderTotal",dto);
			Dto countDto = (BaseDto)CTReader.queryForObject("order.getOrderTotal");
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("ocountDto", ocountDto); 
			request.setAttribute("counDto", countDto); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("flags", "1");
			request.setAttribute("user_id", dto.getAsString("user_id"));
			request.setAttribute("state_id", dto.getAsString("state_id"));
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			return ORDER_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	//进入订单导入
	public String goImportOrder() {
		//获取所有参数信息
		Dto dto =  getPraramsAsDto(request);
		return ORDER_IMPORT;
	}
	
	//进入批量签收
	public String goSignOrder() {
		//获取所有参数信息
		Dto dto =  getPraramsAsDto(request);
		List<Dto> orderList = CTReader.queryForList("order.getOrderInfo", dto);
		if(CTUtils.isNotEmpty(orderList)){
			request.setAttribute("onum", orderList.size());
		}else{
			request.setAttribute("onum", "0");
		}
		return ORDER_SIGN;
	}
	
	
	/**
	 * 签收订单
	 */
	public void signOrder() {
		boolean temp = false;
		int countNmu = 0;
		try {
			Dto dto =  getPraramsAsDto(request);
			int max_num = dto.getAsInteger("max_num");
			List<Dto> orderList = CTReader.queryForList("order.getOrderInfo", dto);
			if(CTUtils.isNotEmpty(orderList)){
				
				String kddh = null;
				String wlgs = null;
				String result = null;
				
				int isChecked = 0;//默认值为未签收
				double totalMoney=0.0;
				double totalPMoney=0.0;
				double totalSMoney=0.0;
				
				Dto inDto = null;
				Dto intDto = null;
				Dto resultMap = null;
				
				for (Dto orderInfo : orderList) {
					//获取物流单号和物流公司
					kddh = orderInfo.getAsString("courier_id");
					wlgs = orderInfo.getAsString("sender_type");
					
					//调用物流查询接口
					result = SenderQueryUtil.getWuliu(kddh, wlgs);
					if(CTUtils.isNotEmpty(result)){
						resultMap = JsonHelper.toMap(result);
						StringUtil.xprint("获取结果为>>>>"+resultMap);
						if(!resultMap.containsKey("ischeck")) break;
						//获取是否签收
						isChecked = resultMap.getAsInteger("ischeck");
						
						if(isChecked==1){//已经签收了则处理状态
							inDto = new BaseDto();
							intDto = new BaseDto();
							inDto.put("order_id", orderInfo.getAsString("order_id"));
							intDto.put("order_id", orderInfo.getAsString("order_id"));
							//处理金额
							String nowtime = CTUtils.getCurrentTime();
							totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
							totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
							totalSMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("service_money"));
							
							//结算金额
							inDto.put("optype", "order_count");
							inDto.put("ocid", UniqueID.getUniqueID(8, 0));
							inDto.put("scid", UniqueID.getUniqueID(8, 0));
							inDto.put("order_money", totalMoney);//订单金额
							inDto.put("order_pmoney", totalPMoney);//订单分成
							inDto.put("server_pmoney", totalSMoney);//客服提成
							
							inDto.put("state_id", "1");//待结算
							inDto.put("createtime", nowtime);
							inDto.put("remark", nowtime+"结算分成返款金额【"+totalPMoney+"】");
							//结算预计支付日期
							String exp_day = getParamValue("ALIPAY_DATE_PERIOD");
							String exp_date = CTUtils.getDateByUDay(Integer.valueOf(exp_day));
							String subTime = nowtime.split(" ")[1];
							inDto.put("exp_pay_date", exp_date+" "+subTime);
							
							StringUtil.xprint("预期付款日期==>"+exp_date+" "+subTime);
							resourceInfoService.operOrderInfo(inDto);
							
							//增加客服流水
							if(totalSMoney!=0L){
								inDto.put("optype", "sorder_count");
								inDto.put("remark", nowtime+"结算客服分成返款金额【"+totalSMoney+"】");
								resourceInfoService.operOrderInfo(inDto);
							}
							
							intDto.put("state", "7");//待结算
							intDto.put("log_txt", "已批量处理签收、待结算");
							intDto.put("order_remark", "批量处理签收");
							intDto.put("createtime", nowtime);
							intDto.put("optype", "edit_order");
							resourceInfoService.operOrderInfo(intDto);
							
							countNmu++;
							if(max_num!=0 && countNmu>max_num){
								break;//超过最大值结束处理
							}
							
							//线程停一秒
							Thread.sleep(1000);
						}
					}
				}
				temp = true;
			}
			
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}finally{
			if(temp){
				this.renderText("true#"+countNmu);
			}else{
				this.renderText("false#0");
			}
		}
	}
	
	
	/**
	 * 导入cod订单信息
	 */
	public String importOrder() {
		String returnMsg = "";
		try {
			//获取xml配置文件地址
			response.setContentType("text/html; charset=UTF-8");
			String ctx = request.getContextPath();
			String templateFile = OrderInfoAction.class.getClassLoader().getResource("").getPath();
			String xmlConfigPath = templateFile + "template/orderTemplate.xml";
			StringUtil.xprint("name="+myOrder.getAbsolutePath());
			
			PrintWriter out = null;
			try {
				out = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			if (myOrder == null) {
				returnMsg = "请选择文件。";
			}
			
			//先将文件保存在//upload路劲
			//String savePath = StringUtil.getContainPath() + "/" + request.getContextPath() + "/upload/";
			//String savePath = ServletActionContext.getServletContext().getRealPath("/") + "upload/";
			String savePath = ServletActionContext.getServletContext().getRealPath("upload");
			File dirFile = new File(savePath);
			if (!dirFile.exists()) {
				dirFile.mkdirs();
			}
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + ".xls" ;
			String xlsPath = savePath+"/"+newFileName;
			FileOutputStream fos = new FileOutputStream(xlsPath);
			FileInputStream fis = new FileInputStream(getMyOrder());
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = fis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			
			//处理订单数据
			JxlsUtil jUtil = new JxlsUtil();
	        List<OrderInfo> oList;
	        MyOrderInfo orderInfo = jUtil.parseExcelFileToBeans(xlsPath,xmlConfigPath);
	        if(CTUtils.isNotEmpty(orderInfo)){
	        	Dto perDto = null;
	        	boolean bool = false;
	        	int countNum = 0;
				oList = orderInfo.getOrder();
				Dto perOrderInfo = null;
				if(CTUtils.isNotEmpty(orderInfo)){
					for (OrderInfo order : oList) {
						String order_id = order.getOrder_id();//订单编号
						String courier_id = order.getCourier_id();//快递单号
						String sender_type = order.getSender_type();//快递公司
						//判断订单是否存在
						perDto = new BaseDto();
						perDto.put("order_id", order_id);
						perOrderInfo = (Dto)CTReader.queryForObject("order.getOrderInfo",perDto);
						//订单存在且未发货
						if(CTUtils.isNotEmpty(perOrderInfo) && perOrderInfo.getAsString("state").equals("2")){
							//单号和公司同时不能为空
							if(CTUtils.isNotEmpty(courier_id) && CTUtils.isNotEmpty(sender_type)){
								perDto.put("optype", "mod_order");
								//处理科学计数法
								String courier_id1 = null;
								if(courier_id.contains("E")&&courier_id.contains(".")){
									BigDecimal bd = new BigDecimal(courier_id);  
									courier_id1 = bd.toPlainString();
									StringUtil.xprint("处理科学计数法后结果："+courier_id1);
								}else{
									courier_id1 = courier_id;
								}
								perDto.put("courier_id", courier_id1);
								perDto.put("sender_type", sender_type);
								//设置状态为已发货
								perDto.put("state", "4");
								bool = resourceInfoService.operOrderInfo(perDto);
								
								
								/*************************发送短信开始*************************/
				        		String send_sms = CTUtils.getParamValue("SEND_DELIVERY_SMS");
				        		String send_sms_temp = CTUtils.getParamValue("SEND_SMS_TEMPLATE");
				        		if(CTUtils.isNotEmpty(send_sms) && send_sms.equals("1") && CTUtils.isNotEmpty(send_sms_temp)){
				        			String buyer_name = perOrderInfo.getAsString("buyer_name");
				        			String buyer_phone = perOrderInfo.getAsString("buyer_phone");
				        			String orderid = perOrderInfo.getAsString("order_id");
				        			String order_money = perOrderInfo.getAsString("sku_price");
				        			int bnum = perOrderInfo.getAsInteger("buy_num");
				        			double totalMoney = Double.valueOf(order_money)*bnum;
				        			
				        			if(send_sms_temp.contains("buyer_name")){
				        				send_sms_temp = send_sms_temp.replaceAll("\\$buyer_name", buyer_name);
				        			}
				        			if(send_sms_temp.contains("order_id")){
				        				send_sms_temp = send_sms_temp.replaceAll("\\$order_id", orderid);
				        			}
				        			if(send_sms_temp.contains("order_money")){
				        				send_sms_temp = send_sms_temp.replaceAll("\\$order_money", ""+totalMoney);
				        			}
				        			
				        			String smString = send_sms_temp;
				        			StringUtil.xprint("发送批量发货提示短信内容为>>>"+smString);
				        			if(SMS_Sender.sendSms(buyer_phone, smString)){
				    					StringUtil.xprint("批量发货提示发送成功!");
				    				}else{
				    					StringUtil.xprint("批量发货提示短信发送失败!");
				    				}
				        		}
				        		/*************************发短信件结束*************************/
								
								
								if(bool){
								   countNum++;
								   //添加日志
								   perDto.put("optype", "add_log");
								   perDto.put("log_txt", "已发货、待签收 ");
								   resourceInfoService.operOrderInfo(perDto);
								}
							}else{
								returnMsg = "对不起！导入的excel文件缺少核心数据【快递单号、快递公司编号】!";
							}
						}
					}
					//处理返回信息
					returnMsg = "共【"+oList.size()+"】条数据,导入成功【"+countNum+"】条!";
				}else{
					returnMsg = "对不起！导入的excel文件没有相关数据!";
		        }
	        }else{
	        	returnMsg = "对不起！导入的excel文件数据为空!";
	        }
			
			//导入完成后删除xls文档
			FileUtils fileUtils = new FileUtils();
			fileUtils.delFile(xlsPath);
			
		} catch (Exception e) {
			returnMsg = "导入数据过程出现错误！";
			e.printStackTrace();
		}finally{
			this.renderHtml("<script>alert('"+returnMsg+"');</script>");
		}
		return null;
	}
	
	
	
	/**
	 * 导出订单到excel
	 */
	public void exportOrder() {
		try {
			//获取待发货的订单
			Dto paramDto = new BaseDto();
			Dto dto =  getPraramsAsDto(request);
			//处理订单
			String tempID = dto.getAsString("tempid");//模板id
			String state_id = dto.getAsString("state_id");
			paramDto.put("state_id",state_id);
			paramDto.put("good_id",dto.getAsString("good_id"));
			paramDto.put("customer_id",dto.getAsString("customer_id"));
			String order_id = dto.getAsString("order_id");
			if(CTUtils.isNotEmpty(order_id)){
				order_id = order_id.replaceAll(",", "','");
				paramDto.put("order_id", order_id);
			}
			List<Dto> orderList = CTReader.queryForList("order.getOrderInfo", paramDto);
			if(CTUtils.isNotEmpty(orderList)){
				//处理总金额
				double totalMoney =0L; 
				for (Dto dto2 : orderList) {
					
					String city = dto2.getAsString("city");
					String area = dto2.getAsString("area");
					String address = dto2.getAsString("address");
					String sku_name = dto2.getAsString("sku_name");
					String good_name = dto2.getAsString("good_name");
					String province = dto2.getAsString("province");
					
					double price = Double.valueOf(dto2.getAsString("sku_price"));
					int buy_num = dto2.getAsInteger("buy_num");
					double tprice = price*buy_num;
					totalMoney+=tprice;
					dto2.put("tprice", tprice);
					dto2.put("userAddr",province+city+area+" "+address);//拼接地址
					dto2.put("goodStr", good_name+"/"+ sku_name);
					
					//处理订单状态
					String order_state = dto2.getAsString("state");
					dto2.put("state_name", CTUtils.getCodeName("ORDER_STATE", order_state));
				}
				
				String templateFile = JxlsUtil.class.getClassLoader().getResource("").getPath(); 
				//处理导出全部的情况
				String tempFile=null;
				String outTempFile = null;
				if(CTUtils.isEmpty(state_id)||tempID.equals("1")){
					tempFile=templateFile+"template/newOrderTemplate.xls";
					outTempFile = templateFile+"template/"+CTUtils.getCurDateNoSplit()+"_simple_outfile.xls";
					StringUtil.xprint("导出全部订单信息...");
				}else{
					tempFile=templateFile+"template/orderTemplate.xls";
					outTempFile = templateFile+"template/"+CTUtils.getCurDateNoSplit()+"_cod_outfile.xls";
					StringUtil.xprint("导出待发货订单信息...");
				}
				OutputStream os=new FileOutputStream(outTempFile);
				
		        Map<String, Object> beans=new HashMap<String, Object>(); 
		        beans.put("orderList", orderList);
		        beans.put("count", orderList.size());
		        beans.put("totalMoney", totalMoney);
				boolean temp = JxlsUtil.export2Excel(beans, tempFile, os);
				if(temp){
					AttachMentMethod attchMethod = new AttachMentMethod();
					attchMethod.downloadFile(outTempFile);
				}else{
					this.renderHtml("<script>alert('很抱歉，导出数据失败！');window.history.back(-1);</script>");
				}
			}else{
				this.renderHtml("<script>alert('很抱歉，没有待发货订单！');window.history.back(-1);</script>");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 导出支付宝日志到excel
	 */
	public void exportPayList() {
		try {
			//获取待发货的订单
			Dto dto =  getPraramsAsDto(request);
			List<Dto> orderList = CTReader.queryForList("order.getPayLogList", dto);
			if(CTUtils.isNotEmpty(orderList)){
				//处理总金额
				int cindex =0; 
				for (Dto dto2 : orderList) {
					cindex++;
					dto2.put("rowNum", cindex);
				}
				
				String templateFile = JxlsUtil.class.getClassLoader().getResource("").getPath(); 
				OutputStream os=new FileOutputStream(templateFile+"template/payLogs.xls");
		        String tempFile=templateFile+"template/payTemplate.xls";
		        Map<String, Object> beans=new HashMap<String, Object>(); 
		        beans.put("payList", orderList);
				boolean temp = JxlsUtil.export2Excel(beans, tempFile, os);
				if(temp){
					AttachMentMethod attchMethod = new AttachMentMethod();
					attchMethod.downloadFile(templateFile+"template/payLogs.xls");
				}else{
					this.renderHtml("<script>alert('很抱歉，导出支付宝数据失败！');window.history.back(-1);</script>");
				}
			}else{
				this.renderHtml("<script>alert('很抱歉，没有支付宝记录！');window.history.back(-1);</script>");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取订单返款单列表数据
	 * @return
	 */
	public String getUserMoneyList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			//处理结算日期
			int exp_date = Integer.valueOf(CTUtils.getParamValue("ALIPAY_DATE_PERIOD"));
			StringUtil.xprint("系统结算周期："+exp_date);
			
			dto.put("page_size", 20);
			dto = getPageDto2(dto);
			dList = CTReader.queryForPage("order.getUserMoneyList", dto);
			if(!StringUtil.checkListBlank(dList)){
				int period = 0;
				String createtime = null;
				for (Dto dto2 : dList) {
					createtime = dto2.getAsString("count_time");
					period = CTUtils.getSubDay(createtime, CTUtils.getCurrentTime());
					if(period>=exp_date){
						dto2.put("pay_state","1");
					}else{
						dto2.put("pay_state","0");
					}
					dto2.put("state_name", CTUtils.getCodeName("PAYSATE", dto2.getAsString("state")));
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getUserMoneyListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount,20);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("state", dto.getAsString("state"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("pay_date", getParamValue("ALIPAY_DATE_PERIOD"));
			
			return ORDER_MLIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 获取客服订单返款单
	 * @return
	 */
	public String getServerMoneyList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			dto.put("page_size", 20);
			dto = getPageDto2(dto);
			dList = CTReader.queryForPage("order.getServerMoneyList", dto);
			if(!StringUtil.checkListBlank(dList)){
				for (Dto dto2 : dList) {
					dto2.put("state_name", CTUtils.getCodeName("PAYSATE", dto2.getAsString("state")));
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getServerMoneyListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount,20);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("state", dto.getAsString("state"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			request.setAttribute("start_date", dto.getAsString("start_date"));
			
			return ORDER_SMLIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 获取付款日志列表数据
	 * @return
	 */
	public String getPayLogList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("order.getPayLogList", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("order.getPayLogListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			
			return PAYLOG_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 获取客户付款日志列表数据
	 * @return
	 */
	public String getSPayLogList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("order.getSPayLogList", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("order.getSPayLogListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			request.setAttribute("alisaname", queryParam);
			
			return SPAYLOG_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 获取订单流水列表数据
	 * @return
	 */
	public String getOrderCountList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			dto.put("state", "7");//只处理待结算订单 状态为已签收
			//处理结算日期
			int exp_date = Integer.valueOf(CTUtils.getParamValue("ALIPAY_DATE_PERIOD"));
			StringUtil.xprint("系统结算周期："+exp_date);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("order.getOrderCountList", dto);
			if(!StringUtil.checkListBlank(dList)){
				int period = 0;
				String createtime = null;
				for (Dto dto2 : dList) {
					dto2.put("state_name", CTUtils.getCodeName("ORDER_STATE", dto2.getAsString("state")));
					dto2.put("state_name1", CTUtils.getCodeName("ORDER_CSTATE", dto2.getAsString("state_id")));
					
					createtime = dto2.getAsString("createtime");
					period = CTUtils.getSubDay(createtime, CTUtils.getCurrentTime());
					if(period>=exp_date){
						dto2.put("pay_state","1");
					}else{
						dto2.put("pay_state","0");
					}
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getOrderCountListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			request.setAttribute("state_id", dto.getAsString("state_id"));
			request.setAttribute("pay_date", getParamValue("ALIPAY_DATE_PERIOD"));
			
			return ORDER_CLIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 获取客服订单流水列表数据
	 * @return
	 */
	public String getSOrderCountList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			dto.put("state", "7");//只处理待结算订单 状态为已签收
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("order.getSOrderCountList", dto);
			if(!StringUtil.checkListBlank(dList)){
				for (Dto dto2 : dList) {
					dto2.put("state_name", CTUtils.getCodeName("ORDER_STATE", dto2.getAsString("state")));
					dto2.put("cstate_name", CTUtils.getCodeName("ORDER_CSTATE", dto2.getAsString("state_id")));
				}
			}
			
			Integer totalCount = (Integer)CTReader.queryForObject("order.getSOrderCountListCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("start_date", dto.getAsString("start_date"));
			request.setAttribute("end_date", dto.getAsString("end_date"));
			request.setAttribute("state_id", dto.getAsString("state_id"));
			
			return SCOUNT_CLIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 订单详情
	 * @return
	 */
	public String showOrder() {
		List<Dto> dList = null;
		try {
			Dto orderInfo = null;
			double totalMoney=0.0;
			double totalPMoney=0.0;
			double totalSMoney=0.0;
			Dto dto =  getPraramsAsDto(request);
			dList = CTReader.queryForList("order.getOrderInfo", dto);
			if(!StringUtil.checkListBlank(dList)){
				orderInfo = dList.get(0);
				totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
				totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
				totalSMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("service_money"));
				orderInfo.put("totalMoney", totalMoney);
				orderInfo.put("totalPMoney", totalPMoney);
				orderInfo.put("totalSMoney", totalSMoney);//客服提成
				orderInfo.put("senderType", getCodeName("SENDER_TYPE", orderInfo.getAsString("sender_type")));
				//号码归属
//				String visit_ip = orderInfo.getAsString("visit_ip");
//				if(visit_ip.equals("0:0:0:0:0:0:0:1")){
//					orderInfo.put("visit_ip", "127.0.0.1");
//					orderInfo.put("iplocation", "本机IP地址");
//				}else{
//					orderInfo.put("iplocation", QueryIPAddress.queryIPAddress(visit_ip.trim()));
//				}
				String buyer_phone = orderInfo.getAsString("buyer_phone");
				if(CTUtils.isNotEmpty(buyer_phone)){
				   String mlocation = MobileLocationUtil.getMobileLocation(buyer_phone);
				   orderInfo.put("mlocation", mlocation);
				}
				
				//获取订单日志
				List<Dto> logList = CTReader.queryForList("order.getOrderLogList",dto);
				request.setAttribute("logList", logList);
				
				//获取客服订单日志
				List<Dto> serverList = CTReader.queryForList("order.getServerLogList",dto);
				request.setAttribute("slogList", serverList);
				
				request.setAttribute("senderList", getCodeList("SENDER_TYPE"));
				request.setAttribute("remarkList", getCodeList("REMARK_TEXT"));
				request.setAttribute("orderInfo", orderInfo);
				//获取订单流水信息
//			    if(orderInfo.getAsInteger("state")==8){//获取退款订单流水
//			    	Dto pDto = new BaseDto();
//			    	pDto.put("reback", "1");
//			    	pDto.put("order_id", orderInfo.getAsString("order_id"));
//			    	Dto bOrderInfo = (BaseDto)CTReader.queryForObject("order.getOrderCount",pDto);
//			    	request.setAttribute("bOrderInfo", bOrderInfo);
//			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SHOW_ORDER;
	}
	
	/**
	 * 锁定订单（同步方法）
	 */
	public synchronized void lockOrder() {
		boolean temp = false;
		String retMsg = "";
		try {
			Dto dto =  getPraramsAsDto(request);
			UserInfo userInfo = getUser(request);
			dto.put("user_id", userInfo.getUser_id());
			dto.put("check_time", CTUtils.getCurrentTime());
			dto.put("optype", "edit_order");
			temp = resourceInfoService.operOrderInfo(dto);
			if(temp){
				retMsg = "true";
			}else{
				retMsg = "false";
			}
			
		} catch (Exception e) {
			retMsg = "false";
			e.printStackTrace();
		}finally{
			this.renderText(retMsg);
		}
	}
	
	
	public String initBatchCheck() {
		Dto paramDto =  getPraramsAsDto(request);
		request.setAttribute("order_id", paramDto.getAsString("order_id"));
		request.setAttribute("senderList", getCodeList("SENDER_TYPE"));
		return BATCH_CHECK;
	}
	
	
	/**
	 * 批量审核订单
	 * @return
	 */
	public void batchCKOrder() {
		boolean temp = false;
		try {
			Dto intDto = new BaseDto();
			Dto paramDto =  getPraramsAsDto(request);
			String oids = paramDto.getAsString("order_id");
			String remark = paramDto.getAsString("remark");
			String check_state = paramDto.getAsString("check_state");
			if(CTUtils.isNotEmpty(oids)&&CTUtils.isNotEmpty(check_state)){//修改
				String o_id [] = oids.split(",");
				for (String order_id : o_id) {
					
					intDto.put("order_id", order_id);
					intDto.put("order_remark", CTUtils.isEmpty(remark)?"批量审核":remark);
					intDto.put("order_id", order_id);
					intDto.put("sender_type", paramDto.getAsString("sender_type"));
					intDto.put("update_time", CTUtils.getCurrentTime());
					
					if(check_state.equals("1")){//审核有效
						intDto.put("state", "2");//已审核
						if(CTUtils.isNotEmpty(remark)){
							intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
						}else{
							intDto.put("log_txt", "已审核、待发货 ");
						}
					}else if(check_state.equals("2")){//待跟进
						intDto.put("state", "9");//待跟进
						if(CTUtils.isNotEmpty(remark)){
						    intDto.put("log_txt", "待跟进 "+"("+remark+")");
						}else{
							intDto.put("log_txt", "待跟进 ");
						}
					}else{//无效取消
						intDto.put("state", "3");//已取消
						if(CTUtils.isNotEmpty(remark)){
						    intDto.put("log_txt", "已取消 "+"("+remark+")");
						}else{
							intDto.put("log_txt", "已取消 ");
						}
					}
					
					//开始处理订单信息
					intDto.put("optype", "edit_order");
					temp = resourceInfoService.operOrderInfo(intDto);
					if(temp){
						intDto.put("optype", "add_log");
						resourceInfoService.operOrderInfo(intDto);
					}
				}
			}
			
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}finally{
			if(temp){
			   RequestUtil.renderText("true");
		    }else{
			   RequestUtil.renderText("false");
		    }
		}
	}
	
	
	/**
	 * 订单撤销
	 */
	public void rollBackOrder() {
		boolean temp = false;
		try {
			Dto intDto = new BaseDto();
			Dto paramDto =  getPraramsAsDto(request);
			String oids = paramDto.getAsString("order_id");
			if(CTUtils.isNotEmpty(oids)){
				String o_id [] = oids.split(",");
				for (String order_id : o_id) {
					
					intDto.put("state", "1");//回退到已下单
					intDto.put("log_txt", "已撤销取消");
					intDto.put("order_remark", "撤销取消");
					intDto.put("order_id", order_id);
					intDto.put("update_time", CTUtils.getCurrentTime());
					//开始处理订单信息
					intDto.put("optype", "edit_order");
					temp = resourceInfoService.operOrderInfo(intDto);
					if(temp){
						intDto.put("optype", "add_log");
						resourceInfoService.operOrderInfo(intDto);
					}
				}
			}
			
		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		}finally{
			if(temp){
				   RequestUtil.renderText("true");
			    }else{
				   RequestUtil.renderText("false");
			    }
			}
		
		
		
	}
	
	/**
	 * 处理订单流程
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public String saveOrderProcess() {
		List<Dto> dList = null;
		double totalMoney=0.0;
		double totalPMoney=0.0;
		double totalSMoney=0.0;
		try {
			boolean temp = false;
			Dto orderInfo = null;
			Dto dto =  getPraramsAsDto(request);
			UserInfo userInfo = getUser(request);

			Dto mDto = new BaseDto();
			Dto intDto = new BaseDto();
			intDto.put("order_id", dto.getAsString("order_id"));
			//intDto.put("user_id", userInfo.getUser_id());
			int stateID = dto.getAsInteger("state_id");
			//处理流程
			String nowtime = CTUtils.getCurrentTime();
			String remark = dto.getAsString("remark");
			switch (stateID) {
			
			case 1://进入审核流程
				if(dto.getAsString("check_state").equals("1")){//审核有效
					intDto.put("state", "2");//已审核
					intDto.put("sender_type", dto.getAsString("sender_type"));//选择快递公司
					if(CTUtils.isNotEmpty(remark)){
						intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已审核、待发货 ");
					}
				}else if(dto.getAsString("check_state").equals("2")){//待跟进
					intDto.put("state", "9");//待跟进
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "待跟进 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "待跟进 ");
					}
				}else{//无效取消
					intDto.put("state", "3");//已取消
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已取消 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已取消 ");
					}
				}
				
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				
				break;
			case 2://进入发货流程
				if(CTUtils.isNotEmpty(dto.getAsString("courier_id"))){//快递单号
					intDto.put("state", "4");//已发货
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已发货、待签收 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已发货、待签收 ");
					}
				}
				intDto.put("sender_type", dto.getAsString("sender_type"));
				intDto.put("courier_id", dto.getAsString("courier_id"));
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				
				/*************************发送短信开始*************************/
        		String send_sms = CTUtils.getParamValue("SEND_DELIVERY_SMS");
        		String send_sms_temp = CTUtils.getParamValue("SEND_SMS_TEMPLATE");
        		if(CTUtils.isNotEmpty(send_sms) && send_sms.equals("1") && CTUtils.isNotEmpty(send_sms_temp)){
        			String buyer_name = dto.getAsString("buyer_name");
        			String buyer_phone = dto.getAsString("buyer_phone");
        			String orderid = dto.getAsString("order_id");
        			String order_money = dto.getAsString("order_money");
        			
        			if(send_sms_temp.contains("buyer_name")){
        				send_sms_temp = send_sms_temp.replaceAll("\\$buyer_name", buyer_name);
        			}
        			if(send_sms_temp.contains("order_id")){
        				send_sms_temp = send_sms_temp.replaceAll("\\$order_id", orderid);
        			}
        			if(send_sms_temp.contains("order_money")){
        				send_sms_temp = send_sms_temp.replaceAll("\\$order_money", ""+order_money);
        			}
        			
        			String smString = send_sms_temp;
        			StringUtil.xprint("发送发货提示短信内容为>>>"+smString);
        			if(SMS_Sender.sendSms(buyer_phone, smString)){
    					StringUtil.xprint("发货提示发送成功!");
    				}else{
    					StringUtil.xprint("发货提示短信发送失败!");
    				}
        		}
        		/*************************发短信件结束*************************/
				break;
			case 4://进入签收流程
				if(dto.getAsString("sign_state").equals("1")){//已签收
					//结算金额
					mDto.put("optype", "order_count");
					mDto.put("ocid", UniqueID.getUniqueID(8, 0));
					mDto.put("scid", UniqueID.getUniqueID(8, 0));
					mDto.put("order_id", dto.getAsString("order_id"));
					mDto.put("order_money", dto.getAsString("omoney"));
					mDto.put("order_pmoney", dto.getAsString("pmoney"));
					mDto.put("server_pmoney", dto.getAsString("smoney"));
					mDto.put("state_id", "1");//待结算
					mDto.put("createtime", nowtime);
					mDto.put("remark", nowtime+"结算分成返款金额【"+dto.getAsString("pmoney")+"】");
					//结算预计支付日期
					String exp_day = getParamValue("ALIPAY_DATE_PERIOD");
					String exp_date = CTUtils.getDateByUDay(Integer.valueOf(exp_day));
					String subTime = nowtime.split(" ")[1];
					mDto.put("exp_pay_date", exp_date+" "+subTime);
					StringUtil.xprint("预期付款日期==>"+exp_date+" "+subTime);
					resourceInfoService.operOrderInfo(mDto);
					
					//增加客服流水
					if(CTUtils.isNotEmpty(dto.getAsString("smoney"))){
						mDto.put("optype", "sorder_count");
						mDto.put("remark", nowtime+"结算客服分成返款金额【"+dto.getAsString("smoney")+"】");
						resourceInfoService.operOrderInfo(mDto);
					}
					
					intDto.put("state", "7");//待结算
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已签收、待结算 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已签收、待结算 ");
					}
					intDto.put("order_remark", remark);
					intDto.put("createtime", nowtime);
					intDto.put("optype", "edit_order");
					
					
					//intDto.put("state", "5");//已签收
					//intDto.put("log_txt", "已签收");
				}else{
					intDto.put("state", "6");//已拒收
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已拒收 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已拒收 ");
					}
				}
			
				intDto.put("courier_id", dto.getAsString("courier_id"));
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				
				break;
			case 5://进入结算流程
				//结算金额
				mDto.put("optype", "order_count");
				mDto.put("ocid", UniqueID.getUniqueID(8, 0));
				mDto.put("scid", UniqueID.getUniqueID(8, 0));
				mDto.put("order_id", dto.getAsString("order_id"));
				mDto.put("order_money", dto.getAsString("omoney"));
				mDto.put("order_pmoney", dto.getAsString("pmoney"));
				mDto.put("server_pmoney", dto.getAsString("smoney"));
				mDto.put("state_id", "1");//待结算
				mDto.put("createtime", nowtime);
				mDto.put("remark", nowtime+"结算分成返款金额【"+dto.getAsString("pmoney")+"】");
				resourceInfoService.operOrderInfo(mDto);
				
				//增加客服流水
				if(CTUtils.isNotEmpty(dto.getAsString("smoney"))){
					mDto.put("optype", "sorder_count");
					mDto.put("remark", nowtime+"结算客服分成返款金额【"+dto.getAsString("smoney")+"】");
					resourceInfoService.operOrderInfo(mDto);
				}
				
				intDto.put("state", "7");//已结算
				if(CTUtils.isNotEmpty(remark)){
				    intDto.put("log_txt", "已结算、待支付 "+"("+remark+")");
				}else{
					intDto.put("log_txt", "已结算、待支付 ");
				}
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				break;
			case 7://进入退款流程
				//结算金额
				/*mDto.put("optype", "order_count");
				mDto.put("ocid", "LSH"+CTUtils.getCurDateNoSplitTime()+UniqueID.getUniqueID(8, 0));
				mDto.put("order_id", dto.getAsString("order_id"));
				mDto.put("back_money", "-"+dto.getAsString("back_money"));
				mDto.put("order_money", "-"+dto.getAsString("back_pmoney"));
				mDto.put("state_id", "1");//退款
				mDto.put("reback", "1");//退款标志
				mDto.put("createtime", nowtime);
				mDto.put("remark", nowtime+"扣除订单金额【"+dto.getAsString("back_money")+"】佣金【"+dto.getAsString("back_pmoney")+"】");
				resourceInfoService.operOrderInfo(mDto);*/
				
				intDto.put("state", "10");//已退货
				intDto.put("log_txt", "进入退货流程");
				intDto.put("order_remark", "进入退货流程");
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				break;
			case 9://进入跟进审核流程
				if(dto.getAsString("check_state").equals("1")){//审核有效
					intDto.put("state", "2");//已审核
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已审核、待发货 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已审核、待发货 ");
					}
					intDto.put("sender_type", dto.getAsString("sender_type"));//选择快递公司
				}else{//无效取消
					intDto.put("state", "3");//已取消
					if(CTUtils.isNotEmpty(remark)){
					    intDto.put("log_txt", "已取消 "+"("+remark+")");
					}else{
						intDto.put("log_txt", "已取消 ");
					}
				}
				
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				
				break;
			case 10://进入跟进审核流程
				intDto.put("state", "8");//已退货
				intDto.put("log_txt", "用户已退货");
				intDto.put("order_remark", remark);
				intDto.put("createtime", nowtime);
				intDto.put("optype", "edit_order");
				
				break;
				
			default:

				break;
			}
			
			//修改订单信息
			intDto.put("update_time", CTUtils.getCurrentTime());
			temp = resourceInfoService.operOrderInfo(intDto);
			if(temp){
				intDto.put("optype", "add_log");
				resourceInfoService.operOrderInfo(intDto);
			}
			//查询订单信息
			dto.put("state_id", "");
			dList = CTReader.queryForList("order.getOrderInfo", dto);
			if(!StringUtil.checkListBlank(dList)){
				orderInfo = dList.get(0);
				totalMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_price"));
				totalPMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("sku_pmoney"));
				totalSMoney = orderInfo.getAsInteger("buy_num") * Double.valueOf(orderInfo.getAsString("service_money"));
				orderInfo.put("totalMoney", totalMoney);
				orderInfo.put("totalPMoney", totalPMoney);
				orderInfo.put("totalSMoney", totalSMoney);//客服提成
				orderInfo.put("senderType", getCodeName("SENDER_TYPE", orderInfo.getAsString("sender_type")));
				//号码归属
//				String visit_ip = orderInfo.getAsString("visit_ip");
//				if(visit_ip.equals("0:0:0:0:0:0:0:1")){
//					orderInfo.put("visit_ip", "127.0.0.1");
//					orderInfo.put("iplocation", "本机IP地址");
//				}else{
//					orderInfo.put("iplocation", QueryIPAddress.queryIPAddress(visit_ip.trim()));
//				}
				String buyer_phone = orderInfo.getAsString("buyer_phone");
				if(CTUtils.isNotEmpty(buyer_phone)){
				   String mlocation = MobileLocationUtil.getMobileLocation(buyer_phone);
				   orderInfo.put("mlocation", mlocation);
				}
				
				//获取订单日志
				List<Dto> logList = CTReader.queryForList("order.getOrderLogList",dto);
				request.setAttribute("logList", logList);
				
				//获取客服订单日志
				List<Dto> serverList = CTReader.queryForList("order.getServerLogList",dto);
				request.setAttribute("slogList", serverList);
				
			    request.setAttribute("orderInfo", orderInfo);
			    request.setAttribute("senderList", getCodeList("SENDER_TYPE"));
			    request.setAttribute("remarkList", getCodeList("REMARK_TEXT"));
			    
			    //获取订单流水信息
//			    if(stateID==7){//获取退款订单流水
//			    	Dto pDto = new BaseDto();
//			    	pDto.put("reback", "1");
//			    	pDto.put("order_id", orderInfo.getAsString("order_id"));
//			    	Dto bOrderInfo = (BaseDto)CTReader.queryForObject("order.getOrderCount",pDto);
//			    	request.setAttribute("bOrderInfo", bOrderInfo);
//			    }
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			StringUtil.xprint("订单流程处理出错!");
		}
		
		return SHOW_ORDER;
		
	}
	
	//获取物流信息
	public String showSenderInfo() {
		Dto dto = getPraramsAsDto(request);
		String sender_type = dto.getAsString("sender_type");
		String courier_id = dto.getAsString("courier_id");
		request.setAttribute("sender_type", sender_type);
		request.setAttribute("courier_id", courier_id);
		
		return SHOW_SENDER;
	}
	
	
	/**
	 * 修改订单信息
	 * @return
	 */
	public String goEditOrder() {
		try {
			//获取所有参数信息
			Dto orderInfo = null;
			List<Dto> orderList = null;
			List<Dto> skuList = null;
			Dto dto =  getPraramsAsDto(request);
			String order_id = dto.getAsString("order_id");
			if(CTUtils.isNotEmpty(order_id)){
				//获取订单信息
				orderInfo = new BaseDto();
				orderList = CTReader.queryForList("order.getOrderInfo", dto);
				if(!StringUtil.checkListBlank(orderList)){
					orderInfo = orderList.get(0);
				}
				dto.put("good_id", orderInfo.getAsString("good_id"));
				skuList = CTReader.queryForList("good.getGoodSkuInfo", dto);
				//获取订单套餐信息
				request.setAttribute("orderInfo", orderInfo);
				request.setAttribute("senderList", getCodeList("SENDER_TYPE"));
				request.setAttribute("skuList", skuList);
			}
			return ORDER_EDIT;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public void getSenderInfo() {
		Dto paramDto = getPraramsAsDto(request);
		String gs = paramDto.getAsString("company");
		String dh = paramDto.getAsString("number");
		if(CTUtils.isNotEmpty(gs) && CTUtils.isNotEmpty(dh)){
			//通过公司编号获取公司名称
			String gsspell = null;
			//String gs_name = CTUtils.getCodeName("SENDER_TYPE", gs);
			try {
				gsspell = ZHToEN.getPingyin(gs).toLowerCase();
		} catch (BadHanyuPinyinOutputFormatCombination e) {
			e.printStackTrace();
		}
		StringUtil.xprint("物流简称为："+gsspell);
		StringUtil.xprint("物流单号为："+dh);
		    String wlString = SenderQueryUtil.getWuliu(dh, gsspell);
		   StringUtil.xprint("物流信息为："+wlString);
		    if(CTUtils.isNotEmpty(wlString)){
		    	RequestUtil.renderText(wlString);
		    }else{
		    	RequestUtil.renderText("");
		    }
		}
	}
	
	/**
	 * 操作订单信息呢
	 */
	public void operOrderInfo() {
		boolean temp = false;
		String err_msg = "false";
		try {
			Dto dto = getPraramsAsDto(request);
			if (CTUtils.isNotEmpty(dto)) {
				temp = resourceInfoService.operOrderInfo(dto);
				if (temp) {
					err_msg = "true";
				} else {
					err_msg = "false";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			renderText(err_msg);
		}
	}

	public File getMyOrder() {
		return myOrder;
	}

	public void setMyOrder(File myOrder) {
		this.myOrder = myOrder;
	}

	public String getMyOrderFileName() {
		return myOrderFileName;
	}

	public void setMyOrderFileName(String myOrderFileName) {
		this.myOrderFileName = myOrderFileName;
	}

}
