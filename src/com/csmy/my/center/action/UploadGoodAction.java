package com.csmy.my.center.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.service.GoodInfoService;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.FileUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.UniqueID;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;

/**
 * 上传食物信息类
 * 
 * @author wgp
 * 
 */
@Controller
@Scope("prototype")
public class UploadGoodAction extends BaseActionSupport {

	private static final long serialVersionUID = 1L;

	private Logger log = Logger.getLogger(UploadGoodAction.class);

	private String good_id;// 商品编号
	private String type_id;// 类型编号
	private String good_name;// 商品名称
	private String customer_id;// 商户名称
	private String promote_desc;// 推广说明
	private String copy_id;// 微信文案ID(多选)
	private String scopy_id;// 站内文案ID
	private String zcopy_id;// 空间文案ID
	private String copy_title;// 文案标题
	private String copy_link;// 文案链接
	private String copy_desc;// 文案说明
	private String sku_str;// sku信息
	private String sku_title;// 套餐名称
	private String sku_cost;// 成本
	private String sku_price;// 价格
	private String sku_pmoney;// 分成
	private String service_money;// 客服分成
	private String order_no;// 排序
	private String tags;// 标签
	private String optype;// 操作类型
	private String enabled;// 是否有效
	private String forward_link;// 跳转地址

	// 上传附件使用
	private List<File> uploadGood;
	private List<String> uploadContentType;
	private List<String> uploadGoodFileName;
	private String savePath;

	//处理参数
	Dto paraDto = null;

	public UploadGoodAction() {
		paraDto = new BaseDto();
		StringUtil.xprint("构造参数收集器成功......");
	}

	//自动注入
	@Autowired
	private GoodInfoService goodInfoService;

	//请求对象
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession session = request.getSession();

	@SuppressWarnings("unchecked")
	public void saveGood() {
		boolean temp = false;
		try {

			Dto dto = paraDto;
			StringUtil.xprint("param==" + paraDto);
			//处理编号
			if(optype.equals("add_good")){
			   String good_id = UniqueID.getUniqueID(8, 0);
			   setGood_id(good_id);
			   dto.put("good_id", good_id);
            }
			
			StringUtil.xprint("开始上传文件==");
			FileUtils fileUtils = new FileUtils();
			String imageFolder = ServletActionContext.getServletContext().getRealPath("imageFolder");
			//String classPath = UploadGoodAction.class.getResource("/").getPath();
		    //String imageFolder = CTUtils.getWebRootPaht(classPath, 1);
			
			List<File> items = getUploadGood();
			 if(CTUtils.isNotEmpty(items)){
				 String fileName = null;
				 for (int i = 0; i < items.size(); i++) {
				    fileName = uploadGoodFileName.get(i);//真实文件名称
				    //生成一级文件夹
				    fileUtils.createFolder(imageFolder);
				    StringUtil.xprint("创建图片一级文件夹：" + imageFolder);
				    String temp_path = imageFolder + "/" + CTUtils.getCurDateNoSplit();
				    
					fileUtils.createFolder(temp_path);
					StringUtil.xprint("创建图片二级文件夹：" + temp_path);
		
					String file_prex = fileUtils.GetExt(fileName);
					String filename = getGood_id()+"_"+(i+1)+ file_prex;
					String file_path = StringUtil.getFilePath(temp_path, "imageFolder");
		
					temp_path = temp_path + "/" + filename;
					System.out.println("file_path=" + file_path);
					System.out.println("temp_path=" + temp_path);
					
					//输出文件
					FileOutputStream fos = new FileOutputStream(temp_path);
					FileInputStream fis = new FileInputStream(items.get(i));
					byte[] buffer = new byte[1024];
					int len = 0;
					while ((len = fis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
					
					//处理文件名称
					dto.put("good_pic"+(i+1), file_path + "/" + filename);
					 
				 }
			 }
			 //保存商品
			 temp = goodInfoService.operGoodInfo(dto);

		} catch (Exception e) {
			temp = false;
			e.printStackTrace();
		} finally {
			if(temp){
			   this.renderHtml("<script>alert('保存商品信息成功!');window.parent.frames['foodTable'].location.href='good!getGoodInfo.ct';</script>");
			}else{
			   this.renderHtml("<script>alert('保存商品信息失败!');window.parent.frames['foodTable'].location.href='good!getGoodInfo.ct';</script>");
			}
		}

	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public String getGood_id() {
		return good_id;
	}

	public void setGood_id(String good_id) {
		paraDto.put("good_id", good_id);
		this.good_id = good_id;
	}

	public String getGood_name() {
		return good_name;
	}

	public void setGood_name(String good_name) {
		paraDto.put("good_name", good_name);
		this.good_name = good_name;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		paraDto.put("customer_id", customer_id);
		this.customer_id = customer_id;
	}

	public String getPromote_desc() {
		return promote_desc;
	}

	public void setPromote_desc(String promote_desc) {
		paraDto.put("promote_desc", promote_desc);
		this.promote_desc = promote_desc;
	}

	public String getCopy_title() {
		return copy_title;
	}

	public void setCopy_title(String copy_title) {
		paraDto.put("copy_title", copy_title);
		this.copy_title = copy_title;
	}

	public String getCopy_link() {
		return copy_link;
	}

	public void setCopy_link(String copy_link) {
		paraDto.put("copy_link", copy_link);
		this.copy_link = copy_link;
	}

	public String getCopy_desc() {
		return copy_desc;
	}

	public void setCopy_desc(String copy_desc) {
		paraDto.put("copy_desc", copy_desc);
		this.copy_desc = copy_desc;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		paraDto.put("tags", tags);
		this.tags = tags;
	}

	public String getForward_link() {
		return forward_link;
	}

	public void setForward_link(String forward_link) {
		paraDto.put("forward_link", forward_link);
		this.forward_link = forward_link;
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public Dto getParaDto() {
		return paraDto;
	}

	public void setParaDto(Dto paraDto) {
		this.paraDto = paraDto;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public String getSku_title() {
		return sku_title;
	}

	public void setSku_title(String sku_title) {
		paraDto.put("sku_title", sku_title);
		this.sku_title = sku_title;
	}

	public String getSku_cost() {
		return sku_cost;
	}

	public void setSku_cost(String sku_cost) {
		paraDto.put("sku_cost", sku_cost);
		this.sku_cost = sku_cost;
	}

	public String getSku_price() {
		return sku_price;
	}

	public void setSku_price(String sku_price) {
		paraDto.put("sku_price", sku_price);
		this.sku_price = sku_price;
	}

	public String getSku_pmoney() {
		return sku_pmoney;
	}

	public void setSku_pmoney(String sku_pmoney) {
		paraDto.put("sku_pmoney", sku_pmoney);
		this.sku_pmoney = sku_pmoney;
	}

	public String getOptype() {
		return optype;
	}

	public void setOptype(String optype) {
		paraDto.put("optype", optype);
		this.optype = optype;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		paraDto.put("order_no", order_no);
		this.order_no = order_no;
	}

	public String getType_id() {
		return type_id;
	}

	public void setType_id(String type_id) {
		paraDto.put("type_id", type_id);
		this.type_id = type_id;
	}

	public GoodInfoService getGoodInfoService() {
		return goodInfoService;
	}

	public void setGoodInfoService(GoodInfoService goodInfoService) {
		this.goodInfoService = goodInfoService;
	}

	public String getCopy_id() {
		return copy_id;
	}

	public void setCopy_id(String copy_id) {
		paraDto.put("copy_id", copy_id);
		this.copy_id = copy_id;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
		paraDto.put("enabled", enabled);
	}
	
	public void setSku_str(String sku_str) {
		this.sku_str = sku_str;
		paraDto.put("sku_str", sku_str);
	}

	public String getSku_str() {
		return sku_str;
	}

	public String getService_money() {
		return service_money;
	}

	public void setService_money(String service_money) {
		this.service_money = service_money;
		paraDto.put("service_money", service_money);
	}

	public String getScopy_id() {
		return scopy_id;
	}

	public void setScopy_id(String scopy_id) {
		this.scopy_id = scopy_id;
		paraDto.put("scopy_id", scopy_id);
	}
	
	public String getZcopy_id() {
		return zcopy_id;
	}

	public void setZcopy_id(String zcopy_id) {
		this.zcopy_id = zcopy_id;
		paraDto.put("zcopy_id", zcopy_id);
	}

	public void setUploadContentType(List<String> uploadContentType) {
		this.uploadContentType = uploadContentType;
	}

	public List<File> getUploadGood() {
		return uploadGood;
	}

	public void setUploadGood(List<File> uploadGood) {
		this.uploadGood = uploadGood;
	}

	public List<String> getUploadGoodFileName() {
		return uploadGoodFileName;
	}

	public void setUploadGoodFileName(List<String> uploadGoodFileName) {
		this.uploadGoodFileName = uploadGoodFileName;
	}

	public List<String> getUploadContentType() {
		return uploadContentType;
	}

}
