package com.csmy.my.center.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.service.ResourceInfoService;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.PageModel;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import common.Logger;

/**
 * 资源管理action
 * @author wgp
 * @see 2013-01-21
 *
 */
@Controller
@Scope("prototype")
public class ResourceInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = -7494234699032976084L;
	// 日志记录
	Logger log = Logger.getLogger(ResourceInfoAction.class);
	@Autowired
	private ResourceInfoService resourceInfoService;
	public PageModel pager = new PageModel();
	
	public static final String LOG_LIST = "log_list";// 日志列表页面
	public static final String LOG_COUNT = "log_count";// 访问日志列表页面
	public static final String NEWS_LIST = "news_list";// 公告列表页面
	public static final String NEWS_ADD = "news_add";// 公告增加页面
	public static final String NEWS_EDIT = "news_edit";// 公告修改页面
	public static final String ERROR = "error";// 错误页面
	
	public static final String MENU_INIT = "menu_init";// 菜单初始化
	public static final String MENU_TREE = "menu_tree";// 菜单树页面
	public static final String MENU_LIST = "menu_list";// 菜单列表页面
	public static final String MENU_ADD = "menu_add";// 菜单列表页面
	public static final String MENU_EDIT = "menu_edit";// 菜单列表页面
	
	private String news_id;// 公告编号
	private String news_title;// 公告标题
	private String news_content;// 公告内容
	private String show_top;// 置顶
	private String order_no;// 排序
	
	
	
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();
	HttpSession session = request.getSession();
	
	

	//处理参数
	Dto paraDto = null;

	public ResourceInfoAction() {
		paraDto = new BaseDto();
		StringUtil.xprint("构造参数收集器成功......");
	}
	
	/**
	 * 增加公告信息
	 * 
	 * @return
	 */
	public String addSiteNews() {
		// 获取下拉数据
		Dto dto = getPraramsAsDto(request);
		request.setAttribute("optype", "add_news");
		return NEWS_ADD;
	}
	
	/**
	 * 修改公告信息
	 * 
	 * @return
	 */
	public String editSiteNews() {
		// 获取下拉数据
		Dto dto = getPraramsAsDto(request);
		//获取当前公告信息
		Dto newsDto = (BaseDto)CTReader.queryForObject("system.getSiteNewsInfo", dto);
		request.setAttribute("siteNewsInfo", newsDto);
		request.setAttribute("optype", "add_news");
		return NEWS_EDIT;
	}
	
	
	/**
	 * 获取公告列表数据
	 * @return
	 */
	public String getSiteNewsList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("system.getSiteNewsInfo", dto);
			
			Integer totalCount = (Integer)CTReader.queryForObject("system.getSiteNewsInfoCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			
			return NEWS_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	/**
	 * 获取日志列表数据
	 * @return
	 */
	public String getSysLogList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("system.getLogInfo", dto);
			
			Integer totalCount = (Integer)CTReader.queryForObject("system.getLogInfoCount", dto);
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			
			return LOG_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 获取访问日志数据
	 * @return
	 */
	public String getVisitCount() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			Dto countData = new BaseDto();
			
			int totalIP = (Integer)CTReader.queryForObject("order.getVisitIPCount");
			int totalOD = (Integer)CTReader.queryForObject("order.getVisitOrderCount");
			countData.put("totalIP", totalIP);
			countData.put("totalOD", totalOD);
			
			String today = CTUtils.getCurDate();
			dto.put("start_date", today);
			dto.put("end_date", today);
			int todayDayIP = (Integer)CTReader.queryForObject("order.getVisitIPCount",dto);
			countData.put("todayDayIP", todayDayIP);
			int todayDayOD = (Integer)CTReader.queryForObject("order.getVisitOrderCount",dto);
			countData.put("todayDayOD", todayDayOD);
			
			String last7Day = CTUtils.getDateByUDay(-7);
			dto.put("start_date", last7Day);
			dto.put("end_date", today);
			int last7DayIP = (Integer)CTReader.queryForObject("order.getVisitIPCount",dto);
			countData.put("last7DayIP", last7DayIP);
			int last7DayOD = (Integer)CTReader.queryForObject("order.getVisitOrderCount",dto);
			countData.put("last7DayOD", last7DayOD);
			
			String last30Day = CTUtils.getDateByUDay(-30);
			dto.put("start_date", last30Day);
			dto.put("end_date", today);
			int last30DayIP = (Integer)CTReader.queryForObject("order.getVisitIPCount",dto);
			countData.put("last30DayIP", last30DayIP);
			int last30DayOD = (Integer)CTReader.queryForObject("order.getVisitOrderCount",dto);
			countData.put("last30DayOD", last30DayOD);
			
			request.setAttribute("countData", countData);
			
			return LOG_COUNT;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 菜单管理初始化
	 * @return
	 */
	public String initSystemMenu() {
		return MENU_INIT;
	}
	
	
	/**
	 * 菜单管理树
	 * @return
	 */
	public String getMenuTree() {
		try {

			Dto pDto = new BaseDto();
			UserInfo userInfo = getUser(request);

			String listerTreeData = "";
			String linkUrl = "resource!getMenuList.ct?flag=1";
			List<Dto> dList = dList = CTReader.queryForList("resource.getSystemMenu");
			if (CTUtils.isNotEmpty(dList)) {
				listerTreeData = getCommZTreeJsonData(dList, "module_code",
						"module_name", "parent_module", linkUrl, "menuTable", "菜单管理",false,
						request);
			}
			request.setAttribute("menuTreeData", listerTreeData);

			return MENU_TREE;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	
	
	/**
	 * 获取菜单列表数据
	 * @return
	 */
	public String getMenuList() {
		List<Dto> dList = null;
		try {
			//获取所有参数信息
			Dto dto =  getPraramsAsDto(request);
			//处理中文
			String page_query = dto.getAsString("pageQuery");
			String queryParam = dto.getAsString("searchText");
			dto.put("queryParam", queryParam);
			
			dto = getPageDto(dto);
			dList = CTReader.queryForPage("resource.getSystemMenu", dto);
			Integer totalCount = (Integer)CTReader.queryForObject("resource.getSystemMenuCount", dto);
			if(!StringUtil.checkListBlank(dList)){
				for (Dto dto2 : dList) {
					dto2.put("leaf", CTUtils.getCodeName("LEAFTYPE", dto2.getAsString("leaf")));
					dto2.put("expanded", CTUtils.getCodeName("EXPAND", dto2.getAsString("expanded")));
				}
			}
			pager = getPageModel(dList, dto.getAsInteger("start"), totalCount);
			//返回参数到页面
			request.setAttribute("pm", pager); 
			request.setAttribute("alisaname", queryParam);
			request.setAttribute("module_code", dto.getAsString("module_code"));
			
			return MENU_LIST;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 增加菜单信息
	 * @return
	 */
	public String addMenuInfo() {
		try {
			Dto dto = getPraramsAsDto(request);
			//获取系统菜单列表
			List<Dto> mList = CTReader.queryForList("resource.getMenuTree");
			if(CTUtils.isNotEmpty(mList)){
				request.setAttribute("menuJsonData",getcCommJsonData(mList, "module_code", "module_name", "parent_module","true"));
			}
			//获取节点类型下拉
			request.setAttribute("leafJsonData",getCodeJsonData("LEAFTYPE"));
			request.setAttribute("epxandJsonData",getCodeJsonData("EXPAND"));
			
			return MENU_ADD;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 修改菜单信息
	 * @return
	 */
	public String editMenuInfo() {
		try {
			Dto menuInfo = null;
			Dto dto = getPraramsAsDto(request);
			String menuid = dto.getAsString("menuid");
			if(CTUtils.isNotEmpty(menuid)){
				menuInfo = (BaseDto)CTReader.queryForObject("resource.getMenuById",dto);
				if(CTUtils.isNotEmpty(menuInfo)){
					request.setAttribute("menuInfo", menuInfo);
				}
			}
			//获取系统菜单列表
			List<Dto> mList = CTReader.queryForList("resource.getMenuTree");
			if(CTUtils.isNotEmpty(mList)){
				request.setAttribute("menuJsonData",getcCommJsonData(mList, "module_code", "module_name", "parent_module","true"));
			}
			
			//获取节点类型下拉
			request.setAttribute("leafJsonData",getCodeJsonData("LEAFTYPE"));
			request.setAttribute("epxandJsonData",getCodeJsonData("EXPAND"));
			return MENU_EDIT;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	
	
	/**
	 * 公告信息
	 */
	public void operNewsInfo() {
		boolean temp = false;
		String err_msg = "false";
		try {
			Dto dto = getPraramsAsDto(request);
			if (CTUtils.isNotEmpty(dto)) {
				temp = resourceInfoService.operContentInfo(dto);
				if (temp) {
					err_msg = "true";
				} else {
					err_msg = "false";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			renderText(err_msg);
		}
	}
	
	
	/**
	 * 操作菜单信息呢
	 */
	public void operMenuInfo() {
		Dto rDto = null;
		String err_msg = "";
		try {
			Dto dto = getPraramsAsDto(request);
			if(CTUtils.isNotEmpty(dto)){
				rDto = resourceInfoService.operMenuInfo(dto);
				if(CTUtils.isNotEmpty(rDto)){
					err_msg=rDto.getAsString("SUC")+"#"+rDto.getAsString("MSG");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			//处理权限更新操作
			renderText(err_msg);
		}
	}
	
	/**
	 * 保存网站公告
	 */
	public void saveSiteNews() {
		boolean temp = false;
		try {
			Dto dto = paraDto;
			StringUtil.xprint("param==" + paraDto);
			String news_id = getNews_id();
			if(CTUtils.isNotEmpty(news_id)){
				dto.put("optype", "edit_news");
			}else{
				dto.put("optype", "add_news");
			}
			
			temp = resourceInfoService.operContentInfo(paraDto);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.renderHtml("<script>window.location.href='resource!getSiteNewsList.ct';</script>");
		}

	}
	

	public PageModel getPager() {
		return pager;
	}

	public void setPager(PageModel pager) {
		this.pager = pager;
	}

	public String getNews_id() {
		return news_id;
	}

	public void setNews_id(String news_id) {
		this.news_id = news_id;
		paraDto.put("news_id", news_id);
	}

	public String getNews_title() {
		return news_title;
	}

	public void setNews_title(String news_title) {
		this.news_title = news_title;
		paraDto.put("news_title", news_title);
	}

	public String getNews_content() {
		return news_content;
	}

	public void setNews_content(String news_content) {
		this.news_content = news_content;
		paraDto.put("news_content", news_content);
	}

	public String getShow_top() {
		return show_top;
	}

	public void setShow_top(String show_top) {
		this.show_top = show_top;
		paraDto.put("show_top", show_top);
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
		paraDto.put("order_no", order_no);
	}
	
}
