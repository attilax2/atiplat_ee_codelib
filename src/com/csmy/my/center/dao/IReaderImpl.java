package com.csmy.my.center.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.csmy.my.center.util.exception.G4Exception;
import com.ibatis.sqlmap.engine.impl.SqlMapClientImpl;
import com.ibatis.sqlmap.engine.mapping.sql.Sql;
import com.ibatis.sqlmap.engine.mapping.statement.MappedStatement;
import com.ibatis.sqlmap.engine.scope.SessionScope;
import com.ibatis.sqlmap.engine.scope.StatementScope;

/**
 * 数据读取器<br>
 * 基于iBatis实现,只有query权限,提供在Action中使用
 * 
 * @author wgp
 * @since 2013-01-16
 */
public class IReaderImpl extends SqlMapClientDaoSupport implements IReader {
	
	private static Log log = LogFactory.getLog(IReaderImpl.class);
	
	/**
	 * 查询一条记录
	 * 
	 * @param SQL语句ID号
	 * @param parameterObject
	 *            查询条件对象(map javaBean)
	 */
	public Object queryForObject(String statementName, Object parameterObject) {
		this.printSQl(statementName,parameterObject);
		return super.getSqlMapClientTemplate().queryForObject(statementName,
				parameterObject);
	}

	/**
	 * 查询一条记录
	 * 
	 * @param SQL语句ID号
	 */
	public Object queryForObject(String statementName) {
		this.printSQl(statementName,null);
		return super.getSqlMapClientTemplate().queryForObject(statementName,
				new BaseDto());
	}

	/**
	 * 查询记录集合
	 * 
	 * @param SQL语句ID号
	 * @param parameterObject
	 *            查询条件对象(map javaBean)
	 */
	public List queryForList(String statementName, Object parameterObject) {
		this.printSQl(statementName,parameterObject);
		return super.getSqlMapClientTemplate().queryForList(statementName,
				parameterObject);
	}

	/**
	 * 按分页查询
	 * 
	 * @param SQL语句ID号
	 * @param parameterObject
	 *            查询条件对象(map javaBean)
	 * @throws SQLException 
	 */
	public List queryForPage(String statementName, Dto qDto) throws SQLException {
		Connection connection = getConnection();
		String dbNameString = connection.getMetaData().getDatabaseProductName().toLowerCase();
		try {
			connection.close();
		} catch (Exception e) {
			log.error(CTConstants.Exception_Head + "未正常关闭数据库连接");
			e.printStackTrace();
		}
		String start = qDto.getAsString("start");
		String limit = qDto.getAsString("limit");
		int startInt = 0;
		if (CTUtils.isNotEmpty(start)) {
			startInt = Integer.parseInt(start);
			if (dbNameString.indexOf("ora") > -1) {
				qDto.put("start", startInt + 1);
			} else if (dbNameString.indexOf("mysql") > -1) {
				qDto.put("start", startInt);
			} else {
				qDto.put("start", startInt);
			}
		}
		if (CTUtils.isNotEmpty(limit)) {
			int limitInt = Integer.parseInt(limit);
			if (dbNameString.indexOf("ora") > -1) {
				qDto.put("end", limitInt + startInt);
			} else if (dbNameString.indexOf("mysql") > -1) {
				qDto.put("end", limitInt);
			} else {
				qDto.put("end", limitInt);
			}
		}
		
		Integer intStart = qDto.getAsInteger("start");
		Integer end = qDto.getAsInteger("end");
		if (CTUtils.isEmpty(start) || CTUtils.isEmpty(end)) {
			try {
				throw new G4Exception(
						CTConstants.ERR_MSG_QUERYFORPAGE_STRING);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		this.printSQl(statementName,qDto);
		return getSqlMapClientTemplate().queryForList(statementName, qDto,
				intStart.intValue(), end.intValue());
	}

	/**
	 * 查询记录集合
	 * 
	 * @param SQL语句ID号
	 */
	public List queryForList(String statementName) {
		this.printSQl(statementName,null);
		return super.getSqlMapClientTemplate().queryForList(statementName,
				new BaseDto());
	}

	/**
	 * 获取Connection对象<br>
	 * 说明：虽然向Dao消费端暴露了获取Connection对象的方法但不建议直接获取Connection对象进行JDBC操作
	 * 
	 * @return 返回Connection对象
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return getSqlMapClientTemplate().getDataSource().getConnection();
	}

	/**
	 * 获取DataSource对象<br>
	 * 说明：虽然向Dao消费端暴露了获取DataSource对象的方法但不建议直接获取DataSource对象进行JDBC操作
	 * 
	 * @return 返回DataSource对象
	 */
	public DataSource getDataSourceFromSqlMap() throws SQLException {
		return getSqlMapClientTemplate().getDataSource();
	}
	
	/**
	 * 打印sql语句
	 * @param sqlName
	 * @param params
	 */
	private void printSQl(String sqlName,Object params){
		SqlMapClientTemplate sqlMapClientTemplate = getSqlMapClientTemplate();
		SqlMapClientImpl sqlMapClientImpl = (SqlMapClientImpl) sqlMapClientTemplate.getSqlMapClient();
		MappedStatement mappedStatement = sqlMapClientImpl.getMappedStatement(sqlName);

		Sql sql = mappedStatement.getSql();
		StatementScope ss = new StatementScope(new SessionScope());
		mappedStatement.initRequest(ss);
		
		String sqlString = sql.getSql(ss, params);
		StringUtil.xprint("执行【sqlname="+sqlName+"】，SQL语句为→"+sqlString);
		
		if (logger.isDebugEnabled()) logger.debug(params);
		if (logger.isDebugEnabled()) logger.debug(sqlString);
		logger.error("执行【sqlname="+sqlName+"】，SQL语句为→"+sqlString);		
	}
	
}
