package com.csmy.my.center.filter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ContextLoaderListener;

import com.csmy.my.center.dao.IReader;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.SpringBeanLoader;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.cache.InitSystemCache;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.opensymphony.xwork2.spring.SpringObjectFactory;

/**
 * 系统启动监听器
 * 
 * @author jackie
 * @since 2013-01-16
 */
@SuppressWarnings("unchecked")
public class SystemInitListener implements ServletContextListener {
	private static Log log = LogFactory.getLog(SystemInitListener.class);
	private boolean success = true;
	private IReader CTReader = null;
	public void contextDestroyed(ServletContextEvent sce) {

	}
	
	public static void main(String[] args) {
	//	StaticLoggerBinder
	 	SpringObjectFactory
	//	ContextLoaderListener
		System.out.println("--start2");
		IReader	CTReader = (IReader) SpringBeanLoader.getSpringBean("CTReader");
		System.out.println(CTReader);
	}

	public void contextInitialized(ServletContextEvent sce) {
		CTReader = (IReader) SpringBeanLoader.getSpringBean("CTReader");
		systemStartup(sce.getServletContext());
	}

	/**
	 * 应用平台启动
	 */
	private void systemStartup(ServletContext servletContext) {
		long start = System.currentTimeMillis();
		log.error("********************************************");
		log.error("........后台管理平台开始启动...........");
		log.error("********************************************");
		
		if (success) {
			log.error("开始加载数据库中所有公用数据到缓存中...");
			Dto reDto = InitSystemCache.initCache();
			if(CTUtils.isNotEmpty(reDto)){
				//设置字典参数
				servletContext.setAttribute(CTConstants.CT_CODE_LIST, reDto.get(CTConstants.CT_CODE_LIST));
				//设置全局参数
				servletContext.setAttribute(CTConstants.CT_PARAM_LIST, reDto.get(CTConstants.CT_PARAM_LIST));
			}
			
			log.error("加载数据库中所有公用数据到缓存中结束...");
	
			long timeSec = (System.currentTimeMillis() - start) / 1000;
			log.error("************************************************************************");
			if (success) {
				log.error("后台管理平台启动成功[" + CTUtils.getCurrentTime() + "]");
				log.error("启动总耗时: " + timeSec / 60 + "分 " + timeSec % 60 + "秒");
			} else {
				log.error("后台管理平台启动失败[" + CTUtils.getCurrentTime() + "]");
				log.error("启动总耗时: " + timeSec / 60 + "分" + timeSec % 60 + "秒");
			}
			log.error("************************************************************************");
	   }
	}
}
