package com.csmy.my.center.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.attilax.sso.LoginX;
import com.csmy.my.center.module.UserInfo;
import com.csmy.my.center.service.UserInfoService;
import com.csmy.my.center.util.CTConstants;
import com.csmy.my.center.util.dataconvert.Dto;
import com.csmy.my.center.util.dataconvert.impl.BaseDto;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
@Controller
@SuppressWarnings("unchecked")
public class AuthorizationInterceptor extends AbstractInterceptor {

	@Autowired
	private UserInfoService userInfoService;
	
	private static final long serialVersionUID = -5269885174703802493L;
	private Logger logger = Logger.getLogger(AuthorizationInterceptor.class);
	// 错误提示信息
	String errorMsg = "";
	public String intercept(ActionInvocation action) throws Exception {
		// 获得用户请求
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		// 获得session
		HttpSession session = request.getSession();
		// 获取地址
		String path = request.getContextPath();
		//是否Ajax请求
		String isAjax = request.getHeader("x-requested-with");
		
		String basePath = request.getScheme() + "://" + request.getServerName()
				+ ":" + request.getServerPort() + path;
		String nowURl = request.getServletPath();
		String temp_path = nowURl.substring(nowURl.lastIndexOf("/") + 1, nowURl.length());
		// 获得用户信息
		UserInfo userInfo = getCurUserInfo(request,this.userInfoService);
		
		logger.error("temp路径:" + temp_path);
		logger.error("正在访问URL:" + basePath + nowURl);
		logger.error("应用路径:" + path);
		logger.error("基础路径:" + basePath);
		logger.error("当前路径:" + nowURl);
		logger.error("当前请求方式:" + isAjax);
		
		logger.error("当前请求功能:" + request.getParameter("menu_id"));
		logger.error("当前资源标识:" + request.getParameter("menu_value"));
		logger.error("当前按钮标识:" + request.getParameter("action_value"));
		
		if(nowURl.contains("importOrder")||nowURl.contains("uploadImg")){//如果是上传直接放行
			return action.invoke();
		}else{
			
			try {
				// 用户没有登录
				if (userInfo == null && nowURl.indexOf("login")<0) {
					logger.info("[用户没有登录,尝试访问数据！]");
					errorMsg = "对不起！您还没有登录系统或者长时间未操作,请重新登录！";
					request.setAttribute("errorMsg", errorMsg);
					request.getRequestDispatcher("/common/error/error.jsp").forward(request, response);
					return null;
				}
				
				
			} catch (Exception e) {
				logger.info("对不起！权限认证错误~~！请与管理员联系！" + e.getMessage());
				e.printStackTrace();
				request.setAttribute("errorMsg", errorMsg);
				request.getRequestDispatcher("/common/error/error.jsp").forward(request,response);
				return null;
			}
			
			return action.invoke();
		}
	}
	public UserInfo getCurUserInfo(HttpServletRequest request 
			 ,UserInfoService userInfoService) {
		HttpSession session = request.getSession();
		UserInfo userInfo = (UserInfo) session.getAttribute(CTConstants.SESSION_USER);
	//ati
		try {
			LoginX lx=new LoginX();
			lx.setModule("bkd");
			// 开始获取用户信息进行验证
			if(userInfo==null)
			{	Dto pDto = new BaseDto();
			pDto.put("user_id", lx.getuid(request));
			  userInfo = userInfoService.checkLogin(pDto);
			  
			}
		} catch (Throwable e) {
		 e.printStackTrace();
		}
	
		return userInfo;
	}
}