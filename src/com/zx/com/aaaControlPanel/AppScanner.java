package aaaControlPanel;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;

public class AppScanner {

	public Set<String> appWithPath = new HashSet<String>();
	public Set<String> appNoWithPath = new HashSet<String>();
	public Set<String> appNameAllWait2Scan = new HashSet<String>();
	public Set<String> pathSet = new HashSet<String>();
	public Set<String> scanFirstFullPathAppSet = new HashSet<String>();

	public static void main(String[] args) {

		AppScanner appScanner = new AppScanner();
		appScanner.scanFirstFullPathAppSet = new filex().toSetByLine(pathx
				.appPath() + "/ControlPanel/scanFirst.txt");
		appScanner.appNameAllWait2Scan = appScanner .get_appNameAll();
		appScanner.appWithPath = get_appWithPath();
		appScanner.	pathSet=new filex().toSetByLine(pathx.appPath() + "/ControlPanel/scanPath.txt");
		System.out.println("ff");
		
		Set<String>  st1=appScanner.scanFullPathApp();
		 appScanner.reduce_appNameAllWait2Scan(st1);
		Set<String>  st2=appScanner.scanAppWithPath();
		 appScanner.reduce_appNameAllWait2Scan(st2);
		 st1.addAll(st2);
		 Map m=convert2m(st1);
		 System.out.println(AtiJson.toJson(m));
	}

	private static Map convert2m(Set<String> st1) {
		Map<String,String> m=new HashMap<String, String>();
		for ( String  app: st1) {
			String mainName=filex.getFileName_mainname_noExtName_nopath(app);
			m.put(mainName.toLowerCase(),app);
		}
		return m;
	}

	private void reduce_appNameAllWait2Scan(Set<String> st1) {
	
		 for (String app : st1) {
			String name=filex.getFileName(app);
			appNameAllWait2Scan.remove(name);
		}
		
	}

	@SuppressWarnings("all")
	private Set<String> scanAppWithPath() {
		Set st_r = new HashSet();
			for (String p : pathSet) {
				for (String a : appWithPath) {
					String pa = p+"/"+a;
					if( new File(pa).exists())
						st_r.add(pa);
				}
				
				 
			}
		return st_r;
	}

	private Set<String> scanFullPathApp() {Set st_r = new HashSet();
		Set<String> st2 = new filex().toSetByLine(pathx.appPath() + "/ControlPanel/scanFirst.txt");
		for (String p : st2) {
			if( new File(p).exists())
				st_r.add(p);
				
		}
		return st_r;
	}

	private static Set<String> get_appWithPath() {
		Set<String> st = new filex().toSetByLine(pathx.appPath() + "/ControlPanel/app.txt");
		Set st_r = new HashSet();
		for (String app : st) {
		 if(app.contains("/") || app.contains("\\"))
			st_r.add(app);
		}

		return st_r;
		//return null;
	}

	private   Set<String> get_appNameAll() {
		Set<String> st = new filex().toSetByLine(pathx.appPath() + "/ControlPanel/app.txt");

		Set st2 = new filex().toSetByLine(pathx.appPath() + "/ControlPanel/scanFirst.txt");
		st.addAll(st2);

		Set st_r = new HashSet();
		for (String app : st) {
			String app_name = filex.getFileName(app);
			st_r.add(app_name);
		}
		appNoWithPath=st_r;
		return st_r;
	}

}
