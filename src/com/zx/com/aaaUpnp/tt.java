package aaaUpnp;

import java.util.Collection;
import java.util.List;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.controlpoint.SubscriptionCallback;
import org.fourthline.cling.model.gena.GENASubscription;
import org.fourthline.cling.model.gena.RemoteGENASubscription;
import org.fourthline.cling.model.message.header.STAllHeader;
import org.fourthline.cling.model.meta.Device;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.state.StateVariableValue;
import org.fourthline.cling.model.types.BooleanDatatype;
import org.fourthline.cling.model.types.Datatype;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;
import org.fourthline.cling.model.meta.RemoteDevice;

import com.attilax.json.AtiJson;
@SuppressWarnings("all")
public class tt {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
//		UpnpServiceImpl us=new UpnpServiceImpl();
//		  for  (Device device : us.getRegistry().getDevices()) { 
//	           System.out.println(AtiJson.toJson(device));
//	        } 
		  
		 // UPnP discovery is asynchronous, we need a callback
        RegistryListener listener = new RegistryListener() {

            public void remoteDeviceDiscoveryStarted(Registry registry,
                                                     RemoteDevice device) {
                System.out.println(
                        "Discovery started: " + device.getDisplayString()
                );
            }

            public void remoteDeviceDiscoveryFailed(Registry registry,
                                                    RemoteDevice device,
                                                    Exception ex) {
                System.out.println(
                        "Discovery failed: " + device.getDisplayString() + " => " + ex
                );
            }

            public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
                System.out.println(
                        "Remote device available: " + device.getDisplayString()
                );
                System.out.println(  AtiJson.toJson(device)  );
            }

            public void remoteDeviceUpdated(Registry registry, RemoteDevice device) {
                System.out.println(
                        "Remote device updated: " + device.getDisplayString()
                );
            }

            public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
                System.out.println(
                        "Remote device removed: " + device.getDisplayString()
                );
            }

            public void localDeviceAdded(Registry registry, LocalDevice device) {
                System.out.println(
                        "Local device added: " + device.getDisplayString()
                );
            }

            public void localDeviceRemoved(Registry registry, LocalDevice device) {
                System.out.println(
                        "Local device removed: " + device.getDisplayString()
                );
            }

            public void beforeShutdown(Registry registry) {
                System.out.println(
                        "Before shutdown, the registry has devices: "
                        + registry.getDevices().size()
                );
            }

            public void afterShutdown() {
                System.out.println("Shutdown of registry complete!");

            }

		 
        };

        // This will create necessary network resources for UPnP right away
        System.out.println("Starting Cling...");
     //   UpnpService upnpService = new UpnpServiceImpl(listener);
        
        
		  final UpnpService upnpService = new UpnpServiceImpl(listener);
		  upnpService.getControlPoint().search(
			        new STAllHeader()
			);
		// Let's wait 10 seconds for them to respond
	        System.out.println("Waiting 10 seconds before shutting down...");
	        Thread.sleep(120000);
		  
	        
	        //----------------------
//		  Registry registry = upnpService.getRegistry();		
//		Collection<Device> foundDevice = registry.getDevices();
//		  for (Device device : foundDevice) {
//			System.out.println(AtiJson.toJson(device));
//		}
		  
		  
		  //
		   // Release all resources and advertise BYEBYE to other UPnP devices
	        System.out.println("Stopping Cling...");
	   //     upnpService.shutdown();
		  
		  System.out.println("--f");
	}

}
