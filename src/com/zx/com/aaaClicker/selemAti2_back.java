package com.example.tests;

import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.regex.Pattern;
import static org.apache.commons.lang3.StringUtils.join;

public class selemAti2_back {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://guang.zhe800.com/guangbaobao";
		selenium = new WebDriverBackedSelenium(driver, baseUrl);
	}

	@Test
	public void testSelemAti2_back() throws Exception {
		selenium.open("/guangbaobao");
		selenium.click("css=img[alt=\"百搭欧简约时尚单肩大包手提包包\"]");
		selenium.click("link=百搭欧简约时尚单肩大包手提包包");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
