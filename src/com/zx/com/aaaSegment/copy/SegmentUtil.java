package aaaSegment.copy;

import java.util.ArrayList;
import java.util.List;

import com.attilax.io.filex;
import com.attilax.json.JSONArray;

public class SegmentUtil {

	public static void main(String[] args) {
		
		String s5="录 注册";
		String[] a5=s5.split(" ");
		String s = filex.read("c:\\txt");
		String[] a7=s.split("\r\n");
		// "倒排索引源于实际应用中需要根据属性的值来查找记录";
		List<String> li = split(s);

		System.out.println(JSONArray.fromObject(li).toString(2));
		System.out.println(li.size());
	}

	private static List<String> split(String s) {
		String[] sentenceLi = sentenceUtil.split(s);

		// 。这种索引表中的每一项都包括一个属性值和具有该属性值的各记录的地址。由于不是由记录来确定属性值，而是由属性值来确定记录的位置，因而称为倒排索引(inverted
		// index)。带有倒排索引的文件我们称为倒排索引文件，简称倒排文件(inverted file)。";
		int max = 5;
		List<String> li = new ArrayList();
		int count=0;

		for (String sen : sentenceLi) {
			char[] a = sen.toCharArray();
			int j = 0;
			for (char c : a) {
				for (int i = 1; i < max; i++) {

					int endIdex = j + i + 1;
					if (endIdex > sen.length())
						endIdex = sen.length();
					String word = sen.substring(j, endIdex);
					if (!li.contains(word) && word.length() > 1)
					{
						li.add(word);
					}
				}
				j++;
			}
		}
		return li;
	}

}
