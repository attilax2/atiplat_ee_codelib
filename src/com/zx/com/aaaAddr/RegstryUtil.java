package aaaAddr;

import java.util.ArrayList;
import java.util.List;

import com.registry.RegStringValue;
import com.registry.RegistryKey;
import com.registry.RegistryValue;
import com.registry.ValueType;

//使用 JRegistry-1.8.1  方法和以前有区别
public class RegstryUtil
{
    
    public static final int SUCCESS=0;//设置注册表成功
    public static final int CAN_NOT_WRITE=-1;//注册表项不可写
    public static final int NULL=87;//输入参数为空
    public static RegistryKey softWareKey=null;//本软件的 key
    public static RegStringValue regStringValue= null;//注册表字符串类型的值
    //获取"HKEY_LOCAL_MACHINE
    public static final RegistryKey LOCALMACHINE = RegistryKey.getRootKeyForIndex(RegistryKey.HKEY_LOCAL_MACHINE_INDEX);
    //获取"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"获取开机启动项key
    public static final RegistryKey RUN_KEY = new RegistryKey(LOCALMACHINE, "\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");

    
    public static void main(String[] args) {
     RegistryKey RUN_KEY2 = new RegistryKey(LOCALMACHINE, "\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion");
     System.out.println(RUN_KEY2.getSubKeys());

	}

    
    public  static List getAllRun()
    {
         List<RegistryValue> vregvalues =null;//注册表值
         ArrayList regStringvalues = new ArrayList ();
         
        //获取"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"
        if(RUN_KEY.hasValues())
        {    
         vregvalues = RUN_KEY.getValues();    
        
         for(RegistryValue regvalue :vregvalues)
         {
             regStringvalues.add((RegStringValue)regvalue);
         }
        }        
         return regStringvalues;        
    }
    
    public  static RegStringValue getRun(String name)
    {
        
        //获取"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"
        if(RUN_KEY.valueExists(name))
        {    
            regStringValue=(RegStringValue)RUN_KEY.getValue(name);
        }        
        return regStringValue;        
    }

    
    //值类型 valueType 开机启动项里  一般只用两种   1--> ValueType.REG_SZ 2-->ValueType.REG_EXPAND_SZ
    
    public static int setRun(String name,String value)//设置开机器启动项
    {

    
        
        if(RUN_KEY.canWrite())
        {
             if(RUN_KEY.valueExists(name))
             {
                 RUN_KEY.deleteValue(name);
                
             }
             regStringValue= (RegStringValue)RUN_KEY.newValue(name, ValueType.REG_SZ);
             regStringValue.setValue(value);
             return  SUCCESS;
        }
            return  CAN_NOT_WRITE;
    
        
    }
    
    public static int deleteRun(String name)
    {
        if ((name == null) || (name.length() == 0) || (name.equals("")))//保证传入参数不为空
        {
            return NULL;
        }else
        {
            if(RUN_KEY.canWrite())
            {
              RUN_KEY.deleteValue(name);
              return SUCCESS;
            }
             return CAN_NOT_WRITE;
        }
        
        
    }

    
    
    

        public static void writeXmlPath(String softwareName,String name,String value)//在注册表中写入xml配置文件的路径
        {
        
        
            softWareKey = new RegistryKey(LOCALMACHINE, "SOFTWARE\\"+softwareName);
            if(!softWareKey.exists())
            {
                softWareKey.create();
                
            }
            regStringValue= (RegStringValue)RUN_KEY.newValue(name, ValueType.REG_SZ);
               regStringValue.setValue(value);
          
            
        }
        
        
        
        public static String readXmlPath(String softwareName,String name,String value)//在注册表中写入xml配置文件的路径
        {
            
              softWareKey = new RegistryKey(LOCALMACHINE, "SOFTWARE\\"+softwareName);
              if(softWareKey.valueExists(name))
              {
                 regStringValue=  (RegStringValue)softWareKey.getValue(name);
                 return regStringValue.getValue();
              }
            
              return  "";
        }
        
        
        
        public static void deleteSoftwareValue(String softwareName,String name)
        {
             softWareKey = new RegistryKey(LOCALMACHINE, "SOFTWARE\\"+softwareName);
            if(softWareKey.exists())
            {
                softWareKey.deleteValue(name);
            }
            
        }
    }
