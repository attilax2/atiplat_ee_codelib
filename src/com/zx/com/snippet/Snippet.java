/**
 * 
 */
package snippet;

/**
 * @author attilax
 *2016年5月11日 下午11:17:19
 */
public class Snippet {
	1.1.项目java体系
	Mvc： 有struts也有自定义，目前准备慢慢抛弃struts，所以最新界面都使用h5+js ajax
	来完成，方便切换后台语言环境，以及提升通用性，方便测试，更加的view分离
	Ioc：旧的模块好像使用了spring，新模块都基本使用guice了。
	Orm框架：旧的模块使用了mybatis ，新的模块使用了自定义orm DataService（基本原理就是获取form提交的request字段，然后调用DatabaseMetaData api取得数据表的字段定义，然后对比排除掉多余字段，剩余字段组装成sql，然后调用apache dbutils执行sql即可），有对应的数据操作js sdk可以参考。。如果对着俩个都不感兴趣，可以使用mybatis   ，或者apache dbutils  也是可以的。。Hibernate就不推荐了，太麻烦。。 
}

