package aaaaScanCompSync;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.text.strUtil;
import com.attilax.linq.CustomFun;
import com.attilax.secury.propertyReader;
import com.attilax.util.PropX;

public class calcPicCustomFun extends CustomFun {

	String recordDir;
	@Override
	public Object ext(Object obj) {
		List<Object> li=(List<Object>) obj;
		String posterPicString=(String) li.get(0);
//		if(posterPicString!=null && posterPicString.trim().length()>3)
//			 return posterPicString;
		String playUrl=(String) li.get(1);
		String movMainDir=get_movMainDir(playUrl);
		recordDir=movMainDir;
		 String post=getPost(movMainDir);
		 
		return  post;
	}
	Object getPicPath() {
		String[] list = new File(recordDir).list(new FilenameFilter() {

			@Override
			public boolean accept(File paramFile, String filename) {
				// path =----filename.ext
				String extname = filex.getExtName(filename);
				if ("jpg".contains(extname.toLowerCase()) || "jpeg".contains(extname.toLowerCase()))
					return true;
				else
					return false;
			}
		});
		return recordDir+"/"+list[0];
	}
	private String getPost(String movMainDir) {
		try {
			String pic=(String) getPicPath();
			return pic.substring(3);
		} catch (Exception e) {
			return "";
		}
		
	}

	private String get_movMainDir(String playUrl) {
		// TODO Auto-generated method stub
		String[] a=playUrl.split("/");
		int index_MainDirEnd=a[0].length()+a[1].length()+2;
		return "z:/"+a[0]+"/"+a[1];
	}

	private String get_convert_himedia_cate(String mycate) {
		try {
			 String f=pathx.webAppPath()+"/cate_map2himedia.txt";
			  PropX px=new PropX(f, "gbk");
			//  propertyReader pr=new propertyReader();	  
				String property = px.getProperty(mycate);
				if(property==null) return "";
				return property;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	 
	}

}
