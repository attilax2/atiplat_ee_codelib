package aaaaScanCompSync;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.office.excelUtil;
import com.sun.media.format.WavAudioFormat;

import static com.attilax.linq.LinqBuilder.*;
@SuppressWarnings("all")
public class Syncer {

	
	/**
	 * c:/cmp_rzt.xls  d:  c:/0202_170228_486.xls
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String f=args[0];
		String tar=args[1];
		
		
		List<Map> li_cmp_rzt=new excelUtil().toListMap(f);
		List<Map> cache=new excelUtil().toListMap(args[2]);
		String fileName = pathx.webAppPath()+"/sync_"+filex.getUUidName()+".bat";
		System.out.println("--------out:"+fileName);
		filex fx=new filex(fileName,"gbk");
		for (Map map : li_cmp_rzt) {
			try {
				String movName=map.get("dirName").toString().split(" ")[0];
				Map row=	from(cache).where(like("dirName",movName)).exe().get(0);
				String waitCopyDir=getCopyDir(row.get("path"));
				String mainDirname= getMainDirName(row.get("path"));
				 
				//xcopy "%~dp0..\../PotPlayer" /e /i   d:\PotPlayer
					String line = "xcopy \""+waitCopyDir+"\"  /e /i  \""+tar+"/"+mainDirname+"\" \r\n";
					System.out.println(line);
					fx.append_HP_Safe(line);
					//FileUtils.copyDirectory(new File( waitCopyDir), new File(tar));

			} catch (Exception e) {
				e.printStackTrace();
			}
				 
			
		}
		fx.append_HP_Safe("pause \r\n");
		fx.closeSF();
		System.out.println("--------out:"+fileName);
		System.out.println(" --f");
		
		

	}

	

	private static String getMainDirName(Object playUrl) {
		String[] a=playUrl.toString().split("/");
		return a[2];
	}



	private static String get_movMainDir(String playUrl) {
		// TODO Auto-generated method stub
		String[] a=playUrl.split("/");
		int index_MainDirEnd=a[0].length()+a[1].length()+a[2].length()+3;
		return a[0]+"/"+a[1]+"/"+a[2];
	}
	private static String getCopyDir(Object playUrl) {
		// TODO Auto-generated method stub
		return get_movMainDir(playUrl.toString());
	}

	

	

}
