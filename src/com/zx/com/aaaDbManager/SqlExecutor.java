package aaaDbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import aaaCfg.IocX2;
import aaaCfg.IocX4casher;
import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.biz.seo.getConnEx;
import com.attilax.db.DBX;
import com.attilax.json.AtiJson;
import com.attilax.persistence.DBCfg;
import com.attilax.persistence.DbutilX;
import com.attilax.web.ReqX;
import com.google.inject.Inject;
 

/**
 * aaaDbManager.SqlExecutor
 * 
 * @author Administrator
 *
 */
public class SqlExecutor {
	
	public SqlExecutor(){
		
		try {
			this.dbx=com.attilax.ioc.IocXq214.getBean(DBX.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "serial" })
	public static void main(String[] args) throws SQLException {
//		
//		SqlExecutor c=IocX4casher.getBean(SqlExecutor.class);
//		final String sql=" select * from ecs_users where user_id>1767 limit 3";
//		System.out.println(c.exe(new HashMap (){{
//			this.put("sql", sql);
//			
//		}}));
		
		
		SqlExecutor c=new SqlExecutor();
		
		System.out.println(c.exe(new HashMap (){{
			Object sql = "select * from exp";
			this.put("sql", sql);
			this.put("db", "shopedb");
			
		}}));
		
		//SqlExecutor
		
//		Connection connection = getConn();
//		PreparedStatement prepareStatement = connection
//				.prepareStatement("select * from customers");
//		prepareStatement.execute();
//		ResultSet resultSet = prepareStatement.getResultSet();
//
//		// PreparedSentence ps = new PreparedSentence(session,
//		// " "); //where name like '%name%'
//		try {
//			List li = convertList(resultSet);
//			System.out.println(core.toJsonStrO88(li));
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("---f");

	}
@Inject
DBX dbx;
//	public static Connection getConn() {
//		
////		Session session;
////		if (JRootFrame.m_rootapp4ati == null) {
////			String[] args = null;
////			AppConfig config = new AppConfig(new String[0] );
////			/* 68 */config.load();
////			// JRootFrame rootframe = new JRootFrame();
////			// rootframe.initFrame(config);
////
////			JRootApp m_rootapp = new JRootApp();
////			/*     */
////			/* 55 */m_rootapp.initApp(config);
////			JRootFrame.m_rootapp4ati = m_rootapp;
////			// JRootApp jra = rootframe.m_rootapp;
////
////			session = m_rootapp.getSession();// SerializerWriteParams.INSTANCE
////
////		}
////		session = JRootFrame.m_rootapp4ati.getSession();
////
////		Connection connection = null;
////		try {
////			connection = session.getConnection();
////		} catch (SQLException e) {
////			// TODO Auto-generated catch block
////			throw new RuntimeException(e);
////		}
////		return connection;
//
//	}

	private static List convertList(ResultSet rs) throws SQLException {
		List list = new ArrayList();
		ResultSetMetaData md = rs.getMetaData();
		int columnCount = md.getColumnCount();
		while (rs.next()) {
			Map rowData = new HashMap();
			for (int i = 1; i <= columnCount; i++) {
				rowData.put(md.getColumnName(i), rs.getObject(i));
			}
			list.add(rowData);
		}
		return list;
	}

	private static Map<String, String> getResultMap(ResultSet rs)
			throws SQLException {
		Map<String, String> hm = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int count = rsmd.getColumnCount();
		for (int i = 1; i <= count; i++) {
			String key = rsmd.getColumnLabel(i);
			String value = rs.getString(i);
			hm.put(key, value);
		}
		return hm;
	}

	@SuppressWarnings("rawtypes")
	public String exe(Object reqx) {
		
		Map req = null;

		if (reqx instanceof HttpServletRequest)
			req = ReqX.toMap((HttpServletRequest) reqx);
		else
			req=(Map) reqx;
		
		Object db = req.get("db");
		if(db!=null)
		{
			IocX4nodb.db.set(db.toString());		
			this.dbx= IocX4nodb.getBean(DBX.class); 
		}

		try {
			Connection conn = null;
			try {
				conn = this.dbx.getConnection();
			} catch (getConnEx e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		List li=	new DBX().findBySql(conn, req.get("sql").toString());
			return  AtiJson.toJson(li);
		} catch (Exception e) {
			e.printStackTrace();
			return core.toJsonStrO88(e);
		}
	//	return null;
	

	}
	
	public String executeUpdate(Map req) {

		try {
			Connection conn = null;
			try {
				conn = this.dbx.getConnection();
			} catch (getConnEx e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		int li=	new DBX().executeUpdate(conn, req.get("sql").toString());
			return  String.valueOf(li);
		} catch (Exception e) {
			return core.toJsonStrO88(e);
		}
	//	return null;
	

	}
	
	public List exe_retLi(String sql) {

		Connection conn = null;
		try {
			conn = this.dbx.getConnection();
		} catch (getConnEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	List li=	new DBX().findBySql(conn, sql);
		return li;

	}

}
