package aaaDbManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;
import com.attilax.lang.MapX;
import com.attilax.web.ReqX;
import com.google.inject.Inject;

public class DbMetaApiService {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("xx2332");

	}
	@Inject
	DbMetaService dbms;
	
	public String getColEx(Object reqx) {

		List li =dbms.getColEx(reqx);

		return AtiJson.toJson(li);

	}

	
	public String getPrimaryKeys(Object reqx) {
		
		Map req = null;

		if (reqx instanceof HttpServletRequest)
			req = ReqX.toMap((HttpServletRequest) reqx);
		else
			req = (Map) reqx;

		List li =dbms.getPrimaryKeys(req);

		return AtiJson.toJson(li);
	}
	
	
	public String getColumns(Object reqx) {
		Map req = null;

		if (reqx instanceof HttpServletRequest)
			req = ReqX.toMap((HttpServletRequest) reqx);
		else
			req = (Map) reqx;
		
		List li =dbms.getColumns(req);

		return AtiJson.toJson(li);
	}

}
