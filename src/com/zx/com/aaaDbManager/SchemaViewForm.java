package aaaDbManager;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import aaaCfg.IocX4casher;

import com.attilax.biz.seo.getConnEx;
import com.attilax.db.DBX;
import com.attilax.json.JSONArray;

public class SchemaViewForm {

	public static void main(String[] args) {
		System.out.println(new SchemaViewForm(). getCatalogs());

	}

	private String getCatalogs() {
		DBX dbx = IocX4casher.getBean(DBX.class);
		DatabaseMetaData dbmd = null;
		try {
			dbmd = dbx.getConnection().getMetaData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (getConnEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// databaseMetaData.getColumns(localCatalog, localSchema,
		// localTableName, null);
		ResultSet rs = null;
		try {
			rs = dbmd.getCatalogs();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List li = null;
		try {
			li = dbx.convertList(rs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (JSONArray.fromObject(li).toString(2));
	}

}
