/***aaa Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package org.apache.struts2.dispatcher;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.apache.commons.lang3.BooleanUtils;

public class StrutsRequestWrapper extends HttpServletRequestWrapper {
	private static final String REQUEST_WRAPPER_GET_ATTRIBUTE = "__requestWrapper.getAttribute";
	private final boolean disableRequestAttributeValueStackLookup;

	public StrutsRequestWrapper(HttpServletRequest req) {
		this(req, false);
	}

	public StrutsRequestWrapper(HttpServletRequest req,
			boolean disableRequestAttributeValueStackLookup) {
		super(req);
		this.disableRequestAttributeValueStackLookup = disableRequestAttributeValueStackLookup;
	}

	public Object getAttribute(String key) {
		if (key == null) {
			throw new NullPointerException("You must specify a key value");
		}

		if ((this.disableRequestAttributeValueStackLookup)
				|| (key.startsWith("javax.servlet"))) {
			return super.getAttribute(key);
		}

		ActionContext ctx = ActionContext.getContext();
		Object attribute = super.getAttribute(key);

		if ((ctx != null) && (attribute == null)) {
			boolean alreadyIn = BooleanUtils.isTrue((Boolean) ctx
					.get("__requestWrapper.getAttribute"));

			if ((!(alreadyIn)) && (!(key.contains("#")))) {
				try {
					ctx.put("__requestWrapper.getAttribute", Boolean.TRUE);
					ValueStack stack = ctx.getValueStack();
					if (stack != null)
						attribute = stack.findValue(key);
				} finally {
					ctx.put("__requestWrapper.getAttribute", Boolean.FALSE);
				}
			}
		}
		return attribute;
	}
}