/* 
   Cifs Password Scanner
   Copyright (C) Patrik Karlsson 2004
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

package cqure;

import cqure.*;
import java.util.*;
import java.io.*;
import jcifs.smb.*;

public class Account {

    private String m_sUsername = "";
    private String m_sPassword = "";
    private String m_sServer = "";
    private String m_sDomain = "";
    private int m_nState;

    public Account( String sU, String sP, String sS, String sD, int nS ) {
	m_sUsername = sU;
	m_sPassword = sP;
	m_sServer = sS;
	m_sDomain = sD;
	m_nState = nS;
    }


}
