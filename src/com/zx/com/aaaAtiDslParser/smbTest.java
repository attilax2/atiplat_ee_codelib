package aaaAtiDslParser;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.attilax.json.AtiJson;
import com.attilax.util.IPUtil;
import com.google.common.collect.Lists;

import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;

public class smbTest {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// createDir("aaa2");

		File f = new File("\\\\192.168.2.106\\电影\\最新电影");
		System.out.println(f.exists());
		// f.createNewFile( );

		List<String> li = scanServers_smbPropt();
		System.out.println(AtiJson.toJson(li));
		System.out.println("fffff");
	}

	private static List<String> scanServers() {
		String subnet = IPUtil.subnet();
		List<String> li = Lists.newArrayList();
		for (int i = 1; i < 256; i++) {
			String ip = "\\\\" + subnet + "." + String.valueOf(i)+"\\电影";
			File f;
			 
			try {
				f = new File(ip);
				if (f.exists())
					li.add(ip);
				System.out.println(ip + "     " + f.exists());
			}catch( Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return li;
	}
	
	private static List<String> scanServers_smbPropt() {
		String subnet = IPUtil.subnet();
		List<String> li = Lists.newArrayList();
		for (int i = 1; i < 256; i++) {
			//admin:admin@
			String ip = "smb://" + subnet + "." + String.valueOf(i)+"/";
			SmbFile f;
			 
			try {
				f = new SmbFile(ip);
				if (f.exists())
					li.add(ip);
				System.out.println(ip + "     " + f.exists());
			}catch( Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return li;
	}

	private static String smburl = "smb://admin:admin@192.168.2.106/电影/最新电影";

	// zhoushun ----------------------------
	public static void createDir(String dir) throws Exception {
		SmbFile fp = new SmbFile(smburl + "//" + dir);
		System.out.println("fieldir+++++++++++++++++++++=" + smburl + "//"
				+ dir);
		// File fp = new File("Z://"+dir);
		// 目录已存在创建文件夹
		if (fp.exists() && fp.isDirectory()) {

		} else {
			// 目录不存在的情况下，会抛出异常
			fp.mkdir();
		}
	}

	public static void copyDir(String fileName, String target) throws Exception {
		InputStream in = null;
		OutputStream out = null;
		try {
			File fp = new File(fileName);

			SmbFile remoteFile = new SmbFile(smburl + "//" + fp.getName());
			System.out.println("remoteFile+++++++++++++++++++++=" + remoteFile);
			in = new BufferedInputStream(new FileInputStream(fp));
			out = new BufferedOutputStream(new SmbFileOutputStream(remoteFile));
			byte[] buffer = new byte[1024];
			int len;
			while ((len = in.read(buffer)) != -1) {
				out.write(buffer, 0, len);
			}
			// 刷新此缓冲的输出流
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void fileUpload(String fileName, String newFileName,
			String filePath) {
		InputStream in = null;
		OutputStream out = null;
		try {
			String newname = "";
			fileName = StringUtils.replace(fileName, "\\", "/");
			if (fileName.indexOf("/") > -1)
				newname = fileName.substring(fileName.lastIndexOf("/") + 1);
			else {
				newname = fileName;
			}
			SmbFileOutputStream file_out = null;
			if ((newFileName != null) && (!(newFileName.equals("")))) {
				file_out = new SmbFileOutputStream(smburl + "//" + filePath
						+ "//" + newFileName);
				System.out.println("filename+++++++++++++++++++++=" + smburl
						+ "//" + filePath + "//" + newFileName);
			} else {
				file_out = new SmbFileOutputStream(smburl + "//" + filePath
						+ "//" + newname);
				System.out.println("filename+++++++++++++++++++++=" + smburl
						+ "//" + filePath + "//" + newFileName);
			}

			File file_in = new File(fileName);

			in = new BufferedInputStream(new FileInputStream(file_in));
			out = new BufferedOutputStream(file_out);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = in.read(buffer)) != -1) {
				out.write(buffer, 0, len);
			}
			// 刷新此缓冲的输出流
			out.flush();
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
