package com.attilax.jql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import aaaCfg.IocX4nodb;
import aaaDbManager.DbMetaService;

import com.attilax.coll.ListX;
import com.attilax.collection.listUtil;
import com.attilax.lang.MapX;
import com.google.inject.Inject;

public class JqlService {
	
	public static void main(String[] args) {
		IocX4nodb.db.set("shopedb");
		JqlService js=IocX4nodb.getBean(JqlService.class);
		Map r=new HashMap();
		r.put("$table", "底单申请表");
		r.put("$db", "shopedb");
		r.put("$op", "merge");
		r.put("kwidi", "shopedb_kwidi");
		r.put("img_col", "shopedb_imgcol");
		System.out.println( js.saveOrMeger(r));
		//saveOrMeger
	}

	@Inject
	DbMetaService metaSvr;
	String prikey;
	public String saveOrMeger(Map req) {
		String table = (String) req.get("$table");
		if(table==null)
			 table = (String) req.get("table");
		String op = (String) req.get("$op");
		if(op==null)
			 op = (String) req.get("op");
		  prikey = metaSvr.getPrimaryKey(req);
		if (req.get(prikey) == null || req.get(prikey).toString().trim().length() == 0)
			return insert(req);
		else
			return update(req);

	}

	private void delete(Map req) {
		// TODO Auto-generated method stub

	}

	private String update(Map req) {
		return null;
		// TODO Auto-generated method stub

	}

	String cols4sql;
	String vals4sql;
	List<String> cols = new ArrayList<String>();
	List<String> valsArr = new ArrayList<String>();

	@SuppressWarnings("all")
	private String insert(Map req) {
		
		String table = (String) req.get("$table");
		if(table==null)
			 table = (String) req.get("table");
		Map req_flted = fltMap(req, table);
		calc_cols4sql_n_valsArr(req_flted);
		String sql = "insert into @tab@(@cols@)values(@vals@)";
		sql = sql.replace("@tab@", table);
		sql = sql.replace("@cols@", cols4sql);
		sql = sql.replace("@vals@", vals4sql);
		return sql;

	}

	Map<String, Object> kvMap;

	private void calc_cols4sql_n_valsArr(Map<String, Object> req_flted) {
		this.kvMap = req_flted;
		for (String key : req_flted.keySet()) {
			if(key.equals(this.prikey))
				continue;
			cols.add(key);
			valsArr.add(req_flted.get(key).toString());
			// System.out.println("key= "+ key + " and value= " + map.get(key));
		}
		cols4sql = listUtil.join(cols, ",");

		vals4sql = gene_vals4sql();

	}

	private String gene_vals4sql() {
		String r = "";
		for (String c : cols) {
			String colType = (String) colTypeMap.get(c);
			r = r + "," + getVal(c, kvMap, colType);
		}
	 
		return r.substring(1);
	 
	}

	private String getVal(String c, Map<String, Object> kvMap2, String colType) {
		String type = (String) new DataTypeUnicomMysql().typeMap.get(colType);
		if ( type!=null && type.equals("num"))
			return (String) kvMap2.get(c);
		else
			return "'" + (String) kvMap2.get(c) + "'";
	}

	Map colTypeMap = new HashMap();
//	@
	/**
	 * ccc
	 */
	List<Map>  getColumnsRzt;
	private Map fltMap(Map req, String table) {
		List<Map> li = metaSvr.getColumns(req);
		for (Map col_map : li) {
			colTypeMap.put(col_map.get("COLUMN_NAME") , col_map.get("TYPE_NAME"));
		//	colTypeMap.put("TYPE_NAME", );
		}
		Set<String> cols = metaSvr.getColumns_set(req);

		Map ret = MapX.reduce(req, cols);
		return ret;
	}

}
