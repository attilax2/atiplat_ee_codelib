package com.attilax.formatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.attilax.lang.MapX;
import com.attilax.util.mapUtil;

public class FormatterX {
	String lineSplitor = "\r\n";
	String colsSplitor = "\t";
	public   String tableFmt(List li) {
		String t="";
		for (Object object : li) {
			Map m=(Map) object;
			String line=getLine(m);
			t=t+lineSplitor+line;
			
			
		}
		return t;
	}

	private   String getLine(Map m) {
		
		List<String> li=new ArrayList<String>();
		Set<String> key = m.keySet();
		String line="";
		for (Iterator it = key.iterator(); it.hasNext();) {
			 String key_single = (String) it.next();
			String value = (String) m.get(key_single);
			  line=line+colsSplitor+value;
			//li.add(line);
		}
		return line;
	}

}
