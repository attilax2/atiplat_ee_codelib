package com.attilax.order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import aaaCfg.IocX4casher;
import bsh.StringUtil;

import com.attilax.acc.Acc;
import com.attilax.acc.AccService;
//import com.attilax.bet.AmountCalcService;
import com.attilax.data.DataStoreService;
import com.attilax.db.DBX;
import com.attilax.db.DbService;
import com.attilax.function.Function;
import com.attilax.io.filex;
import com.attilax.ioc.IocUtilV2;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.FunctinImp;
import com.attilax.lang.Global;
import com.attilax.lang.SecuryEx;
import com.attilax.log.LogSvr;
import com.attilax.math.ADecimal;
import com.attilax.orm.AOrm;
import com.attilax.orm.AtiOrm;
import com.attilax.sms.SmsService;
import com.attilax.sql.SqlSecuryCheckor;
//import com.attilax.sql.DbService;
import com.attilax.store.StoreService;
import com.attilax.user.User;
import com.attilax.user.UserService;
import com.attilax.util.DataMapper;
import com.attilax.web.ReqX;
import com.google.common.collect.Maps;
import com.google.inject.ImplementedBy;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.attilax.trigger.Trigger_after;

/**
 * v3 add refuse and accept com.attilax.order.OrderService4jobus.refuse
 * 
 * com.attilax.order.RechargeOrderService
 * @author attilax 2016年4月14日 下午12:36:44
 */
public class RechgOrdSv extends OrderService {

	public static void main(String[] args) {
		System.out.println("--f");
		System.setProperty("apptype", "jobus");
		System.setProperty("prj", "jobus");
		RechgOrdSv srv = IocUtilV2
				.getBean(RechgOrdSv.class);

		// System.out.println(srv.refuse("0301_152839_178"));
		// System.out.println(srv.accept("0301_152839_178"));

		Map m = Maps.newLinkedHashMap();
		m.put("$table", "orderv2");
		m.put("order_id", "198201");
		//
		String finishMsg = (String) srv.finish(m);
		System.out.println(finishMsg);

		System.out.println("--f");

	}

	@Inject
	DataStoreService storeSvr;

	@Inject
	UserService userSvr;
	@Inject
	AccService accSvr;
	@Inject
	AmountCalcService amoutCalcSvr;
	@Inject
	private LogSvr logSvr;

	// @Inject @Named("order_service_dataMaper")
	// @ImplementedBy(FunctinImp.class) should ostion in interface java hto..
	// public Function dataMaper;
	@Inject
	public Trigger_after trig_bef;
	@Inject
	public Trigger_after trig_aft;

	public int insert(HttpServletRequest req) {
		return insert(ReqX.toMap(req));
	}

	public int insert(Map order) {
		if (userSvr == null)
			throw new RuntimeException("#userSvr_is_null");
		if (accSvr == null)
			throw new RuntimeException("#accSvr_is_null");
		if (amoutCalcSvr == null)
			throw new RuntimeException("#amoutCalcSvr_is_null");

		if (userSvr.isNotLogin()) {
			throw new RuntimeException(" not login 没登录,请先登录..#not_login");
		}

		User u = userSvr.getLoginUser();
		Acc a = accSvr.getAcc(u.id);

		BigDecimal needMoney = amoutCalcSvr.calc(order);

		if (new ADecimal(needMoney).biggerEqualThan(a.amount))
			throw new RuntimeException(
					"  amount not enough 金额不足够 ..#amount_not_enough ");

		// /...insert
		order.put("$op", "insert");
		order.put("order_id", filex.getUUidName());
		order.put("order_money", needMoney);
		trig_bef.apply(order);

		storeSvr.insert(order);

		int rzt = accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());
		logSvr.log(order);
		return rzt;

	}

	@Inject
	AtiOrm ormx;

	/**
	 * for req
	attilax    2016年4月20日  下午4:11:03
	 * @return
	 */
	public Object finish() 
	{
		HttpServletRequest	 req=Global.req.get();
		return finish(ReqX.toMap(req));
	}
	public Object finish(Map m) {
		// if(StringUtils.isEmpty((String)m.get("$where")) )
		// throw new RuntimeException("no $where contion..");
		String order_id = (String) SqlSecuryCheckor.val(m.get("order_id"));
		if (finished(order_id))
			return "already_finish";
		
		//-------check money equ
		if(m.get("feeFromUrl")==null)
			throw new SecuryEx(" feeFromUrl is null");
		BigDecimal money_frmDb=(BigDecimal) ormx.querySingleRow.get("money");
		BigDecimal money_frmUrl=new BigDecimal( m.get("feeFromUrl").toString());
		if( money_frmDb.compareTo(money_frmUrl)!=0 )
			throw new FeeNotEquEx(" froom url total_fee.dbfee:"+m.get("feeFromUrl").toString()+"-"+  ormx.querySingleRow.get("money") );
		
		m.put("stat", 1);

		String where = " order_id='$order_id$'".replace("$order_id$",
				SqlSecuryCheckor.val(m.get("order_id")));
		m.put("$where", where);
		m.remove("order_id");
		// m.put("stat",1);
		ormx.m = m;
		ormx.setOp(ormx.update);
		ormx.setTable("orderv2");

		Object r = ormx.exe(m);
		if ((Integer) r == 1)
		{
			String uid=ormx.querySingleRow.get("uid").toString();
			BigDecimal money=(BigDecimal) ormx.querySingleRow.get("money");
			accSvr.addAmount(uid, money);
			return "ok";
			
		}
		throw new OrderFinishEx(" order finish ex");

	}

	/**
	attilax    2016年4月21日  下午9:29:02
	 * @param string
	 * @return
	 */
	private Exception FeeNotEquEx(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean finished(String order_id) {
		String s = "select * from orderv2 where order_id='" + order_id + "'";
		Map m = ormx.tabletype("sql").querySingleRow(s).querySingleRow;
		if (m == null)
			throw new CantFindRechargeOrder("order id:" + order_id);
		String stat = m.get("stat").toString();
		if (stat.equals("1"))
			return true;
		if (stat.equals("0"))
			return false;
		throw new RechargeOrderStatErr(" order.stat:" + order_id + "." + stat);
		// return m.get("stat").toString().equals("1");
		// ormx.tabletype("sql").exist(s).existRzt;
		// false;
	}

	public List<Map> query(Map order) {

		if (userSvr.isNotLogin()) {
			throw new RuntimeException(" not login 没登录,请先登录..#not_login");
		}
		User u = userSvr.getLoginUser();

		return null;

		// /...insert
		// return accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());

	}

	@Deprecated
	public String query2json(Map order) {

		return AtiJson.toJson(query(order));

		// /...insert
		// return accSvr.reduceAmount(u.id.toString(), needMoney.doubleValue());

	}

}
