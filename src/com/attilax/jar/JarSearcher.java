package com.attilax.jar;

import java.util.ArrayList;
import java.util.List;

import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.json.AtiJson;
import com.attilax.lang.Closure;

public class JarSearcher {

	public static void main(String[] args) {
		String strPath="E:\\jar2";
		final String search="EcoreResourceFactoryImpl";
		final List<String> li=new ArrayList<String>();
		
		
		new dirx().traveFile_noIncDir(strPath, new Closure () {

			@Override
			public Object execute(Object arg0) throws Exception {
				final String jar=(String) arg0;
				if(!jar.toLowerCase().endsWith("jar"))
					return jar;//continue
				JarFileView.travClassFile(jar, new Closure () {

					@Override
					public Object execute(Object arg0) throws Exception {
						// TODO Auto-generated method stub
						String f=  arg0.toString();
						f=f.replace("/", ".");
						String mainname=filex.getFileName_mainname_noExtName_nopath(f);
						if(mainname.toLowerCase().equals(search.toLowerCase()))
							li.add(jar+"//"+f);
						return null;
					}
				});
				
				return null;
			}

			 
		});
		System.out.println(AtiJson.toJson(li));
		System.out.println("--f");
		//return tab;
		
		// TODO Auto-generated method stub
	
	}

}
