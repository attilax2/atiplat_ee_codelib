/**
 * 
 */
package com.attilax.curr;

import java.util.concurrent.ExecutorService;

/**
 * @author attilax
 *2016年9月28日 下午11:55:41
 */
public class PoolUtil {

	/**
	attilax    2016年9月28日  下午11:55:52
	 * @return
	 */
	public static Runnable endTask(ExecutorService es,String poolname ) {

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				System.out.println("end task");
				es.shutdown();
			//	picPool.shutdown();
				System.out.println("--$pool$ shutdow after".replace("$pool$", poolname));
			}

		};
		return runnable;
	}

}
