/**
 * 
 */
package com.attilax.cfg;

import java.util.List;
import java.util.Map;

import com.attilax.io.filex;
import com.attilax.lang.text.strUtil;
import com.google.common.collect.Maps;

/**
 * @author attilax
 *2016年4月29日 下午4:50:12
 */
public class PropByColon {

	public Map m;

	/**
	 * @param cfgFile
	 */
	public PropByColon(String cfgFile) {
		List<String> li=filex.read2list(cfgFile);
		Map m=Maps.newLinkedHashMap();
		for (String line : li) {
			line=strUtil.toEnChar(line);
			String[] la=line.split(":");
			if(la.length>=2)
				m.put(la[0], la[1]);
		}
		this.m= m;
		
	}

}
