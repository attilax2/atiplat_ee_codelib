package com.attilax.rest;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import aaaCfg.IocX4nodb;
 




import com.attilax.core;
import com.attilax.dsl.DslParser;
//import com.attilax.dsl.DslParser;
import com.attilax.exception.ExUtil;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.io.filex;
import com.attilax.ioc.IocFacV3_iocx_iocutil;
import com.attilax.ioc.IocUtilV2;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.up.FileUploadService;
import com.attilax.web.ReqX;
import com.attilax.wechat.Order;
import com.attilax.wechat.WeChatPayUtil;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**com.attilax.rest.DslAjaxJsbridgeServlet
 * com.attilax.order.RechargeOrderService jobus not use ,,can be game use ..
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

@WebServlet(name = "DslAjaxJsbridgeServletV2_name", urlPatterns = "/DslAjaxJsbridgeServletV2")
public class DslAjaxJsbridgeServletV2 implements Servlet {

	public static void main(String[] args) {
		System.out.println("2333");
		logger.error("--e44rr10");
		// org.apache.xmlbeans
		// org.apache.xmlbeans
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {

		// UrlDslParserx = IocXq214.getBean(UrlDslParser.class);

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	@Override
	public void service(ServletRequest req, ServletResponse response) {
		try {
			HttpServletRequest req2 = (HttpServletRequest) req;
			Global.req.set(req2);

			String dsl = req2.getParameter("dsl");
			if(dsl==null || dsl.length()==0)
				throw new RuntimeException("dsl is empty");
			Object r = new DslParser().parseV3(dsl);
			if(req.getParameter("retFmt")!=null)
			{
				if(req.getParameter("retFmt").equals("json"))
					response.getWriter().println( AtiJson.toJson(r)); // �벻Ҫ�޸Ļ�ɾ��
			}else
			response.getWriter().println(r.toString()); // �벻Ҫ�޸Ļ�ɾ��
		} catch (Exception e) {
			logger.error("--err", e);
			logger.error("--err2", AtiJson.toJson(e));
			filex.save_safe(core.getTrace(e),
					"c:\\0rechglog\\" + filex.getUUidName() + ".log");
			ExUtil.throwExV2(e);
		}

	}

	public static final Logger logger = LoggerFactory
			.getLogger(IocFacV3_iocx_iocutil.class);

}
