package com.attilax.text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.attilax.json.JSONObject;
import com.attilax.json.JsonUtil4jackjson;
import com.attilax.util.numUtil;

public class CamelStrUtil {
	
	public static void main(String[] args) {
		String s="camelGbnfm";
     	s="CamelGbnfm";
		List<String> li=camelCase2List(s);
		System.out.println( JSONObject.fromObject(li).toString(2));
		
	}
	
	
	public static String camelCase2UslCase(String clazzName)  
	{  
	   Pattern p = Pattern.compile("([A-Z]+[a-z])");  
	   Matcher m = p.matcher(clazzName);  
	   StringBuilder sb = new StringBuilder();  
	   while(m.find())  
	   {  
	     sb.append(m.group().toLowerCase()).append("_");  
	   }  
	   sb.deleteCharAt(sb.length()-1);  
	   return sb.toString();  
	} 
	
	//cant use ret GB
//	public static List<String> camelCase2List(String clazzName)  
//	{  
//		List<String> li=new ArrayList<String>();
//	   Pattern p = Pattern.compile("([A-Z]+[a-z])");  
//	   Matcher m = p.matcher(clazzName);  
//	   StringBuilder sb = new StringBuilder();  
//	   while(m.find())  
//	   {  
//		   li.add(m.group());
//	   //  sb.append(m.group().toLowerCase()).append("_");  
//	   }  
//	 //  sb.deleteCharAt(sb.length()-1);  
//	   return li;
//	} 
	
	public static List<String> camelCase2List(String clazzName)  
	{  
		clazzName=clazzName.trim();
		List<String> li=new ArrayList<String>();
	   String s = clazzName;
       String[] ss = s.split("(?<!^)(?=[A-Z])");
       for(int i = 0 ;i < ss.length; i ++){
         //  System.out.println(ss[i]);
    	   String trim = ss[i].trim();
    	   if(numUtil.isNum(trim))
				continue;
		li.add(trim);
       }
       return li;
	}
}
