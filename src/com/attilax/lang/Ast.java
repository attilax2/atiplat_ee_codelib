package com.attilax.lang;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.attilax.fsm.JavaExpFsm;
import com.attilax.json.AtiJson;
import com.attilax.net.UrlEncode_del;
import com.google.common.collect.Lists;

public class Ast {
	public Object obj;
	public Object rzt;
	public static void main(String[] args) throws UnsupportedEncodingException {
		
		System.out.println(URLEncoder.encode("new(com.attilax.util.connReduceDync).set_resfile(userPhone4jobusImp/uc_js.txt).joinNoutV2()", "utf8"));
		System.out.println("..");
//		System.setProperty("prj","jobus");
//		String code="new(com.attilax.util.connReduceDync).set_resfile(userPhone4jobusImp/uc_js.txt).joinNoutV2() ".trim();
//		Ast astParser = new Ast();
//		List ast=astParser.getExprsLiAst(code);
//		System.out.println(AtiJson.toJson(   astParser.parse(ast)));
		
	}
//	public Object parse;
	public Object parse(List ast) {
	      for (Object object : ast) {
		//	String exp=(String) object;
			parseSingle(object);
		}
		return this.rzt;
	}

	private void parseSingle(Object exp) {
		Exprs e= (Exprs) (exp);
	 	e.obj=this.obj;
	
	this.rzt=	e.calc();
	this.obj=e.obj;
		
	}

	public List  getExprsLiAst(String code) {
		
		List    li=	new ArrayList();
		
		List tokens = new JavaExpFsm(code).getTokens();
		List<String>  tokens_slice_li=Lists.newLinkedList();
		for ( int i=0;i<tokens.size();i++) {
			tokens_slice_li.add((String) tokens.get(i));
			//last
			
			if(  tokens.get(i).toString().trim().equals(".") || i==tokens.size()-1)
			{
				Exprs e=getExprs(tokens_slice_li);			
				li.add(e);
				 tokens_slice_li=Lists.newLinkedList();
			}
		 
				
		}
		
	//	Collections.addAll(li, a);
		return li;
	}

	private Exprs getExprs(List<String> tokens_slice_li) {
		Exprs e=new Exprs();
		if(tokens_slice_li.size()==5)
		{
		e.method=(String) tokens_slice_li.get(0);
		e.params=e.parseParams(  tokens_slice_li.get(2).toString());
		}
		if(tokens_slice_li.size()==4)
		{
		e.method=(String) tokens_slice_li.get(0);
		e.params=Lists.newLinkedList();
		}
		//last
		if(tokens_slice_li.size()==3)
		{
		e.method=(String) tokens_slice_li.get(0);
		e.params=Lists.newLinkedList();
		}
		
		return e;
	}


}
