package com.attilax.lang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import aaaCfg.IocX;

import com.attilax.atian.PinyinX;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.text.strUtil;
import com.attilax.util.dirx;

/**
 * dir recycleProcessor ... com.attilax.lang.RecycleProcessor
 * 
 * @author Administrator
 *
 */
public class RecycleProcessor_Files extends RecycleProcessor {
	filex fx;
	private String encode;

	public RecycleProcessor_Files(String f) {
		try {
			fx = new filex(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public RecycleProcessor_Files(String f, String enc) {
		this.encode=enc;
		try {
			fx = new filex(f,enc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) throws IOException {
		String rzt_fileName = "c:\\scanMovRzt.csv";
		RecycleProcessor_Files recylX = new RecycleProcessor_Files(
				"c:\\scanMovRzt.csv","gbk");
		// 
		//
		// // RecycleProcessor_Lines rp = new RecycleProcessor_Lines();
		// String scanpath = "z:";
		// if (args.length > 0)
		// scanpath = args[0];
		// if (args.length >= 2)
		// rzt_fileName = args[1].trim();
		//
		// final String first="avi,vob,rmvb";
		//
		//
		// final filex fx = new filex(rzt_fileName, "gbk");
		// String title_line =
		// "material_description,material_type,file_path,thumb,material_keyword";
		// System.out.println("line:" + title_line);
		// fx.append_HP_Safe(title_line + "\r\n");
		// String scanpath3 = scanpath.replaceAll("\\\\", "\\\\\\\\") + "\\\\";
		// final String scanpath2 = scanpath3;
		recylX.exe(rzt_fileName, new Closure<String, Object>() {
			@Override
			public Object execute(String line) throws Exception {
				System.out.println(line);
				String[] a=line.trim().split(",");
				String thumb=a[3];
				if(thumb.trim().length()==0)
				{
					
				}
//				System.out.println("dir:" + dirx);
//				File dir = new File(dirx);
//				File[] files = dir.listFiles();
//				String line = "";
//				String basename = "";
//				String mkv = "";
//				String thumb = "";
//				String png = "";
//				Map<String, String> movs_map = new HashMap();
//				for (File fil : files) {
//
//					fil.getCanonicalPath();// //z:\悬疑类\消失的爱人 豆瓣8.8
//											// [大卫芬奇年度巨制，口碑爆棚！]\Gone.Girl.2014.1080p.BluRay.REMUX.AVC.DTS-HD
//											// MA.1-CHD.nfo
//					fil.getName();// aa.jpg
//					fil.getParent();// z:\悬疑类\消失的爱人 豆瓣8.8 [大卫芬奇年度巨制，口碑爆棚！]
//					fil.getPath();// z:\悬疑类\消失的爱人 豆瓣8.8
//									// [大卫芬奇年度巨制，口碑爆棚！]\Gone.Girl.2014.1080p.BluRay.REMUX.AVC.DTS-HD
//									// MA.1-CHD.nfo
//					String f = fil.getAbsolutePath();
//					if (f.contains("@Recycle"))
//						continue;
//
//					if (f.endsWith("mkv") || f.endsWith("mp4")) {
//						basename = filex.getFileName_noExtName(f);
//						basename = getTitleName(basename, f);
//
//						mkv = f;
//						mkv = mkv.replaceAll(scanpath2, "");
//						// mkv.substring(3);
//
//					}
//					setMovsMap(movs_map, first, f, scanpath2);
//
//					if (f.endsWith("jpg")) {
//						thumb = f.replaceAll(scanpath2, "");
//					}
//					if (f.endsWith("png")) {
//
//						png = f.replaceAll(scanpath2, "");
//					}
//
//				}
//				if (mkv.contains("喜爱夜蒲"))
//					System.out.println("--");
//				String cateID = "";
//
//				mkv = getMkv(movs_map, first, mkv);
//
//				try {
//					cateID = getCateid(mkv);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				if (thumb.length() == 0)
//					thumb = png;
//				String pinyin = PinyinX.getSimple(basename);
//				line = basename + "," + cateID + "," + mkv + "," + thumb + ","
//						+ pinyin;
//				System.out.println("line:" + line);
//				fx.append_HP_Safe(line + "\r\n");
				return null;
			}

		});
		System.out.println("---f");

	}

	protected static String getMkv(Map<String, String> movs_map, String first,
			String mkv) {
		if (mkv.length() > 0)
			return mkv;
		String[] a = first.split(",");
		for (String mov_ext : a) {
			if (movs_map.get(mov_ext) != null)
				return movs_map.get(mov_ext);
		}
		return mkv;
	}

	protected static void setMovsMap(Map<String, String> movs_map,
			String first, String f, String replacePathHead) {
		String[] a = first.split(",");
		for (String mov_ext : a) {
			if (f.endsWith(mov_ext)) {
				String basename = filex.getFileName_noExtName(f);
				basename = getTitleName(basename, f);

				String mkv = f;
				mkv = mkv.replaceAll(replacePathHead, "");
				// mkv.substring(3);
				movs_map.put(mov_ext, mkv);

			}
		}

	}

	private static String getTitleName(String basename, String f) {
		if (f.contains("喜爱夜蒲"))
			System.out.println("--");
		if (!strUtil.isContainCnchar(basename))// basename.getBytes().length
												// ==
												// basename.length())
												// // en
			basename = getParentName(f);
		// if en start
		if (basename.length() > 3) {
			String start2ch = basename.substring(0, 3);
			if (!strUtil.isContainCnchar(start2ch))
				basename = getParentName(f);
		}

		if (basename.contains("豆瓣")) {
			int idx = basename.indexOf("豆瓣");
			basename = basename.substring(0, idx).trim();
		}
		if (basename.contains(".")) {
			// if (!strUtil.isContainCnchar(basename))
			{
				int idx = basename.indexOf(".");
				basename = basename.substring(0, idx).trim();
			}
		}
		return basename;
	}

	protected static String getCateid(String mkv) {
		int a = mkv.indexOf("\\");
		String trim = "";

		trim = mkv.substring(0, a).trim();

		String f = pathx.classPath(IocX.class) + "\\cate.txt";
		Map m = YamlAtiX.getMapReverse(f);

		return (String) m.get(trim);
	}

	protected static String getParentName(String f) {
		if (f.contains("异能"))
			System.out.println("00");
		File f2 = new File(f);
		File prt = f2.getParentFile();

		String basename = "";
		try {
			basename = filex.getFileName_noExtName(prt.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!strUtil.isContainCnchar(basename)) // en
			return getParentName(prt.getAbsolutePath());
		// if en start
		String start2ch = basename.substring(0, 3);
		if (!strUtil.isContainCnchar(start2ch))
			return getParentName(prt.getAbsolutePath());
		return prt.getName();
	}

	public void exe(String strPath, Closure<String, Object> closure) {
		
		
		filex.traveLines(strPath, closure,encode);
	}

}
