/**
 * 
 */
package com.attilax.lang;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.attilax.exception.ExUtil;
import com.attilax.io.filex;
import com.attilax.io.pathx;

/** com.attilax.lang.YamlAtiX
 * @author ASIMO
 *
 */
public class YamlAtiX {

	/**
	@author attilax 老哇的爪子
	@since   p32 k_g_7
	 
	 */
	public static void main(String[] args) {
		// String s=pathx.classPath()+"/orgcate.txt";
		
		// List li=getLi(s);
		 String s="promoter:$pid$,fld2:v2";
		 s="pid:0,startDate:,endDate:";
		 Map  m=YamlAtiX.getMap_fromTxt(s);
		 System.out.println(m);
	}

		/**
		@author attilax 老哇的爪子
		@since   p32 k_k_0
		 
		 */
	public static List getLi(String f) {
		 String t=filex.read(f);
		 t=t.trim();
		 String[] a=t.split(",");
		 List li=new ArrayList();
		 for (String obj : a) {
			 String[] obj_a=obj.split(":");
			 Map m=new HashMap();
			 m.put("k", obj_a[0]);
			   m.put("v", obj_a[1]);
			   li.add(m);
		}
		
		return li;
	}

	
	/**
	 * from file
	attilax    2016年6月21日  下午11:11:48
	 * @param f
	 * @return
	 */
		public static Map getMap(String f) {
			if(!new File(f).exists())
				try {
					throw new FileNotFoundException(f);
				} catch (FileNotFoundException e) {
					ExUtil.throwExV3(e, f);
				}
			 String t=filex.read(f);
			 t=t.trim();
			 String[] a=t.split(",");
			 Map m=new HashMap();
			 for (String obj : a) {
				 String[] obj_a=obj.split(":");
			//	 Map m=new HashMap();
				 m.put(obj_a[0], obj_a[1]);
				//   li.add(m);
			}
			
			return m;
			
		}
		public static Map getMapReverse(String f) {
			 String t=filex.read(f);
			 t=t.trim();
			 String[] a=t.split(",");
			 Map m=new HashMap();
			 for (String obj : a) {
				 String[] obj_a=obj.split(":");
				 m.put(obj_a[1], obj_a[0]);
			}
			return m;
		}
		
		public static Map getMapReverse_fromTxt(String txt) {
			 
			String t=txt.trim();
			 String[] a=t.split(",");
			 Map m=new HashMap();
			 for (String obj : a) {
				 String[] obj_a=obj.split(":");
				 m.put(obj_a[1], obj_a[0]);
			}
			return m;
		}
		
		
		public static List getList(String f) {
			 String t=filex.read(f);
			 t=t.trim();
			 String[] a=t.split(",");
			List li=new ArrayList<Map>();
			 for (String obj : a) {
				 String[] obj_a=obj.split(":");
				 Map m=new HashMap();
				 m.put("id",obj_a[0]);
				 m.put("lab",obj_a[1]);
 
				 li.add(m);
			}
			return li;
		}

		/**
		attilax    2016年4月12日  下午5:04:23
		 * @param roottype
		 * @return
		 */
		public static Map getMap_fromTxt(String txt) {
			String t=txt.trim();
			 String[] a=t.split(",");
			 Map m=new HashMap();
			 for (String obj : a) {
				 String[] obj_a=obj.split(":");
				 if(obj_a.length>=2)
				 m.put(obj_a[0], obj_a[1]);
			}
			return m;
		}

}
