package com.attilax.lang.ui;

import java.awt.Point;

import javax.swing.JFrame;

public class drager {

	Point loc = null;

	Point tmp = null;

	boolean isDragged = false;

	// press move release

	private void setDragable(final JFrame frame) {

		
		frame.addMouseListener(new java.awt.event.MouseAdapter() {

			public void mousePressed(java.awt.event.MouseEvent e) {

				tmp = new Point(e.getX(), e.getY());

				isDragged = true;

				// jFrame.setCursor(new Cursor(Cursor.MOVE_CURSOR));

			}

			public void mouseReleased(java.awt.event.MouseEvent e) {

				isDragged = false;

				// jFrame.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

			}

		});

		frame.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {

			public void mouseDragged(java.awt.event.MouseEvent e) {

				if (isDragged) {

					// jFrame.().x

					int x = frame.getBounds().x;

					int y = frame.getBounds().y;

					loc = new Point(x + e.getX() - tmp.x,

					y + e.getY() - tmp.y);

					frame.setLocation(loc);

				}

			}

		});

	}

	public   void dragWin(JFrame frame) {
	setDragable(frame);

	}

}
