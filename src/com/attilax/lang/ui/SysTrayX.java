package com.attilax.lang.ui;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import com.attilax.seo.AppIco;

public class SysTrayX {
	private static SystemTray st;
	private static PopupMenu pm;
	TrayIcon ti;

	public SysTrayX(String path, String title, PopupMenu pm2) {
		createPopupMenu();
		if (SystemTray.isSupported()) {// 判断当前平台是否支持系统托盘
			st = SystemTray.getSystemTray();
			Image image = Toolkit.getDefaultToolkit().getImage(path);// 定义托盘图标的图片

			if (pm2 == null)
				pm2 = pm;
			ti = new TrayIcon(image, title, pm2);

			try {
				st.add(ti);
			} catch (AWTException ex) {
				ex.printStackTrace();
			}

			ti.displayMessage("title", "本程序已经放入系统托盘!",
					TrayIcon.MessageType.INFO);
		}
	}

	public static void main(String[] args) {

		// System.out.println(new File(
		// AppIco.class.getResource("/").getPath()).getParent()+"\\icon.ico");

		String path = new File(AppIco.class.getResource("/").getPath())
				.getParent() + "\\icon.png";
		SysTrayX tray = new SysTrayX(path, "tray title", pm);
		System.out.println("--f");
		//
	}

	public static void createPopupMenu() {

		pm = new PopupMenu();
		MenuItem openBrowser = new MenuItem("&OpenMainPanel 打开主面板");
		openBrowser.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// openBrowser("http://hi.baidu.com/riffling/blog");
			}
		});
		//
		MenuItem Setting = new MenuItem("&Setting 设置");
		Setting.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// sendMail("mailto:chinajash@yahoo.com.cn");
			}
		});

		MenuItem lock = new MenuItem("&Lock 锁定");
		lock.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// sendMail("mailto:chinajash@yahoo.com.cn");
			}
		});
		//
		//
		MenuItem help = new MenuItem("&Help  帮助");
		help.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem OnlineSuport = new MenuItem("&OnlineSuport 在线支持");
		OnlineSuport.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem lang = new MenuItem("&Language 语言");
		lang.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem abt = new MenuItem("&About 关于。。");
		abt.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem LoginOut = new MenuItem("&LoginOut 注销");
		LoginOut.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem HideFloat = new MenuItem("&Hide Float 隐藏悬浮窗");
		HideFloat.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		MenuItem HideMain = new MenuItem("&Hide Main Plane 隐藏主界面");
		HideMain.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});

		// MenuItem LoginOut= new MenuItem("&Change");
		// LoginOut.addActionListener(new ActionListener() {
		//
		// public void actionPerformed(ActionEvent e) {
		//
		// // edit();
		// }
		// });

		MenuItem Web = new MenuItem("&Web web版");
		Web.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});
		
		MenuItem Feedback = new MenuItem("&Feedback 反馈");
		Feedback.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				// edit();
			}
		});


		MenuItem exitMenu = new MenuItem("&Exit 退出");
		exitMenu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});
	 
		pm.add(openBrowser);
		pm.add(HideMain);
		pm.add(HideFloat);

		pm.addSeparator();
		pm.add(Web);
		pm.add(lock);
		pm.add(LoginOut);

		pm.addSeparator();
		pm.add(lang);
		pm.add(Setting);
		pm.addSeparator();
		pm.add(help);
		pm.add(OnlineSuport);
		pm.add(abt);
		pm.addSeparator();
		pm.add(Feedback);
		pm.add(exitMenu);
	}

	public void refreshTooltip() {
		ti.displayMessage("title", "本程序已经放入系统托盘!", TrayIcon.MessageType.INFO);

	}

}
