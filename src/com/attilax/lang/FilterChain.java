package com.attilax.lang;

import java.util.LinkedHashMap;
import java.util.Map;

public class FilterChain {
	
	public static void main(String[] args) {
		
	}
	public static Dictionary<String, Map<String,filterHandler>> pluginsAti = new Dictionary<String, Map<String,filterHandler>>();
	// public delegate Object filterHandler(Object obj, FilterFilterChain
	// nextChain);//第一步：定义委托类型

	filterHandler handler; // filter

	// private FilterFilterChain next1;
	// private filterHandler filterHandler;

	public FilterChain() {
	}

//	public FilterChain(log.Log logx1) {
//		// TODO: Complete member initialization
//		//this.logx = logx1;
//	}
	
	public void remove_all_filters(String tag_hookpoint) {
		pluginsAti.put(tag_hookpoint,null);
		
	}

	public void add_action(String hookPoint, filterHandler func, int priority, Object accepted_args, String function_ID) {

		try {
			 Map<String,filterHandler> iList = pluginsAti.get(hookPoint);
			iList.put(function_ID,func);
		} catch (KeyNotFoundException e) {
			pluginsAti.Add(hookPoint, new  MapAti<String,filterHandler>());

			Map<String,filterHandler> iList = pluginsAti.get(hookPoint);
			iList.put(function_ID,func);
		}
		// pluginsAti.hookPoint.push(func);
	}

	public void do_action(String hookPoint, Object args,
			filterHandler fltFinishCallback) {

		LinkedHashMap<String,filterHandler> fun_arr = null;
		;

		// if no act reg...ret
		try {
			fun_arr = (MapAti<String, filterHandler>) pluginsAti.get(hookPoint);
			 
		} catch (KeyNotFoundException e) {
			
			System.out.println("hookpoint  is empty but invoke:"+hookPoint);
			Debugx.printCallStack();
			// pluginsAti.Add(hookPoint, new List<filterHandler>());
			// fun_arr = pluginsAti[hookPoint];

			return;
		}

		// ===============add to list
		//fun_arr.
		IList<FilterChain> chains = new List<FilterChain>();

		//for (int i = 0; i < fun_arr.Count(); i++) {
		 for (filterHandler v : fun_arr.values()) {

			FilterChain cur = new FilterChain();
			cur.handler =v;
			chains.Add(cur);

		}

		if (fltFinishCallback != null) {
			FilterChain next2 = new FilterChain();
			next2.handler = fltFinishCallback;

			chains.Add(next2);
		}

		// /set next
		for (int i = 0; i < chains.Count(); i++) {
			try {
				chains.get(i).nextChain = chains.get(i + 1);
			}
		//	catch (ArgumentOutOfRangeException e) {			}
			catch (Throwable e) {
			}

		}
		chains.get(chains.Count() - 1).nextChain = new FilterChain();
		// fn(mp);

		FilterChain firstChain = chains.get(0);
		filterHandler hd = firstChain.handler;
		//try {
		//rem  on p93   filter must try catch self...should by chain container
			hd.doFilter(args, firstChain.nextChain);
//		} catch (Exception e) {
//			Debugx.printCallStack();
//			// this.logx.outputLog2UI(e.StackTrace);
//		}

	}

	public void doFilter(Object data) {

		filterHandler hd = this.handler;
		if (hd != null)

			try {
				hd.doFilter(data, this.nextChain);

			} catch (Exception e) {
				Debugx.printCallStack();
				// this.logx.outputLog2UI(e.StackTrace);
			}

	}

	public FilterChain nextChain;

//	public log.Log logx;
//	private log.Log logx1;
}
