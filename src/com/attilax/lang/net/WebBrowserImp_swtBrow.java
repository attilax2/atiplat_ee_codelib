package com.attilax.lang.net;

import org.eclipse.swt.browser.Browser;

import aaaCfg.IocX;

public class WebBrowserImp_swtBrow extends  WebBrowser{
	 Browser br;
	public WebBrowserImp_swtBrow(Browser brow) {
		this.br=brow;
	}

	public WebBrowserImp_swtBrow() {
		// TODO Auto-generated constructor stub
	}

	public void executeJavascript(String javascript) {
		if(br==null)
			br=	(Browser) IocX.map.get("browser");
		br.execute(javascript);
	}

}
