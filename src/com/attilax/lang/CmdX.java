package com.attilax.lang;

    import java.io.BufferedReader;  
import java.io.File;  
import java.io.FileWriter;  
import java.io.IOException;
import java.io.InputStreamReader;  
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.StringTokenizer;

import com.attilax.core;
import com.attilax.sms.NoDeviceEx;
import com.attilax.sms.pullEx;
import com.google.common.collect.Lists;
    /** 
     * Java 获得  CMD 输出信息 
     * @author MrChu 
     * 2013-2-19 
     */  
@Deprecated
    public class CmdX {  
       
     public static void main(String[] args) throws NoDeviceEx, pullEx {  
      // CMD 执行命令  
//      String cmd = "cmd /c adb devices" ;  
//        
//    String r = exec(cmd);
//    String  no=getDeviceNo(r);
//	System.out.println( r); ;  
//	System.out.println(no); ;  
//	AdrX ax=new AdrX();
//	ax.pull("");
    	 String cmd2="\"C:\\Program Files (x86)\\CyberLink\\PowerDVD14\\PDVDLP.exe\" \"\\\\192.168.0.128\\Imcafe\\爱情类\\五十度灰.未删减\\五SD灰.未删减.Fifty.Shades.of.Grey.2015.DVD.X264.AAC.English.CHS.Mp4Ba (2).mp4\" ";
    	 String r = exec(cmd2);
    	 System.out.println(r);
     }
     	/**
		@author attilax 老哇的爪子
     	 * @throws NoDeviceEx 
		@since   p14 e_52_6
		 
		 */
	private static String getDeviceNo(String txt) throws NoDeviceEx {
		  String[] a=txt.split("\n");
		    for (String line : a) {
				if(line.endsWith("device"))
				return (getDeviceNoSingle(line));
			}
		throw new NoDeviceEx("");
	}
		/**
			@author attilax 老哇的爪子
			@since   p14 e_52_y
			 
			 */
		private static String getDeviceNoSingle(String line) {
			 String[] a=line.split("\t");
			return a[0];
		}
		
		@Deprecated
		public static Process exec(String paramString, String[] paramArrayOfString,
				File paramFile) throws IOException {
			if (paramString.length() == 0) {
				throw new IllegalArgumentException("Empty command");
			}
			StringTokenizer localStringTokenizer = new StringTokenizer(paramString);
			String[] arrayOfString = new String[localStringTokenizer.countTokens()];
			for (int i = 0; localStringTokenizer.hasMoreTokens(); ++i)
				arrayOfString[i] = localStringTokenizer.nextToken();
			return Runtime.getRuntime(). exec(arrayOfString, paramArrayOfString, paramFile);
		}
		@Deprecated
		public static String exec_EX(String cmd, List<String> li) {
	 		 
			if(li==null)
				li=Lists.newArrayList();
	 		   // 执行 CMD 命令  
	 		   Process process = null;
	 		  String[]   b=new   String[li.size()];   
	 		  li.toArray(b);   
			try {
				 
				process =Runtime.getRuntime(). exec(b,null,null);
			} catch (IOException e) {
				 
				e.printStackTrace();
				throw new RuntimeException(e );
			}  
	 		//  process.geto
	 		   // 从输入流中读取文本  
	 		   BufferedReader reader;
			try {
				reader = new BufferedReader(new InputStreamReader(process.getInputStream(),"gbk"));
			} catch (UnsupportedEncodingException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();throw new RuntimeException(e2 );
			}  
	 		     String r="";
	 		   // 构造一个写出流并指定输出文件保存路径  
	 		//   FileWriter fw = new FileWriter(new File("C:/Users/Administrator/Desktop/CmdInfo.txt"));  
	 		     
	 		   String line = null;  
	 		     
	 		   // 循环读取  
	 		   try {
				while ((line = reader.readLine()) != null) {  
				    // 循环写入  
				  r=r+(line + "\n");  
				   }
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw new RuntimeException(e1 );
			}  
	 		     
	 		 
	 		   // 关闭输出流  
	 		   try {
				process.getOutputStream().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();throw new RuntimeException(e );
			}  
	 		     
	 		   System.out.println("cmd ext finish!");  
	 		   return r;
	 		  
			
	 	} 
		
		public static String exe(String cmd) {
	 		try {  
	 		   // 执行 CMD 命令  
	 		   Process process = Runtime.getRuntime().exec(cmd);  
	 		//  process.geto
	 		   // 从输入流中读取文本  
	 		   BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));  
	 		     String r="";
	 		   // 构造一个写出流并指定输出文件保存路径  
	 		//   FileWriter fw = new FileWriter(new File("C:/Users/Administrator/Desktop/CmdInfo.txt"));  
	 		     
	 		   String line = null;  
	 		     
	 		   // 循环读取  
	 		   while ((line = reader.readLine()) != null) {  
	 		    // 循环写入  
	 		  r=r+(line + "\n");  
	 		   }  
	 		     
	 		   
	 		   BufferedReader reader2 = new BufferedReader(new InputStreamReader(process.getErrorStream()));  
	 		   // 循环读取  
	 		   while ((line = reader2.readLine()) != null) {  
	 		    // 循环写入  
	 		  r=r+(line + "\n");  
	 		   }  
	 		 
	 		   // 关闭输出流  
	 		  process.getInputStream().close();
	 		  process.getErrorStream().close();
	 		   process.getOutputStream().close();  
	 		     
	 		   System.out.println("cmd ext finish!");  
	 		   return r;
	 		  } catch (Exception e) {  
	 			 throw  new RuntimeException(e);
	 		  }
			
	 	} 
		
		@Deprecated
	public static String exec(String cmd) {
 		try {  
 		   // 执行 CMD 命令  
 		   Process process = Runtime.getRuntime().exec(cmd);  
 		//  process.geto
 		   // 从输入流中读取文本  
 		   BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));  
 		     String r="";
 		   // 构造一个写出流并指定输出文件保存路径  
 		//   FileWriter fw = new FileWriter(new File("C:/Users/Administrator/Desktop/CmdInfo.txt"));  
 		     
 		   String line = null;  
 		     
 		   // 循环读取  
 		   while ((line = reader.readLine()) != null) {  
 		    // 循环写入  
 		  r=r+(line + "\n");  
 		   }  
 		     
 		 
 		   // 关闭输出流  
 		   process.getOutputStream().close();  
 		     
 		   System.out.println("cmd ext finish!");  
 		   return r;
 		  } catch (Exception e) {  
 			 return core.toJsonStrO88(e);
 		  }
		
 	}  
	@Deprecated
	public static void exec0(String cmd) {
		try {  
		   // 执行 CMD 命令  
		   Process process = Runtime.getRuntime().exec(cmd);  
		     
		   // 从输入流中读取文本  
		   BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));  
		     
		   // 构造一个写出流并指定输出文件保存路径  
		   FileWriter fw = new FileWriter(new File("C:/Users/Administrator/Desktop/CmdInfo.txt"));  
		     
		   String line = null;  
		     
		   // 循环读取  
		   while ((line = reader.readLine()) != null) {  
		    // 循环写入  
		    fw.write(line + "\n");  
		   }  
		     
		   // 刷新输出流  
		   fw.flush();  
		     
		   // 关闭输出流  
		   fw.close();  
		     
		   // 关闭输出流  
		   process.getOutputStream().close();  
		     
		   System.out.println("程序执行完毕!");  
		  } catch (Exception e) {  
		   e.printStackTrace();  
		  }
	}  
       
    }  