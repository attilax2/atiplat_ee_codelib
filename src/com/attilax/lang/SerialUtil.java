package com.attilax.lang;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map;

public class SerialUtil {

	public static void write(Object obj,
			String file) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(
					file));

			// 步骤二：通过对象输出流的writeObject()s方法写对象：

			out.writeObject(obj);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
			}
		}

	}

	public static Object read(String movListssSSeriialOBjFile) {ObjectInputStream in = null ;
		try {
		  in = new ObjectInputStream(new FileInputStream(
				movListssSSeriialOBjFile));

		// 步骤二：通过对象输出流的readObject()方法读取对象：

	
			return in.readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);

		}finally
		{
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
