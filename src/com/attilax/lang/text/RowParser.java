package com.attilax.lang.text;

import java.util.Map;

import com.attilax.json.AtiJson;
import com.google.common.collect.Maps;

public class RowParser {

	@FunctionalInterface
	public interface RunnableAti {

		void run(String string);
	}

	String[] key = { "工作时间", "性别要求","结算模式","工作地点","薪资水平"  };
	String[] splitors = { "：", ":" };

	public static void main(String[] args) {
		String x = "工作时间：08:00-22:00\r\n性别要求：男女不限";
		Map m = new RowParser().parse(x);
		System.out.println(AtiJson.toJson(m));
		System.out.println("---f");

	}

	public Map parse(String x) {
		Map m = Maps.newLinkedHashMap();

	x=x.replace("\r\n", "\n");
		String[] lines = x.split("\n");

		RunnableAti r = (lin) -> {

			String line = lin.toString().trim();
			if(line.length()==0)
				return;
			Map kv = getMapArr(line);

			m.put(kv.get("k"), kv.get("v"));

		};
		foreachSafe(lines, r);

		return m;
	}

	private void foreachSafe(Object[] lines, RunnableAti r) {
		for (Object lin : lines) {

			try {
				r.run(lin.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	@SuppressWarnings("rawtypes")
	private Map getMapArr(String line) {
		if(line.contains("工作地点"))
			System.out.println("dbg");
		Map m = Maps.newLinkedHashMap();
		for (String sp : splitors) {
			try {
				int i = line.indexOf(sp);
				if(i<0)
					continue;
				m.put("k", line.substring(0, i));
				m.put("v", line.substring(i+1));
				break; //only onece
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return m;
	}

}
