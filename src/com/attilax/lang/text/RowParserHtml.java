package com.attilax.lang.text;

import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.jsoup.nodes.Document;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.AtiJson;
import com.google.common.collect.Maps;

public class RowParserHtml {

	@FunctionalInterface
	public interface RunnableAti {

		void run(String string);
	}

	String[] key = { "工作时间", "性别要求" };
	String[] splitors = { "：", ":" };

	public static void main(String[] args) {
		pathx.isWebPathMode = true;
		String f = pathx.classPath() + "/com/attilax/cms/cms_tmplt.html";
		String txt = filex.read(f);
		Document doc = Jsoup.parse(txt);
		// Document doc = Jsoup.connect(url).get();
		String plainText;
		HtmlToPlainText formatter = new HtmlToPlainText();
		plainText = formatter.getPlainText(doc);
		System.out.println(plainText);

		String x = "工作时间：08:00-22:00\r\n性别要求：男女不限";
		x = plainText;
		@SuppressWarnings("rawtypes")
		Map m = new RowParser().parse(x);
		System.out.println(AtiJson.toJson(m));
		System.out.println("---f");

	}

}
