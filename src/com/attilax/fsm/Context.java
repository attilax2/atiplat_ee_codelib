package com.attilax.fsm;

import java.util.ArrayList;
import java.util.List;

import com.attilax.lang.text.strUtil;

public class Context {

	public State state;
	
	
	public List<Token> tokenList=new  ArrayList <Token>();
	public Token curToken=new Token();
	
	
	public int curcharIndex;
	public char[] sa;

	 

	public State getState() {
		return state;
	}



	public void setState(State state) {
		this.state = state;
	}



	public void request(String s) {
		if(sa==null)
			sa=s.toCharArray();
		state.handle(s, this);
		 
		
	}

}
