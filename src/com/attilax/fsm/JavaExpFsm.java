package com.attilax.fsm;

import java.util.List;

import com.google.common.collect.Lists;

public class JavaExpFsm {
	List<String> tokens = Lists.newLinkedList();
	String curToken = "";
//	String splitors = "(),\"";
	String curStat = "ini";
	private String code;
	public char[] code_char_arr;

	public JavaExpFsm(String code) {
		this.code = code;
	}

	
	/**
	 * http://localhost/new(com.attilax.util.connReduceDync).set_resfile(uc_js.txt).joinNout()
	 * http://localhost/wrmiServlet?code=new(com.attilax.util.connReduceDync).set_resfile(uc_js.txt).joinNout()
	 * @param args
	 */
	public static void main(String[] args) {
		String code = "new(com.attilax.util.connReduceDync).set_resfile(uc_js.txt).joinNout() "
				.trim();
		code= " new(com.attilax.agent.AgentRechargeService).getSubMemTotalsRecycleByAgentId(\"promoter:$pid$,fld2:v2\")";
		List li = new JavaExpFsm(code).getTokens();
		for (Object object : li) {
			System.out.println(object);
		}
		System.out.println(li);

	}

	public List getTokens() {
		code_char_arr = code.toCharArray();
		for (char c : code_char_arr) {
			// get next char,,then change stat
			// jude cur char and cur stat...then if or not chage stat
			if (c == '(' && !this.curStat.equals("strStart")) {    //&&  cur stta=ini
				this.curStat = "brkStart";
				tokens.add(this.curToken);
				tokens.add("(");
				this.curToken = "";
				continue;
			}

//			if (c == '.' && this.curStat.equals("brkStart")) {
//
//				curToken = curToken + String.valueOf(c);
//				continue;
//
//				// this.curStat.equals("brkEnd"))
//
//			}
//			
			if (c == ')'  && !this.curStat.equals("strStart") ) {    //&& cur stat =brk start
				this.curStat = "brkEnd";
				if(this.curToken.length()>0)
				tokens.add(this.curToken);
				tokens.add(")");
				this.curToken = "";
				continue;
			}
			
			if (c == '.' && this.curStat.equals("brkEnd")) {
				tokens.add(".");
				curToken = "";
				continue;
			}
			
			if(c=='\"' && this.curStat.equals("brkStart"))
			{
				this.curStat = "strStart";
			//	tokens.add(c);
				this.curToken = "";
				continue;
			}
			
			
			if(c=='\"' && this.curStat.equals("strStart"))
			{
				this.curStat = "strEnd";
				tokens.add(this.curToken);
				this.curToken = "";
				continue;
			}
			
			
			if(c==',' && this.curStat.equals("brkStart"))
			{
				//this.curStat = "strEnd";
				tokens.add(this.curToken);
				this.curToken = "";
				continue;
			}
		//	if (this.curStat.equals("ini"))
				curToken = curToken + String.valueOf(c);
		}
		return tokens;
	}

}
