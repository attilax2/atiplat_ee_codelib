package com.attilax.fsm.camelScanner;

import java.util.List;

import com.attilax.fsm.Context;
import com.attilax.fsm.State;
import com.attilax.lang.text.strUtil;
import com.attilax.util.numUtil;

public class iniState extends State {

	@Override
	public void handle(String sampleParameter, Context context) {
		
		
		context.curcharIndex=0;
		 char curChar=context.sa[context.curcharIndex];
		 if(strUtil.isBigLetter(curChar))
		 {
			 newToken(context, curChar);
			 
			 context.state=new BigCaseLetterState();
			 
		 }
		 if(strUtil.isLowerLetter(curChar))
		 {
			 newToken(context, curChar);
			  context.state=new LowerLetterState();  
			 
		 }
		 if(numUtil.isNum(curChar))
		 {
			 newToken(context, curChar);
			 context.state=new NumState();
		 }
		 
		
	}

	

}
