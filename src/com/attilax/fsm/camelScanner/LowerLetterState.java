package com.attilax.fsm.camelScanner;

import com.attilax.fsm.Context;
import com.attilax.fsm.FinishState;
import com.attilax.fsm.State;
import com.attilax.fsm.Token;
import com.attilax.lang.text.strUtil;
import com.attilax.util.numUtil;

public class LowerLetterState extends State {

	@Override
	public void handle(String sampleParameter, Context context) {
		// TODO Auto-generated method stub
		if(MoveNextisEnd(context) )
			return;
		
		String trim = context.curToken.value.trim();
	String allUpperLetter	 =trim.substring(0,trim.length()-1);
	
		if(allUpperLetter.startsWith("J"))
			System.out.println("");
		if(strUtil.isBigLetter(allUpperLetter)  && allUpperLetter.length()>1)  //multi upperLetter
		{
			final String word=allUpperLetter.substring(0,trim.length()-2);
			Token t=new Token(){
				{
					this.value=word;
				}
			};
			
			context.tokenList.add(t);
			
			Token t2=new Token();
			t2.value=trim.substring( trim.length()-2);
			context.curToken=t2;
		//	context.tokenList.add(t2);
			
		}
		
		 char curChar=context.sa[context.curcharIndex];
		 if(strUtil.isBigLetter(curChar))
		 {
			 addCurTokenNnewToken(context, curChar);
			 
			 context.state=new BigCaseLetterState();
		 }
		 if(strUtil.isLowerLetter(curChar))
		 {
			 context.curToken.value=context.curToken.value+String.valueOf(curChar);
			// context.state=new LowerLetterState();  //state not change
			 
		 }
		 if(numUtil.isNum(curChar))
		 {
			 addCurTokenNnewToken(context, curChar);
			 context.state=new NumState();
		 }
	}

	

	

}
