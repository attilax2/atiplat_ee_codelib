/**
 * 
 */
package com.attilax.sync;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.rails.foreachProcessor;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author attilax 2016年11月2日 下午8:25:17
 */
public class SyncUtil {
	public static int n = 0;
	public static void main(String[] args) throws IOException {
		String dir = "C:\\0workspace\\AtiPlatf_cms\\WebRoot";
		String snap = "c:\\webdir.txt";
		
 	String compareRzt = "c:\\delfile.txt";
 //、、	、、new String[ ]
 String[] args1=new String[]{ "com.attilax.sync.SyncUtil.geneSnap('D:\\ati\\foto ppl', 'D:\\ati\\foto ppl snap\\foto_ppl_snap_pb2.txt')"};
 	com.attilax.dsl.DslParser.main(args1);
 	
	//com.attilax.sync.SyncUtil.geneSnap(dir, snap);
 //	com.attilax.sync.SyncUtil.geneDelFileRztFile(dir, snap, compareRzt);
 	SyncUtil.execSyncFile4delFile(compareRzt,"","");
		System.out.println("--f");
	}

	/**
	attilax    2016年11月9日  下午10:14:12
	 * @param compareRzt
	 * @param string
	 */
	public static void execSyncFile4delFile(String compareRzt, String dir,String movDestDir) {
		 
		List<String> li = filex.read2list(compareRzt, "gbk");
		for (String f : li) {
			String full_file=dir+"/"+f;
			filex.move(full_file, movDestDir, dir);		 
			
		}
	}

	public static void geneDelFileRztFile(String dir, String snap,
			String compareRztFile,String pathHead) throws IOException {
		Set<String> set = getSetFrmFile(snap);
		Set<String> set_dir = getSetFrmDir(dir);
		filex fx = new filex(compareRztFile, "gbk");
//	
//		
			boolean  delSet=set.removeAll(set_dir);
			List<String>  li=Lists.newArrayList();
			li.addAll(set);
			Collections.sort(li);
			li=delPathHead(li,pathHead);
		
			
			for (String f : li) {
				fx.appendLine_flush_safe(f);
				System.out.println(f);
			}
			
			// geneOriTxt(s);
			fx.closeSF();
	}
	
	
	/**
	attilax    2016年11月9日  下午10:03:23
	 * @param li
	 * @param pathHead 
	 * @return
	 */
	private static List<String> delPathHead(List<String> li, String pathHead) {
		List<String> li_r=Lists.newArrayList();
		for (String p : li) {
			int idx=p.indexOf(pathHead);
			String p1=p.substring(idx);
			li_r.add(p1);
			
		}
		return li_r;
	}

	public static void geneDelFileRztFile(String dir, String snap,
			String compareRztFile) throws IOException {
		Set<String> set = getSetFrmFile(snap);
		Set<String> set_dir = getSetFrmDir(dir);
		filex fx = new filex(compareRztFile, "gbk");
//	
//		
			boolean  delSet=set.removeAll(set_dir);
			List<String>  li=Lists.newArrayList();
			li.addAll(set);
			Collections.sort(li);
			
			for (String f : li) {
				fx.appendLine_flush_safe(f);
				System.out.println(f);
			}
			
			// geneOriTxt(s);
			fx.closeSF();
	}

	/**
	attilax    2016年11月2日  下午9:04:42
	 * @param dir
	 * @return
	 */
	private static Set<String> getSetFrmDir(String dir) {
		Set<String> set = Sets.newHashSet();
		Function closure = (p) -> {

			 set.add((String) p);
			return p;

		};

		dirx.traveV3(dir, closure);
		return set;
	}

	private static Set<String> getSetFrmFile(String snap) {
		List<String> li = filex.read2list(snap, "gbk");
		Set<String> set = Sets.newHashSet();
		set.addAll(li);
		return set;
	}

	public static void geneSnap(String dir ,String snap) throws IOException {
	//	String snap = "c:\\webdir.txt";
		filex fx = new filex(snap, "gbk");

		Function closure = (p) -> {
			System.out.println(p);
			fx.appendLine_flush_safe((String) p);
			n++;
			System.out.println(n+":"+p);
			// if(n>10)
			// throw new RuntimeException("stop");
			return p;

		};

		dirx.traveV3(dir, closure);
		fx.closeSF();
	}

}
