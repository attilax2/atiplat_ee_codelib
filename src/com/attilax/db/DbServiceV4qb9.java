/**
 * 
 */
package com.attilax.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.NonUniqueResultException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

 













import aaaCfg.ConnectionImp;
import aaaCfg.IocX;

import com.attilax.core;
 
import com.attilax.api.HandlerChain;
 
 
import com.attilax.biz.seo.getConnEx;
import com.attilax.db.DBX;
import com.attilax.io.pathx;
 
import com.attilax.lang.Global;
import com.attilax.persistence.DBCfg;
import com.attilax.sql.ex.DuplicateEntryEx;
import com.attilax.time.timeUtil;
import com.attilax.util.PropX;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.mysql.jdbc.JDBC4Connection;
 
 

/**dbutils
 * @author ASIMO
 *
 */
public class DbServiceV4qb9   {

	public DbServiceV4qb9(DBCfg cfg) {
		this.dbcfg=cfg;
	}
	
	
 
	public DbServiceV4qb9(Connection conn) {
		this.conn=conn;
	}
	
	
	


	public DbServiceV4qb9() {
	System.out.println("-------db til  no para ini");
	}


	/**
	@author attilax 老哇的爪子
	@since   p1p g_r_w
	 
	 */
	public static void main(String[] args) {
//		 String sql = " SELECT * FROM `mall_shop_info` sp left join  mall_user_info  ui on  sp.user_id=ui.user_id where sp.user_id=8 ";
//		//  sql= " update mall_shop_info set shop_intro='myintro5' where shop_id=8 ";
//		//  sql=sql+" ; update  mall_user_info set  user_mobile='1358891563'    where user_id=8 ";
//sql="SELECT * FROM  mall_user_info   where  user_id=6 ; SELECT * FROM  mall_user_info   where  user_id=8 ";
//		
//  sql="select * from hx_car where name like '%豪车%'";
//		 DbutilX c= IocX.getBean(DbutilX.class);
//	//	 System.out.println(c.update(sql));
//	 	 List li=c.findBySql(sql);
		 
//	 	 ApiX2 hc=new ApiX2();
//		 hc.hbx=c;
//		
//		 String sql="select * from gv_material";
//			DbServiceQ5 dx= IocX.getBean(DbServiceQ5.class);
//			 List<Map> li=dx.findBySql(sql);
// 		  System.out.println(core.toJsonStrO88(li));
//		 System.out.println("--f");
		System.out.println("--f");

	}
	 
	Connection conn;
	@Inject
	DBCfg dbcfg;
//	String path = pathx.classPath() + "/website.properties";
	public Connection getConnection() throws getConnEx {
		if(this.conn instanceof ConnectionImp)
			this.conn=null;
		
		if(this.conn!=null)
			return this.conn;
		// com.microsoft.sqlserver.jdbc.SQLServerDriver
	
		//System.out.println(PropX.getConfig(path, "jdbc.url"));
		
		
	
		try {

			Class.forName(dbcfg.getDriver());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Class.forName driver err,drive class name is :  "+dbcfg.getDriver(),e);
		}catch(NullPointerException e)
		{
			throw new RuntimeException("Class.forName driver err,drive class name is $null",e);
		}
 
		
		Connection conn;
		try {
			conn = DriverManager.getConnection(
					dbcfg.getUrl(),
					dbcfg.getUser(),
					dbcfg.getPassword());
			System.out.println("get conn: "+ conn+" at:"+timeUtil.Now_CST());
			
		} catch (SQLException e) {
			throw new getConnEx("getconnex" + e.getMessage());
		}
		return conn;
	}
	
	public List<Map> execSql(String sql)
	{
		if(sql.trim().toLowerCase().startsWith("update") || sql.trim().toLowerCase().startsWith("insert"))
		{
			int intx = update(sql);
			 List li=new ArrayList ();
			 li.add(intx);
			return li;
		}
		return findBySql(  sql);
		
	}
	
	
	public int executeUpdate(Connection conn, String string) {
		PreparedStatement prepareStatement;
		ResultSet resultSet;
		try {
			prepareStatement = conn.prepareStatement(string);
		return 	prepareStatement.executeUpdate() ;
			 
		} catch (SQLException e) {
			 
			 throw new RuntimeException(e);
		}
	}

	public Map uniqueResult(String $sql) {
		List li=findBySql($sql);
		return (Map) li.get(0);
	}
	
	/**
	 * can be null
	attilax    2016年6月10日  下午5:13:14
	 * @param $sql
	 * @return
	 */
	public Map uniqueResultAsRow(String $sql) {
		List li=findBySql($sql);
		if(li.size()==0)
			return null;
		if(li.size()>1)
			throw new  NonUniqueResultException("query did not return a unique result: "+li.size());
		return (Map) li.get(0);
	}
	
	public List findBySql(String sql)  {
		Connection conn;
		 try{
			conn = getConnection();

			// 创建一个QueryRunner
			QueryRunner queryRunner = new QueryRunner(true);
			List<Map<String, Object>> list;

			list = queryRunner.query(conn,
					sql,
					new MapListHandler());
		 
			return list;
	 
		 }
		catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
	public int executeUpdateV2qb0(String sql) throws getConnEx, DuplicateEntryEx, SQLException {
		Connection conn;
		 
			conn = getConnection();

			// 创建一个QueryRunner
			QueryRunner queryRunner = new QueryRunner(true);
			int rzt = 0;

			try {
				rzt = queryRunner.update(conn, sql);
			} catch (SQLException e) {
				if(e.getMessage().startsWith("Duplicate entry"))
				{
					throw new  DuplicateEntryEx(e.getMessage(),e);
				}
				throw e;
			}
		 
			return rzt;
	 
		 }
		 

//			e.printStackTrace();
//			throw new RuntimeException(e);
		 
//	return  execSql_retInt(sql);
 
	//for secury cause ..
//	public int executeUpdate(String sql) {
//		// TODO Auto-generated method stub
//		return  execSql_retInt(sql);
//	}
	
	public List executeQuery(String sql)
	{
		String s;
if(Global.req.get()!=null)
	if(Global.req.get().getParameter("sql")!=null)
		sql=Global.req.get().getParameter("sql");
// new RuntimeException(" no implt" );
return findBySql(  sql);
	}
	public Integer execSql_retInt(String sql)
	{

// new RuntimeException(" no implt" );
return update(  sql);
	}
	
	
	public int update(String sql)  {
		Connection conn;
		 try{
			conn = getConnection();

			// 创建一个QueryRunner
			QueryRunner queryRunner = new QueryRunner(true);
			int list;

			list = queryRunner.update(conn, sql);
		 
			return list;
	 
		 }
		catch (Exception e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}


}
