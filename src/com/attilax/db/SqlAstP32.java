/**
 * 
 */
package com.attilax.db;

/**
 * @author ASIMO
 *
 */
public class SqlAstP32 {
	
	public String preOther;
	public String getPreOther() {
		return preOther;
	}

	public void setPreOther(String preOther) {
		this.preOther = preOther;
	}

	public SqlPageParam pagePart;

	public SqlPageParam getPagePart() {
		return pagePart;
	}

	public void setPagePart(SqlPageParam pagePart) {
		this.pagePart = pagePart;
	}

}
