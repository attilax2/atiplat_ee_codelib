package com.attilax.wechat;

/**
 * @api {post} /order/editOrderPayStatus 修改订单支付状态(支付宝)
 * @apiName editOrderPayStatus API
 * @apiGroup Order
 * @apiDescription 修改订单支付状态(支付宝)
 * @apiVersion 0.1.0
 * @apiParam {String} id 订单编号.
 * @apiParam {String} orderStatus 订单状态
 * @apiParam {String} PayStatus 支付状态 Paying,Paied,PartPaied,PartRefund,Refund
 * @apiParam {String} ShipStatus  Pending("0"),Shipped("1"),PartPending("2"),PartShipped("3"),Returned("4")
 */
@RequestMapping(value = "/editOrderPayStatus", method = RequestMethod.POST)
public @ResponseBody DisplayData editOrderPayStatus(HttpServletRequest request,HttpServletResponse response) {
    Order oldorder = null;
    try {
        String trade_status=request.getParameter("trade_status");//交易状态
        Long trade_no=Long.parseLong(request.getParameter("out_trade_no"));//交易订单号
        float total_fee=Float.parseFloat(request.getParameter("total_fee"));//订单总金额
        oldorder=orderManager.get(Order.class,trade_no);
        float FinalAmount=oldorder.getFinalAmount();//数据库订单金额
        if("TRADE_SUCCESS".equals(trade_status)&&trade_status!=null){
            if(null!=oldorder){
                if(total_fee==FinalAmount){
                    //待支付订单状态条件
                    if ((oldorder.getOrderStatus() == OrderStatus.Active) && (oldorder.getPayStatus() == PayStatus.Paying || oldorder.getPayStatus() == PayStatus.PartPaied) && (oldorder.getShipStatus() == ShipStatus.Pending)) {
                        oldorder.setOrderStatus(OrderStatus.Active);//已支付
                        oldorder.setPayStatus(PayStatus.Paied);
                        oldorder.setShipStatus(oldorder.getShipStatus());
                        orderManager.update(oldorder);
                        response.getWriter().println("success");	//请不要修改或删除
                    }
                }else{
                    response.getWriter().println("fail");//请不要修改或删除
                }
            }
        }else{
            response.getWriter().println("fail");//请不要修改或删除
        }
        return new DisplayData(request,StatusCode.SUCCESS,"inf.jade.bss.commons.success",oldorder);
    } catch (Exception e) {
        return new DisplayData(request, StatusCode.FAIL,e.getMessage());
    }
}



/**
 * @api {post} /order/editOrderWxPayStatus 修改订单支付状态(微信)
 * @apiName editOrderWxPayStatus API
 * @apiGroup Order
 * @apiDescription 修改订单支付状态(微信)
 * @apiVersion 0.1.0
 * @apiParam {String} id 订单编号.
 * @apiParam {String} orderStatus 订单状态
 * @apiParam {String} PayStatus 支付状态 Paying,Paied,PartPaied,PartRefund,Refund
 * @apiParam {String} ShipStatus  Pending("0"),Shipped("1"),PartPending("2"),PartShipped("3"),Returned("4")
 */
@RequestMapping(value = "/editOrderWxPayStatus", method = RequestMethod.POST)
public @ResponseBody DisplayData editOrderWxPayStatus(HttpServletRequest request,HttpServletResponse response) {
    Order oldorder = null;
    try {
            // 将解析结果存储在HashMap中
            Map map = new HashMap();
            // 从request中取得输入流
            InputStream inputStream = request.getInputStream();
            // 读取输入流
            SAXReader reader = new SAXReader();
            Document document =reader.read(inputStream);
            // 得到xml根元素
            Element root = document.getRootElement();
            // 得到根元素的所有子节点
            List<Element> list = root.elements();
            // 遍历所有子节点
            for (Element e : list){
                map.put(e.getName(), e.getText());
            }
            inputStream.close();
            inputStream=null;
            if("SUCCESS".equals(map.get("result_code"))){
                Long trade_no=Long.parseLong(map.get("out_trade_no").toString());//订单号
                float total_fee=Float.parseFloat(map.get("total_fee").toString());//订单金额
                oldorder=orderManager.get(Order.class,trade_no);
                float FinalAmount=oldorder.getFinalAmount();//数据库订单金额
                if(null!=oldorder){
                    if((total_fee/100)==FinalAmount){
                        //待支付订单状态条件
                        if ((oldorder.getOrderStatus() == OrderStatus.Active) && (oldorder.getPayStatus() == PayStatus.Paying || oldorder.getPayStatus() == PayStatus.PartPaied) && (oldorder.getShipStatus() == ShipStatus.Pending)) {
                            oldorder.setOrderStatus(OrderStatus.Active);//已支付
                            oldorder.setPayStatus(PayStatus.Paied);
                            oldorder.setShipStatus(oldorder.getShipStatus());
                            orderManager.update(oldorder);
                            String entity = WeChatPayUtil.returnCodeSUCCESS();
                            response.getWriter().println(entity);	//请不要修改或删除
                            //return entity;
                        }else{
                            String entity = WeChatPayUtil.returnCodeFAIL();
                            response.getWriter().println(entity);	//请不要修改或删除
                            //return entity;
                        }

                    }
                }else{
                    String entity = WeChatPayUtil.returnCodeFAIL();
                    response.getWriter().println(entity);	//请不要修改或删除
                    //return entity;
                }
            }
    } catch (Exception e) {
        return new DisplayData(request, StatusCode.FAIL,e.getMessage());
    }
    return new DisplayData(request,StatusCode.SUCCESS,"inf.jade.bss.commons.success",oldorder);
}
