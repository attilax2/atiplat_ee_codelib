package com.attilax.dataspider;

import com.attilax.device.CpuUtil;

public class Reducer extends BusPart {

	//public int freq;

	public int maxCpu;



	public Reducer(Runnable reduceRun) {
		
		Runnable ra=()->{
			int cpu=new CpuUtil().getCpuRate_avg();
			if(cpu>=maxCpu)
			{
				reduceRun.run();
			}
		};
	
		rx=ra;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	

}
