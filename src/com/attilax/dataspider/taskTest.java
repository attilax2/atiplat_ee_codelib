/**
 * 
 */
package com.attilax.dataspider;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.attilax.exception.ExUtil;
import com.google.common.collect.Lists;

/**
 * @author attilax 2016年9月28日 下午7:38:57
 */
public class taskTest {

	public static void main(String[] args) throws TimeoutException,
			InterruptedException {
	//	t2();
		List<FutureTask> li=Lists.newArrayList();
		FutureTask ft=new FutureTask<>(new Callable<String>() {

			@Override
			public String call() throws Exception {
				while (true) {
					Thread.sleep(4000);
					System.out.println("--slp");
				}
			//	return null;
			}
		});
		li.add(ft);
	 	Thread thread = new Thread(ft);
		thread.setName("thread_");
		thread.setPriority(Thread.MAX_PRIORITY);
	//	thread.set
		thread.start();
		
		
		try {
			ft.get(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			ExUtil.throwExV2(e, "--url:" );
			 
		} catch (ExecutionException e) {
			ExUtil.throwExV2(e, "--url:" );
		 
		} catch (TimeoutException e) {
			 
			e.printStackTrace();
		} finally {
			//closeStreamAll();
		 	//ft.cancel(true);
		}
		System.out.println("--ok");
	}

	private static void t2() {
		ExecutorService es = Executors.newSingleThreadExecutor();

		Future<String> fut = es.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				while (true) {
					Thread.sleep(4000);
					System.out.println("--slp");
				}
			}
		});

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				System.out.println("sec task");
				es.shutdown();
				System.out.println("--es shutdow after");
			}

		};
		
		
	

		try {
			System.out.println("--pre get");
			fut.get(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			ExUtil.throwExV2(e);

		} catch (ExecutionException e) {
			ExUtil.throwExV2(e);

		} catch (TimeoutException e) {
			fut.cancel(true);
			e.printStackTrace();
		} finally {
			System.out.println("--re shutdown");
			// es.shutdownNow();
			// es.awaitTermination(0, TimeUnit.SECONDS);
			// closeFutureTask(es, fut);
		}
		System.out.println("--ok");
		es.execute(runnable);
		System.out.println("--ok2");
	}

}
