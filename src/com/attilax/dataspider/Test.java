/**
 * 
 */
package com.attilax.dataspider;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;

import com.attilax.concur.TaskUtil;
import com.attilax.exception.ExUtil;
import com.attilax.net.HttpUtil;

/**
 * @author attilax
 *2016年9月27日 下午6:20:38
 */
public class Test {

	/**
	attilax    2016年9月27日  下午6:20:39
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url="http://img6.showhaotu.xyz/2016/09/27/8bb9a9a08bda454c7ace14f428ca6625.jpg";
		url="https://is04.picsgonewild.com/2016/09/13/YCYTbc.jpg";
		
	//	HttpUtil.down(url, "c:\\00test");
		while(true)
		{
			TaskUtil.sleep_sec(1);
			MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
			MemoryUsage memoryUsage = memoryMXBean.getHeapMemoryUsage(); //椎内存使用情况
			long totalMemorySize = memoryUsage.getInit(); //初始的总内存
			long maxMemorySize = memoryUsage.getMax(); //最大可用内存
			long usedMemorySize = memoryUsage.getUsed(); //已使用的内存
			System.out.println("usedMemorySize MB:"+usedMemorySize/1000000);
			
			
			ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		//	threadMXBean.get
			 long currentCpuTime = threadMXBean.getCurrentThreadCpuTime();
			 OperatingSystemMXBean osmxb = (OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();
			 osmxb.get
				System.out.println("currentCpuTime:"+currentCpuTime);
		}
		
	//	org.slf4j.spi.LocationAwareLogger
		
//		String pic="http://www.php100.com/dirdir/image.php%3Fid=659C_57DE45E3&";
//		pic="http://img5.uploadhouse.com/fileuploads/22655/22655895cd6fcdf539b16878ce4f69ed9ab39bc8.jpg";
//		pic="http://www.imageshot.eu/images/2016/02/17/SDC11393.jpg";
//		TsaolyoNetDataSpider tsaolyoNetDataSpider = new TsaolyoNetDataSpider();
//		tsaolyoNetDataSpider.picSaveDir="c:\\000picSaveDir";
//		 tsaolyoNetDataSpider.downPic(pic, "title");
//		
		
	 
	//	 System.out.println("==f");
	}
	/**
	   * 取得 CPU 占用率。
	   * 
	   * @return
	   * @throws IOException
	   */
	  private static double getCpuLoadPercentage() throws IOException {
		  final String CHARSET = "GBK";
		  final String CAP_LOAD_PERCENTAGE = "LoadPercentage";
		   final String CMD_CPU = "wmic cpu get " + CAP_LOAD_PERCENTAGE;
		  //wmic cpu get LoadPercentage
	    Process process = Runtime.getRuntime().exec(CMD_CPU);
	    InputStream is = process.getInputStream();
	    BufferedReader br = new BufferedReader(new InputStreamReader(is, CHARSET));
	    br.readLine(); // 舍弃标题行
	    br.readLine(); // 舍弃标题行下空行
	    String percentageLine = br.readLine();
	    if (percentageLine == null) {
	      return 0;
	    }
	    return Double.parseDouble(percentageLine.trim());
	  }
	
    /** *//**  
     * 获得CPU使用率.  
     * @return 返回cpu使用率  
     * @author amg     * Creation date: 2008-4-25 - 下午06:05:11  
     */  
    private double getCpuRatioForWindows() {   
        try {   
            String procCmd = System.getenv("windir")   
                    + "//system32//wbem//wmic.exe process get Caption,CommandLine,"  
                    + "KernelModeTime,ReadOperationCount,ThreadCount,UserModeTime,WriteOperationCount";   
            // 取进程信息   
            long[] c0 = readCpu(Runtime.getRuntime().exec(procCmd));   
            Thread.sleep(CPUTIME);   
            long[] c1 = readCpu(Runtime.getRuntime().exec(procCmd));   
            if (c0 != null && c1 != null) {   
                long idletime = c1[0] - c0[0];   
                long busytime = c1[1] - c0[1];   
                return Double.valueOf(   
                        PERCENT * (busytime) / (busytime + idletime))   
                        .doubleValue();   
            } else {   
                return 0.0;   
            }   
        } catch (Exception ex) {   
            ex.printStackTrace();   
            return 0.0;   
        }   
    }   
  
    /** *//**  
     * 读取CPU信息.  
     * @param proc  
     * @return  
     * @author amg     * Creation date: 2008-4-25 - 下午06:10:14  
     */  
    private long[] readCpu(final Process proc) {   
        long[] retn = new long[2];   
        try {   
            proc.getOutputStream().close();   
            InputStreamReader ir = new InputStreamReader(proc.getInputStream());   
            LineNumberReader input = new LineNumberReader(ir);   
            String line = input.readLine();   
            if (line == null || line.length() < FAULTLENGTH) {   
                return null;   
            }   
            int capidx = line.indexOf("Caption");   
            int cmdidx = line.indexOf("CommandLine");   
            int rocidx = line.indexOf("ReadOperationCount");   
            int umtidx = line.indexOf("UserModeTime");   
            int kmtidx = line.indexOf("KernelModeTime");   
            int wocidx = line.indexOf("WriteOperationCount");   
            long idletime = 0;   
            long kneltime = 0;   
            long usertime = 0;   
            while ((line = input.readLine()) != null) {   
                if (line.length() < wocidx) {   
                    continue;   
                }   
                // 字段出现顺序：Caption,CommandLine,KernelModeTime,ReadOperationCount,   
                // ThreadCount,UserModeTime,WriteOperation   
                String caption = Bytes.substring(line, capidx, cmdidx - 1)   
                        .trim();   
                String cmd = Bytes.substring(line, cmdidx, kmtidx - 1).trim();   
                if (cmd.indexOf("wmic.exe") >= 0) {   
                    continue;   
                }   
                // log.info("line="+line);   
                if (caption.equals("System Idle Process")   
                        || caption.equals("System")) {   
                    idletime += Long.valueOf(   
                            Bytes.substring(line, kmtidx, rocidx - 1).trim())   
                            .longValue();   
                    idletime += Long.valueOf(   
                            Bytes.substring(line, umtidx, wocidx - 1).trim())   
                            .longValue();   
                    continue;   
                }   
  
                kneltime += Long.valueOf(   
                        Bytes.substring(line, kmtidx, rocidx - 1).trim())   
                        .longValue();   
                usertime += Long.valueOf(   
                        Bytes.substring(line, umtidx, wocidx - 1).trim())   
                        .longValue();   
            }   
            retn[0] = idletime;   
            retn[1] = kneltime + usertime;   
            return retn;   
        } catch (Exception ex) {   
            ex.printStackTrace();   
        } finally {   
            try {   
                proc.getInputStream().close();   
            } catch (Exception e) {   
                e.printStackTrace();   
            }   
        }   
        return null;   
    } 
    
   

}
