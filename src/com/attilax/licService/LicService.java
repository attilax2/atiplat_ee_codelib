package com.attilax.licService;

import java.io.UnsupportedEncodingException;
import java.security.spec.InvalidKeySpecException;

import com.attilax.encry.RSACoder;
import com.attilax.exception.ExUtil;
import com.attilax.io.filex;
import com.attilax.lang.Base64;
import com.attilax.license.HardWareUtils;

public class LicService {

	/** decry by prikey serverside mc_178BFBFF00610F01_140425106500082
	 * mc_178BFBFF00610F01_140425106500082
AMNamVvzohmAG5KugtgB5iNQJlLSNIgYGxv+gsPJVKrVCOR+wlZa7ISp52UX8nFaYLk98P0Jd7b7yv3CgBx0vw==

	 * @param args
	 */
	public static void main(String[] args) {
	//	String 
	//	String str=args[0];
	//	String keyfile=args[1];
		
//		System.out.println(new LicService().getMachiCode());
//		System.out.println(new LicService().getMachiCode_rsaEncryed(
//				"c:\\0key\\pub.txt", false));
		
		String mc="AMNamVvzohmAG5KugtgB5iNQJlLSNIgYGxv+gsPJVKrVCOR+wlZa7ISp52UX8nFaYLk98P0Jd7b7yv3CgBx0vw==";
		geneLic("c:\\0key\\pri.txt",mc,"c:\\0key\\lic.txt");
	}

	private static String geneLic(String prikey, String mc_wz_encry, String lic_file) {
		// String lic=
	String	mc=new LicService().getMachiCode_DecryByPrikey(prikey, mc_wz_encry);
				 try {
						String key = prikey;
//						if (!isStr)
 							key = filex.read(prikey, "gbk");
//						String mc = getMachiCode();
						byte[] mc_a = mc.getBytes("gbk");
						byte[] key_a;

						key_a = org.apache.commons.codec.binary.Base64.decodeBase64(key);

						byte[] lic_byte = RSACoder.encryptByPrivateKey(mc_a, key_a);
						String lic = Base64.encode(lic_byte, false);
						filex.save(lic, lic_file, "gbk");
						return lic;
						
					} catch (Exception e) {
						if(e instanceof InvalidKeySpecException)
						{
							System.out.println("--need prikey ,maybe input pubkey..");
						}
						ExUtil.throwEx(e);
					}
				return " $ex";
		
	}

	// mc_178BFBFF00610F01_140425106500082
	public String getMachiCode() {

		return "mc_" + HardWareUtils.getCPUSerial() + "_"
				+ HardWareUtils.getMotherboardSN();
	}
	
	public String getMachiCode_DecryByPrikey(String prikey_file, String mc_encryed) {
		 try {
			//	String key = prikey;
//				if (!isStr)
				String 	prikey = filex.read(prikey_file, "gbk");
				byte[] key_a = org.apache.commons.codec.binary.Base64.decodeBase64(prikey);
//				String mc = getMachiCode();
				byte[] mc_a =  org.apache.commons.codec.binary.Base64.decodeBase64(mc_encryed);
				byte[] a = RSACoder.decryptByPrivateKey(mc_a, key_a);
			    
				return new String(a);
				
			} catch (Exception e) {
				if(e instanceof InvalidKeySpecException)
				{
					System.out.println("--need prikey ,maybe input pubkey..");
				}
				ExUtil.throwEx(e);
			}
		return " $ex";
	}

	public String getMachiCode_rsaEncryed(String pubkeyBase64edOrFile,
			boolean isStr) {
		try {
			String key = pubkeyBase64edOrFile;
			if (!isStr)
				key = filex.read(pubkeyBase64edOrFile, "gbk");
			String mc = getMachiCode();
			byte[] mc_a = mc.getBytes("gbk");
			byte[] key_a;

			key_a = org.apache.commons.codec.binary.Base64.decodeBase64(key);

			byte[] a = RSACoder.encryptByPublicKey(mc_a, key_a);
			return Base64.encode(a, false);
			
		} catch (Exception e) {
			if(e instanceof InvalidKeySpecException)
			{
				System.out.println("--need pubkey ,maybe input prikey..");
			}
			ExUtil.throwEx(e);
		}
		// ExUtil.throwEx();
		return " not imp";
	}

	public String getPCid() {

		return HardWareUtils.getCPUSerial() + HardWareUtils.getMotherboardSN();
	}

}
