package com.attilax.clr.imp;

import com.attilax.clr.IClrerPart;
import com.attilax.clr.imp.MoveExcuter;
import com.attilax.io.filex;

public class EmptyDirClrerPartImp extends MoveExcuter implements IClrerPart{

	@Override
	public boolean isGabFile(Object object) {
		//pb28 
			 
		boolean emptyDir = filex.isEmptyDir(object);
		if(emptyDir)
			System.out.println("------pb30");
		return emptyDir;
	}

}
