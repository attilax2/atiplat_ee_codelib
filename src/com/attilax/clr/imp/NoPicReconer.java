package com.attilax.clr.imp;

import java.util.Set;

import com.attilax.clr.GabFileReconger;
import com.attilax.io.filex;
import com.attilax.text.strUtil;

public class NoPicReconer implements GabFileReconger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isGabFile(Object object) {
		Set picFileExts = strUtil.toSet("jpg,jpeg,png,bmp");
		String ext = filex.getExtName((String) object);
		if(ext==null || ext.equals(""))
		{
			filex.rename(object.toString(), object.toString()+".jpg");
			return false;
		}
		ext=ext.toLowerCase();
		if (!picFileExts.contains(ext))
			return true;
		else
			return false;
	}

}
