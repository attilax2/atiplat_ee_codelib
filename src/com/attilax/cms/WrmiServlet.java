package com.attilax.cms;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.io.pathx;
import com.attilax.ioc.IocXq214;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.up.FileUploadService;
import com.attilax.util.connReduceDync;
import com.attilax.web.ReqX;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

//@WebServlet(name = "indexResServlet_servfletName", urlPatterns = "/indexResServlet")
@Deprecated
public class WrmiServlet implements Servlet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {
		
	//paf	UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	@Override
	public void service(ServletRequest req, ServletResponse paramServletResponse)
			throws IOException, ServletException {
		//
		UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		connReduceDync c=new connReduceDync();
		String s="com.attilax,core/import.js,core/core.js,core/yaml.js|,cmsPhone4jobusImp/style.css ";
		c.base = pathx.webAppPath();
		c.output=c.base+"/index/jobus.js";   //index/jobus.js
		c.output_css=c.base+"/index/jobus.css";
		c.isGeneCss=false;
		c.recreateMode=true;
		
		
		Global.req.set((HttpServletRequest) req);
		Global.resp.set((HttpServletResponse) paramServletResponse);
		Global.mainCfg.set(req.getParameter("$maincfg"));
		if(req.getParameter("$maincfg")==null)
			Global.mainCfg.set(req.getParameter("$mainCfg"));
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		@SuppressWarnings("unused")
		String p = httpServletRequest.getContextPath();
		resp.set(paramServletResponse);
		// StrutsRequestWrapper
		// if( UrlDslParserx==null)
		paramServletResponse.setContentType("text/html;charset=utf-8");

		Map m = ReqX.toMap(req);// req.getParameterMap();

		String ret = new Wrmi().exe(m);
		// UrlDslParserx.exe((HttpServletRequest)req);
		writer.println(ret);

	}

}
