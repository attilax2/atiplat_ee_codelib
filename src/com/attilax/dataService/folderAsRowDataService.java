package com.attilax.dataService;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import aaaCms.Folder2Row;

import com.attilax.atian.PinyinX;
import com.attilax.json.AtiJson;
import com.attilax.lang.MapX;
//import com.attilax.net.UrlEncode;
import com.attilax.util.urlUtil;
import com.attilax.web.UrlX;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import static com.attilax.linq.LinqBuilder.eq;
import static com.attilax.linq.LinqBuilder.from;
import static com.attilax.linq.LinqBuilder4List.*;

public class folderAsRowDataService {

	public static void main(String[] args) {
		// Map<String, Integer> map = {"key" : 1};
		String url = "$op=q&$storeEngiee=folderAsRow&$table=" + ("Z:\\动作类");
		@SuppressWarnings("rawtypes")
		Map m = urlUtil.Param2Map(url);
		Object mm = new folderAsRowDataService().exe(m);
		System.out.println(AtiJson.toJson(mm));
	}

	public Map reqMap;
	
	Predicate<String> pre;

	public Object exe(Map m) {
		reqMap = m;
		if(m.get("$op").equals("q"))
			m.put("$op", "select");
		
		if (m.get("$op").equals("select")  && m.get("$where")==null ) {
			List<Map> folders = parse_by_storeEngiee(m.get("$storeEngiee"));
			// [{data_id=飓风营救3}, {data_id=飓风营救3}, {data_id=飓风营救3},
			// {data_id=飓风营救3}, {data_id=飓风营救3}]
			List<Map> rows = from(folders).exe();
			return rows;

		}
		if(m.get("$op").equals("select")&& m.get("$where")!=null)
		{
			
	//	// 
			  pre = (moiveName) -> {
				//  Map m=(Map) p;
				  if(PinyinX.getSimple(moiveName.toString().toLowerCase()).contains( m.get("kw").toString().toLowerCase() ))
				//System.out.print(moiveName);
				return true;return false;
			};
			
		//	};
			//search 
			List<Map> movs=Lists.newLinkedList();
			File f = new File(m.get("$table").toString());
			File[] parts = f.listFiles();
			int sum=0;
			for (File part : parts) {
				String partname=part.getName();
				if(partname.equals("爱情类"))
					System.out.println("dbg");
				Map tmp=MapX.colon(m);
				tmp.put("$table", m.get("$table")+"/"+partname);
				List<Map> rows=Lists.newLinkedList();
				
				try {
					rows=parse_by_storeEngiee4search(tmp.get("$storeEngiee"),tmp);
				} catch (Exception e) {
					
				}
				
				movs.addAll(rows);
				sum=sum+rows.size();
				if(sum>30)
					break;
			}
			return movs;
			
			
		}
		

		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	private List<Map> parse_by_storeEngiee4search(Object object,Map p) {
		List<Map> li = Lists.newArrayList();
		if (object.equals("folderAsRow")) {
			String path = (String) p.get("$table");
			String cate=path.substring(3);

			File f = new File(path);
			
			
			File[] fs = f.listFiles();
			for (File file : fs) {
			 	Map m = Maps.newLinkedHashMap();
			 	String movieName=file.getName();
			 	if(pre!=null)  //q44
			 		if(!pre.test(movieName))
			 			continue;
			 	m=new Folder2Row().toRow(file.getAbsolutePath());
			 	if(m.get("thumb")!=null)
			 		m.put("thumb", "maindir/"+cate+"/"+m.get("thumb"));
			//	m.put("data_id", file.getName());
			 	m.put("bigpic", m.get("thumb"));
			 	if(m.containsKey("data_id"))
			 		li.add(m);
			}

		}
		return li;

	}


	@SuppressWarnings("unchecked")
	private List<Map> parse_by_storeEngiee(Object object) {
		List<Map> li = Lists.newArrayList();
		if (object.equals("folderAsRow")) {
			String path = (String) reqMap.get("$table");
			String cate=path.substring(3);

			File f = new File(path);
			
			
			File[] fs = f.listFiles();
			for (File file : fs) {
			 	Map m = Maps.newLinkedHashMap();
			 	String movieName=file.getName();
			 	if(pre!=null)  //q44
			 		if(!pre.test(movieName))
			 			continue;
			 	m=new Folder2Row().toRow(file.getAbsolutePath());
			 	if(m.get("thumb")!=null)
			 		m.put("thumb", "maindir/"+cate+"/"+m.get("thumb"));
			//	m.put("data_id", file.getName());
			 	m.put("bigpic", m.get("thumb"));
			 	if(m.containsKey("data_id"))
			 		li.add(m);
			}

		}
		return li;

	}

}
