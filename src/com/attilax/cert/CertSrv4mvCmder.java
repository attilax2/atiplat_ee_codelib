/**
 * 
 */
package com.attilax.cert;

import com.attilax.io.FileExistEx;

/**  com.attilax.cert.CertSrv4mvCmder 
 * @author attilax
 *2016年12月6日 下午10:30:32
 */
public class CertSrv4mvCmder {

	/**
	attilax    2016年12月6日  下午10:30:32
	 * @param args
	 * @throws FileExistEx 
	 */
	public static void main(String[] args) throws FileExistEx {
		 new CertSrv4mv().geneCert(args[0]);

	}

}
