package com.attilax.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.attilax.io.filex;

/*
 *  com.attilax.sms.Sms1xinxi.send
 功能:		1xinxi.cn HTTP接口 发送短信

 说明:		http://sms.1xinxi.cn/asmx/smsservice.aspx?name=登录名&pwd=接口密码&mobile=手机号码&content=内容&sign=签名&stime=发送时间&type=pt&extno=自定义扩展码
 */
public class Sms1xinxi extends SmsService {

	private String SmsSign;

	public Sms1xinxi()
	{
		this.pwd = "ADF4C44D9930025A8EA017D18FD5";
		this.uname="zhida";
		this.SmsSign="";//职达巴士
	}

	public Sms1xinxi(String  name, String key_pwd) {
		this.pwd = key_pwd;
		this.uname=name;
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
//		String s = new Sms1xinxi("zhida","ADF4C44D9930025A8EA017D18FD5").addSmsSign("职达巴士").send(
//				"18573344543", "ttt"+filex.getUUidName());
//		System.out.println("rr:" + s);
		System.out.println(new Sms1xinxi("zhida","ADF4C44D9930025A8EA017D18FD5").addSmsSign("职达巴士").send(
				"18630157606", "ttt"+filex.getUUidName()));
		System.out.println(new Sms1xinxi("zhida","ADF4C44D9930025A8EA017D18FD5").addSmsSign("职达巴士").send(
				"18662795800", "ttt"+filex.getUUidName()));

	}

	private Sms1xinxi addSmsSign(String string) {
		this.SmsSign=string;
		return this;
	}

	private String pwd;
	private String uname;

	
	/**
	 * 0,2016040319043901506198394,0,1,0,提交成功
rr:0,2016040319043901506198394,0,1,0,提交成功
	 * @param mobile
	 * @param content
	 * @return
	 */
	public String send(String mobile, String content) {

		try {
			// 发送内容
			// String content = "第一信息 JAVA示例测试";
		//	String sign = "签名";

			// 创建StringBuffer对象用来操作字符串
			StringBuffer sb = new StringBuffer(
					"http://sms.1xinxi.cn/asmx/smsservice.aspx?");

			// 向StringBuffer追加用户名
			sb.append("name="+uname);

			// 向StringBuffer追加密码（登陆网页版，在管理中心--基本资料--接口密码，是28位的）
			sb.append("&pwd=" + this.pwd);

			// 向StringBuffer追加手机号码
			sb.append("&mobile=" + mobile);

			// 向StringBuffer追加消息内容转URL标准码

			sb.append("&content=" + URLEncoder.encode(content, "UTF-8"));

			// 追加发送时间，可为空，为空为及时发送
			sb.append("&stime=");

			// 加签名

			sb.append("&sign=" + URLEncoder.encode(this.SmsSign, "UTF-8"));

			// type为固定值pt extno为扩展码，必须为数字 可为空
			sb.append("&type=pt&extno=");
			// 创建url对象
			// String temp = new String(sb.toString().getBytes("GBK"),"UTF-8");
			System.out.println("sb:" + sb.toString());
			URL url;
			String returnStr;

			url = new URL(sb.toString());

			// 打开url连接
			HttpURLConnection connection;

			connection = (HttpURLConnection) url.openConnection();

			// 设置url请求方式 ‘get’ 或者 ‘post’

			connection.setRequestMethod("POST");

			// 发送
			InputStream is;

			is = url.openStream();

			// 转换返回值
			returnStr = Sms1xinxi.convertStreamToString(is);

			// 返回结果为‘0，20140009090990,1，提交成功’ 发送成功 具体见说明文档
			System.out.println(returnStr);
			// 返回发送结果
			return returnStr;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * 转换返回值类型为UTF-8格式.
	 * 
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {
		StringBuilder sb1 = new StringBuilder();
		byte[] bytes = new byte[4096];
		int size = 0;

		try {
			while ((size = is.read(bytes)) > 0) {
				String str = new String(bytes, 0, size, "UTF-8");
				sb1.append(str);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb1.toString();
	}

}
