package com.attilax.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import com.attilax.collection.ACollections;
import com.attilax.dbEngieer.DbStroreEngieen;
import com.attilax.json.AtiJson;
import com.attilax.lang.AString;
import com.attilax.lang.Closure;
import com.attilax.math.ANum;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import static com.attilax.linq.LinqBuilder.*;

public class TreeUtil {

	private String encode;
	private String path;

	public TreeUtil(String p, String e) {
		path = p;
		encode = e;
	}

	public static String lastDir;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 4>1,7>2,11>3,13>4
		// dir 2>2, 5>3,9>4, 13>5 17>6 20>7
		// countLengestFolderPath();
		final String start = "c:\\myoa";
		String dirFlagChar = "└├─";
		final String dirFlagChar1 = "─";
		AFile f = new AFile("c:\\myoa\\tree.txt", "gbk");
		final Set st = Sets.newConcurrentHashSet();
		lastDir = "";
		final DbStroreEngieen dbs = new DbStroreEngieen(pathx.webAppPath()
				+ "/db/db1");
		final List<Map> table = dbs.getTableRecs("dirLevel");
		f.trave(new Closure<String, Object>() {

			@Override
			public Object execute(String line) throws Exception {
				if (line.contains(dirFlagChar1)) {
					int preIndex = line.indexOf(dirFlagChar1) + 1;
					String curdirName = get_curdirName(preIndex, line);
					if( curdirName.equals("com"))
						System.out.println("dbg");
					Map row = dbs.getRow(table, "prefix", preIndex);
					int level_curdir = new ANum( row.get("lev1")).toInt();
					int level_lastdir = get_level_curdir(lastDir);
					if (level_curdir > level_lastdir) //src/com/zhaohui/oa/freemarker/templates
						lastDir = lastDir + "/" + curdirName;
					else if(level_curdir==level_lastdir)
						lastDir = getCurDirParent(level_curdir, lastDir) + "/"+curdirName;
					else
						lastDir = getCurDirParent(level_curdir, lastDir) + "/"
								+ curdirName;
					lastDir=  AString.LTrim(lastDir,"/");
					st.add(preIndex);
					String idex = new ANum(preIndex).toStr();

				//	System.out.println(idex + ":" + line);
					System.out.println(lastDir);
				}
				return null;
			}
//1    ".sett"
			private String getCurDirParent(int level_curdir, String lastDir) {
				String[] a = lastDir.split("/");
				List<String> li = Lists.newArrayList();
				for (int i = 0; i < level_curdir-1; i++) {
					li.add(a[i]);
				}
				return com.attilax.collection.CollectionUtils.join(li, "/");
			}

			private String get_curdirName(int preIndex, String line) {
				// TODO Auto-generated method stub
				return line.substring(preIndex);
			}

			private int get_level_curdir(String lastDir) {
				if(lastDir.equals("")) return 0;
				String[] a = lastDir.split("/");
				return a.length ;
			}
		});
		List li = Lists.newArrayList();
		li.addAll(st);
		Collections.sort(li);
		// List li2=
		// from(li).orderby().exe();

		// ACollections.set2list(st);
		System.out.println(AtiJson.toJson(li));

		System.out.println("--f");
	}

	private static void countLengestFolderPath() {
		String start = "c:\\myoa";
		String dirFlagChar = "└├─";
		final String dirFlagChar1 = "─";
		AFile f = new AFile("c:\\myoa\\tree.txt", "gbk");
		final Set st = Sets.newConcurrentHashSet();
		f.trave(new Closure<String, Object>() {

			@Override
			public Object execute(String line) throws Exception {
				if (line.contains(dirFlagChar1)) {
					int id = line.indexOf(dirFlagChar1) + 1;
					st.add(id);
					String idex = new ANum(id).toStr();
					if (id > 60)
						System.out.println(idex + ":" + line);
				}
				return null;
			}
		});
		List li = Lists.newArrayList();
		li.addAll(st);
		Collections.sort(li);
		// List li2=
		// from(li).orderby().exe();

		// ACollections.set2list(st);
		System.out.println(AtiJson.toJson(li));
	}

}
