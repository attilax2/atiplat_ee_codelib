package com.attilax.io;

import java.io.IOException;

import com.attilax.Closure;

/**
 * com.attilax.io.CountFileTypes
 * 
 * @author Administrator
 *
 */
public class CountFileTypes {

	public static void main(String[] args) throws IOException {
		String scanpath = "z:";
		String rzt_fileName = "c:\\filetype.csv";
		if (args.length > 0)
			scanpath = args[0];
		if (args.length >= 2)
			rzt_fileName = args[1].trim();
		if (!rzt_fileName.endsWith("csv")) {
			System.out.println("rzt file err:" + rzt_fileName);
			return;
		}
		final filex fx = new filex(rzt_fileName, "gbk");
		// String title_line =
		// "material_description,material_type,file_path,thumb,material_keyword";
		// System.out.println("line:" + title_line);

		dirx.trave(scanpath, new Closure<String, Object>() {

			@Override
			public Object execute(String f) throws Exception {
				String title_line = f + "," + filex.getExtName(f) + ","
						+ filex.getSize(f);
				System.out.println(title_line);
				fx.append_HP_Safe(title_line + "\r\n");
				return null;
			}
		});
		fx.close();
		System.out.println("--f");
	}

}
