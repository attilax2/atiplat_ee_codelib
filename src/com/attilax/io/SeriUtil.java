package com.attilax.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.attilax.exception.ExUtil;

public class SeriUtil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static void Serialize(Object o, String pathname) {
		// ObjectOutputStream
		// 对象输出流，将Person对象存储到E盘的Person.txt文件中，完成对Person对象的序列化操作

		ObjectOutputStream oo;
		try {
			// String pathname = "c:/Person.txt";
			FileOutputStream fileOutputStream = new FileOutputStream(new File(
					pathname));
			oo = new ObjectOutputStream(fileOutputStream);
			oo.writeObject(o);
			System.out.println("Person对象序列化成功！");
			oo.close();
		} catch (IOException e) {
			ExUtil.throwExV2(e);
		}

	}

	/**
	 * MethodName: DeserializePerson Description: 反序列Perons对象
	 * 
	 * @author xudp
	 * @return
	 * @return
	 * @throws Exception
	 * @throws IOException
	 */
	public static <t> t Deserialize(String pathname) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(new File(pathname)));
		} catch (IOException e) {
			ExUtil.throwExV2(e);
		}
		t person = null;
		try {
			person = (t) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			ExUtil.throwExV2(e);
		}
		System.out.println("Person对象反序列化成功！");
		return person;
	}

}
