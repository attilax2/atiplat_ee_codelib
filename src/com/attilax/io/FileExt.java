/**
 * 
 */
package com.attilax.io;

import java.util.function.Function;

import com.attilax.cmsPoster.ItemImp;
import com.attilax.cmsPoster.Traver;

/**
 * @author attilax
 *2016年4月9日 下午3:27:28
 */
public class FileExt {

	public String file_str;
	/**
	 * @param s
	 */
	public FileExt(String s) {
		this.file_str=s;
	}

	/**
	attilax    2016年4月9日  下午3:27:37
	 * @param s
	 * @param object
	 */
	public void trave( Function<String, Object> fun) {
		// TODO Auto-generated method stub
		new Traver(this.file_str).exe(new ItemImp() {
			
			@Override
			public void exe(String string) {
				fun.apply(string);
				
			}
		});
	}

}
