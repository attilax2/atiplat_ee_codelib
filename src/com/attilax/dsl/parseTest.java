/**
 * 
 */
package com.attilax.dsl;

import java.util.List;

import com.attilax.ast.AstBuilder;
import com.attilax.ast.AstParser;
import com.attilax.ast.Expression;
import com.attilax.fsm.JavaTokener;
import com.attilax.fsm.Token;
import com.attilax.json.AtiJson;

/**
 * @author attilax
 *2016年10月24日 下午5:26:58
 */
public class parseTest {
	
	public static void main(String[] args) {
		System.setProperty("cfgfile", "pay.ini");
		String a="  com.attilax.encry.RSACoder.main(['fixseed','c:/0k/pri.txt','c:/0k/pub.txt']) ";
		a="com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('123466',125,1598)";
		a="com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('123466':s,'125':i,'1598':i)";
		a="com.attilax.ioc.IocFacV3_iocx_iocutil.getBean(com.attilax.order.RechargeOrderService.class).insert('20161024_172851950':s,5:i,888:i)";
		//	args=" aaaPKg.DslParser.m3(123).m4()";
			List<Token> tokens = new JavaTokener(a).getTokensV4();// er(args).getTokens();
			System.out.println(AtiJson.toJson(tokens));
		 	Expression buildAst = new AstBuilder().buildAstV2(tokens);
			 Object rzt = new AstParser().parse(buildAst);
			 System.out.println(rzt);
	}

}
