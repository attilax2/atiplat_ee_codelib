package com.attilax.wrmi;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.ioc.IocXq214;
import com.attilax.lang.Exprs;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.up.FileUploadService;
import com.attilax.web.ReqX;
import com.attilax.web.UrlX;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

@WebServlet( name = "wrmiServlet_name", urlPatterns = "/Q5wrmiServletV2")
public class Q5wrmiServletV2 implements Servlet {

	public static void main(String[] args) {
	System.out.println("");

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {
		
	
		

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	

	@Override
	public void service(ServletRequest req, ServletResponse paramServletResponse)
			throws IOException, ServletException {
		UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		System.out.println("ss..");
		
		boolean isMultipart = ServletFileUpload.isMultipartContent((HttpServletRequest) req);
		PrintWriter writer = paramServletResponse.getWriter();
		if(isMultipart)
		{
			String ret=new FileUploadService().upload((HttpServletRequest) req);
			paramServletResponse.setContentType("text/html;charset=utf-8");
			writer.println(ret);
			return;
		}
//		if(req.getParameter("$method").equals("com.attilax.up.FileUploadService"))
//		{
//			HttpServletRequest req2 = (HttpServletRequest) req;
//			String ret = new FileUploadService().upload(req2);
//			paramServletResponse.setContentType("text/html;charset=utf-8");
//
//			paramServletResponse.getWriter().println(ret);
//			return;
//		}
		Global.req.set((HttpServletRequest) req);
		Global.resp.set((HttpServletResponse) paramServletResponse);
		Global.mainCfg.set(req.getParameter("$maincfg"));
		if(req.getParameter("$maincfg")==null)
			Global.mainCfg.set(req.getParameter("$mainCfg"));
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		@SuppressWarnings("unused")
		String p = httpServletRequest.getContextPath();
		resp.set(paramServletResponse);
		// StrutsRequestWrapper
		// if( UrlDslParserx==null)
		paramServletResponse.setContentType("text/html;charset=utf-8");

		Map m = ReqX.toMap(req);// req.getParameterMap();
		String QueryString=((HttpServletRequest)req).getQueryString();
		Map reqMap=(Map) UrlX.getHeader_from_QueryStr(QueryString);
		String code=(String) reqMap.get("code");
	//	List  Ast=getExprsLiAst(code);
	//	String ret =(String) parse(Ast);
		// UrlDslParserx.exe((HttpServletRequest)req);
	 	writer.println(" wrmi servlet v2 ret.");
	 	throw new RuntimeException("...");

	}

	
}
