package com.attilax.wrmi;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.Ast;
import com.attilax.lang.AstParser;
import com.attilax.lang.Exprs;
import com.attilax.lang.Global;
import com.attilax.ref.refx;
import com.attilax.up.FileUploadService;
import com.attilax.web.ReqX;
import com.attilax.web.UrlX;
import com.attilax.wrmi.JsnaInvoker;
import com.attilax.wrmi.Wrmi;
import com.google.inject.Inject;

/**com.attilax.wrmi.wrmiServletV3
 *  for agent 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * 
 * @author Administrator
 * 
 *         / /jsnaServlet?$method=com.attilax.jsna.test.add&param1=2&param2=3
 *
 */

@WebServlet( name = "Q68wrmiServletV3_name", urlPatterns = "/wrmiServletV3")
public class wrmiServletV3 implements Servlet {

	public static void main(String[] args) {
		System.out.println("aaaa");
		System.setProperty("agent_cfgfile","cfg_game.properties");
		 
	//	UrlDslParser UrlDslParserx ;
			String iocx="com.attilax.ioc.Ioc4agent";
			Global.iocCurV2.set(iocx);
		
		String code="new(com.attilax.user.AgentService).login(2981,111111)";
		AstParser astParser = new AstParser();
		List ast=astParser.getExprsLiAst(code);
	//	System.out.println(AtiJson.toJson( ast  ));
		String ret;  Object rztObj ;
		 
			    rztObj = astParser.parse(ast);
			    System.out.println(rztObj);
			    System.out.println("ff");
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {
		
	//qa5	UrlDslParserx = IocXq214.getBean(UrlDslParser.class);
		

	}

	public static ThreadLocal<ServletResponse> resp = new ThreadLocal<ServletResponse>();

	@Inject
	UrlDslParser UrlDslParserx;

	

	@Override
	public void service(ServletRequest reqx, ServletResponse paramServletResponse)
			throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) reqx;
		String iocx=req.getParameter("iocx");
		Global.iocCurV2.set(iocx);
		//Object o=	 com.attilax.reflect.MethodUtils.invokeStaticMethod(iocx, "getBean", (Object)UrlDslParser.class);
		UrlDslParserx = (UrlDslParser) com.attilax.reflect.MethodUtils.invokeStaticMethod(iocx, "getBean", (Object)UrlDslParser.class);
				//IocXq214.getBean(UrlDslParser.class);
		System.out.println("ss..");
		
		boolean isMultipart = ServletFileUpload.isMultipartContent((HttpServletRequest) reqx);
		PrintWriter writer = paramServletResponse.getWriter();
		if(isMultipart)
		{
			String ret=new FileUploadService().upload((HttpServletRequest) reqx);
			paramServletResponse.setContentType("text/html;charset=utf-8");
			writer.println(ret);
			return;
		}
 
		Global.req.set((HttpServletRequest) reqx);
		Global.resp.set((HttpServletResponse) paramServletResponse);
		Global.mainCfg.set(reqx.getParameter("$maincfg"));
		if(reqx.getParameter("$maincfg")==null)
			Global.mainCfg.set(reqx.getParameter("$mainCfg"));
	
		@SuppressWarnings("unused")
		String p = req.getContextPath();
		resp.set(paramServletResponse);
		// StrutsRequestWrapper
		// if( UrlDslParserx==null)
		paramServletResponse.setContentType("text/html;charset=utf-8");

		Map m = ReqX.toMap(reqx);// req.getParameterMap();
		String QueryString=((HttpServletRequest)reqx).getQueryString();
		Map reqMap=(Map) UrlX.getHeader_from_QueryStr(QueryString);
	//	String code=(String) reqMap.get("code");


		
		String code=reqx.getParameter("$code");
		AstParser astParser = new AstParser();
			List ast=astParser.getExprsLiAst(code);
 	//	System.out.println(AtiJson.toJson( ast  ));
			String ret;  Object rztObj ;
			try {
				    rztObj = astParser.parse(ast);
			    
			} catch (Exception e) {
				ret=AtiJson.toJson(e);
				paramServletResponse.getWriter().println(ret);
				return;
			}
			
			if(rztObj instanceof String)
				ret =(String)rztObj;
			else if(rztObj instanceof Integer)
				ret=String.valueOf(rztObj);
			else
				ret=AtiJson.toJson(rztObj);
		
 
		paramServletResponse.getWriter().println(ret);
		return;
	// 	writer.println(" wrmi servlet v2 ret.");
	// 	throw new RuntimeException("...");

	}

	
}
