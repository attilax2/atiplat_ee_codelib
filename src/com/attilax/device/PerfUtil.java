package com.attilax.device;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.attilax.asyn.AsynUtil;
import com.attilax.cmd.CmdX;
import com.attilax.collection.list;
import com.attilax.concur.TaskUtil;
import com.attilax.dataspider.BusPart;
import com.attilax.dataspider.TsaolyoNetDataSpider;
import com.google.common.collect.Lists;

public class PerfUtil {
	public static final Logger logger = LoggerFactory
			.getLogger(TsaolyoNetDataSpider.class);

	public static Integer getCpuRate_retNull() {
		try {
			return getCpuRate();
		} catch (CantGetData e) {
			return null;
		}
	}

	public static int getCpuRate() throws CantGetData {
		String getCpuRate = "wmic cpu get LoadPercentage";
		String ret = CmdX.exec(getCpuRate);
		logger.info("--wmic cpu:" + ret);
		String[] a = ret.split("\n");
		List<String> li = Lists.newArrayList();
		for (String string : a) {
			String line = string.trim();
			if (line.length() == 0)
				continue;
			li.add(line);
		}
		String cpu = null;
		if (li.size() == 1)
			throw new CantGetData(ret);
		// String cpu = li.get(li.size() - 1);
		// try {
		cpu = li.get(1).toString().trim();

		// } catch (Exception e) {
		// throw new
		// }

		return Integer.parseInt(cpu.toString());
	}

	public static void ajdCpuUseRate(Object object) {
		// TODO Auto-generated method stub

	}
	Calcor ccr ;
	public void ajdCpuUseRateV2(int max, Runnable reduceRun, Runnable upRun) {
		  ccr = new Calcor(100);
		int emecUse = 93;
		AsynUtil.execMeth_Ays(() -> {

			while (true) {
				int cpuuse = 100;
				try {
					cpuuse = getCpuRate();
				} catch (Exception e1) {
					reduceRun.run();
					 cpuuse = 85;
					 TaskUtil.sleep(50);continue;
				}

				if (cpuuse >= emecUse) // eme state
				{
					reduceRun.run();
					ccr.setInfo(cpuuse);
					TaskUtil.sleep(20);
					continue;

				}
				ccr.setInfo(cpuuse);
				int cpu_avg_use = ccr.getInt();

				if (cpu_avg_use > max) {
					reduceRun.run();
				}

				TaskUtil.sleep(100);
			}

		}, "perf_cpu_auto_reduce");

		AsynUtil.execMeth_Ays(() -> {

			while (true) {
				int cpuuse = 100;
				try {
					cpuuse = getCpuRate();
				} catch (Exception e1) {
					//cpuuse = 100;
					reduceRun.run();
					TaskUtil.sleep(500);continue;
					
				}
				if (cpuuse >= emecUse) { //emecstate
					reduceRun.run();
					ccr.setInfo(cpuuse);
					TaskUtil.sleep(30);
					continue;
				}
				ccr.setInfo(cpuuse);
				int cpu_avg_use = ccr.getInt();

				if (cpu_avg_use< max) {
					upRun.run();
				}
				
				
				TaskUtil.sleep(700);

			}
		}, "perf_cpu_auto_up_adj");

	}

	public static void ajdCpuUseRate(int max, Runnable reduceRun, Runnable upRun) {

		AsynUtil.execMeth_Ays(() -> {

			while (true) {
				int cpuuse = 0;
				try {
					cpuuse = getCpuRate();
				} catch (Exception e1) {
					reduceRun.run();
				}
				if (cpuuse >= 90) {
					reduceRun.run();
					continue;
				}
				if (cpuuse >= max)
					reduceRun.run();
				else
					upRun.run();

				try {
					TaskUtil.sleep(50);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		}, "perf_cpu_auto_adjst");

	}

	public Integer getCpuRate_avg() {
		if(ccr!=null)
		return  ccr.getInt();
		return -1;
	}
	public int bus_freq = 50;//ms
	public void ajdCpuUseRateV3() {
		AsynUtil.execMeth_Ays(() -> {

			while (true) {
				 
				for (BusPart busPart : busParts) {
					//busPart.incream();
					busPart.run();
				}
				System.out.println("--bus run....");
				TaskUtil.sleep(bus_freq);

			}
		}, "perf_bus_thrd");
		
	}
List<BusPart> busParts=Lists.newArrayList();
	public void addPart(BusPart taskPool) {
		busParts.add(taskPool);
		
	}

}
