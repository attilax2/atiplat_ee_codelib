/**
 * 
 */
package com.attilax.agent;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.attilax.db.DbServiceV3Q68;
import com.attilax.ioc.IocUtilV2;
import com.attilax.lang.Global;
import com.attilax.reflect.MethodUtils;
import com.attilax.service.BaseService;
import com.attilax.util.DataRang;
import com.google.common.collect.Maps;
import com.google.inject.Inject;

/**
 * com.attilax.agent.AgentRechargeService
 * 
 * @author attilax 2016年6月14日 下午11:22:31
 */
@SuppressWarnings("all")
public class AgentRechargeService extends BaseService {

	@Inject
	DbServiceV3Q68 dbsvr;

	/**
	 * attilax 2016年6月14日 下午11:22:31
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.setProperty("prj", "game");
		System.setProperty("cfgfile", "cfg_game.properties");
		System.setProperty("ioccfg", "aaaCfg.Ioc4game");
	//	="cfg_game.properties"
				System.setProperty("agent_cfgfile", "cfg_game.properties");
		
		
//		Map<String, Integer> m = Maps.newLinkedHashMap();
//		m.put("startTime", null);
//		m.put("endTime", null);
//		m.put("promoter", 0);
	//	new AgentRechargeService().getSubMemTotalByAgentId(m);
		
	// List li=IocUtilV2.getBean(AgentRechargeService.class)	 .getSubAgentNtotalsByParentAgentId("0");
//	 new(com.attilax.agent.AgentRechargeService).getSubMemTotalsRecycleByAgentId(promoter:$pid$,fld2:v2)
	AgentRechargeService bean = com.attilax.ioc.Ioc4agent.getBean(AgentRechargeService.class);
	List li=bean.getRchgTotal_groupby_SubAgent_where_ParentAgentId("888888");
//	Object[] params=new Object[1];
//	params[0]="promoter:0,fld2:v2";
//Object li=	MethodUtils.invoke(bean, "getSubMemTotalsRecycleByAgentId",params );
//	BigDecimal li=	bean	.getSubMemTotalsRecycleByAgentId("promoter:$pid$,fld2:v2");
	System.out.println( li);
	System.out.println("--f");

	}

	/**  only dircet ly  mem
	 * attilax 2016年6月14日 下午11:29:14
	 * 
	 * @param m
	 */
	public BigDecimal getSubMemTotalByAgentId(Map m) {
		BigDecimal SubMemTotal= new BigDecimal(0);
		String sql = "select sum(rmb) as sumx from acc_rechg where promoter=$pid$";
		sql = sql.replace("$pid$", m.get("promoter").toString());
		
		if(  m.get("promoter").toString().equals("0"))
			sql = "select sum(rmb) as sumx from acc_rechg where (promoter is null or promoter='') ";
		
		
		String where="";
		if(m.get("startDate")!=null&& m.get("startDate").toString().trim().length()>0)
			where=" and `time`>'"+m.get("startDate").toString()+" 00:00:01'";
		if(m.get("endDate")!=null&& m.get("endDate").toString().trim().length()>0)
			where=" and `time`<'"+m.get("endDate").toString()+" 23:59:59'";
		sql=sql+where;
		System.out.println(sql);
		
		List<Map> li = dbsvr.findBySql(sql);
		if (li.size() == 0)
			SubMemTotal= new BigDecimal(0);
		else
		{
			Map m2 = li.get(0);
			if(m2.get("sumx")!=null)
			SubMemTotal= new BigDecimal(m2.get("sumx").toString());
			else
				SubMemTotal=new BigDecimal(0);
		}
		
		return SubMemTotal; 

	}
	/*
	 * 
	 * get sum from rechg where referer is 
	 */
	public BigDecimal getSubMemTotalByAgentId_ByAgtOnly(String agtid, String startDate,String endDate) {
		 drDataRang=new DataRang();
			drDataRang.startDate=startDate;
			drDataRang.endDate=endDate;
			return getSubMemTotalByAgentId(agtid,Maps.newConcurrentMap());
	}
	
	public BigDecimal getSubMemTotalByAgentId(String agtid, Map m) {
		BigDecimal SubMemTotal = new BigDecimal(0);

		String sql = "select sum(rmb) as sumx from acc_rechg where promoter=$pid$ and `time`>'$start$' and `time`<'$end$'";
		sql = sql.replace("$pid$", agtid);
		if(drDataRang!=null)
		{
			if (drDataRang.startDate != null)
				sql = sql.replace("$start$", drDataRang.startDate);
			if (drDataRang.endDate != null)
				sql = sql.replace("$end$", drDataRang.endDate);
		}

		if (agtid.equals("0"))
			sql = "select sum(rmb) as sumx from acc_rechg where (promoter is null or promoter='') ";

		String where = "";
		if (m.get("startDate") != null && m.get("startDate").toString().trim().length() > 0)
			where = " and `time`>'" + m.get("startDate").toString() + " 00:00:01'";
		if (m.get("endDate") != null && m.get("endDate").toString().trim().length() > 0)
			where = " and `time`<'" + m.get("endDate").toString() + " 23:59:59'";
		sql = sql + where;
		System.out.println(sql);

		List<Map> li = dbsvr.findBySql(sql);
		if (li.size() == 0)
			SubMemTotal = new BigDecimal(0);
		else {
			Map m2 = li.get(0);
			if (m2.get("sumx") != null)
				SubMemTotal = new BigDecimal(m2.get("sumx").toString());
			else
				SubMemTotal = new BigDecimal(0);
		}

		return SubMemTotal;

	}
	
	/**
	 * meme and agent
	attilax    2016年6月27日  下午5:06:02
	 * @param mapParam
	 * @return
	 */
	@Deprecated
	public BigDecimal getSubMemTotalsRecycleByAgentId(Map mapParam) {
		BigDecimal SubMemTotal= getSubMemTotalByAgentId(mapParam);
		String agent_id= mapParam.get("promoter").toString();
		
		//calc sub agent 's  member recharge
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", agent_id);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
			Map m2=Maps.newLinkedHashMap();
			m2.putAll(mapParam);
			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getSubMemTotalsRecycleByAgentId(m2);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}
		
		
		return SubMemTotal; 

	}
	
	/**
	 * sub mem ,self ,and subagt
	attilax    2016年11月7日  下午5:14:57
	 * @param subAgentId
	 * @return
	 */
	@Deprecated
	public BigDecimal getRchgTotalsRecycleByAgentId(String subAgentId) {
		
		//Map m
		BigDecimal SubMemTotal= getSubMemTotalByAgentId(subAgentId,Maps.newConcurrentMap());
		String agent_id=subAgentId;
		
		BigDecimal self=getSelfTotalByUid(subAgentId);
		SubMemTotal=SubMemTotal.add(self);
		
		//calc sub agent 's  member recharge
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", agent_id);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
//			Map m2=Maps.newLinkedHashMap();
//			m2.putAll(subAgentId);
//			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getRchgTotalsRecycleByAgentId(sub_id);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}
		
		
		return SubMemTotal; 

	}
	
	/**
	 *and subagt    only
	 * not include  sub mem ,  and selft
	attilax    2016年11月7日  下午5:14:57
	 * @param subAgentId
	 * @return
	 */
	public BigDecimal getRchgTotalsRecycleByAgentId_onlySubAgt(String agent_id) {
		
		//Map m
	//	BigDecimal SubMemTotal= getSubMemTotalByAgentId(subAgentId,Maps.newConcurrentMap());
	// String agent_id=subAgentId;
		
	//	BigDecimal self=getSelfTotalByUid(subAgentId);
	//	SubMemTotal=SubMemTotal.add(self);
		
		BigDecimal SubMemTotal= new BigDecimal(0);
		
		//calc sub agent 's  member recharge
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", agent_id);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
//			Map m2=Maps.newLinkedHashMap();
//			m2.putAll(subAgentId);
//			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getRchgTotalsRecycleByAgentId_onlySubAgt(sub_id);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}
		
		
		return SubMemTotal; 

	}
 
	
	public BigDecimal getRchgTotalsRecycleByAgentId_SubagtWzSubmem(String agent_id) {
		
		//Map m
	 	BigDecimal SubMemTotal= getSubMemTotalByAgentId(agent_id,Maps.newConcurrentMap());
	// String agent_id=subAgentId;
		
	//	BigDecimal self=getSelfTotalByUid(subAgentId);
	//	SubMemTotal=SubMemTotal.add(self);
		
	//	BigDecimal SubMemTotal= new BigDecimal(0);
		
		//calc sub agent 's  member recharge
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", agent_id);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
//			Map m2=Maps.newLinkedHashMap();
//			m2.putAll(subAgentId);
//			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getRchgTotalsRecycleByAgentId_SubagtWzSubmem(sub_id);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}
		
		
		return SubMemTotal; 

	}
	DataRang drDataRang;
	
	public BigDecimal getRchgTotalsRecycleByAgentId_SubagtWzSubmem4Hiagt(String agent_id,String startDate,String endDate) {
		
		 drDataRang=new DataRang();
		drDataRang.startDate=startDate;
		drDataRang.endDate=endDate;
		//Map m
	 //	BigDecimal SubMemTotal= getSubMemTotalByAgentId(agent_id,Maps.newConcurrentMap());
	// String agent_id=subAgentId;
		
	//	BigDecimal self=getSelfTotalByUid(subAgentId);
	//	SubMemTotal=SubMemTotal.add(self);
		
	 	BigDecimal SubMemTotal= new BigDecimal(0);
		
		//calc sub agent 's  member recharge
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", agent_id);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
//			Map m2=Maps.newLinkedHashMap();
//			m2.putAll(subAgentId);
//			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getRchgTotalsRecycleByAgentId_SubagtWzSubmem(sub_id);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}
		
		
		return SubMemTotal; 

	}
	/**
	attilax    2016年11月7日  下午5:25:43
	 * @param subAgentId
	 * @return
	 */
	private BigDecimal getSelfTotalByUid(String subAgentId) {
	String sql="select sum(rmb) sumx from recharge where status=1 and accountId=@acc@";
	sql=sql.replace("@acc@", subAgentId);
	System.out.println(sql);
	List<Map> li = dbsvr.findBySql(sql);

	BigDecimal SubMemTotal= new BigDecimal(0);
	if (li.size() == 0)
		SubMemTotal= new BigDecimal(0);
	else
	{
		Map m2 = li.get(0);
		if(m2.get("sumx")!=null)
			SubMemTotal= new BigDecimal(m2.get("sumx").toString());
		else
			SubMemTotal=new BigDecimal(0);
	}
	
	return SubMemTotal; 
	
		//return null;
	}

	/**
	 * only subagetn total
	attilax    2016年6月27日  下午5:03:03
	 * @param pid
	 * @return
	 */
	public BigDecimal getSubAgentAllTotalsByParentId(String pid) {
		BigDecimal SubMemTotal=new BigDecimal(0);
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", pid);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String sub_id=(String) map.get("uid");
			Map m2=Maps.newLinkedHashMap();
			m2.put("promoter",sub_id);
			BigDecimal SubMemTotal2=getSubMemTotalsRecycleByAgentId(m2);
			SubMemTotal=SubMemTotal.add(SubMemTotal2);
		}

		return SubMemTotal;

	}
	
	
	 
		/**
		 * only subagetn total
		attilax    2016年6月27日  下午5:03:03
		 * @param pid
		 * @return
		 */
		public BigDecimal getSubAgentTotal(Map m) {
			BigDecimal SubMemTotal=new BigDecimal(0);
			String sql = " select uid from  agent where  parent_id=$pid$   ";
			sql = sql.replace("$pid$", m.get("pid").toString());
		
			
			List<Map> li = dbsvr.findBySql(sql);
			for (Map map : li) {
				String sub_id=(String) map.get("uid");
				Map m2=Maps.newLinkedHashMap();
				m2.putAll(m);
				m2.put("promoter",sub_id);
			
				BigDecimal SubMemTotal2=getSubMemTotalsRecycleByAgentId(m2);
				SubMemTotal=SubMemTotal.add(SubMemTotal2);
			}

			return SubMemTotal;

		}
		
		public BigDecimal getSubTotal_groupbySub(Map m) {
			return null;
			
		}
		@Deprecated
	public List getSubAgentNtotalsByParentAgentId(String pid) {
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", pid);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String subAgentId = (String) map.get("uid");
			Map m = Maps.newLinkedHashMap();
			m.put("promoter", subAgentId);
			BigDecimal money = getSubMemTotalsRecycleByAgentId(m);
			map.put("total", money);
		}

		return li;

	}
	
	Map otherParams=Maps.newConcurrentMap();
	/**
	 * 
	attilax    2016年11月14日  下午11:31:56
	 * @param pid
	 * @return
	 */
	public List getRchgTotal_groupby_SubAgent_where_ParentAgentId(String pid) {
		String sql = " select uid from  agent where  parent_id=$pid$";
		sql = sql.replace("$pid$", pid);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String subAgentId = (String) map.get("uid");
			Map m = Maps.newLinkedHashMap();
			m.put("promoter", subAgentId);
			BigDecimal money = getRchgTotalsRecycleByAgentId_onlySubAgt(subAgentId);
			map.put("total", money);
		}

		return li;

	}
	
	/**
	 * 
	attilax    2016年11月14日  下午11:31:56
	 * @param pid
	 * @return
	 */
	public List RchgSum_grpby_hiagt() {
		
		 drDataRang=iniDataRang();
			
			HttpServletRequest req = Global.req.get();
			String hiagtid=req.getParameter("prtid");
		String sql = " select * from  hiagt  where 1=1 ";
		if(hiagtid!=null)
			sql+="  and uid="+hiagtid;
		
		String hagt_startDate=req.getParameter("hagt_startDate");
		if(hagt_startDate!=null &&hagt_startDate.length()>0 )
		{
			hagt_startDate+=" 00:00:01";
			sql+="  and timex>'"+hagt_startDate+"'";
		}
		
		String hagt_endDate=req.getParameter("hagt_endDate");
		if(hagt_endDate!=null &&hagt_endDate.length()>0)
		{
			hagt_endDate+=" 23:59:59";
			sql+="  and timex<'"+hagt_endDate+"'";
		}
		System.out.println(sql);
		
		//sql = sql.replace("$pid$", pid);
		List<Map> li = dbsvr.findBySql(sql);
		for (Map map : li) {
			String hiagt_id = (String) map.get("uid");
			Map m = Maps.newLinkedHashMap();
			m.put("hiagt_id", hiagt_id);
		 	m.put("hiagt_nick", (String) map.get("nick"));
			BigDecimal money = getRchgTotals_byHiagt(hiagt_id);
			map.put("total", money);
		}

		return li;

	}

	/**
	attilax    2016年11月17日  下午9:40:25
	 * @param hiagt_id
	 * @return
	 */
	private BigDecimal getRchgTotals_byHiagt(String hiagt_id) {
		BigDecimal SubMemTotal = new BigDecimal(0);

		String sql = "select sum(rmb) as sumx from rchg_agt_hiagt where hiagt_id=$pid$ ";
		sql = sql.replace("$pid$", hiagt_id);
		if(drDataRang!=null)
		{
			if (drDataRang.startDate != null)
			{  String exp=" and `time`>'$start$' ";
				exp = exp.replace("$start$", drDataRang.startDate);
				sql+=exp;
			}
			if (drDataRang.endDate != null)
			{
				 String exp=" and `time`<'$end$'  ";
					exp = exp.replace("$end$", drDataRang.startDate);
					sql+=exp;
			}
			//	sql = sql.replace("$end$", drDataRang.endDate);
		}

		 
	 	System.out.println(sql);

		List<Map> li = dbsvr.findBySql(sql);
		if (li.size() == 0)
			SubMemTotal = new BigDecimal(0);
		else {
			Map m2 = li.get(0);
			if (m2.get("sumx") != null)
				SubMemTotal = new BigDecimal(m2.get("sumx").toString());
			else
				SubMemTotal = new BigDecimal(0);
		}

		return SubMemTotal;
	}



}
