/**
 * 
 */
package com.attilax.agent;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.attilax.collection.list;
import com.attilax.ioc.Ioc4agent;
import com.attilax.ioc.IocFacV3_iocx_iocutil;
import com.attilax.lang.Global;
import com.attilax.net.cookieUtil;
import com.attilax.net.requestImp;
import com.attilax.sql.SqlService;
import com.attilax.token.TokenServiceV2;
import com.attilax.token.TokenServiceV3;
import com.attilax.user.UserService;
import com.csmy.my.center.util.CTUtils;
import com.google.common.collect.Lists;
import com.google.inject.Inject;

/**
 * @author attilax 2016年11月2日 下午4:58:08
 */
public class AgtSrv extends UserService {
	
	public static void main(String[] args)   {
		
		 System.setProperty( "agent_cfgfile","cfg_game.properties" );
		AgtSrv srv=com.attilax.ioc.IocFacV3_iocx_iocutil.getBean("com.attilax.ioc.Ioc4agent",AgtSrv.class);
		srv=com.attilax.ioc.Ioc4agent.getBean(com.attilax.agent.AgtSrv.class);
		 
		//   null_uid_agentUtype
		requestImp ri=new requestImp();
		ri.setParam("$utype", "agent");
		Global.req.set(ri);
		Cookie ck=new Cookie("null_uid_agentUtyp", "200006");
		List  li=   Lists.newArrayList ();
		li.add(ck);
		ri.setcookies(li);
		try {
			System.out.println(srv.resetPwd("1111", "222222"));
		} catch (CantFindUserInTokenSrv e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--f");
		
	}
	
	@Inject 
	SqlService sqlSrv;
	
	@Inject
	TokenServiceV3 tkSrv;

	public Object resetPwd(String oldPwd, String newPwd) throws CantFindUserInTokenSrv {
		HttpServletRequest req = Global.req.get();
	//	HttpSession sess = req.getSession();
		String uid =  tkSrv.getuid();
		if(uid.equals(""))
			throw new CantFindUserInTokenSrv("uid:"+uid);
		// Set<String> capts = (Set<String>) sess.getAttribute("captSet");
		// if (capts == null)
		// throw new RuntimeException("chkex,capt_null,验证码错误capt_null");
		// if (!capts.contains(req.getParameter("captcha")))
		// throw new RuntimeException("chkex,capt_err,验证码错误");

		String sql = "update agent set pwd='$pwd$' where uid='$acc$' and pwd='$old$'";
		// String pwd = req.getParameter("password");
		sql = sql.replace("$old$", oldPwd);
		sql = sql.replace("$pwd$", newPwd);
		sql = sql.replace("$acc$", uid);

		System.out.println("--resetPwd:" + sql);
		return sqlSrv.executeUpdate(sql);
		// StringUtil.getAttrFromPro("ct_user_password", null)));

	}

}
