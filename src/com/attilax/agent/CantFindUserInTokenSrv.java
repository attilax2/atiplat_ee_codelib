/**
 * 
 */
package com.attilax.agent;

/**
 * @author attilax
 *2016年11月2日 下午5:23:04
 */
public class CantFindUserInTokenSrv extends Exception {

	/**
	 * @param string
	 */
	public CantFindUserInTokenSrv(String string) {
		super(string);
	}

}
