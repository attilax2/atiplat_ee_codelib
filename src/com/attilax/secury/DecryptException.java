/**
 * 
 */
package com.attilax.secury;

/**
 * @author attilax
 *2016年4月21日 下午5:36:51
 */
public class DecryptException extends RuntimeException {

	/**
	 * @param e
	 */
	public DecryptException(Exception e) {
		super(e);
	}

}
