/**
 * 
 */
package com.attilax.secury;

/**   com.attilax.secury.LoginException
 * @author attilax
 *2016年6月10日 下午5:04:26
 */
public class LoginException extends RuntimeException {

	/**
	 * @param string
	 */
	public LoginException(String string) {
		super(string);
	}

}
