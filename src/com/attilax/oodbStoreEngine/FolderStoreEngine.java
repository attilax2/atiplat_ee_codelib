package com.attilax.oodbStoreEngine;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.attilax.linq.AExpression;
import com.attilax.oodb.Partition;
import com.attilax.oodb.Table;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class FolderStoreEngine extends StoreEngine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public List<Map> where(Optional<AExpression> likeExpression) {
		List<Map> li = Lists.newArrayList();
		String dir = tab.storePath;
		List<String> partsDir_lst = getPartsDirs(likeExpression);

		// else

		// read lis..
		if (partsDir_lst.size() > 0) {
			for (String partName : partsDir_lst) {
				String part_fullpath = dir + "/" + partName;
				gerProps(li, partName, part_fullpath);
			}
		} else {
			gerProps(li, "", dir);
		}

		return li;
	}

	private void gerProps(List<Map> li, String partName, String part_fullpath) {
		String[] fs = new File(part_fullpath).list();
		for (String f : fs) {
			Map m = Maps.newConcurrentMap();
			tab.getProps(m, f, partName);
			li.add(m);
		}
	}

	private List<String> getPartsDirs(Optional<AExpression> likeExpression) {
		List<String> partsDir_lst = Lists.newArrayList();
		if (this.tab.parts.size() > 0) {
			likeExpression.ifPresent(x -> {

				List<String> partNames = Lists.newArrayList();
				if (hasContainPartiCol(likeExpression)) {
					partNames = partiStoreDir(likeExpression);

				} else {
					partNames = partiStoreDir();
				}
				for (String name : partNames) {
					// String partFulldir = name;
					partsDir_lst.add(name);
				}

			});
			if (!likeExpression.isPresent()) {
				return partiStoreDir();

			}

		} else {
			partsDir_lst.add("");
		}
		return partsDir_lst;
	}

	private List<String> partiStoreDir() {
		List<String> list = Lists.newArrayList();
		List<Partition> li = tab.parts;
		li.forEach(pt -> list.add(pt.colVal));
		return list;
	}

	private List<String> partiStoreDir(Optional<AExpression> likeExpression) {
		// TODO Auto-generated method stub
		List<String> li = Lists.newArrayList();
		li.add("爱情类");
		return li;
	}

	private boolean hasContainPartiCol(Optional<AExpression> likeExpression) {
		// TODO Auto-generated method stub
		return true;
	}

	public List<Map> select() {
		// TODO Auto-generated method stub
		return null;
		// return this;
	}

}
