package com.attilax.store;

import java.util.List;
import java.util.Map;

import com.attilax.db.DBX;
import com.attilax.io.filex;
import com.attilax.ioc.IocXq214;
import com.attilax.json.AtiJson;
import com.attilax.lang.MapX;
import com.attilax.orm.AOrm;
import com.attilax.orm.Aorm4view;
import com.attilax.orm.View;
import com.attilax.sql.Dsl2sqlService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Inject;

public class OrmStoreService extends StoreService {

	public static void main(String[] args) {
		System.setProperty("apptype", "bet");
		OrmStoreService ormSvr = IocXq214.getBean(OrmStoreService.class);
		Map m = Maps.newLinkedHashMap();
		m.put("$table", "orderView");
		m.put("$tabletype", "view");
		m.put("$view_store_path","com/attilax/order");
		m.put("$op", "insert");
		m.put("good_id","2005");
		m.put("order_id",filex.getUUidName());
		
		List<String> insertSql = ormSvr.insertSql(m);
		System.out.println( AtiJson.toJson( insertSql));
		System.out.println("--f1");
	}

	@Inject
	Dsl2sqlService dsl2sqlSvr;
	@Inject
	DBX dbSvr;

	public List<String> insertSql(Map m) {
		List<String> li = Lists.newArrayList();
		if (m.get("$tabletype") != null && m.get("$tabletype").equals("view")) {
			String[] tabs = new View(m.get("$table"), m.get("$view_store_path"))
					.getTables();
			for (String tab : tabs) {
				Map m2 = MapX.clone(m);
				m2.put("$table", tab);
				String s = get_insert_singleTable_sql(m2);
				li.add(s);
			}

			return li;
		}
		return li;
	}

	public void insert(Map m) {

		List<String> li = insertSql(m);
		for (String sql : li) {
			dbSvr.execSql(sql);
		}

	}

	public String get_insert_singleTable_sql(Map m) {

		String sql = dsl2sqlSvr.dsl2sql(m);
		return sql;

	}

}
