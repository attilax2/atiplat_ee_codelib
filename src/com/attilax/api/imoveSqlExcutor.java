package com.attilax.api;

import java.util.HashMap;
import java.util.Map;

import com.attilax.cms.Trigger4elemtShow;
import com.attilax.io.pathx;
import com.attilax.lang.ParamX;
import com.attilax.net.requestImp;

import aaaCfg.IocX;

/**
 * com.attilax.api.imoveSqlExcutor
 * 
 * @author Administrator
 *
 */
public class imoveSqlExcutor {

	public static void main(String[] args) {
		IocX.dbcfg_file = pathx.classPathParent() + "/db_imovie.ini";
		IocX.ini();
		String url = " param=select  * from gv_material limit 10  ";
		System.out.println(new imoveSqlExcutor().exe(url));
	}

	public Object exe(Object obj) {
		IocX.cfgMap.put("uploadfiles", "z:");

		ApiSqlHandler ax = IocX.getBean(ApiSqlHandler.class);
		ax.trigger = new Trigger4elemtShow();
		// requestImp ri = new requestImp();
		// ri.setParam("http_param", "select 1");
		// System.out.println(ax.handleReq(ri));
		Map m = new ParamX().urlParams2Map((String) obj);
		requestImp ri = new requestImp();
		ri.setParam("http_param", m.get("param"));

		return ax.handleReq(ri);

	}

}
