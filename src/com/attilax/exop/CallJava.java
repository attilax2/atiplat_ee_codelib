package com.attilax.exop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;

import com.attilax.Closure;

public final class CallJava extends BrowserFunction {
    public static Map eventMap=new HashMap();
	public static void main(String[] args) {
		System.out.println("--");
	}

        /**
         * Constructor
         * 
         * @param arg0
         * @param arg1
         */
        public CallJava (Browser arg0, String arg1) {
            super(arg0, arg1);
        }

        /**
         * {@inheritDoc}
         * 
         * @see org.eclipse.swt.browser.BrowserFunction#function(java.lang.Object[])
         */
        @Override
        public Object function(Object[] arg0) {
           // logger.debug("ImageSelect");
        	String meth=arg0[0].toString();
        	List li=new ArrayList ();
        	 for(int i=1;i<arg0.length;i++)
        	 {
        		 li.add(arg0[i]);
        	 }
        	
        	
        	Closure evet=(Closure) eventMap.get(meth);
        	try {
			return	evet.execute(li);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return super.function(arg0);
        }

    }
