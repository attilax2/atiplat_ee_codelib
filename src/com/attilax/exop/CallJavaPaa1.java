package com.attilax.exop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.BrowserFunction;

import com.attilax.Closure;
import com.attilax.hre.UrlDslParser;
import com.attilax.lang.net.HRE_urlparamsDSLparser;
import com.attilax.lang.net.HRE_urlparamsDSLparserV2;
import com.attilax.lang.net.WebBrowserImp_swtBrow;

public final class CallJavaPaa1 extends BrowserFunction {
    public static Map eventMap=new HashMap();
	public static void main(String[] args) {
		System.out.println("--");
	}
	Browser brow;

        /**
         * public InitFunction(Browser browser, String name) { 
    //name 为该函数的名字，JavaScript 根据这个名字调用该函数
    super(browser, name); 
  } 
         * Constructor
         * 
         * @param arg0
         * @param arg1
         */
        public CallJavaPaa1 (Browser arg0, String arg1) {
            super(arg0, arg1);
           // System.out.println("==CallJavaPaa1 cons fun exe  ");
            brow=arg0;
        }

        /**
         * {@inheritDoc}
         * 
         * @see org.eclipse.swt.browser.BrowserFunction#function(java.lang.Object[])
         */
        @SuppressWarnings("all")
		@Override
        public Object function(Object[] arg0) {
           // logger.debug("ImageSelect");
        	//paa
        	try {
        		if(	 ( (String)arg0[1]).trim().toLowerCase().equals("hre"))
    			{
    				String parameters = (String)arg0[0];
					WebBrowserImp_swtBrow webBrowser = new WebBrowserImp_swtBrow();
					new   HRE_urlparamsDSLparserV2(webBrowser).exe(parameters);
    				return null ;
    			}
			} catch (Exception e) {
				// TODO: handle exception
			}
        	
        	if(arg0[0].toString().contains("getRows_callback"))
        		System.out.println("");
        	//q17
        	try {
        		if(	 ( (String)arg0[1]).trim().toLowerCase().equals("hre3"))
    			{
        			try {
        				String parameters = (String)arg0[0];
    					WebBrowserImp_swtBrow webBrowser = new WebBrowserImp_swtBrow(this.brow);
    					new   UrlDslParser(webBrowser).exe(parameters);
        				return null ;
					} catch (Exception e) {
						return null;
					}
    				
    			}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
        	String meth=arg0[0].toString();
        	List li=new ArrayList ();
        	 for(int i=1;i<arg0.length;i++)
        	 {
        		 li.add(arg0[i]);
        	 }
        	
        	
        	Closure evet=(Closure) eventMap.get(meth);
        	try {
			return	evet.execute(li);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return super.function(arg0);
        }

    }
