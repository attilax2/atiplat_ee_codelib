/**
 * 
 */
package com.attilax.vcf;

import java.util.List;

import com.attilax.io.filex;

/**
 * @author attilax
 *2016年11月25日 下午5:39:14
 */
public class VcfT {

	/**
	attilax    2016年11月25日  下午5:39:15
	 * @param args
	 */
	public static void main(String[] args) {
		String s="C:\\Users\\Administrator\\Pictures\\1146792035.vcf";
		List<String> li=filex.read2list(s);
		for (String string : li) {
			if(!string.contains("TEL"))
				continue;
			String[] a=string.split(":");
		
			if(a.length>0) {
				String x = a[a.length-1];
				x=fmt(x);
				if(isCp(x))
				System.out.println(x);
			}
		}

	}

	/**
	attilax    2016年11月25日  下午5:46:43
	 * @param x
	 * @return
	 */
	private static boolean isCp(String x) {
		if(x.startsWith("1") && x.length()==11)
			return true;
		return false;
	}

	/**
	attilax    2016年11月25日  下午5:45:10
	 * @param x
	 * @return
	 */
	private static String fmt(String x) {
		 x=x.replace("-", "");
		 x=x.replace(" ", "");
		 x=x.replace("+86", "");
		return x;
	}

}
