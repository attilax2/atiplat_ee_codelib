package com.attilax.dbEngieer;

import java.util.List;
import java.util.Map;

import com.attilax.io.filex;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

 

public class DbStroreEngieen {

	private String db;

	public DbStroreEngieen(String db) {
		this.db=db;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	List<Map> rows;
	@SuppressWarnings("all")
	public List<Map> getTableRecs(String string) {
		String f=db+"/"+string+"/"+string+".csv";
		List<String> li=filex.read2list(f);
		String schema=li.get(0);
		
		Map schema_mp = getSchema(schema);
		  rows=Lists.newArrayList();
		int i=0;
		for (String string2 : li) {
			if(i==0)
			{
				i++;continue;
			}
				
			String[] row_stra=string2.split(",");
			
			Map row = getRow(schema_mp, row_stra);
			rows.add(row);
			i++;
		}
		
		return rows;
	}

	private Map getRow(Map schema_mp, String[] row_stra) {
		Map row=Maps.newConcurrentMap();
		for (int j = 0; j < row_stra.length; j++) {
			String col=(String) schema_mp.get(j);
			if(col==null)break;
			String v=row_stra[j];
			row.put(col,v);
		}
		return row;
	}

	private Map getSchema(String schema) {
		Map schema_mp=Maps.newConcurrentMap();
		String[] a=schema.split(",");
		for (int i = 0; i < a.length; i++) {
			schema_mp.put(i, a[i]);
		}
		return schema_mp;
	}

	public Map getRow(List<Map> table, String col, int v) {
		 for (Map map : table) {
			if(map.get(col).toString().equals(String.valueOf(v)))
				return map;
		}
		return null;
	}

}
