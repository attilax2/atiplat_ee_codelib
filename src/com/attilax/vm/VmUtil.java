package com.attilax.vm;

import java.util.List;
import java.util.Stack;

import com.google.common.collect.Lists;

public class VmUtil {

	public static Object[] pop2objArr(Stack stack, int paramNum) {
		Object[] objs_tmp = new Object[paramNum];
		for (int i = 0; i < paramNum; i++) {
			objs_tmp[i] = stack.pop();
		}
		return objs_tmp;
	}

	public static Object[] pop2objArr(Stack stack) {

		List<Object> params_li = Lists.newArrayList();

		while (true) {
			if (stack.isEmpty())
				break;
			Object o = stack.pop();
			params_li.add(o);

		}
		return params_li.toArray();
	}

}
