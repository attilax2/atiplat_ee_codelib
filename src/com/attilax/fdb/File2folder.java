package com.attilax.fdb;

import java.io.File;

import com.attilax.io.filex;


/**
 * com.attilax.fdb.File2folder
 * @author Administrator
 *
 */
public class File2folder {

	public static void main(String[] args) {
		String s=args[0];  //D:\molwe
		File dir = new File(s);
		File[] files = dir.listFiles();
		for (File file : files) {
			if(file.isDirectory())
				continue;
			exec(file);
		}

	}

	private static void exec(File file) {
		try {
			String f = file.getAbsolutePath();
			String mainName=filex.getFileName_noExtName(f);
			String path=file.getParent();
		    String newDir = path+"/"+mainName;
			filex.createAllPath(newDir+"/tt.txt");
		    String target = newDir+"/"+file.getName();
			filex.move(f, target);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
