package com.attilax.fdb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.attilax.coll.ListX;
import com.attilax.io.dirx;
import com.attilax.io.pathx;
import com.attilax.lang.MapX;
import com.attilax.lang.YamlAtiX;
import com.attilax.lang.text.strUtil;

public class FdbX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Map sql=  MapX.newMap();
//		sql.put("op", "select");	sql.put("table", "d:/z");
//		Map cols=MapX.newOrderMap();
//		cols.put("img",);

	}

	private Map ssql;
	private Object table;
	
	public Object exe(Map ssql)
	{
		if(ssql.get("op").equals("select"))
		{
			return executeQuery(ssql);
		}
		
		if(ssql.get("op").equals("update") ||ssql.get("op").equals("insert")||ssql.get("op").equals("delete"))
		{
			return executeUpdate(ssql);
		}
		return ssql;
		
	}

	private Object executeUpdate(Map ssql) {
		// TODO Auto-generated method stub
		return null;
	}
	ColsGener colsGener;
	private Map whereExprs;
	private List executeQuery(Map ssql) {
		this.ssql=ssql;
		this.table=ssql.get("table");
		colsGener=(ColsGener) ssql.get("cols");
		
		this.whereExprs = (Map) ssql.get("where");
		List<String> partitions = ListX.newList();
		if (whereExprs.containsKey("cate")) {
			String cate_id = strUtil.toStr( whereExprs.get("cate"));
			String PatitionFolderName = getCateCnnameDir(cate_id);
			partitions.add(PatitionFolderName);
			// List<Map> list_t=list_by_partition(PatitionFolderName);
			// return list_t;
		} else
			partitions = listPatition();

		List<Map> li=new ArrayList<Map>();
		for (String part : partitions) {
			List<Map> list_t=list_by_partition(part);
			li.addAll(list_t);
		}
		return li;
	}
	
	private String getCateCnnameDir(String cate) {
		String s = pathx.classPathParent_jensyegeor() + "/cate.txt";
	//	cate_file = s;
		Map m = YamlAtiX.getMap(s);
		return (String) m.get(cate);
	}
	public List<Map> list_by_partition(String partition) {
	 
		String cur_partition_dirname = this.table+"/" + partition;
		List<String> subDirs = new ArrayList<String>();
	
		try {

			subDirs = new dirx(cur_partition_dirname).subDirs();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<String> mov_names = subDirs;
		List<Map> movs = new ArrayList<Map>();
		for (String mov_name : mov_names) {
			try {
				String resDir = cur_partition_dirname + "/" + mov_name;
			 
				Map mv =(Map) colsGener.exe(resDir);
			
				 
				
				movs.add(mv);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return movs;
	}

	private List<String> listPatition() {
		List<String> subDirs = new dirx((String) this.table).subDirs();
		return subDirs;
	}

	
	 
}
