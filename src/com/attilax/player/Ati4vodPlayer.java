/*
 * Christopher Deckers (chrriis@nextencia.net)
 * http://www.nextencia.net
 *
 * See the file "readme.txt" for information on usage and redistribution of
 * this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */
package com.attilax.player;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

//import org.eclipse.swt.widgets.Display;
//import org.seleniumhq.jetty7.webapp.WebXmlConfiguration;




















//import aaaBlogger.EncodeX;
import aaaCfg.IocX;

import com.attilaax.encode.EncodeX;
import com.attilax.Closure;
import com.attilax.core;
import com.attilax.exception.ExUtil;
import com.attilax.exception.MsgBoxSrv;
import com.attilax.io.FileNotExist;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.ioc.Ioc4vodV2;
import com.attilax.ioc.ioc4vod;
import com.attilax.laisens.LaisensManger;
import com.attilax.laisens.OverTimeEx;
import com.attilax.lang.CmdX;
import com.attilax.lang.GUIx;
import com.attilax.lang.ParamX;
import com.attilax.lang.Trigger;
import com.attilax.lang.net.BrowserApi_nativeSwingImp;
import com.attilax.lang.net.HRE_urlparamsDSLparser;
import com.attilax.lang.process.ProcessX;
import com.attilax.log.logxT;
import com.attilax.media.MediaInfo;
import com.attilax.net.urlUtil;
import com.attilax.net.websitex;
import com.attilax.ui.MsgBox;
import com.attilax.util.IPUtil;
import com.attilax.util.PropX;
import com.attilax.util.randomx;
import com.google.inject.Inject;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserAdapter;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;
import chrriis.dj.nativeswing.swtimpl.demo.examples.vlcplayer.AtiVLCPlayer;

/**  com.attilax.player.Ati4vodPlayer
 * @author Christopher Deckers
 */
public class Ati4vodPlayer {
	
	//@Inject
	//public static Trigger trig;

	// public class Cmd1Listener extends WebBrowserAdapter {
	// @Override
	//
	//
	// }
	public static String cfgPath;
	public static Map jsCmdMap;
	public static JWebBrowser webBrowser ;//= new JWebBrowser();
	public static JComponent createContent() {
		System.out.println("--aa");
		JPanel contentPane = new JPanel(new BorderLayout());
		JPanel webBrowserPanel = new JPanel(new BorderLayout());
		// webBrowserPanel.setBorder(BorderFactory.createTitledBorder("--"));
		// JWebBrowser.useXULRunnerRuntime()
	//	final 
		String url = "http://www.baidu.com";

		url = "http://localhost/vod/11/jsjava.htm";
		cfgPath = getCfg();
	//	IocX.cfgPath=cfgPath;

		url = getIndexpage();
		url=url+"?"+randomx.random(99999999);
		System.out.println("--open url:" + url);
		webBrowser.navigate(url);
		// ati
		webBrowser.setDefaultPopupMenuRegistered(false);// �����Ҽ������˵�
		webBrowser.setBarsVisible(false);// ���������������
		webBrowser.setJavascriptEnabled(true);
		webBrowser.setAutoscrolls(false);
		// webBrowser.setv
		// webBrowser.set
		// webBrowser.addWebBrowserListener(listener)
		// webBrowser.addWebBrowserListener(new Cmd1Listener());
		webBrowser.addWebBrowserListener(new WebBrowserAdapter() {

			// �������ؽ���
			public void loadingProgressChanged(WebBrowserEvent e) {
				// ���������ʱ
				if (e.getWebBrowser().getLoadingProgress() == 100) {
					// String result = (String) webBrowser
					// .executeJavascriptWithResult("return '123';"
					// .toString());
					// System.out.println(result);
				//	GUIx.cfgPath=cfgPath;
					GUIx.hideMouse();
				}
			}

			// js java comm
			public void commandReceived(WebBrowserCommandEvent e) {
				String command = e.getCommand();
				System.out.println("  receve commd: " + command);
				Object[] parameters = e.getParameters();
				//pa6
				if(	 ( (String)parameters[0]).trim().toLowerCase().equals("hre"))
				{
					new   HRE_urlparamsDSLparser(webBrowser).exe(command);
					return;
				}
				
				if ("cmd1".equals(command)) {
					String html = (String) parameters[0];
					System.out.println(html);
				}

				
			
				if ("closewin".equals(command)) {
				//	String html = (String) parameters[0];
				//	System.out.println(html);
					System.exit(0);
				}
				if ("play".equals(command)) {
				//	new Ati4vod().	play(parameters);
					System.out.println("--");
					// AtiVLCPlayer.getInstance().ini(new String[]{url});
					return;
				}
				//end play 
				
				//ati p7L
				Object cmdHandler = jsCmdMap.get(command);
				if(cmdHandler==null)   //pa1
				{
					new BrowserApi_nativeSwingImp(webBrowser).exe(parameters);
					return;
				}
					
				Closure comHander=(Closure) cmdHandler;
				try {
					comHander.execute(parameters);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
				//	e1.printStackTrace();
				}
				
				//p7l end

			}

		

		
		});

		// //ati end
		webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
		// webBrowserPanel.setv

		contentPane.add(webBrowserPanel, BorderLayout.CENTER);
		// Create an additional bar allowing to show/hide the menu bar of the
		// web browser.
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1,1));
		JCheckBox menuBarCheckBox = new JCheckBox("Menu Bar",
				webBrowser.isMenuBarVisible());
		menuBarCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				webBrowser.setMenuBarVisible(e.getStateChange() == ItemEvent.SELECTED);
			}
		});
		
	
		
		//p929
		buttonPanel.add(new JButton("mini") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mainFrame.setSize(200, 200);
						mainFrame.setLocation(500, 500);
					}
				});

			}
		});
		buttonPanel.add(new JButton("max") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);  
					}
				});

			}
		});
		////p929
		
		buttonPanel.add(new JButton("exit..") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});

			}
		});
		
		buttonPanel.add(menuBarCheckBox);
		buttonPanel.add(new JButton("test play..") {
			{

				this.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String url = PropX.getConfig(cfgPath, "testMv");
						System.out.println(" will play:" + url);
						AtiVLCPlayer.getInstance().ini(new String[] { url });
					}
				});

			}
		});
		contentPane.setBackground(Color.black);// ati
		// ati p3g
		int showmenu = 1;
		try {
			showmenu = Integer.parseInt(PropX.getConfig(cfgPath, "showmenu")
					.trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (showmenu == 1)
			contentPane.add(buttonPanel, BorderLayout.SOUTH);
		return contentPane;
	}
	private static String getIndexpage() {
		String url;
		url = PropX.getConfig(cfgPath, "listpage");
		String[] a=url.split(",");
//		if(a.length==1)
//			return url;
//		else
//		{
			for (String ur : a) {
				
				try {
					//pac
					 if(!ur.trim().startsWith("http:"))
					 {
						 ur=pathx.classPathParent_jensyegeor()+"/"+ur;
						 if(new File(ur).exists())
							 return ur;
					 }
					 else
					 {
						websitex.WebpageContent(ur);
						return ur;
					 }
				} catch (Exception e) {
					System.out.println( " this url is cant use:"+ur);
					 e.printStackTrace();
				}
			}
			
			
//		}
		return pathx.classPathParent_jensyegeor()+"/aimuh5/index.html";
		//throw new RuntimeException("no ok url");
	}
@Inject
	private MsgBoxSrv MsgBoxServ;
	private void play(Object[] parameters) {
		ini();
		
//		try {
//			new LaisensManger().checkOvertime_1y("2015-11-12");
//		} catch (OverTimeEx e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			return;
//		}
		try {
			checkPlayerProcessExist();
			checkMovieLibExist();
		
		String video_url = (String) parameters[0];
		IocX.ini();
	//	checkAuthcode();

		countPlyaerNCoperyCount(parameters);
		
		
		//-------------get move url
		video_url = video_url.replaceAll("/vdx/", "");
		System.out.println(video_url);
		String playserver =getPlayServer();
		String url_full =playserver + "/"
				+ video_url;
		// p48
		if (playserver.trim().startsWith("\\\\"))
			url_full = playserver + "\\" + video_url;

		File f = new File(url_full);
		if (!f.exists()) {
			url_full=pathx.fixSlash(url_full);
			String string = "file maybe not exist文件可能不存在::" + url_full;
			System.out.println(string);
			throw new FileNotExist(string);
		//	return;
		}
		url_full = url_full.replaceAll("/", "\\\\");
		System.out.println(" will play:" + url_full);
		// will
		// play:z:/爱情类\五十度灰.未删减\五SD灰.未删减.Fifty.Shades.of.Grey.2015.DVD.X264.AAC.English.CHS.Mp4Ba
		// (2).mp4
		
		//--------------------get plyaer path
        String player = getPlayer(parameters, url_full);
        if(!new File(player).exists())
        {
        	player=pathx.fixSlash(player);
        	String string = "player maybe not exisit文件可能不存在::" + player;
			System.out.println(string);
		//	MsgBox_show(string);
			throw new FileNotExist(string);
        //	return;
        }
     //   String url_full_final=pathx.fixSlash(url_full);
        //pab must use backSlash mode .beir srt cant show ..cant auto load
      //  url_full=url_full.replace(" ", "  ");
        player=pathx.fixSlash(player);
		final String cmd = "\"" + player + "\" \"" + url_full + "\"";
		System.out.println(cmd);
		
		
		
		
		core.newThread(new Runnable() {
			
			@Override
			public void run() {
				CmdX.exec(cmd);
				
			}
		}, "main_therdad ");

	
		toniceBoxSys_thread(url_full);
		
		
	 
		System.out.println("---play evenr finish");
		
		} catch (Exception e) {
			e.printStackTrace();
			MsgBox_show("e::" + core.getTrace(e));
		}
	}
	
	
private void ini() {
		if(this.MsgBoxServ==null)
			this.MsgBoxServ=	Ioc4vodV2.getBean(MsgBoxSrv.class);
		
	}
public void play(  String video_url) {
		
	video_url=urlUtil.decodeByUtf8(video_url);
	String string1 = "2017-12-12";
		try {
		
			new LaisensManger().	checkOvertime_1y(string1);
		} catch (OverTimeEx e1) {
			 
			ExUtil.throwExV3(e1, "exp:"+string1);
		}
		try {
			checkPlayerProcessExistV2(getPlayer2(null));
			checkMovieLibExist();
		
	//	String video_url = (String) parameters[0];
	//	IocX.ini();
	//	checkAuthcode();

	//	countPlyaerNCoperyCount(parameters);
		
		
		//-------------get move url
		String url_full = getVurlFull(video_url);
		System.out.println(" will play:" + url_full);
		// will
		// play:z:/爱情类\五十度灰.未删减\五SD灰.未删减.Fifty.Shades.of.Grey.2015.DVD.X264.AAC.English.CHS.Mp4Ba
		// (2).mp4
		
		//--------------------get plyaer path
        String player = getPlayer2( url_full);
        if(!new File(player).exists())
        {
        	player=pathx.fixSlash(player);
        	String string = "player maybe not exisit文件可能不存在::" + player;
			System.out.println(string);
			MsgBox_show(string);
        	return;
        }
     //   String url_full_final=pathx.fixSlash(url_full);
        //pab must use backSlash mode .beir srt cant show ..cant auto load
      //  url_full=url_full.replace(" ", "  ");
        player=pathx.fixSlash(player);
		final String cmd = "\"" + player + "\" \"" + url_full + "\"";
		System.out.println(cmd);
		
		
		
		
		core.newThread(new Runnable() {
			
			@Override
			public void run() {
				CmdX.exec(cmd);
				
			}
		}, "main_therdad ");

	
		toniceBoxSys_thread(url_full);
		
		
	 
		System.out.println("---play evenr finish");
		
		} catch (Exception e) {
			e.printStackTrace();
			//MsgBox_show("e::" + core.getTrace(e));
			MsgBoxServ.show("catnt play",e);
		}
	}
private String getVurlFull(String video_url) {
	video_url = video_url.replaceAll("/vdx/", "");
	System.out.println(video_url);
	String playserver =getPlayServer();
	if(playserver.trim().equals(""))
		throw new RuntimeException("ciant find playserver");
	String url_full =playserver + "/"
			+ video_url;
	// p48
	if (playserver.trim().startsWith("\\\\"))
		url_full = playserver + "\\" + video_url;

	File f = new File(url_full);
	if (!f.exists()) {
		url_full=pathx.fixSlash(url_full);
		String string = "file maybe not exist文件可能不存在::" + url_full;
		System.out.println(string);
	//	throw new run
		MsgBoxServ.show(string);
	//	MsgBox_show(string);
		//return;
	}
	url_full = url_full.replaceAll("/", "\\\\");
	return url_full;
}
	
	List useableSvrs=new ArrayList();
	private String getPlayServer() {
		useableSvrs=new ArrayList();
		// TODO Auto-generated method stub
		String svs_s=PropX.getConfig(cfgPath, "playserver");
		String[] a=svs_s.split(",");
		for ( String svr : a) {
			if(new File(svr).exists())
			{
				useableSvrs.add(svr);
				return svr;
			}
				
		}
		return a[0].trim();
	}
	
	private void checkPlayerProcessExistV2(	String player) {
		boolean existProcess=false;
		
		try {
			
			player=pathx.fixSlash(player);
			String[] a=player.split("/");
			String name=a[a.length-1];
			
			existProcess= ProcessX.isExistProcess(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		
			if(existProcess)
			//String playserver = PropX.getConfig(cfgPath, "playserver");
		  {
				
				String string = "  player be   exist... 播放器可能正在启动，请稍等::";
				System.out.println(string);
				MsgBox_show(string);
				throw new RuntimeException(string);
			}
	 
		
		
	}
	private void checkPlayerProcessExist() {
		boolean existProcess=false;
		
		try {
			String player=getPlayer(null,null);
			player=pathx.fixSlash(player);
			String[] a=player.split("/");
			String name=a[a.length-1];
			
			existProcess= ProcessX.isExistProcess(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		
			if(existProcess)
			//String playserver = PropX.getConfig(cfgPath, "playserver");
		  {
				
				String string = "  player be   exist... 播放器可能正在启动，请稍等::";
				System.out.println(string);
				MsgBox_show(string);
				throw new RuntimeException(string);
			}
	 
		
		
	}
	private void checkAuthcode() {
		Trigger trig=	(Trigger) IocX.map.get("check-playCode-trigger");
		System.out.println("check-playCode-trigger::"+trig);
		if(trig!=null)
		{
			boolean CanPlay=false;
			try {
				CanPlay=(Boolean) trig.exec(null);
				
 			} catch (Exception e) {
 				
				MsgBox_show( "check code ex:"+e.getMessage());
				
				return;
			}
		
			if(!	CanPlay)
			{
				MsgBox_show("授权码错误。。。auth code is err");
				return;
			}
		}
	}
	private void countPlyaerNCoperyCount(final Object[] parameters) {
		
		
		core.newThread(new Runnable() {
			
			@Override
			public void run() {
				try {
					String mov_id = null;
					try {
						mov_id=String.valueOf( ((Double) parameters[1]).intValue()) ;
					} catch (Exception e2) {
						mov_id= (String) parameters[1];
					}
					
					countPlayTimes(mov_id);

					//notice copy right center
					try {
					//Trigger trig2=	(Trigger) IocX.map.get("copyright-trigger");
				//	trig2.exec(mov_id);
			//trig
					} catch (Exception e2) {
						//e2.printStackTrace();
					 
					}
				} catch (Exception e) {
					//e.printStackTrace();
					System.out.println(" countPlyaerNCoperyCount e:"+e.getMessage());
					
				}
				
			}
		}, "threadName");
		
		
	}
	private void checkMovieLibExist() {

		File f = new File("z:");
		if (!f.exists()) {
			
			String string = "z: may be not exist... z盘映射可能不存在，请检查::";
			System.out.println(string);
			MsgBox_show(string);
			throw new RuntimeException(string);
		}
		
	}
	
	/**
	 * url_full ma shma yon
	 * @param parameters
	 * @param url_full
	 * @return
	 */
	private String getPlayer(Object[] parameters, String url_full) {
		String playerNum=getPlayerNum(parameters);
		String player = PropX.getConfig(cfgPath,playerNum);
		player=autoPaddPlayerPath(player);
		
		//p929
		if(!new File(player).exists())
			if(new File("d:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="d:\\PotPlayer\\PotPlayerMini64.exe";
		
		if(!new File(player).exists())
			if(new File("c:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="c:\\PotPlayer\\PotPlayerMini64.exe";
		
		if(!new File(player).exists())
			if(new File("e:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="e:\\PotPlayer\\PotPlayerMini64.exe";
		//p929 end
	
		
		if (!new File(player).exists()) {
			MsgBox_show("player path err.播放器可路径错误::" + player+ " " );
			throw new RuntimeException("player path err.播放器可路径错误::" + player);
		}
		return player;
	}
	
	private String getPlayer2(  String url_full) {
	//	String playerNum=getPlayerNum(parameters);
		String player = PropX.getConfig(cfgPath,"player");
		player=autoPaddPlayerPath(player);
		
		//p929
		if(!new File(player).exists())
			if(new File("d:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="d:\\PotPlayer\\PotPlayerMini64.exe";
		
		if(!new File(player).exists())
			if(new File("c:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="c:\\PotPlayer\\PotPlayerMini64.exe";
		
		if(!new File(player).exists())
			if(new File("e:\\PotPlayer\\PotPlayerMini64.exe").exists())
				player="e:\\PotPlayer\\PotPlayerMini64.exe";
		//p929 end
	
		
		if (!new File(player).exists()) {
			MsgBoxServ. show("player path err.播放器可路径错误::" + player+ " " );
			throw new RuntimeException("player path err.播放器可路径错误::" + player);
		}
		return player;
	}
	String duration = "7200";
	
	private void toniceBoxSys_thread(final String url_full) {
		
	     
		final Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				
				toniceBoxSys(url_full);
				
			}
		};
		core.newThread(new Runnable() {
			
			@Override
			public void run() {
				try {
					 SwingUtilities.invokeLater(runnable);
					 //jeig e haosyo yme bd trig m..
					} catch (Exception e) {
						//e.printStackTrace();
					 	String string = "toniceBoxSys_e:"+url_full +e.getMessage();
					 	System.out.println(string);
						MsgBox_show(string);
					}
				
			}
		}, "threadName");
		
	//	Display.getDefault().ti
//		try {
//			 Display.getDefault().asyncExec(runnable);
//		} catch (Exception e) {
//			e.printStackTrace();
//			MsgBox_show(core.getTrace(e));
//		}
		 
	//	core.execMeth_AysGui(runnable, "threadName_toniceBoxSys_thread"+filex.getUUidName());
	}
	@SuppressWarnings("all")
	private void toniceBoxSys(final String url_full) {
		
		try {
			
			try {
				String def_duration = PropX.getConfig(cfgPath, "def_duration");
				duration=def_duration;
			} catch (Throwable e) {
			e.printStackTrace();
			}
			
			String boxsys = getBoxSysUrlApi();
			
		
			FutureTask ft=	new FutureTask(new Callable<String>() {

				@Override
				public String call() throws Exception {
					 
				
						try {
							MediaInfo mediaInfo = new MediaInfo();
							Map mediainfo2 = mediaInfo.getMediainfo(url_full);
							Long duration_sec2 = (Long) mediainfo2.get(
									"duration");
							Long	duration_sec=duration_sec2;
							duration = duration_sec.toString();
						
						} catch (Exception e) {
							e.printStackTrace();
							filex.saveLog(e, "c:\\e");
							
						}
						return duration;
						 

				 
				//	return null;
				}
			});
			core.newThread(ft, "threadName");
			
			if(duration==null)
			{
				try {
					String def_duration = PropX.getConfig(cfgPath, "def_duration");
					duration=def_duration;
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
			try {
				duration=	(String) ft.get(7, TimeUnit.SECONDS);
				// MediaInfo
		
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			boxsys = boxsys.replaceAll("@timex", duration);
		//	boxsys = boxsys.replaceAll("@timex", duration);
			boxsys=	new ParamX(). sqlFmt(boxsys,new HashMap(){{
				this.put("movie", URLEncoder.encode(pathx.fixSlash(url_full), "utf-8") );
				this.put("mov_cn", pathx.fixSlash(url_full));
			}});
			System.out.println(boxsys);
			String v = websitex.WebpageContent(boxsys);
			System.out.println(v);
			
			//pac
			String boxsys_dbg_info = PropX.getConfig(cfgPath, "boxsys_dbg_info");
			if(boxsys_dbg_info!=null && boxsys_dbg_info.trim().equals("1"))
		
			MsgBox_show("notice box sys rte:" + v);
			
		} catch (Exception e) {
		//	e.printStackTrace();
			String string = "*** toniceBoxSys err:"+e.getMessage();
			System.out.println(string);
			filex.saveLog(e, "c:\\e");
			
			
			String boxsys_dbg_info = PropX.getConfig(cfgPath, "boxsys_dbg_info");
			if(boxsys_dbg_info!=null && boxsys_dbg_info.trim().equals("1"))
			 MsgBox_show(string);
		}
	
	

	}
	private String getBoxSysUrlApi() {
		String urls = PropX.getConfig(cfgPath, "boxsys");
		String subnet=IPUtil.subnet();
		String[] urls_a=urls.split(",");
		for (String url : urls_a) {
			String subnet_now=IPUtil.subnet_url(url);
			if(subnet.equals(subnet_now))
				return url;
		}
		return urls;
	}
//	private FutureTask fut(final String url_full) {
//		return 
//	}
	private void MsgBox_show(String string) {
		
//string=EncodeX.jsEncodeSingleQuoue(string);
	//	webBrowser.executeJavascript("alert('"+string+"')");
		 ini();
		if(	this.MsgBoxServ!=null)
		this.MsgBoxServ.show(string);
		
	}
	private String autoPaddPlayerPath(String player) {
		if(player.contains(":"))
			return player;
		 
		String player_fullpath=	pathx.classPathParent()+"/"+player;
		if (!new File(player_fullpath).exists())
		{
			return 	"d:/atibrow/"+player;
		}
		return player_fullpath;
		
	}
	private String getPlayerNum(Object[] parameters) {
		String id="";
		try{
			try {
				id=String.valueOf( ((Double) parameters[2]).intValue()) ;
			} catch (Exception e2) {
				id=(String) parameters[2];
				//e2.printStackTrace();
			}
			if(id.equals("1"))
				id="";
		}catch (Exception e){}
	
	
		return "player"+id;
		//return null;
	}
	private void countPlayTimes(String id) {
		//ati p519 count 
		try {
			
			String sql="  update [gv_material] set [play_count]=[play_count]+1 where material_id="+id;
			
			String 	url_app = PropX.getConfig(cfgPath, "count_server");
			String url=url_app+"/api_dsl.jsp?http_param=@sql&ret_datatype=xml&xylID=0.035275804344564676";
			url=url.replaceAll("@sql", URLEncoder.encode(sql, "utf-8"));
		String t=	websitex.WebpageContent(url, "utf-8");
			System.out.println(t);
			
		} catch (Exception e2) {
			//e2.printStackTrace();
			System.out.println(" countPlayTimes err:"+e2.getMessage());
		}
	}
	
	public Ati4vodPlayer() {
	//	if(System.getenv("cfgfile")!=null)
	//	try {
			cfgPath = pathx.webrootPath()+"/" +System.getProperty("vod_cfgfile");
			if(System.getProperty("vod_cfgfile")==null)
				cfgPath = pathx.webrootPath() +"/cfg.properties";
//		} catch (Exception e) {
//			cfgPath = pathx.webrootPath() +"/cfg.properties";
//		}
		
			
		
	}
	
	public static String getCfg() {

		if (cfgPath == null)
			cfgPath = pathx.classPathParent() + "/cfg.properties";
		System.out.println(cfgPath);
		if (!new File(cfgPath).exists())
			cfgPath = pathx.classPath() + "/cfg.properties";
		return cfgPath;
	}

	public static boolean canbeClose = false;
	public static JFrame mainFrame;
	/* Standard main method to try that test as a standalone application. */
	public static void main(String[] args) {
		// System.setProperty("org.eclipse.swt.browser.XULRunnerPath",
		// "C:\\xulrunner");
		//System.get
	//	System.setProperty("vod_cfgfile","cfg.properties");
		Ati4vodPlayer plr=Ioc4vodV2.getBean(Ati4vodPlayer.class);
		plr.play("aaa.mkv");
		System.out.println("--ff");
		
		//main0(args);

	}
	private static void main0(String[] args) {
		jsCmdMap=new HashMap();
		jsCmdMap.put("play_multi",new Closure () {

			@Override
			public Object execute(Object arg0) throws Exception {
				Object[] parameters=(Object[]) arg0;
			//	new Ati4vod().play(parameters);
				return null;
			}
		});
		try {
			String cfgPathname = args[0];
			cfgPath = pathx.classPathParent() + "/"+cfgPathname;
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("--");
		final ImageIcon icon = null ;//= new ImageIcon(pathx.classPath(Ati4vod.class) 				+ "/mov.png");
		// setIconImage(image);//����setIconImage���������Ƕ�����JFrame�ĸ���Frame���еķ�����

		NativeInterface.open();
		UIUtils.setPreferredLookAndFeel();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("") {
					
					// 重写这个方法

					// @Override
					protected void processWindowEvent(WindowEvent e) {
						System.out.println(e.getID());
						if (e.getID() == WindowEvent.WINDOW_CLOSING) {
							// KeyEvent evt = (KeyEvent) e;
							// if (evt.getKeyCode() == KeyEvent.VK_ESCAPE &&
							// evt.isControlDown()) {
							//
							// }
							// MsgBox.show("close  a");
							if (canbeClose) {
								System.out.println(" can be close..");
								super.processWindowEvent(e);
								return;
							} else {
								System.out.println(" can not close..");
								return; // 直接返回，阻止默认动作，阻止窗口关闭
							}

						} else {
							System.out.println("no close evert..");
							super.processWindowEvent(e); // 该语句会执行窗口事件的默认动作(如：隐藏)
						}
					}

				};
				mainFrame=frame;
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane()
						.add(createContent(), BorderLayout.CENTER);
				frame.setSize(800, 600);
				frame.setLocationByPlatform(true);
				frame.setIconImage(icon.getImage());
				
				
				frame.setExtendedState(JFrame.MAXIMIZED_BOTH); // todox max win
				frame.setUndecorated(true);
				frame.setVisible(true);
				// frame. addWindowListener(new WindowAdapter() {
				// private int oldValue; // 记忆原默认值
				// public void windowClosing(WindowEvent evt) {
				// MsgBox.show("close");
				// // if (notClose) { // 不能关闭时
				// // if (getDefaultCloseOperation() != 0) {
				// // oldValue = getDefaultCloseOperation();
				// // setDefaultCloseOperation(0);
				// // }
				// // // 提示不能关闭
				// // } else { // 恢复默认动作
				// // if (oldValue != 0 && getDefaultCloseOperation() == 0)
				// setDefaultCloseOperation(oldValue);
				// // }
				// }
				// });
			}
		});

//		core.execMeth_Ays(new Runnable() {
//
//			@Override
//			public void run() {
//				 String act="1";
//				 try {
//					   act =  PropX.getConfig(pathx.classPath()
//								+ "/cfg.properties", "act");
//				} catch (Exception e2) {
//					 
//				}
//				 try {
//					
//					 if(act.trim().equals("1"))
//						 GUIx.activeWin();
//					 else
//						 System.out.println("no act win");
//				 } catch (AWTException e) {
//				 // TODO Auto-generated catch block
//				 e.printStackTrace();
//				 }
//
//			}
//		}, "--act win");

		// && (arg0.isControlDown()))
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		toolkit.addAWTEventListener(new AWTEventListener() {
			@Override
			public void eventDispatched(AWTEvent e) {
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					KeyEvent evt = (KeyEvent) e;
					// if (evt.getKeyCode() == KeyEvent.VK_ESCAPE &&
					// evt.isControlDown()) {
					if (evt.getKeyCode() == KeyEvent.VK_F7&& !evt.isAltDown()) {
						// frame.dispose();
						if (MsgBox.confirm("is close??确认关闭")) {
							canbeClose = true;
							System.exit(0);
						}
					}
					
					if (evt.getKeyCode() == KeyEvent.VK_F6&& evt.isAltDown() && evt.isControlDown()) {
						 switchMaxMin();
					}
				}
			}
		}, AWTEvent.KEY_EVENT_MASK);
//		GUIx. hideMouse();
//		GUIx.hideMouseMulti();
		NativeInterface.runEventPump();
	}
	static boolean isMax=true;
	protected static void switchMaxMin() {
		 //JFrame.MAXIMIZED_BOTH
		if(isMax)
		{
		mainFrame.setExtendedState(JFrame.NORMAL); 
		isMax=false;
		}
		else
		{
			mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
			isMax=true;
		}
	}

}
