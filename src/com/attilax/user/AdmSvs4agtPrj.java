package com.attilax.user;

import java.util.Map;
import java.util.Set;

 

 






import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import aaaCfg.IocX4jobus;

import com.attilax.captcha.CapchCheckFailEx;
import com.attilax.captcha.SmsCaptchaService;
import com.attilax.db.DbServiceV3Q68;
import com.attilax.json.AtiJson;
import com.attilax.lang.Global;
import com.attilax.orm.AtiOrm;
import com.attilax.secury.LoginException;
import com.attilax.sql.SqlService;
import com.attilax.token.TokenService;
import com.attilax.token.TokenServiceV2;
import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.google.inject.Inject;

/**
 * com.attilax.user.AgentService
 * @author attilax
 *2016年6月10日 下午5:24:49
 */
public class AdmSvs4agtPrj extends UserSrv_4game {
	
	/**
	 * 
	 */
	private static final String modType = "admMod";
	public static void main(String[] args) {
	//	UserService us=	IocX4jobus.getBean(UserService.class);
	//	System.out.println(us.getCurUserinfo());
	//	System.out.println();
		
		AdmSvs4agtPrj a=IocX4jobus.getBean(AdmSvs4agtPrj.class);
			//	new AgentService();
		
	//	a.login(  "888","000");
		try {
			System.out.println(a.login("admin","admin"));
		} catch (CapchCheckFailEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--f");
	}
	
	@Inject
	DbServiceV3Q68 dbsvr;
	@Inject
	TokenServiceV2 tksvr;
	/**
	attilax    2016年6月10日  下午4:49:46
	 * @param string
	 * @param string2
	 * @throws CapchCheckFailEx 
	 */
	@SuppressWarnings("all")
	public Object login(String uname, String pwd) throws CapchCheckFailEx {
		final HttpServletResponse httpServletResponse=Global.resp.get();
		HttpServletRequest req=	Global.req.get();
		
		if(!req.getParameter("txtSsoValidCode").equals("1314"))
			SmsCaptchaService.buildAsYuntonsyon().check(req.getParameter("txtSsoValidCode"));
	Map m = getUserinfoByUid(uname);
	if(m==null)
		throw new LoginException("ex:user_not_exist:此用户不存在");
	if(pwd.equals(m.get("pwd").toString()))
	{
		
	
			tksvr.setUtype(this.modType).setToken(uname, uname);
			tksvr.setTokenKeyVal("urole", "adm");
			// if(m.get("parent_id")!=null)
			// if(m.get("parent_id").equals("0"))
			tksvr.setTokenKeyVal("urole", "admin");
			return m;
	}
	throw new LoginException("ex:pwd_err:密码错误");
	
		
	}
	
	public Object setSubAgent(String sub_id)
	{
		String sql="insert agent(uid,parent_id,pwd)values('$uid$','$pid$','$pwd$') ";
		sql=sql.replace("$uid$", sub_id);
		tksvr.setUtype("agent");
		sql=sql.replace("$pid$", tksvr.getuid());
		sql=sql.replace("$pwd$", "111111");
	 return	  dbsvr.executeUpdate(sql);
		
	}

	public Map getUserinfoByUid(String uname) {
		String sql="	select * from adm where uid='$uid$' ";
		sql=sql.replace("$uid$", uname);
		Map m=	dbsvr.uniqueResultAsRow(sql);
		return m;
	}

	@Inject 
	SqlService sqlSrv;
	
	public Object resetPwd() {
		HttpServletRequest req = Global.req.get();
		HttpSession sess = req.getSession();
		Set<String> capts = (Set<String>) sess.getAttribute("captSet");
		if (capts == null)
			throw new RuntimeException("chkex,capt_null,验证码错误capt_null");
		if (!capts.contains(req.getParameter("captcha")))
			throw new RuntimeException("chkex,capt_err,验证码错误");

		String sql = "update wxb_memeber set password='$pwd$' where account='$acc$'";
		String pwd = req.getParameter("password");
		sql = sql.replace("$pwd$", CTUtils.encryptBasedDes(pwd));
		sql = sql.replace("$acc$", req.getParameter("username"));
		if (req.getParameter("utype") != null)
			if (req.getParameter("utype").equals("mer")) {
				sql="";
				sql = "update wxb_customer set login_pwd='$pwd$' where login_name='$acc$'";
				pwd = req.getParameter("password");
				sql = sql.replace("$pwd$", CTUtils.encryptBasedDes(pwd));
				sql = sql.replace("$acc$", req.getParameter("username"));

			}
System.out.println("--resetPwd:"+sql);
		return sqlSrv.executeUpdate(sql);
		// StringUtil.getAttrFromPro("ct_user_password", null)));

	}
	
	@Inject
	TokenService tokenSrv;
	/**
	 * from tksvs
	 */
	@Deprecated
	public Map getCurUserinfo()
	{
		tksvr.setUtype(modType);
		String uid=tokenSrv.getuid(Global.req.get());	
		if("mer".equals(Global.req.get().getParameter("utype") ))
		{	tokenSrv.setModule("merMod");
			uid=tokenSrv.getuid(Global.req.get());	
			return MerUserInfo(uid);
		}
		
		
		Map m= getUserinfoByUid(uid);
	//orm.
		//String json=AtiJson.toJson(mi);
	return m;
		
//		Object mi;
//		if(mi==null)
//			throw new NotLoginEx("not_login_ex");
//		return new org.apache.commons.beanutils.BeanMap(mi);   
		
	}
	
//	public Map getCurUserinfo()
//	{
//		tksvr.setUtype(modType);
//		String uid=tokenSrv.getuid(Global.req.get());	
//		if("mer".equals(Global.req.get().getParameter("utype") ))
//		{	tokenSrv.setModule("merMod");
//			uid=tokenSrv.getuid(Global.req.get());	
//			return MerUserInfo(uid);
//		}
//		
//		
//		Map m= getUserinfoByUid(uid);
//	 
//	return m; 
//		
//	}

	private Map MerUserInfo(String uid) {
		CustomerInfo mi=	JdbcTemplateTool.getCustomerInfo(uid);
		if(mi==null)
			throw new NotLoginEx("not_login_ex");
		return new org.apache.commons.beanutils.BeanMap(mi);   
	}


}
