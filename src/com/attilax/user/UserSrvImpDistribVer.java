package com.attilax.user;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

import aaaCfg.IocX4jobus;

import com.attilax.json.AtiJson;
import com.attilax.lang.Global;
import com.attilax.sql.SqlService;
import com.csmy.my.center.module.CustomerInfo;
import com.csmy.my.center.module.MemeberInfo;
import com.csmy.my.center.util.CTUtils;
import com.csmy.my.center.util.StringUtil;
import com.csmy.my.center.util.db.JdbcTemplateTool;
import com.google.inject.Inject;

public class UserSrvImpDistribVer extends UserService {
	
	public static void main(String[] args) {
	//	UserService us=	IocX4jobus.getBean(UserService.class);
	//	System.out.println(us.getCurUserinfo());
	//	System.out.println();
		System.out.println(addMer("mer","aaaaaa"));
	}
	
	private static String addMer(String string, String password) {
		// TODO Auto-generated method stub
		return CTUtils.encryptBasedDes(password);
	}

	@Inject 
	SqlService sqlSrv;
	
	public Object resetPwd() {
		HttpServletRequest req = Global.req.get();
		HttpSession sess = req.getSession();
		Set<String> capts = (Set<String>) sess.getAttribute("captSet");
		if (capts == null)
			throw new RuntimeException("chkex,capt_null,验证码错误capt_null");
		if (!capts.contains(req.getParameter("captcha")))
			throw new RuntimeException("chkex,capt_err,验证码错误");

		String sql = "update wxb_memeber set password='$pwd$' where account='$acc$'";
		String pwd = req.getParameter("password");
		sql = sql.replace("$pwd$", CTUtils.encryptBasedDes(pwd));
		sql = sql.replace("$acc$", req.getParameter("username"));
		if (req.getParameter("utype") != null)
			if (req.getParameter("utype").equals("mer")) {
				sql="";
				sql = "update wxb_customer set login_pwd='$pwd$' where login_name='$acc$'";
				pwd = req.getParameter("password");
				sql = sql.replace("$pwd$", CTUtils.encryptBasedDes(pwd));
				sql = sql.replace("$acc$", req.getParameter("username"));

			}
System.out.println("--resetPwd:"+sql);
		return sqlSrv.executeUpdate(sql);
		// StringUtil.getAttrFromPro("ct_user_password", null)));

	}
	
	public Map getCurUserinfo()
	{
		tokenSrv.setModule("userMod");
		String uid=tokenSrv.getuid(Global.req.get());	
		if("mer".equals(Global.req.get().getParameter("utype") ))
		{	tokenSrv.setModule("merMod");
			uid=tokenSrv.getuid(Global.req.get());	
			return MerUserInfo(uid);
		}
		
		
		MemeberInfo mi=JdbcTemplateTool.getMemeberInfo(uid);
		//String json=AtiJson.toJson(mi);
		
		if(mi==null)
			throw new NotLoginEx("not_login_ex");
		return new org.apache.commons.beanutils.BeanMap(mi);   
		
	}

	private Map MerUserInfo(String uid) {
		CustomerInfo mi=	JdbcTemplateTool.getCustomerInfo(uid);
		if(mi==null)
			throw new NotLoginEx("not_login_ex");
		return new org.apache.commons.beanutils.BeanMap(mi);   
	}


}
