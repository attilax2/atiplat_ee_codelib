package com.attilax.cmsPoster.imp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.attilax.core;
import com.attilax.cmsPoster.BlogPubberAbs;
import com.attilax.cmsPoster.BlogPubberMainform;
import com.attilax.cmsPoster.Ipub;
import com.attilax.formH5.EncodeX;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.util.PropX;
import com.gargoylesoftware.htmlunit.javascript.host.Document;

/**
 * com.attilax.cmsPoster.imp.QzoneImp
 * 
 * @author Administrator
 *
 */
public class QzoneImp extends BlogPubberAbs implements Ipub {

	@Override
	public void pub2(String p1tit, String context, FirefoxDriver driver) {
	
		jumpPubUrl(context, driver);
		
		
	 	String t = getAuthorTxt("qzone");
	 
		context=context.replace("$attilax_blog_sign$", t);
		
		//-----------------title
		 String title_inputbox_id = "blog-title-input";
		driver.findElement(By.id(title_inputbox_id)).clear();
		driver.findElement(By.id(title_inputbox_id)).sendKeys(p1tit);
		
		//--------------context 
		context = changeBlogLink(context);
		context = EncodeX.jsEncodeSingleQuoue(context);
		
		// p2_txt="test in html";
		String baseDir=pathx.classPathParent_jensyegeor()
				 + "/blogger";
		String js =filex.read(baseDir + "/db/" + BlogPubberMainform.blogName.get()
				 
				+ "/editor__form.js");
		String script = js.replace("@txt", context);

		// window.document.getElementById('editor').value='..xxx'
		driver.executeScript(script,
				driver.findElement(By.id(title_inputbox_id)));
		// try {
		// WebElement findElement = driver.findElement(By.linkText("写日志"));
		// System.out.println(core.toJsonStrO88(findElement));
		// findElement.click();
		// System.out.println(" pub span by 写日志");
		// } catch (Exception e3) {
		//
		// try {
		// WebElement findElement = driver.findElement(By
		// .cssSelector("a.gb_bt2"));
		// System.out.println(core.toJsonStrO88(findElement));
		// findElement.click();
		// System.out.println(" pub span by a.gb_bt2");
		// } catch (Exception e2) {
		// WebElement findElement = driver.findElement(By
		// .cssSelector("span.inner_wrap"));
		// System.out.println(core.toJsonStrO88(findElement));
		// findElement.click();// start pub btn
		// System.out.println(" pub span by span.inner_wrap");
		// }
		//
		// }

		// }

		// driver.switchTo().defaultContent();
		// driver.switchTo().frame("tblog");
		
		// driver.findElement(By.id("saveBlogButton")).click();
	}

	private void jumpPubUrl(String context, FirefoxDriver driver) {
		driver.switchTo().frame("tblog");

		String baseDir = pathx.classPathParent_jensyegeor() + "/blogger";
		String script = filex.read(baseDir + "/db/"
				+ BlogPubberMainform.blogName.get()

				+ "/gotoPubpage.js");
		// try {
		// WebElement findElement2 = driver.findElement(By
		// .id("write-ordinaryblog"));
		// System.out.println(core.toJsonStrO88(findElement2));
		// findElement2.click();
		// } catch (Exception e) {
		context = EncodeX.jsEncodeSingleQuoue(context);
		
		try {
			driver.executeScript(script, driver.findElement(By.linkText("写日志")));
		} catch (WebDriverException e) {
			try {
				script = script.replace("frames[2]", "frames[1]");
				driver.executeScript(script,
						driver.findElement(By.linkText("写日志")));
			} catch (Exception e2) {
				script = script.replace("frames[2]", "frames[0]");
				driver.executeScript(script,
						driver.findElement(By.linkText("写日志")));
			}

		}

		System.out.println(" exe js after..");
	}

	@Override
	public void login(FirefoxDriver driver,Object  px2) {
		PropX px=(PropX) px2;
		driver.switchTo().frame("login_frame");
		System.out.println("  switch switchTo().frame( login_frame");
		WebElement loginImg = driver.findElement(By.cssSelector("a.face"));
		driver.findElement(By.id("img_out_1466519819")).click();
		;
		System.out.println(core.toJsonStrO88(loginImg));
		loginImg.click();
		try {
			loginImg.click();
		} catch (Exception e) {
			// TODO: handle exception
		}

		System.out.println("  	loginImg.click after.. ");

	}

}
