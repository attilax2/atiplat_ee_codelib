package com.attilax.cmsPoster;

//import org.openqa.jetty.http.SSORealm;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Global;
import com.attilax.util.PropX;

public class BlogPubberAbs {
	public String changeBlogLink(String p2_txt) {
		String blogName=Global.blogName.get();
		if(blogName.equals("nonex"))
			return "";
		PropX px=(PropX) Global.propMap.get(blogName);
		String blog_index=px.getProperty("blog_index");
		p2_txt=p2_txt.replace("http://blog.csdn.net/attilax", blog_index);
		return p2_txt;
	}
	
	protected String getAuthorTxt(String bloggerwebsite) {
		String blogName=Global.blogName.get();
		System.out.println("---thread:"+Thread.currentThread());
		if(blogName.equals("nonex"))
			return "";
		PropX px=(PropX) Global.propMap.get(blogName);
		String blog_index=px.getProperty("blog_index");
		
		String p=pathx.classPath()+"/aaaBlogger/img/author.txt";
		String t=filex.read(p);
		 ;
		t=t.replace("$attilax_blog_url$", blog_index);
		return t;
	}
}
