package com.attilax.cmsPoster;

import com.attilax.Closure;
import com.attilax.io.dirx;
import com.attilax.util.travDir;

public class Traver {

	private String path;

	public Traver(String path) {
		this.path=path;
	}

	public void exe(final ItemImp itemImp) {
		 dirx.trave(this.path, new Closure<Object, Object>() {

			@Override
			public Object execute(Object f) throws Exception {
				try {
					itemImp.exe(f.toString());
					return null;
				} catch (Exception e) {
					e.printStackTrace();
//					if("1".equals("2"))
//						throw new RuntimeException("xx");
				}
				return null;
			
			}

			 
		});
		
	}

}
