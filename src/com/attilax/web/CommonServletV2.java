package com.attilax.web;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.struts2.dispatcher.StrutsRequestWrapper;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.hre.UrlDslParser;
import com.attilax.hre.UrlDslParserV2;
import com.attilax.ioc.IocXq214;
import com.attilax.ref.refx;
import com.google.inject.Inject;

/**
 * /CommonServlet?$method= com.attilax.up.FileUploadService.upload
 * 
 * /CommonServlet?$method= com.attilax.up.FileUploadService.process
 * @author Administrator
 *
 */

@WebServlet(name = "CommonServletNameV2", urlPatterns = "/CommonServletV2") 
public class CommonServletV2 implements Servlet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig paramServletConfig) throws ServletException {
		
		
	}
	public static ThreadLocal<ServletResponse> 	 resp=new ThreadLocal<ServletResponse>();
	
	@Inject
	UrlDslParser UrlDslParserx;
	@Override
	public void service(ServletRequest req,
			ServletResponse paramServletResponse) throws IOException,
			ServletException {
		UrlDslParserx=IocXq214.getBean(UrlDslParser.class);
	HttpServletRequest httpServletRequest = (HttpServletRequest)	req;
	@SuppressWarnings("unused")
	String p=httpServletRequest.getContextPath();
		resp.set(paramServletResponse);
	 //	StrutsRequestWrapper
		//if( UrlDslParserx==null)
		paramServletResponse.setContentType("text/html;charset=utf-8");
		String ret=UrlDslParserx.exe((HttpServletRequest)req);
		paramServletResponse.getWriter().println(ret);
		
	}

}
