package com.attilax.web;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class javafx1showIMg extends Application {

	@Override
	public void start(Stage primaryStage) {
	//	Group
		String orcimg = "http://docs.oracle.com/javafx/"  
		        + "javafx/images/javafx-documentation.png";
		javafx.scene.image.Image image = new javafx.scene.image.Image(orcimg); 
		StackPane root = getPane(orcimg, image);

		Scene scene = new Scene(root, 600	,600 );

		primaryStage.setTitle("Hello World!");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private StackPane getPane(String orcimg, javafx.scene.image.Image image) {
		System.out.println(orcimg);
	    ImageView imageView = new ImageView();  
        imageView.setImage(image);  
		
		StackPane root = new StackPane();
		root.getChildren().add(imageView);
		return root;
	}

	public static void main(String[] args) {
		 javafx.application.Application.launch(args);
	}
}
