package com.attilax.orm;

import java.util.Map;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.sql.SqlJoinAst;
import com.attilax.sql.SqlJoinParser;

public class View {

	private Object obj;
	private Object viewStorePath;

	public View(String viewName) {
		// TODO Auto-generated constructor stub
	}

	public View(Object objectName) {
		this.obj = objectName;
	}

	public View(Object objectName, Object viewStorePath) {
		this.obj = objectName;
		this.viewStorePath = viewStorePath;
	}

	public String[] getTables() {
		String f = pathx.classPath() + "/" + this.viewStorePath + "/"
				+ obj.toString() + ".txt";
		// String t=filex.read(f);
		SqlJoinAst ast = new SqlJoinParser().parse(f);
		String tabs = ast.table;
		for (Map join_tab : ast.joinTables) {
			tabs = tabs + "," + join_tab.get("joinTable");
		}
		return tabs.split(",");
	}

}
