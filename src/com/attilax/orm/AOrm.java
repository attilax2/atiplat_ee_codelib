package com.attilax.orm;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

 


import com.attilax.jql.JqlService;
import com.attilax.sql.SqlService;
import com.attilax.web.ReqX;
import com.google.inject.Inject;

public class AOrm {
	
	
	@Inject
	JqlService jqlSrv;
	@Inject
	SqlService sqlSrv;

	public void exe(HttpServletRequest req) {
		 Map m=convertOrmMap(req);
		 exe(m);
		
	}

	public void exe(Map m) {
		 
		if(m.get("$tabletype")!=null)
		{
			if(m.get("$tabletype").equals("view"))
			{
				new Aorm4view().exe(m,m.get("$table").toString());
			}
		}
		String sql=jqlSrv.insert(m);
		sqlSrv.exe(sql);
	}

	private Map convertOrmMap(HttpServletRequest req) {
		
		return ReqX.toMap(req);
	}

 

}
