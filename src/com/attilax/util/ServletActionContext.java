package com.attilax.util;

import javax.servlet.http.HttpServletResponse;

import com.attilax.lang.Global;

public class ServletActionContext {

	public static HttpServletResponse getResponse() {
		// TODO Auto-generated method stub
		HttpServletResponse 	response = org.apache.struts2.ServletActionContext.getResponse();
		if( response==null)
		return (HttpServletResponse) Global.resp.get();
		else
			return response;
	}

}
