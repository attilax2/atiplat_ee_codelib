/**
 * 
 */
package com.attilax.util;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author attilax 2016年11月10日 下午6:39:43
 */
public class Watchdog {
	Timer tmr = new Timer();
	public int calcSpan;

	public int timeSum;

	/**
	 * @param mills
	 * 
	 */
	public Watchdog(int mills) {
		this.calcSpan = mills;
	}

	public int MaxFeedTime;
	public Runnable hungryEvt;

	static int stat;
	boolean hungry = false;

	/**
	 * attilax 2016年11月10日 下午6:49:55
	 */
	void ini() {
		tmr.schedule(new TimerTask() {

			@Override
			public void run() {
				if (timeSum > MaxFeedTime)
				{
					hungryEvt.run();
				//	timeSum=0;
				//	tmr.cancel();
					timeSum=0;
				}
				else
					timeSum = timeSum + calcSpan;
				System.out.println(" dog hungry val:"+timeSum);
			}
		}, 0,calcSpan);
	//	tmr.

	}

	/**
	 * attilax 2016年11月10日 下午6:53:18
	 */
	public void feed() {
		timeSum=0;
System.out.println("  feed dog..");
	}

}
