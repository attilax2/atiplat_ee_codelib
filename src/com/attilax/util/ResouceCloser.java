/**
 * 
 */
package com.attilax.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * @author attilax 2016年4月28日 下午6:52:11
 */
public class ResouceCloser {

	/**
	 * attilax 2016年4月28日 下午6:52:19
	 * 
	 * @param out
	 * @return
	 */
	public ResouceCloser close(Object out) {
		if (out instanceof OutputStreamWriter) {
			try {
				((OutputStreamWriter) out).close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		
		if (out instanceof BufferedReader) {
			try {
				((BufferedReader) out).close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		
		if (out instanceof InputStreamReader) {
			try {
				((InputStreamReader) out).close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		
		if (out instanceof InputStream) {
			try {
				((InputStream) out).close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		return this;
	}

}
