/**
 * 
 */
package com.attilax.util;

/**
 * @author attilax
 *2016年11月10日 下午6:51:25
 */
public class watchdogTest {

	private static int stat;

	/**
	attilax    2016年11月10日  下午6:51:25
	 * @param args
	 */
	public static void main(String[] args) {
		
		Watchdog dog=new Watchdog(300);
		dog.MaxFeedTime=2000;//喂狗的最大时间,单位毫秒，超过此时间，狗就饥饿了，出来触发事件来。。
		//狗饥饿了，出来触发事件的定义
		dog.hungryEvt=()->{
			 
			System.out.println("btn evt...");
		};
		dog.ini();
		
		
		//测试 每隔一秒喂狗，就不会触发事件
		while(true)
		{
			try {
				Thread.sleep(1700);
			} catch (InterruptedException e) {
			 
				e.printStackTrace();
			}
			dog.feed();
		}
	}

}
