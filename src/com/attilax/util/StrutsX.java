/**
 * @author attilax 老哇的爪子
	@since  o08 j_52_3$
 */
package com.attilax.util;

import com.attilax.core;
import com.attilax.exception.ExUtil;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.json.JsonUtil4jackjson;
import com.google.gson.Gson;

import static com.attilax.core.*;

import java.util.*;
import java.util.regex.Pattern;
import java.net.*;
import java.io.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.Dispatcher;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

/**
 * @author attilax 老哇的爪子
 * @since o08 j_52_3$
 */
public class StrutsX {

	public static void main(String[] args) {
		String f = pathx.classPath() + "/struts.xml";
		String pats = get_excludePattern(f);
		System.out.println(pats);
	//	Gson
	//	JsonUtil4jackjson
	//	JsonNode
	}

	public static String get_excludePattern(String f) {
		String t = filex.read(f);
		org.dom4j.Document document;
		try {
			document = DocumentHelper.parseText(t);
			Node root = document.getRootElement();
			// root.selectNodes(arg0)
			List<Element> nodes = ((Element) root).elements("constant");
			for (Element e : nodes) {
				if (e.attributeValue("name").equals(
						"struts.action.excludePattern")) {
					return e.attributeValue("value");
				}
			}
		} catch (Throwable e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		 
			ExUtil.throwEx(e1);
		}
		return "";

	}
	
	public boolean isUrlExcluded(	HttpServletRequest request,		 String patts) {
		List<Pattern> excludedPatterns=buildExcludedPatternsList(patts);
		if (excludedPatterns != null) {
			String uri =  (request.getRequestURI());
			for (Pattern pattern : excludedPatterns) {
				if (pattern.matcher(uri).matches()) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isUrlExcluded(HttpServletRequest request,
			List<Pattern> excludedPatterns) {
		if (excludedPatterns != null) {
			String uri =  (request.getRequestURI());
			for (Pattern pattern : excludedPatterns) {
				if (pattern.matcher(uri).matches()) {
					return true;
				}
			}
		}
		return false;
	}

//	public List<Pattern> buildExcludedPatternsList(Dispatcher dispatcher) {
//		return buildExcludedPatternsList(dispatcher.getContainer().getInstance(
//				String.class, StrutsConstants.STRUTS_ACTION_EXCLUDE_PATTERN));
//	}

	private List<Pattern> buildExcludedPatternsList(String patterns) {
		if (null != patterns && patterns.trim().length() != 0) {
			List<Pattern> list = new ArrayList<Pattern>();
			String[] tokens = patterns.split(",");
			for (String token : tokens) {
				list.add(Pattern.compile(token.trim()));
			}
			return Collections.unmodifiableList(list);
		} else {
			return null;
		}
	}

	/**
	 * @author attilax 老哇的爪子
	 * @since o08 j_52_a
	 * 
	 * @return
	 */
	public static HttpServletRequest getReq() {
		// attilax 老哇的爪子 j_52_a o08

		return ServletActionContext.getRequest();

	}
	// attilax 老哇的爪子 j_52_3 o08
}

// attilax 老哇的爪子