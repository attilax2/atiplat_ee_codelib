package com.attilax.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.web.ReqX;
import com.csmy.my.center.util.zto.HttpUtil;
import com.google.common.collect.Lists;

public class ConnReducerV2 {
	public String res;
	public  List<String> res_li = Lists.newLinkedList();
 
	public  String base ="";// "C:\\workspace\\AtiPlatf_cms\\WebRoot";
	public  String output;
 
 
	public  boolean recreateMode;

	public static void main(String[] args) {
//	System.out.println("aa");
//		base = "C:\\workspace\\AtiPlatf_cms\\WebRoot";
//		output=base+"/index/jobus.js";   //index/jobus.js
//		output_css=base+"/index/jobus.css";
//		isGeneCss=false;
//		isDelExistFile=true;
//		// ConnReducer x=new ConnReducer();
//		List<String> li = filex
//				.read2list_filtEmptyNstartSpace("c:\\jspath.txt");
//		for (String line : li) {
//			line = line.trim();
//			if (line.length() == 0)
//				continue;
//			importx(line);
//
//		}
//
//		// importx("com.attilax/dataService/dataServiceV2q329.js");
//		// importx("com.attilax/web/dslUtil.js");
//		// importx("com.attilax/core/jqUtil.js");
//		// importx("com.attilax/core/ex.js");
//		// importx("com.attilax/core/core.js");
//		// importx("com.attilax/text/str.js");
//		// importx("com.attilax/web/urlUtil.js");
//		//
//		// //com.attilax/text/str.js
//		// importx("com.attilax/jsbridge/jsb_v7q329.js");
//		// importx("com.attilax/dataService/dataServiceV3q41.js");
//		// importx("com.attilax/io/uploadV3.js");
//		// importx("com.attilax/ui/AForm.js");
//
//		reduce();
//		System.out.println("--f");

	}
	public boolean genefile=true;
	public String cssCache="";
public String resCache="";
	public   void reduce() {
		
		iniOutPut();
		String r = "";
		try {
			if(recreateMode)
				if(new File(output).exists())
					new File(output).delete();
		 
			filex f = new filex(output);
			int idx=0;
			for (String js : res_li) {
				String t ="";
				
				if(js.startsWith("http://"))
					t=	HttpUtil.sendGet(js);
				else if(js.contains(":"))
					t=	filex.read(js);
				else
					t=	filex.read(pathx.join("$web_app_root$",js));
				// r=r+"\r\n"+t;
				idx++;
				String line = "/* no:"+String.valueOf(idx)+"-------------" + js + "*/\r\n"+t+"\r\n";
				f.append_HP(line);
				resCache+=line;
			 
			}
			f.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	 
	}
	public   String fillFullpath(String output) {
		iniBase();
		if(output.startsWith("/"))
			output=output.substring(1);
		if(output.contains(":"))
			return output;
		 
			 output=this.base+"/"+output;
		return output;
	}
	public String resfile;
	public String resfileBase;
	public boolean resfileUse=false;
	public  String restype="js";
	private void iniOutPut() {
		if(output==null)
		{
			output=this.resfile+".output."+this.restype;
			
		}
		output=	fillFullpath(output);
		System.out.println("--output file:"+output);
	}
	protected void iniBase() {
		if(this.base.length()==0)
			this.base=			pathx.webAppPath()+"";
		if(this.base.equals("$jsp"))
			this.base=ReqX.getRealpathDir(); 
		
		if(this.resfileBase==null)
		{
			this.resfileBase=new File(this.base+"/"+this.resfile).getParent();
		}
	}
	public boolean resfileBaseUse=false;
	protected  void importx(String string) {
		iniBase();
		string = string.trim();
		if (string.startsWith("http://")) {
		 
				res_li.add(string);
		 
			return;
		}
	 if(resfileBaseUse)
		 res_li.add(resfileBase + "/" + string);
	 else
			res_li.add(base + "/" + string);
	 
	}

}
