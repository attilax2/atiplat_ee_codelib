package com.attilax.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.attilax.io.filex;
import com.csmy.my.center.util.zto.HttpUtil;
import com.google.common.collect.Lists;

public class ConnReducer {
	public String res;
	public  List<String> js_li = Lists.newLinkedList();
	public  List<String> css_li = Lists.newLinkedList();
	public  String base = "C:\\workspace\\AtiPlatf_cms\\WebRoot";
	public  String output_js;
	public  String output_css;
	public  boolean isGeneCss=true;
	public  boolean isDelExistFile;

	public static void main(String[] args) {
//	System.out.println("aa");
//		base = "C:\\workspace\\AtiPlatf_cms\\WebRoot";
//		output=base+"/index/jobus.js";   //index/jobus.js
//		output_css=base+"/index/jobus.css";
//		isGeneCss=false;
//		isDelExistFile=true;
//		// ConnReducer x=new ConnReducer();
//		List<String> li = filex
//				.read2list_filtEmptyNstartSpace("c:\\jspath.txt");
//		for (String line : li) {
//			line = line.trim();
//			if (line.length() == 0)
//				continue;
//			importx(line);
//
//		}
//
//		// importx("com.attilax/dataService/dataServiceV2q329.js");
//		// importx("com.attilax/web/dslUtil.js");
//		// importx("com.attilax/core/jqUtil.js");
//		// importx("com.attilax/core/ex.js");
//		// importx("com.attilax/core/core.js");
//		// importx("com.attilax/text/str.js");
//		// importx("com.attilax/web/urlUtil.js");
//		//
//		// //com.attilax/text/str.js
//		// importx("com.attilax/jsbridge/jsb_v7q329.js");
//		// importx("com.attilax/dataService/dataServiceV3q41.js");
//		// importx("com.attilax/io/uploadV3.js");
//		// importx("com.attilax/ui/AForm.js");
//
//		reduce();
//		System.out.println("--f");

	}
	public String cssCache="";
public String jsCache="";
	public   void reduce() {
		String r = "";
		try {
			if(isDelExistFile)
				new File(output_js).delete();
			filex f = new filex(output_js);
			for (String js : js_li) {
				String t ="";
				if(js.startsWith("http://"))
					t=	HttpUtil.sendGet(js);
				else
					t=	filex.read(js);
				// r=r+"\r\n"+t;
				String line = "//-------------" + js + "\r\n"+t+"\r\n";
				f.append_HP(line);
				jsCache+=line;
			 
			}
			f.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		if(!isGeneCss)return;
		try {
			if(isDelExistFile)
				new File(output_css).delete();
			filex f = new filex(output_css);
			for (String js : css_li) {
				String t ="";
				if(js.startsWith("http://"))
					t=	HttpUtil.sendGet(js);
				else
					t=	filex.read(js);
				// r=r+"\r\n"+t;
				f.append_HP("//-------------" + js + "\r\n");
				f.append_HP(t);
				f.append_HP("\r\n");
			}
			f.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	protected  void importx(String string) {
		string = string.trim();
		if (string.startsWith("http://")) {
			if (string.endsWith(".js"))
				js_li.add(string);
			if (string.endsWith(".css"))
				css_li.add(string);
			return;
		}
		if (string.endsWith(".js"))
			js_li.add(base + "/" + string);
		if (string.endsWith(".css"))
			css_li.add(base + "/" + string);
	}

}
