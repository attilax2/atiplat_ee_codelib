/**
 * 
 */
package com.attilax.concur;

import java.util.concurrent.Callable;

import com.attilax.core;
import com.attilax.exception.ExUtil;

/**
 * @author attilax
 *2016年9月29日 下午10:45:53
 */
public class TaskUtil {

	/**
	attilax    2016年9月29日  下午10:46:00
	 * @param secs
	 */
	public static void sleep_sec(int secs) {
		try {
			Thread.sleep(secs*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void sleep_sec_withCountdown(int secs) {
		for(int i=0;i<secs;i++)
		{
			try {
				Thread.sleep(1*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(" now contdown to:"+String.valueOf(secs-i));
		}
		
		
	}
	
	public static void sleep_sec_throwEx(int secs) {
		try {
			Thread.sleep(secs*1000);
		} catch (InterruptedException e) {
		ExUtil.throwExV2(e);
		}
		
	}

	public static void sleep(int millSec) {
		try {
			Thread.sleep(millSec);
		} catch (InterruptedException e) {
			ExUtil.throwExV2(e);
		}
		
	}

	public static void asyn(Runnable ra,String threadName ) {
		core.execMeth_Ays(ra, threadName);
		
	}

	public static void block(Callable  object) {
		while (true) {
			try {
				if(!(Boolean) object.call())
					break;
				sleep(1000);
				
			} catch (Exception e) {
				 ExUtil.throwExV2(e);
			}
		
		}
		
	}

 

 

}
