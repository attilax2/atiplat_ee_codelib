/**
 * 
 */
package com.attilax.function;

import java.util.Map;

import com.attilax.lang.FunctinImp;
import com.google.inject.ImplementedBy;

/**
 * 
 * for (int cur_row = 0; cur_row < height; cur_row++) {
			// int ta = 0, tr = 0, tg = 0, tb = 0;
			for (int cur_col = 0; cur_col < width; cur_col++) {
				mtrx.fill_and_setMtrxPositionXY(cur_row, cur_col);

				Map mtrxCenterXy_inImg = null;
				if (mtrx.hasAnyForgeColor(check_isWhether_forge_color_fun)) {
					mtrxCenterXy_inImg = mtrx.getCenterXy();
					dest.setRGB((int) mtrxCenterXy_inImg.get("x"),
							(int) mtrxCenterXy_inImg.get("y"),
							mtrx.getForgeColor());
				}

			}
		}
 * @author attilax
 *2016年4月14日 下午2:42:20
 */
@ImplementedBy(FunctinImp.class)
public interface Function<T, R> extends java.util.function.Function<T, R> {

}
