package com.attilax.json;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.attilax.core;
import com.google.gson.Gson;

public class JSONObject {

	public JSONObject(Object m) {
		this.obj = m;
	}

	public static void main(String[] args) {

		Map m = new HashMap();
		m.put("d", new Date());
		// m.put(key, value)

		JSONObject.fromObject(m).toString(2);
	}

	public String toString(int i) {

		// if(i==2) //fmt
		{
			try {
				return net.sf.json.JSONObject.fromObject(this.obj).toString(i);
			} catch (Throwable e) {

				try {
					// 创建一个gson对象
					Gson gson = new Gson();

					// 转换成json
					String json = gson.toJson(this.obj);
					return json;
				} catch (Throwable e2) {
					return JsonUtil4jackjson.buildNormalBinder().toJson(
							this.obj);
				}

			}

		}

	}

	public Object obj;

	public static JSONObject fromObject(Object m) {
		// TODO Auto-generated method stub
		return new JSONObject(m);
	}

}
