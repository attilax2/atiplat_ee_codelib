package com.attilax.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyledEditorKit.ForegroundAction;

import aaaCms.CmsImpLocalFileVer;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.SerialUtil;
import com.attilax.lang.YamlAtiX;
import com.attilax.core;
import com.attilax.atian.PinyinX;
import com.attilax.formatter.FormatterEasyuiImp;
import com.attilax.index.IndexManager;
import com.attilax.index.IndexStoreService;
import com.attilax.io.dirx;
import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Closure2;
import com.attilax.lang.ParamX;
import com.attilax.lang.SerialUtil;
import com.attilax.lang.YamlAtiX;
import com.attilax.text.strUtil;


@Deprecated  
/**
 * use index service
 * @author attilax
 *2016年4月7日 下午2:34:03
 */
public class IndexManager {

	private String indexDir;
	private movFulltxtIdxGener fulltxtIndexGener;

	public IndexManager(String indexDir) {
		// TODO Auto-generated constructor stub
		this.indexDir = indexDir;
	}
	@SuppressWarnings("all")
	public static void main(String[] args) {
		// mov index manager
		IndexManager im = new IndexManager(pathx.classPathParent_jensyegeor()
				+ "/index_movs");
		im.fulltxtIndexGener = new movFulltxtIdxGener();
		// im.createFulltxtIndex("searchIndex", "");
	 	CmsImpLocalFileVer.maindir = "D:\\z/";
		
		
		List<Map> li = new CmsImpLocalFileVer(). getitemNameList_fullinfo();
		
		Closure2 filedVaalClosure = new Closure2 () {

			@Override
			public Object execute(Object arg0) {
				Map map=(Map) arg0;
				String f = (String) map.get("file_path");
				String cate_id =new CmsImpLocalFileVer(). getCateid(f);
				return cate_id;
			}
		};
		im.createIndex("cateIndexs", "material_type",li, filedVaalClosure);
	  

		System.out.println("--f");
	}

	
	/**
	 * normal 
	 * @param indexName
	 * @param filedName
	 * @param table
	 * @return
	 */
	public Object createIndex(String indexName, final String filedName,
			List table,Closure2 filedVaalClosure) {
		String idxDir = this.indexDir + "/" + indexName;
		filex.createAllPath(idxDir + "/aa.txt");

		Object idx = null;
		if(filedVaalClosure!=null)
			  idx = colIndexGener_exe(table, filedName,  filedVaalClosure);
//		else
//			idx = colIndexGener_exe(table, "",new Closure2() {
//
//				@Override
//				public Object execute(Object arg0) {
//					Map m = (Map) arg0;
//					return m.get(filedName);
//				}
//			}.execute(null));
			
		String indexFile = idxDir + "/1.txt";

		String idxFile2 = idxDir + "/2.txt";

		new IndexStoreService().geneIndex(idx, indexFile, idxFile2);
		return filedName;
	}
	
	 

	@SuppressWarnings("all")
	private Object colIndexGener_exe(List<Map> table,String filedName, Closure2 filedVaalClosure) {

		Map idx = new HashMap();
		
		for (Map map : table) {
		
			Object filedVal=map.get(filedName);
			if(filedVaalClosure!=null)
				filedVal=filedVaalClosure.execute(map);
			List<Map> cate_Li = (List<Map>) idx.get(filedVal);
			if (cate_Li == null) {
				// cate_Li=n;
				idx.put(filedVal, new ArrayList<Map>());
				cate_Li = (List<Map>) idx.get(filedVal);
			}
			cate_Li.add(map);
		}
		return idx;
	}


	/**
	 * manu gegenenere
	 * 
	 * @param indexName
	 * @param fldname
	 * @return
	 */
	public Object createFulltxtIndex(String indexName, String fldname) {
		String idxDir = this.indexDir + "/" + indexName;
		filex.createAllPath(idxDir + "/aa.txt");
		String indexFile = idxDir + "/1.txt";
		Object idx = this.fulltxtIndexGener.exe();
		SerialUtil.write(idx, indexFile);
		String idxFile2 = idxDir + "/2.txt";
		SerialUtil.write(idx, idxFile2);

		return "";
	}

	public Object rebuildIndex(String fld) {
		return fld;

	}

	public Object del(String indexName) {
		return indexName;

	}

}
