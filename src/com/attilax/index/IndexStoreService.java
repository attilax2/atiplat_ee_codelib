package com.attilax.index;

import java.io.File;

import com.attilax.core;
import com.attilax.io.filex;
import com.attilax.lang.SerialUtil;

public class IndexStoreService {

	/**
	 * safe  asyn mode
	 * 
	 * @param itemNameList
	 * @param index_file1
	 * @param idx_f2
	 */
	public void geneIndex(final Object itemNameList, final String index_file1,
			final String idx_f2) {
		core.newThread(new Runnable() {

			@Override
			public void run() {
				try {
					filex.createAllPath(index_file1);
					if (!new File(index_file1).exists())
						SerialUtil.write(itemNameList, index_file1);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, "threadName");

		core.newThread(new Runnable() {

			@Override
			public void run() {

				try {
					filex.createAllPath(idx_f2);
					if (!new File(idx_f2).exists())
						SerialUtil.write(itemNameList, idx_f2);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, "threadName2");

	}
}
