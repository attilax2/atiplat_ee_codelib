/**
 * 
 */
package com.attilax.bankcard;

import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.attilax.ioc.IocXq214;
import com.attilax.lang.Global;
import com.attilax.net.requestImp;
import com.attilax.sql.SqlCheckor;
import com.attilax.sql.SqlService;
import com.attilax.sql.uniqCheckEx;
import com.attilax.token.TokenService;
import com.attilax.user.NotLoginEx;
import com.attilax.web.ReqX;
import com.google.inject.Inject;

/**
 * com.attilax.bankcard.BankcardService
 * 
 * @author attilax 2016年4月18日 下午9:36:06
 */
public class BankcardService {
	@Inject
	public TokenService tkSrv;

	public static void main(String[] args) {

		System.setProperty("apptype", "jobus");

		BankcardService bs = IocXq214.getBean(BankcardService.class);
		requestImp ri=new requestImp();
		ri.setParam("aliAccount", "aaa@aa.com");
		ri.setParam("aliUserName", "namexxx");
		Global.req.set(ri);
		
		Cookie ck=new  Cookie("null_uid_userMod", "attilaxID");
		ri.addCookie(ck);
 	//    ri.getCookies();
		
	System.out.println(bs.merge());	

	}

	@Inject
	SqlCheckor SqlCheckor2;
	@Inject
	SqlService sqlSrv;

	public Object merge() {
		// tkSrv.setModule(req.getParameter("$utype") + "Mod");
		if(StringUtils.isEmpty(tkSrv.getuid()))
			throw new NotLoginEx("");
		HttpServletRequest req = Global.req.get();
		Map m = ReqX.toMap(req);
		iniBankno();
		String s = "update bankcard set cardno='$cardno$',realname='$name$' where uid='$uid$'";
		s = s.replace("$cardno$", m.get("aliAccount").toString())
				.replace("$uid$", tkSrv.getuid())
				.replace("$name$", m.get("aliUserName").toString());
		System.out.println(s);
		return sqlSrv.executeUpdate(s);

		// return null;

	}

	/**
	 * attilax 2016年4月18日 下午10:41:19
	 */
	private void iniBankno() {

		try {
			SqlCheckor2.uniqCheck("bankcard", "uid", tkSrv.getuid());

			String s = " insert bankcard(uid,cardno) value('$uid$','')";
			s = s.replace("$uid$", tkSrv.getuid());
			sqlSrv.executeUpdate(s);
		} catch (uniqCheckEx e) {
			// TODO: handle exception
		}
	

	}

}
