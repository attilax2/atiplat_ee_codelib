/**
 * 
 */
package com.attilax.service;

import com.attilax.lang.Global;
import com.attilax.util.DataRang;

/**
 * @author attilax
 *2016年11月17日 下午4:47:52
 */
public class BaseService {
	/**
	attilax    2016年11月17日  下午4:48:35
	 * @return
	 */
	public DataRang iniDataRang() {
		DataRang drDataRang=	new DataRang();
		drDataRang.startDate=getFrmReq("startDate");
	    drDataRang.endDate=getFrmReq("endDate");
		return drDataRang;
	}

	/**
	attilax    2016年11月17日  下午4:44:35
	 * @return
	 */
	public String getFrmReq(String httpPara) {
		String rzt = null;
		if(Global.req.get()!=null)
			if(Global.req.get().getParameter(httpPara)!=null)
				rzt=Global.req.get().getParameter(httpPara);
		return rzt;
	}
}
