package com.attilax.shbye;

import com.attilax.util.randomx;

public class Konfyoren {
	
	public static void main(String[] args) {
		String s=new Konfyoren().exec("1234");
		System.out.println(s);
	}

	public String exe(int prob, String result) {
	 int n=randomx. random(1,100);
	 if(n>prob)
		 return exec(result);
		return result;
	}

	private String exec(String result) {
		
		return result.substring(0, 2)+randomx.randomChar(2);
	}

}
