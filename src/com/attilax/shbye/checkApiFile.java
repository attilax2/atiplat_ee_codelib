package com.attilax.shbye;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class checkApiFile {
	
	/*	 DLL 文件MD5值校验
	 *  用处：近期有不法份子采用替换 官方dll文件的方式，极大的破坏了开发者的利益
	 *  
	 *  所以建议所有开发者在软件里面增加校验官方MD5值的函数
	 *   
	 */
	
	public static boolean check(String ApiFile,int softId,String softKey) throws IOException{
	    /**
	     * 验证API文件是否是官方版本 
	     * http校验接口说明：http://dll.uuwise.com/index.php?n=ApiDoc.ApiFileCheck
	     * 
	     * @param ApiFile
	     *            API文件的相对路径
	     * @param softId
	     *            软件ID
	     * @param softKey
	     *            软件KEY
	     * @return 所代表远程资源的响应结果
	     * 			  校验成功返回 1 失败返回 0
	     */
		
		/*	API文件验证服务器列表	*/
		String[] checkServer={"http://v.uuwise.com/service/verify.aspx","http://v.uuwise.net/service/verify.aspx","http://v.uudama.com/service/verify.aspx","http://v.uudati.cn/service/verify.aspx","http://v.taskok.com/service/verify.aspx"};
		int i=0;
		
		String apiFileMd5=checkApiFile.GetFileMD5(ApiFile);		//api文件的MD5值
		StringBuffer k=new StringBuffer();
		StringBuffer o=new StringBuffer();
		k.append(softId).append(apiFileMd5.toUpperCase()).toString();	
		String key=checkApiFile.Md5(k.toString());			//key = md5(SoftID+ dllkey.大写)
		o.append(softId).append(softKey.toUpperCase()).toString();
		String ok=checkApiFile.Md5(o.toString());			//md5(softID+softkey.大写)
		
		String c="SID="+softId+"&dllkey="+apiFileMd5+"&key="+key;	//post参数
		
		while(i<5){
			String status=checkApiFile.sendPost(checkServer[i],c);
			if(status.equals(ok)){
				return true;
			}
			i++;
		}
		return false;
	}
	
	
	//MD5校验函数开始
    /**
     * 获取指定文件的MD5值
     * 
     * @param inputFile
     *            文件的相对路径
     */
	public static String GetFileMD5(String inputFile) throws IOException {
		int bufferSize = 256 * 1024;
		FileInputStream fileInputStream = null;
		DigestInputStream digestInputStream = null;
		try {
			MessageDigest messageDigest =MessageDigest.getInstance("MD5");
			fileInputStream = new FileInputStream(inputFile);
			digestInputStream = new DigestInputStream(fileInputStream,messageDigest);
			byte[] buffer =new byte[bufferSize];
			while (digestInputStream.read(buffer) > 0);
			messageDigest= digestInputStream.getMessageDigest();
			byte[] resultByteArray = messageDigest.digest();
			return byteArrayToHex(resultByteArray);
		} catch (NoSuchAlgorithmException e) {
			return null;
		}finally {
			try {
				digestInputStream.close();
			}catch (Exception e) {
				
			}try {
				fileInputStream.close();
			}catch (Exception e) {
				
			}
		}
	}
	public static String Md5(String s) throws IOException{
		try {
			byte[] btInput = s.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			return byteArrayToHex(md);
		}catch (Exception e) {
            e.printStackTrace();
            return null;
        }
		
	}
	public static String byteArrayToHex(byte[] byteArray) {
		char[] hexDigits = {'0','1','2','3','4','5','6','7','8','9', 'a','b','c','d','e','f' };
		char[] resultCharArray =new char[byteArray.length * 2];
		int index = 0;
		for (byte b : byteArray) {
			resultCharArray[index++] = hexDigits[b>>> 4 & 0xf];
			resultCharArray[index++] = hexDigits[b& 0xf];
		}
		return new String(resultCharArray);
	}
	
	//MD5校验函数结束
    /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent","VersionClient");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }  
}
