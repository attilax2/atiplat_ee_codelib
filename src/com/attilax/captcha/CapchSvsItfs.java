/**
 * 
 */
package com.attilax.captcha;

/**
 * @author attilax
 *2016年11月22日 下午8:13:20
 */
public interface CapchSvsItfs {

	public abstract String setCapt();

	public abstract boolean check(String captcha) throws CapchCheckFailEx;

}