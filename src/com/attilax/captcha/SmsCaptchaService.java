package com.attilax.captcha;

import java.util.Set;

import javax.servlet.http.HttpSession;

import com.attilax.captcha.CaptchaService;
import com.attilax.corePkg.RandomX;
import com.attilax.json.AtiJson;
import com.attilax.lang.Global;
import com.attilax.net.requestImp;
import com.attilax.secury.AesV2q421;
import com.attilax.sms.Sms1xinxi;
import com.attilax.sms.SmsService;
import com.attilax.sms.SmsServiceYyuntongxunImp;
import com.attilax.web.sessionImp;
import com.csmy.my.center.util.RequestUtil;
import com.google.common.collect.Sets;
import com.google.inject.Inject;

/**
 * com.attilax.sms.SmsCaptchaService
 * 
 * @author Administrator
 *
 */
public class SmsCaptchaService extends CaptchaService {
	@Inject
	CaptchSvsSpec captSvs;

	@Inject
	SmsService smsSrv;
	// @Inject
	RandomX rdmx = new RandomX();

	public static void main(String[] args) {
		// System.out.println( new SmsCaptchaService().send("18573344543",
		// "xxcontent"));
		requestImp ri = new requestImp();
		sessionImp si = new sessionImp();
		ri.setSession(si);
		Global.req.set(ri);
		SmsCaptchaService SmsCaptchaService1 = SmsCaptchaService.buildAsYuntonsyon();
		// System.out.println(SmsCaptchaService1.send("18867311166" )); ;
		// 135240
	//	List<String>
		for (int i = 0; i <1; i++) {
			SmsCaptchaService1.send("18573344543");
//			SmsCaptchaService1.send("15675393017");
//			SmsCaptchaService1.send("18573344543");
//			SmsCaptchaService1.send("15675393017");
//			SmsCaptchaService1.send("18573344543");
//			SmsCaptchaService1.send("15675393017");
		}

		// SmsCaptchaService1.send("18182055888" );
		// SmsCaptchaService1.send("18867311166" );

	}

	public static SmsCaptchaService buildAsYuntonsyon() {

		SmsServiceYyuntongxunImp smsS = new SmsServiceYyuntongxunImp();
		smsS.tmp_id = "135240";
		smsS.tmp_id = "135886";
		smsS.tmp_id = "135240";
		
		
		SmsCaptchaService svs = new SmsCaptchaService();
		svs.smsSrv = smsS;
		return svs;
	}

	public static SmsCaptchaService buildAsSms1xinxi() {

		SmsService smsS = new Sms1xinxi();

		SmsCaptchaService svs = new SmsCaptchaService();
		svs.smsSrv = smsS;
		return svs;
	}

	public Object send(String mobile) {

		String Capt = super.setCapt();

		Object sendRzt = smsSrv.send(mobile, String.valueOf(Capt));
		return sendRzt;
	}

	public Object send(String mobile, String content) {
		int rdm = new RandomX().randomNum(1000, 9999);

		content = content.replace("$capt$", String.valueOf(rdm));
		// if(smsSrv==null)
		// Sms1xinxi smsSrv = new Sms1xinxi();
		Object send = smsSrv.send(mobile, content);
		HttpSession session = Global.req.get().getSession();
		Set<String> st = (Set<String>) session.getAttribute("captSet");
		if (st == null)
			st = Sets.newHashSet();
		st.add(String.valueOf(rdm));
		session.setAttribute("captSet", st);
		session.setAttribute("capt", String.valueOf(rdm));
		return send;
	}

}
