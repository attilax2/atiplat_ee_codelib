/**
 * 
 */
package com.attilax.captcha;

/**com.attilax.captcha.CapchCheckFailEx
 * @author attilax
 *2016年11月22日 下午4:16:25
 */
public class CapchCheckFailEx extends Exception {

	/**
	 * @param string
	 */
	public CapchCheckFailEx(String string) {
		super(string);
	}

}
