package com.attilax.oodb;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.attilax.exception.ExUtil;
import com.attilax.linq.AExpression;
import com.attilax.linq.LikeExpression;
import com.attilax.oodb.imp.MovTable;
import com.attilax.oodbStoreEngine.StoreEngine;

public class DBms {
	public Table tab;
	public   DBms from(Class<? extends Table>  class1) {
		//  tab;
		try {
			  tab =(Table) class1.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			ExUtil.throwExV2(e);
		}
		  return this;
		
	}
	public List<Map> where(Optional<AExpression> likeExpression) {
		StoreEngine se=tab.ENGINE;
		
		return  se.where(likeExpression);
	}
	public List<Map> select() {
	 
		StoreEngine se=tab.ENGINE;
		return se.select();
	}

}
