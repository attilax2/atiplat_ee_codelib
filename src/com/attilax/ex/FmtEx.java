package com.attilax.ex;

import java.io.IOException;

public class FmtEx extends Exception {

	public FmtEx(String message, Exception e) {
		super(message,e);
	}

	public FmtEx(String message) {
		super(message);
	}

}
