/**
 * 
 */
package com.attilax.agt;

/**
 * @author attilax
 *2016年11月14日 下午8:37:25
 */
public class CantSetSelfEx extends Exception {

	/**
	 * @param string
	 */
	public CantSetSelfEx(String string) {
		super(string);
	}

}
