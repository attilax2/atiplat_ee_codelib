package com.attilax.namespace;

import java.util.ArrayList;
import java.util.List;

import com.attilax.fsm.camelScanner.CamelScannerQ1;
import com.attilax.json.JSONObject;

public class ClassNameSplitor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	//	["com","attilax","camel","JDBC","Gbnfm","hhh","comm"]
		String s="com.attilax_camelJDBCGbnfm-hhh/comm$123";
	     //	s="CamelGbnfm";
			List<String> li=new ClassNameSplitor().split(s);
			System.out.println( JSONObject.fromObject(li).toString(2));
		 
	}
	
	public static List<String> split(String clsPath)
	{
		List<String> li_r=new ArrayList<String>();
		List<String> li_Multi=NamespaceUtil.toList_byDotByCase(clsPath);
		for (String li_Multi_entry : li_Multi) {
			List<String> tokenList = new CamelScannerQ1().getTokenList_retStr(li_Multi_entry);
			li_r.addAll(tokenList);
		}
		
		
//		for (String tk : tokenList) {
//			List<String> li_t=NamespaceUtil.toList_byDotByCase(tk);
//		
//		}
		return li_r;
	}

}
