package com.attilax.js;

import org.apache.commons.lang3.StringUtils;

import com.attilax.lang.AString;
import com.attilax.lang.text.strUtil;

public class Str {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AString as=new AString("abc");
		AString left = as.Left(  2);
	//	String string = left.toString();
		System.out.println(left);

	}
	
	public  static String	 Mid(String string,int  start, int length)
	{
		return string.substring(start, start+length);
	}
	
	public  static String		 LTrim( String string)
	{
		return string.trim();
	}
	public  static String		 RTrim( String string)
	{
		return string.trim();
	}
	public  static String		 Space( int  number)
	{
		String r="";
		for(int i=0;i<number;i++)
		{
			r=r+" ";
		}
		return  r;
	}
	  
	public  static String   Left(String string, int length)
	
	{
		return string.substring(length);
		
	}
	
public  static String   Right(String string, int length)
	
	{
	return string.substring(string.length()-length);
	}
public  static String   LCase(String string )

{
	return string.toLowerCase();
}
public  static String   UCase(String string )

{
	return string.toUpperCase();
}
	
/**
 *     Join(list, delimiter)
	    返回一个字符串，此字符串由包含在数组中的许多子字符串联接创建。
	    list：必选。包含要联接的子字符串一维数组。 
	    delimiter：可选。在返回字符串中用于分隔子字符串的字符。如果省略，将使用空字符 ("")。 如果 delimiter 是零长度字符串，则在同一列表中列出全部项，没有分界符。
	    
 * @param list
 * @param string
 * @return
 */
public  static String   Join(String[] list,String string )

{
	return string; 
 
}
	
/**
 *     返回字符串，此字符串与指定字符串顺序相反。
	    string：参数是要进行字符反向的字符串。如果 string 是零长度字符串 ("")，则返回零长度字符串。如果 string1 为 Null，则会出现错误。
 * @param string
 * @return
 */
public  static String   StrReverse( String string )

{
	return string; 
 
}
		
//	    StrComp(string1, string2, compare)
//	    返回指示字符串比较结果的值。StrComp 函数有以下返回值：-1, 0, 1, Null。
//	    string1, string2：必选。任意有效的字符串表达式。 
//	    compare：可选。参数值同上。
 

}
