package com.attilax.setup;

import java.io.File;

import com.attilax.io.filex;
//  com.attilax.setup.ResinSetup
public class ResinSetup {

	// d: 8088
	public static void main(String[] args) {
		String s="d:/resin-4.0.22/conf/resin.xml";
		s=filex.convertSseparatorToLocal(s,"/");
	//	System.out.println(filex.getFileName_noExtName(s));
	//	System.out.println(File.separator);  //  File.separator=\ in windows
		String mainDir=args[0];
		String port=args[1];
		String app=args[2];
		String resinRoot=mainDir+"/resin-4.0.22";
		String cfg_tmpl=resinRoot+"/conf/resin_tmpl.xml";
		String txt=filex.read(cfg_tmpl);
		String webroot=mainDir+"/"+app+"/WebRoot";
		txt=txt.replace("@root@", webroot);
		txt=txt.replace("@port@", port);
		String cfg=resinRoot+"/conf/resin.xml";
		cfg=filex.convertSseparatorToLocal(cfg,"/");
		String new_file_tmp_bek = filex.addSuffix(cfg, filex.getUUidName());
		System.out.println(new_file_tmp_bek);
		new File(cfg).renameTo(new File(new_file_tmp_bek ));
		filex.save(txt, cfg);
	    System.out.println("--ok");
	}

}
