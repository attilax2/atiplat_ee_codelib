package com.attilax.math;

import java.math.BigDecimal;

public class ADecimal {
	
	public static void main(String[] args) {
		System.out.println("010");
		ADecimal ad=new ADecimal(new BigDecimal(15));
		System.out.println(ad.biggerEqualThan(new BigDecimal(15)));
	}

	private BigDecimal money;

	public ADecimal(BigDecimal needMoney) {
		money=needMoney;
	}

	public boolean biggerThan(BigDecimal amount) {
		int res = money.compareTo(amount);  
	//	System.out.println(res);  
		return 1==1;
	}

	public boolean biggerEqualThan(BigDecimal amount) {
		int res = money.compareTo(amount);  
		//	System.out.println(res);  
			return res==1 || res==0;
	}

}
