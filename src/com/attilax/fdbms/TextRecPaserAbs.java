package com.attilax.fdbms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.attilax.core;
import com.attilax.formatter.FormatterX;
import com.attilax.io.filex;
import com.attilax.lang.MapX;
import com.attilax.lang.arrX;

public class TextRecPaserAbs {
	String f;
	String lineSplitor = ",";
	String colsSplitor = " ";
	private String encode;

	public static void main(String[] args) {
		t2();
		System.out.println("--f");
	}

	private static void t2() {
		String f = "d:\\m_p9.txt";

		TextRecPaserAbs p = new TextRecPaserAbs();
		p.f = f;
		p.lineSplitor = "\r\n";
		p.encode = "gbk";
		List li = p.getRows();
		String t=new FormatterX().tableFmt(li);
		System.out.println(t);
	//	System.out.println(core.toJsonStrO88(li));
	}
	private static void t() {
		String f = "d:\\m_pa0.txt";

		TextRecPaserAbs p = new TextRecPaserAbs();
		p.f = f;
		p.lineSplitor = "/";
		p.encode = "gbk";
		List li = p.getRows();
		String t=new FormatterX().tableFmt(li);
		System.out.println(t);
	//	System.out.println(core.toJsonStrO88(li));
	}

	public List getRows() {
		List li = new ArrayList();
		String t = filex.read(f, encode);
		String[] ta = t.split(lineSplitor);

		for (String line : ta) {
			try {
				Map m = getRow(line);
				nowDate = (String) m.get("date");
				li.add(m);
			} catch (Exception e) {
				e.printStackTrace();
			}
			

		}
		return li;
	};

	String nowDate;

	public Map getRow(String line) {
		Map m = MapX.newOrderMap();
		String[] a = line.split(colsSplitor);
		if (isDate(a[0])) {
			m.put("date", a[0]);
			m.put("money", a[1]);
			m.put("cls", arrX.get(a, 2));
			m.put("demo", arrX.get(a, 3));
		} else {
			// m cls mode
			m.put("date", nowDate);
			m.put("money", a[0]);
			m.put("cls", arrX.get(a, 1));
			m.put("demo", arrX.get(a, 2));
		}
		String d=(String) m.get("date");
		d=d.replace(".", "-");
		m.put("date", d);
		return m;
	}

	private boolean isDate(String string) {
		if (string.contains("."))
			return true;
		else
			return false;
	};

}
