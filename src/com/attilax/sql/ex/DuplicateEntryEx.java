/**
 * 
 */
package com.attilax.sql.ex;

import java.sql.SQLException;

/**
 * @author attilax
 *2016年11月10日 下午4:56:39
 */
public class DuplicateEntryEx extends Exception {

	/**
	 * @param string 
	 * @param e
	 */
	public DuplicateEntryEx(String string, SQLException e) {
		super(string,e);
	}

	/**
	 * @param string
	 * @param e
	 */
	public DuplicateEntryEx(String string, DuplicateEntryEx e) {
		super(string,e);
	}

}
