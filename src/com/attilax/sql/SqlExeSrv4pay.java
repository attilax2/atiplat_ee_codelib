package com.attilax.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import aaaCfg.ConnectionImp;

import com.attilax.persistence.DBCfg;
import com.attilax.time.timeUtil;
import com.google.inject.Inject;

/**
 * gamePay
 * @author Administrator
 *
 */
public class SqlExeSrv4pay extends SqlExeService {
	
	@Inject
	protected
	DBCfg dbcfg;
	public Connection getConnection()  {
		if(this.conn instanceof ConnectionImp)
			this.conn=null;
		
		if(this.conn!=null)
			return this.conn;
		// com.microsoft.sqlserver.jdbc.SQLServerDriver
	
		//System.out.println(PropX.getConfig(path, "jdbc.url"));
		
		if(dbcfg==null)
			throw new connEx("Cdbcfg is null  " );
		try {

			Class.forName(dbcfg.getDriver());
		} catch (ClassNotFoundException e) {
			throw new connEx("Class.forName driver err,drive class name is :  "+dbcfg.getDriver(),e);
		}catch( NullPointerException e)
		{
			throw new connEx("dbcfg.getDriver() is null  " );
		}
	//	Connection conn;
//
//		conn = DriverManager.getConnection(dbcfg.getUrl(), dbcfg.getUser(), dbcfg.getPassword());
//		
//		try {
//
//			Class.forName("com.mysql.jdbc.Driver");
//		} catch (ClassNotFoundException e) {
//			throw new getConnEx("getconnex" + e.getMessage());
//		}
		Connection conn;
		try {
			conn = DriverManager.getConnection(
					dbcfg.getUrl(),
					dbcfg.getUser(),
					dbcfg.getPassword());
			System.out.println("get conn frm sql serv 4 pay implt : "+ conn+" at:"+timeUtil.Now_CST());
			
		} catch (SQLException e) {
			throw new connEx("getconnex" + e.getMessage());
		}
		this.conn=conn;
		return conn;
	}
	
	
	public Map executeQueryFirstRow(String s) throws EmptyRzt {
		List li= executeQuery(s);
		if(li.size()==0)
			throw new EmptyRzt(s);
		return (Map) li.get(0);
		
	}
 
	
	public Map uniqueResult2row(String sql) throws EmptyRzt 
	{
		return  executeQueryFirstRow(sql);
	}


	/**
	attilax    2016年10月24日  下午8:28:13
	 */
	public void openTrans() {
		super.openTrans();
		
	}

}

