package com.attilax.encry;

//package com.ca.test;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;

import org.apache.commons.codec.binary.Base64;

import com.attilax.exception.ExUtil;
import com.attilax.io.filex;
import com.attilax.json.AtiJson;
import com.google.common.collect.Maps;
//import com.sun.media.ExtBuffer;

/**
 * 非对称加密算法RSA算法组件 非对称算法一般是用来传送对称加密算法的密钥来使用的，相对于DH算法，RSA算法只需要一方构造密钥，不需要
 * 大费周章的构造各自本地的密钥对了。DH算法只能算法非对称算法的底层实现。而RSA算法算法实现起来较为简单
 * 
 * @author kongqz
 * */
public class RSACoder {
	// 非对称密钥算法
	public static final String KEY_ALGORITHM = "RSA";

	/**
	 * 密钥长度，DH算法的默认密钥长度是1024 密钥长度必须是64的倍数，在512到65536位之间
	 * */
	private static final int KEY_SIZE = 512;
	// 公钥
	public static final String PUBLIC_KEY = "RSAPublicKey";

	// 私钥
	public static final String PRIVATE_KEY = "RSAPrivateKey";

	/**
	 * 初始化密钥对
	 * 
	 * @return Map 甲方密钥的Map
	 * */
	public static Map<String, Object> initKey() throws Exception {
		// 实例化密钥生成器
		KeyPairGenerator keyPairGenerator = KeyPairGenerator
				.getInstance(KEY_ALGORITHM);
		// 初始化密钥生成器
		keyPairGenerator.initialize(KEY_SIZE, new myFixSeed());
		// keyPairGenerator.set
		// keyPairGenerator.
		// 生成密钥对
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		// 甲方公钥
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		// 甲方私钥
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		// 将密钥存储在map中
		Map<String, Object> keyMap = new HashMap<String, Object>();
		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;

	}

	public static Map<String, Object> initKey_fixKey(String md5_deta)
			throws Exception {
		/** RSA算法要求有一个可信任的随机数源 */
		SecureRandom secureRandom = new SecureRandom("fixx".getBytes("gbk"));
		// SecureRandom.getInstance("12345678");
		// 实例化密钥对生成器
		KeyPairGenerator keyPairGen = KeyPairGenerator
				.getInstance(KEY_ALGORITHM);
		// 初始化密钥对生成器
		// keyPairGen.initialize(KEY_SIZE);
		// 初始化的时候固定随机源的值
		keyPairGen.initialize(KEY_SIZE, secureRandom);
		// 生成密钥对
		KeyPair keyPair = keyPairGen.generateKeyPair();
		// 公钥
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		// 私钥
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		// 得到公钥字符串
		// String publicKeyString = Base64.encode(publicKey.getEncoded());
		// 得到私钥字符串
		// String privateKeyString = Base64.encode(privateKey.getEncoded());
		// 封装密钥
		Map<String, Object> keyMap = new HashMap<String, Object>(2);
		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}

	public   Map<String, Object> initKey_fixKeyM2(String fixSeed) {
		/** RSA算法要求有一个可信任的随机数源 */
		SecureRandom secureRandom = new SecureRandom();
		try {
			secureRandom.setSeed(fixSeed.getBytes("gbk"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// new SecureRandom("fixx".getBytes("gbk"));
		// SecureRandom.getInstance("12345678");
		// 实例化密钥对生成器
		KeyPairGenerator keyPairGen = null;
		try {
			keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			ExUtil.throwEx(e);
		}
		// 初始化密钥对生成器
		// keyPairGen.initialize(KEY_SIZE);
		// 初始化的时候固定随机源的值
		keyPairGen.initialize(KEY_SIZE, secureRandom);
		// 生成密钥对
		KeyPair keyPair = keyPairGen.generateKeyPair();
		// 公钥
		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
		// 私钥
		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
		// 得到公钥字符串
		// String publicKeyString = Base64.encode(publicKey.getEncoded());
		// 得到私钥字符串
		// String privateKeyString = Base64.encode(privateKey.getEncoded());
		// 封装密钥
		Map<String, Object> keyMap = new HashMap<String, Object>(2);
		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		
		
		// 公钥
		try {
			pubkey = RSACoder.getPublicKey(keyMap);
		} catch (Exception e) {
			ExUtil.throwEx(e);
		}

				// 私钥
		prikey = RSACoder.getPrivateKey(keyMap);
				  pubkey_s = com.attilax.encode.Base64.encode(pubkey);
						//  .encodeBase64String(pubkey);
				  prikey_s = com.attilax.encode.Base64.encodeBase64String(prikey);
			//	System.out.println("公钥：/n" + encodeBase64String);
			//	System.out.println("私钥：/n" + Base64.encodeBase64String(privateKey));
		return keyMap;
	}
	public byte[] pubkey;
	public	byte[] prikey;
	public	String pubkey_s;
	public	String prikey_s;
	/**
	 * 私钥加密
	 * 
	 * @param data待加密数据
	 * @param key
	 *            密钥
	 * @return byte[] 加密数据
	 * */
	public static byte[] encryptByPrivateKey(byte[] data, byte[] key)
			  {

		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
		KeyFactory keyFactory;
		try {
			keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		
		// 生成私钥
		PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		return cipher.doFinal(data);
		} catch (Exception e) {
			ExUtil.throwEx(e);
		}
		return key;
	}

	/**
	 * 公钥加密
	 * 
	 * @param data待加密数据
	 * @param key
	 *            密钥
	 * @return byte[] 加密数据
	 * */
	public static byte[] encryptByPublicKey(byte[] data, byte[] key)
			throws Exception {

		// 实例化密钥工厂
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 初始化公钥
		// 密钥材料转换
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
		// 产生公钥
		PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

		// 数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, pubKey);
		return cipher.doFinal(data);
	}

	/**
	 * 私钥解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            密钥
	 * @return byte[] 解密数据
	 * */
	public static byte[] decryptByPrivateKey(byte[] data, byte[] key)
			  {
		try {
			// 取得私钥
			PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(key);
			KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
			// 生成私钥
			PrivateKey privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
			// 数据解密
			Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return cipher.doFinal(data);
		} catch (Exception e) {
		ExUtil.throwEx(e);
		}
		return null;
		
	}

	/**
	 * 公钥解密
	 * 
	 * @param data
	 *            待解密数据
	 * @param key
	 *            密钥
	 * @return byte[] 解密数据
	 * */
	public static byte[] decryptByPublicKey(byte[] data, byte[] key)
			throws Exception {

		// 实例化密钥工厂
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 初始化公钥
		// 密钥材料转换
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(key);
		// 产生公钥
		PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);
		// 数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, pubKey);
		return cipher.doFinal(data);
	}

	/**
	 * 取得私钥
	 * 
	 * @param keyMap
	 *            密钥map
	 * @return byte[] 私钥
	 * */
	public static byte[] getPrivateKey(Map<String, Object> keyMap) {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return key.getEncoded();
	}

	/**
	 * 取得公钥
	 * 
	 * @param keyMap
	 *            密钥map
	 * @return byte[] 公钥
	 * */
	public static byte[] getPublicKey(Map<String, Object> keyMap)
			throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);
		return key.getEncoded();
	}
	/**
	 * "fixseed" "c:\\0key\\pri.txt"  "c:\\0key\\pub.txt" 
	 * @param args
	 */
synchronized	public static void main(String[] args)
	{
	 main_rzt=new ThreadLocal<>(); 
		String fix_seed=args[0];
		String prikeySaveFile=args[1];
	
		String pubkeySaveFile=args[2];
		//filex.createAllPath(pubkeySaveFile);
	
		
	//	String isEcho 
		RSACoder rsaCoder = new RSACoder();
		Map<String,Object> keyMap= rsaCoder.initKey_fixKeyM2(fix_seed);
		
		if(prikeySaveFile.trim().length()>0)
		{
			filex.createAllPath(prikeySaveFile,false);
			filex.save(rsaCoder.prikey_s, prikeySaveFile.trim(),"gbk");
		}
		if(pubkeySaveFile.trim().length()>0)
		{
			filex.createAllPath(pubkeySaveFile);
			filex.save(rsaCoder.pubkey_s, pubkeySaveFile.trim());
		}
		Map m=Maps.newHashMap();
		m.put("pri", rsaCoder.prikey_s);
		m.put("pub", rsaCoder.pubkey_s);
		main_rzt.set( AtiJson.toJson(m));
		System.out.println(main_rzt.get());
	}
synchronized	public static ThreadLocal<String> getMain_rzt() {
		return main_rzt;
	}

	public static void setMain_rzt(ThreadLocal<String> main_rzt) {
		RSACoder.main_rzt = main_rzt;
	}
	public static  ThreadLocal<String>  main_rzt=new ThreadLocal<>(); 
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main0(String[] args) throws Exception {
		// 初始化密钥
		// 生成密钥对
//		Map<String, Object> keyMap = RSACoder.initKey();
//		// 公钥
//		byte[] publicKey = RSACoder.getPublicKey(keyMap);
//
//		// 私钥
//		byte[] privateKey = RSACoder.getPrivateKey(keyMap);
//		System.out.println("公钥：/n" + Base64.encodeBase64String(publicKey));
//		System.out.println("私钥：/n" + Base64.encodeBase64String(privateKey));
//
//		System.out
//				.println("================密钥对构造完毕,甲方将公钥公布给乙方，开始进行加密数据的传输=============");
//		String str = "RSA密码交换算法";
//		System.out.println("/n===========甲方向乙方发送加密数据==============");
//		System.out.println("原文:" + str);
//		// 甲方进行数据的加密
//		byte[] code1 = RSACoder.encryptByPrivateKey(str.getBytes(), privateKey);
//		System.out.println("加密后的数据：" + Base64.encodeBase64String(code1));
//		System.out.println("===========乙方使用甲方提供的公钥对数据进行解密==============");
//		// 乙方进行数据的解密
//		byte[] decode1 = RSACoder.decryptByPublicKey(code1, publicKey);
//		System.out.println("乙方解密后的数据：" + new String(decode1) + "/n/n");
//
//		System.out.println("===========反向进行操作，乙方向甲方发送数据==============/n/n");
//
//		str = "乙方向甲方发送数据RSA算法";
//
//		System.out.println("原文:" + str);
//
//		// 乙方使用公钥对数据进行加密
//		byte[] code2 = RSACoder.encryptByPublicKey(str.getBytes(), publicKey);
//		System.out.println("===========乙方使用公钥对数据进行加密==============");
//		System.out.println("加密后的数据：" + Base64.encodeBase64String(code2));
//
//		System.out.println("=============乙方将数据传送给甲方======================");
//		System.out.println("===========甲方使用私钥对数据进行解密==============");
//
//		// 甲方使用私钥对数据进行解密
//		byte[] decode2 = RSACoder.decryptByPrivateKey(code2, privateKey);
//
//		System.out.println("甲方解密后的数据：" + new String(decode2));
	}
}

/*
 * 控制台输出： 公钥：
 * MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAM0qc+eVm4pyBbCBuM4jRxOYsxWkylLXVklyWp3JNs71
 * B6vLVg2Iwh6TwPbpXbGWOI11RMLhe5bwLpdeBqP4f4MCAwEAAQ== 私钥：
 * MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAzSpz55WbinIFsIG4ziNHE5izFaTK
 * UtdWSXJanck2zvUHq8tWDYjCHpPA9uldsZY4jXVEwuF7lvAul14Go/h/gwIDAQABAkByhW3pKSOH
 * Zhoy6lYkCqEUGTptrGybTUQG/2QBi12iqzenm7rKlasjdMEr6Q8RX5RpGArzvhvLepAPqPZ5LXjR
 * AiEA7vI8/9HHrZSbHQ4Up7xby7bOULknoRjZy68HRMPRSXUCIQDbzwQfvrYNqLiauTcNsg6ejCJw
 * cxSCLlJupgwBBjyOFwIgVEbWIviPr/ZpGz9bI6o4ykoozKnxg01ri/6o1qUmTP0CIQDBC0XO73oJ
 * 8vT2BdZA8/D884vHgHoxfqcswC3otGZ4TwIhANGXz8GHxM0zuOg8pds54S1ARlrkUXFULocVZ2Ka
 * ngFf ================密钥对构造完毕,甲方将公钥公布给乙方，开始进行加密数据的传输=============
 * ===========甲方向乙方发送加密数据============== 原文:RSA密码交换算法
 * 加密后的数据：kF3vekz0DpJmLzwdrWjfZLyGBWsKzFOwm
 * +8im85KZr6QA49csvCl9KgsfjSVLuXwgsPrFoEhkiqQ 0+VApr001A==
 * ===========乙方使用甲方提供的公钥对数据进行解密============== 乙方解密后的数据：RSA密码交换算法
 * 
 * ===========反向进行操作，乙方向甲方发送数据==============
 * 
 * 原文:乙方向甲方发送数据RSA算法 ===========乙方使用公钥对数据进行加密==============
 * 加密后的数据：xM1MB3DlDKRwSozf9z8YQlftrWpKT8lkabi17cN
 * /ZLOEkOO8jVLKVAjpjsMwtJMcpHTjntdkHKOm s1V8xbh3og==
 * =============乙方将数据传送给甲方======================
 * ===========甲方使用私钥对数据进行解密============== 甲方解密后的数据：乙方向甲方发送数据RSA算法
 */
