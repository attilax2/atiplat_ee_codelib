package com.attilax.encry;

import java.io.FileInputStream;
import java.security.cert.CertificateExpiredException;  
import java.security.cert.CertificateFactory;  
import java.security.cert.CertificateNotYetValidException;  
import java.security.cert.X509Certificate;  
import java.util.Calendar;  
import java.util.Date;  

import com.attilax.json.AtiJson;
  
public class certUtil {  
    public static void main(String args[]) throws Exception {  
  
        // 获取X509Certificate的对象  
        // 从命令行中读入需要验证的证书文件  
        CertificateFactory of = CertificateFactory.getInstance("x.509");  
        FileInputStream in = new FileInputStream("C:\\youan.cer");  
        java.security.cert.Certificate ceof = of.generateCertificate(in);  
        System.out.println(ceof.getPublicKey().getEncoded() + "\t"  
                + ceof.getEncoded());  
        in.close();  
        X509Certificate t = (X509Certificate) ceof;  
  System.out.println("Certificate:"+AtiJson.toJson(ceof));
  System.out.println("X509Certificate"+AtiJson.toJson(t));    
        // 获取日期  
        // 验证证书在某个日期是否有效，从命令行读入年月日，由此生成Date()对象。有Date类的很多设置年月日的方法已经不提倡使用，因此改用Calendar类，Calendar类也是一个工厂类，通过getInstance()方法获得对象，然后使用set()方法设置时间，最后通过其getTime()方法获得Date()对象由于Calendar类的set()方法参数是整数，因此对命令行参数读入的年月日字符串使用Integer.parseInt()方法转换为整数。由于Calendar类的set()方法设置月份时从0开始，0代表1月，11代表12月，因此命令读入的月份要减去1  
        Calendar cld = Calendar.getInstance();  
        int year = Integer.parseInt("2012");  
        int month = Integer.parseInt("9");  
        int day = Integer.parseInt("24");  
        cld.set(year, month, day);  
        Date d = cld.getTime();  
        System.out.println(d);  
        try {  
            // 检验证书  
            t.checkValidity(d);  
  
            System.out.print("OK");  
        }  
  
        // 处理CertificateExpiredException异常  
        // 若证书在指定日期已经过期，则产生CertificateExpiredException异常，在cath语句中作相关处理  
        catch (CertificateExpiredException e) {  
            System.out.println("expired");  
            System.out.println(e.getMessage());  
        }  
  
        // 处理CertificateNorYetValidException异常  
        // 若证书在指定日期尚未生效，则产生CertificateNorYetValidException异常，在cath语句中作相关处理  
        catch (CertificateNotYetValidException e) {  
            System.out.println("Too early");  
            System.out.println(e.getMessage());  
        }  
    }  
}  