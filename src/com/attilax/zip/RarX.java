package com.attilax.zip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.attilax.Closure2;

import de.innosystec.unrar.Archive;
import de.innosystec.unrar.exception.RarException;
import de.innosystec.unrar.rarfile.FileHeader;

/**
 * IDocHandler 为自己写的接口类
 * 
 */
public class RarX {

	public static void main(String[] args) throws Exception {
		unrar("E:\\down\\51vpn.rar", new Closure2() {

			@Override
			public Object execute(Object arg0) {
			System.out.println(arg0);
				return null;
			}
		});
	}

	/**
	 * } 解压rar格式压缩包。
	 * 对应的是java-unrar-0.3.jar，但是java-unrar-0.3.jar又会用到commons-logging-1.1.1.jar
	 */
	private static void unrar(String sourceRar, Closure2 cls) {
		Archive a = null;
		FileOutputStream fos = null;

		try {
			a = new Archive(new File(sourceRar));
		} catch (RarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		FileHeader fh = a.nextFileHeader();
		while (fh != null) {
			if (!fh.isDirectory()) {
				cls.execute(fh.getFileNameString());
			}
			fh = a.nextFileHeader();
		}
		try {
			a.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		a = null;

	}
}
