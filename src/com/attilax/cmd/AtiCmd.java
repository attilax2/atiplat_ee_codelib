package com.attilax.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.Callable;

import com.attilax.io.filex;
import com.attilax.io.pathx;
import com.attilax.lang.Closure2;


/**
 *com.attilax.cmd.AtiCmd
 * @author attilax
 *2016年6月8日 下午7:13:27
 */
public class AtiCmd {

	public static void main(String[] args) {
	 
	 	String r = new AtiCmd().exe("sc query wampmysqld");
System.out.println("----echo:"+r);
System.out.println("--f");


	}
//	private static String t2() {
//	//	pathx.isWebPathMode=true;
//		String bat=pathx.classPathParent()+"/0screenUtil.bat";
//		bat=bat.replace("/", "\\");
//	 
//		String cmd="schtasks /create /tn atitask@id@ /tr \"'@cmd@'\" /sc MINUTE /mo 2  ".replace("@cmd@", bat).replace("@id@", filex.getUUidName());
//		System.out.println(cmd);
//		String r=exe(cmd);
//		
//		return r;
//	}
//	private static String t() {
//	//	pathx.isWebPathMode=true;
//String bat=pathx.classPathParent()+"/other/del_indexs.bat";
//bat=bat.replace("/", "\\");
//String cmd=" cmd.exe /c \""+bat+"\"";
//System.out.println(cmd);
//String r=exe(cmd);
//		return r;
//	}
@Deprecated
	public static void redirectEcho2Screen(Process process) {
		System.out.println("--echo stdin info:");
		echoCmdResult_out(process.getInputStream());
		System.out.println("--echo err info:");
		echoCmdResult_out(process.getErrorStream());
		
	}

	public  String   exe(String cmd) {
		try {
			String r = "";
			// 执行 CMD 命令
			Process process = Runtime.getRuntime().exec(cmd);
		//	System.out.println("--echo stdin info:");
			r = echoCmdResult_asStr(process.getInputStream());
	//		System.out.println("--echo err info:");
			r = r + "\r\n" + echoCmdResult_asStr(process.getErrorStream());

			//System.out.println("cmd ext finish!");
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
	
	public static String exe(String cmd,Runnable rx) {
		try {
			String r = "";
			// 执行 CMD 命令
			Process process = Runtime.getRuntime().exec(cmd);
		//	System.out.println("--echo stdin info:");
			r = echoCmdResult_asStr(process.getInputStream());
	//		System.out.println("--echo err info:");
			r = r + "\r\n" + echoCmdResult_asStr(process.getErrorStream());

			//System.out.println("cmd ext finish!");
			return r;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
	
	private static void echoCmdResult_out(InputStream cmdStream,Closure2   lineEvent)

	{
		
		 
		 
		BufferedReader reader = null;
		// BufferedReader reader;
		InputStreamReader inputStreamReader;
		try {
			inputStreamReader = new InputStreamReader(cmdStream,
					"gbk");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		reader = new BufferedReader(inputStreamReader);
		String line = null;
		try {
			line = reader.readLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		while (line != null) {
			lineEvent.execute(line);
			try {
				line = reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	//	return reader;
	}
	
	private static void echoCmdResult_out(InputStream cmdStream)

	{
		
		 
		 
		BufferedReader reader = null;
		// BufferedReader reader;
		InputStreamReader inputStreamReader;
		try {
			inputStreamReader = new InputStreamReader(cmdStream,
					"gbk");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		reader = new BufferedReader(inputStreamReader);
		String line = null;
		try {
			line = reader.readLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		while (line != null) {
			System.out.println(line);
			try {
				line = reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	//	return reader;
	}
	
	private static String echoCmdResult_asStr(InputStream cmdStream)

	{
		String r="";
		
		 
		 
		BufferedReader reader = null;
		// BufferedReader reader;
		InputStreamReader inputStreamReader;
		try {
			inputStreamReader = new InputStreamReader(cmdStream,
					"gbk");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		reader = new BufferedReader(inputStreamReader);
		String line = null;
		try {
			line = reader.readLine();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		while (line != null) {
		//	System.out.println(line);
			r=r+"\r\n"+line;
			try {
				//int count = reader.available();   BufferedReader.readLine
				line = reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	 	return r;
	}
	

}
