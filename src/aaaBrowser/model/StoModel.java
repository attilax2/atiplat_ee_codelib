package aaaBrowser.model;

/**
 * 存储实体类
 * 
 * @author  moyan
 * @version  [版本号, 2015年4月26日]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class StoModel
{ 
    private String  uid;
    
    private  String siteid;
    
    private String username;
    
    private String password;

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getSiteid()
    {
        return siteid;
    }

    public void setSiteid(String siteid)
    {
        this.siteid = siteid;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    
    
}
