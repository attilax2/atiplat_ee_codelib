package aaaBrowser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;

import javax.swing.SpringLayout;

import aaaBrowser.Reg.User;
import aaaBrowser.model.PushModel;
import aaaBrowser.model.RegModel;
import aaaBrowser.model.StoModel;
import aaaCfg.IocX;
import chrriis.dj.nativeswing.swtimpl.demo.examples.webbrowser.Ati4vod;

import com.attilax.core;
import com.attilax.io.pathx;
import com.attilax.lang.IniEx;
import com.attilax.lang.ui.Effec;
import com.attilax.lang.ui.Swingx;
import com.attilax.lang.ui.SysTrayX;
import com.attilax.lang.ui.TipsFrame;
import com.attilax.net.websitex;
import com.attilax.seo.AppIco;
import com.attilax.shbye.YejmX;
import com.attilax.skin.IS‌kinInir;
import com.attilax.skin.SkinX;
import com.attilax.ui.MsgBox;
import com.attilax.web.UrlX;
//import com.birosoft.liquid.LiquidLookAndFeel;

import javax.swing.JComboBox;

import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JPasswordField;

import net.sf.json.JSONObject;

import javax.swing.JCheckBox;
/**
 * aaaBrowser.LoginForm
 * @author Administrator
 *
 */
public class LoginForm extends JFrame {

	private JFrame frame;
	private JTextField txtUname;
	JComboBox comboBox;
	private JPasswordField passwordField;
	public static SysTrayX tray;

	/**
	 * Launch the application.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException, UnsupportedEncodingException {
		// UIManager.setLookAndFeel("javax.swing.plaf.mac.MacLookAndFeel");
		// LiquidLookAndFeel.setLiquidDecorations(true );
		// JFrame.setDefaultLookAndFeelDecorated(true);
		// UIManager.setLookAn dFeel(LnF);
		// SwingUtilities.updateComponentTreeUI(this);
		String parent =pathx.classPathParent();
		parent=	URLDecoder.decode(parent,"utf-8");
		final String path = parent + "\\icon.png";
		System.out.println("--icon path:"+path);

		final ImageIcon icon = new ImageIcon(path);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm window = new LoginForm();
					window.setIconImage(icon.getImage());
					// window.show();
					// window.comboBox.addItem(arg0);
					// window.comboBox.addItem(new String[]{"a","b"});
					// SkinX.setComboBoxVal(window.comboBox);
					// window.comboBox.addItem(SkinX.skinNames());
					// window.frame.setUndecorated(true);
					window.comboBox.setVisible(false);
					// window.txtUname.setVisible(false);
					// window.passwordField.setVisible(false);
					window.frame.setIconImage(icon.getImage());
					window.frame.setTitle("..destoon信息发布器");
					Swingx.setBackgroudImg(window.frame,
							pathx.classPath(IocX.class) + "\\loginP4na.png");
					Swingx.NoTitleNboder(window.frame);
					window.frame.setVisible(true);
					Swingx.dragWin(window.frame);
					window.frame.setLocationRelativeTo(null);
					Swingx.midShow(window.frame);
					tray = new SysTrayX(path, "destoon信息发布器", null);
					// Effec.show(window.frame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public LoginForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 662	, 392);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);

		JLabel lblNewLabel = new JLabel("密  码");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 54,
				SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 27,
				SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel, 103,
				SpringLayout.WEST, frame.getContentPane());
		// springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel, 0,
		// SpringLayout.NORTH, frame.getContentPane());

	//	frame.getContentPane().add(lblNewLabel);

		txtUname = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, txtUname, 125, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, txtUname, 440, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, txtUname, -55, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, txtUname, -241, SpringLayout.SOUTH, frame.getContentPane());
		txtUname.setText("Leather");
		frame.getContentPane().add(txtUname);
		txtUname.setColumns(10);
		txtUname.setBorder(BorderFactory.createEmptyBorder());
		Swingx.setBorderNull(txtUname);

		JButton btnNewButton = new JButton("");
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 395, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -90, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -50, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 265, SpringLayout.NORTH, frame.getContentPane());
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					btn_click();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedLookAndFeelException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String name = txtUname.getText();
				// String
				// imgPostion=txtimagesallwatchesttwimagemedium.getText();
				// String
				// tmp=" ln -s   @img  /home_src/@wbst0/public_html/images/";
				// tmp=tmp.replaceAll("@img", imgPostion).replaceAll("@wbst",
				// name);
				// tmp=tmp+"small"+"\r\n"+tmp+"medium"+"\r\n"+tmp+"large";
				// textArea.setText(tmp);
			}
		});
	//	btnNewButton.setBackground(Color.TRANSLUCENT);
	//	btnNewButton.setOpaque(true);
	//	btnNewButton.setFocusPainted(false);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
 	// 	btnNewButton.setIcon(new ImageIcon(pathx.classPath(IocX.class)+"\\transp4.png"));
		btnNewButton.setContentAreaFilled(false);
		Swingx.setTransBtn(btnNewButton);
		frame.getContentPane().add(btnNewButton);

		JLabel label = new JLabel("用户名");
		springLayout.putConstraint(SpringLayout.NORTH, label, 24,
				SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, label, 27,
				SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, label, 103,
				SpringLayout.WEST, frame.getContentPane());
	//	frame.getContentPane().add(label);

		comboBox = new JComboBox();
		springLayout.putConstraint(SpringLayout.EAST, comboBox, -148,
				SpringLayout.EAST, frame.getContentPane());
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				skinCombox_change_event(arg0);
			}
		});
	//	frame.getContentPane().add(comboBox);

		JLabel lblSkin = new JLabel("skin");
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 49,
				SpringLayout.EAST, lblSkin);
		springLayout.putConstraint(SpringLayout.NORTH, lblSkin, 22,
				SpringLayout.SOUTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.NORTH, comboBox, -3,
				SpringLayout.NORTH, lblSkin);
		springLayout.putConstraint(SpringLayout.WEST, lblSkin, 40,
				SpringLayout.WEST, frame.getContentPane());
	//	frame.getContentPane().add(lblSkin);

		passwordField = new JPasswordField();
	
		springLayout.putConstraint(SpringLayout.SOUTH, passwordField, -189, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, passwordField, 174, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, passwordField, 440, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, passwordField, -55, SpringLayout.EAST, frame.getContentPane());
	 Swingx.setBorderNull(passwordField);
		frame.getContentPane().add(passwordField);
		
		JCheckBox checkBox = new JCheckBox("");
		springLayout.putConstraint(SpringLayout.SOUTH, checkBox, -155, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, checkBox, -260, SpringLayout.EAST, frame.getContentPane());
		Swingx.setBorderNull(checkBox);
		frame.getContentPane().add(checkBox);
		
		JButton buttonCtrl = new JButton("");
		buttonCtrl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//frame.setMinimumSize(minimumSize);
				Swingx.miniWin(frame);
				//frame.minimumSize();
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, buttonCtrl, 0, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, buttonCtrl, 0, SpringLayout.EAST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, buttonCtrl,35, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, buttonCtrl, -100, SpringLayout.EAST, frame.getContentPane());
		Swingx.setTransBtn(buttonCtrl);
		frame.getContentPane().add(buttonCtrl);
	}

	protected void skinCombox_change_event(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			String index = (String) this.comboBox.getSelectedItem();
			SkinX.setSkin(index, this.frame);
			// MsgBox.setTxt("ttt");
		}

	}

	protected void btn_click() throws ClassNotFoundException,
			InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
	//	passwordField.setText("806101");
		Map mp = new HashMap();

		// 结构：$val={'task_id':'','task_type':'login','username':’’,'password':’’}
		// Leather 806101
		mp.put("task_id", "");
		mp.put("task_type", "login");
		mp.put("username", txtUname.getText());

		mp.put("password", passwordField.getText());// "806101"
		String url = "http://sh.weijicn.cn/api/client.php";

		String param = core.toJsonStrO88(mp);
		System.out.println(param);
		String s;// =HttpRequest. sendGet(url, URLEncoder.encode(param,
					// "utf-8"));
		// mjj79v8v8ufep5rrnore4knk46
		String url2;
		try {
			url2 = url + "?" + URLEncoder.encode(param, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		url2 = url + "?" + UrlX.getQueryStr(mp, "utf-8");
		System.out.println(url2);
		s = websitex.WebpageContent(url2);
		System.out.println(s);
		JSONObject jo = JSONObject.fromObject(s);
		if (jo.containsKey("task_ssid")) {
			// hide win
		 final   Map hmp= new HashMap();
            hmp.put("task_id", (String)jo.get("task_id"));
            hmp.put("task_ssid", (String)jo.get("task_ssid"));
            getRegTask geTask=new getRegTask();
            ArrayList<RegModel> reglist= geTask.getRegtask(hmp);
            
			this.setVisible(false);
			this.frame.setVisible(false);
			tooltipsHide();
		
		  /*  for(int i=3;i<=reglist.size();i++){
		    Reg reg=new Reg();
		    System.out.println(reglist.get(i).getCracturl());
	       User user=reg.start(reglist.get(i),hmp);
	      // reglist.get(i).setUsername(user.getUsername().toString());
	      // reglist.get(i).setPassword(user.getPassword().toString());
	       ResTask retsk = new ResTask();
           StoModel store = new StoModel();
           store.setSiteid(reglist.get(i).getCrsiteid());
           store.setUid(reglist.get(i).getCruid());
           store.setUsername(user.getUsername().toString());
           store.setPassword(user.getPassword().toString());
           System.out.println(store);
	        if(!reg.isAlive()){
	            retsk.store(store, hmp);
	            String urlm=reglist.get(i).getCracturl().replace("register.php", "edit.php").toString();
	            Login_finish_info ffinfo=new Login_finish_info();
	            ffinfo.start(urlm);
	            continue;
	        }
	        }*/
			
		    ArrayList<PushModel> pushlist = new ArrayList();
		    pushlist= geTask.getPushTask(hmp);
		    for(int i=1;i<=pushlist.size();i++){
	            LoginAndPub pub=new LoginAndPub();
	            pub.start(pushlist.get(i));
	            if(!pub.isAlive()){
	                continue;
	            }
	            }
		    
		 	tray.refreshTooltip();
	     	iniCaptchRecong();
			

		} else {
			MsgBox.setTxt("不能登录" + s);
		}

		// SwingUtilities.updateComponentTreeUI(this);

		// LiquidLookAndFeel.setLiquidDecorations(true );

		// change title
		// frame.getContentPane().setw
		// frame.hide();//is outtime ,,will use setVisible
		// frame.setVisible(false);

		// frame.setDefaultLookAndFeelDecorated(arg0);
		// frame.fre

		// SwingUtilities.updateComponentTreeUI(this);//apply new ui

		// window.frame
		// SwingUtilities.upd

	}

	// YejmX yx
	public static YejmX captx;

	private void iniCaptchRecong() {
		// BufferedImage img=ImageIO.read(new File(pathname));
		captx = new YejmX();
		try {
			captx.inix();
		} catch (IniEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		// System.out.println( captx.getResult(img));

	}

	private void tooltipsHide() {
		String title = "友情提示！";
		String message = "<strong>窗口已经隐藏在系统托盘，可以点击托盘菜单恢复</strong><br>";
		// Runnable translucent=new
		// TranslucentFrame(250,180,10,4,title,message);
		Runnable translucent = new TipsFrame(JRootPane.FRAME, title, message);
		Thread thread = new Thread(translucent);
		thread.start();

	}
}
