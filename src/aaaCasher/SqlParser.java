package aaaCasher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aaaCfg.IocX4nodb;

import com.attilax.core;
import com.attilax.biz.seo.getConnEx;
import com.attilax.db.DBX;
import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.PreparedSentence;
import com.openbravo.data.loader.SerializerWriteParams;
import com.openbravo.data.loader.Session;
import com.openbravo.pos.forms.AppConfig;
import com.openbravo.pos.forms.JRootApp;
import com.openbravo.pos.forms.JRootFrame;

/**
 * aaaCasher.SqlParser
 * 
 * @author Administrator
 *
 */
public class SqlParser {
	
	public SqlParser(){}

	@SuppressWarnings("rawtypes")
	public static void main(String[] args) throws SQLException, getConnEx {
		IocX4nodb ic=new IocX4nodb();
		DBX dbx=ic.getBean(DBX.class);
		IocX4nodb.db.set("information_schema");
		Connection connection =dbx.getConnection();
		PreparedStatement prepareStatement = connection
				.prepareStatement("select * from COLUMNS limit 2 ");
		prepareStatement.execute();
		ResultSet resultSet = prepareStatement.getResultSet();

		// PreparedSentence ps = new PreparedSentence(session,
		// " "); //where name like '%name%'
		try {
			List li = convertList(resultSet);
			System.out.println(core.toJsonStrO88(li));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//tt();
		System.out.println("---f");

	}

	private static void tt() throws SQLException {
		Connection connection = getConn();
		PreparedStatement prepareStatement = connection
				.prepareStatement("select * from COLUMNS limit 2 ");
		prepareStatement.execute();
		ResultSet resultSet = prepareStatement.getResultSet();

		// PreparedSentence ps = new PreparedSentence(session,
		// " "); //where name like '%name%'
		try {
			List li = convertList(resultSet);
			System.out.println(core.toJsonStrO88(li));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection getConn() {
		Session session;
		if (JRootFrame.m_rootapp4ati == null) {
			String[] args = null;
			AppConfig config = new AppConfig(new String[0] );
			/* 68 */config.load();
			// JRootFrame rootframe = new JRootFrame();
			// rootframe.initFrame(config);

			JRootApp m_rootapp = new JRootApp();
			/*     */
			/* 55 */m_rootapp.initApp(config);
			JRootFrame.m_rootapp4ati = m_rootapp;
			// JRootApp jra = rootframe.m_rootapp;

			session = m_rootapp.getSession();// SerializerWriteParams.INSTANCE

		}
		session = JRootFrame.m_rootapp4ati.getSession();

		Connection connection = null;
		try {
			connection = session.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
		return connection;

	}

	private static List convertList(ResultSet rs) throws SQLException {
		List list = new ArrayList();
		ResultSetMetaData md = rs.getMetaData();
		int columnCount = md.getColumnCount();
		while (rs.next()) {
			Map rowData = new HashMap();
			for (int i = 1; i <= columnCount; i++) {
				rowData.put(md.getColumnName(i), rs.getObject(i));
			}
			list.add(rowData);
		}
		return list;
	}

	private static Map<String, String> getResultMap(ResultSet rs)
			throws SQLException {
		Map<String, String> hm = new HashMap<String, String>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int count = rsmd.getColumnCount();
		for (int i = 1; i <= count; i++) {
			String key = rsmd.getColumnLabel(i);
			String value = rs.getString(i);
			hm.put(key, value);
		}
		return hm;
	}

	@SuppressWarnings("rawtypes")
	public String exe(String sql) {

		Connection conn = getConn();
	List li=	new DBX().findBySql(conn, sql);
		return core.toJsonStrO88(li);

	}
	
	public List exe_retLi(String sql) {

		Connection conn = getConn();
	List li=	new DBX().findBySql(conn, sql);
		return li;

	}

}
