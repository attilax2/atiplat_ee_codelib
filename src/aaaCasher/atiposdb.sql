/*
Navicat MySQL Data Transfer

Source Server         : loc
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : atiposdb

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-01-05 17:17:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for applications
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  `VERSION` varchar(1024) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of applications
-- ----------------------------

-- ----------------------------
-- Table structure for attribute
-- ----------------------------
DROP TABLE IF EXISTS `attribute`;
CREATE TABLE `attribute` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attribute
-- ----------------------------

-- ----------------------------
-- Table structure for attributeinstance
-- ----------------------------
DROP TABLE IF EXISTS `attributeinstance`;
CREATE TABLE `attributeinstance` (
  `ID` varchar(256) NOT NULL,
  `ATTRIBUTESETINSTANCE_ID` varchar(256) NOT NULL,
  `ATTRIBUTE_ID` varchar(256) NOT NULL,
  `VALUE` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ATTINST_SET` (`ATTRIBUTESETINSTANCE_ID`),
  KEY `ATTINST_ATT` (`ATTRIBUTE_ID`),
  CONSTRAINT `ATTINST_SET` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `ATTINST_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attributeinstance
-- ----------------------------

-- ----------------------------
-- Table structure for attributeset
-- ----------------------------
DROP TABLE IF EXISTS `attributeset`;
CREATE TABLE `attributeset` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attributeset
-- ----------------------------

-- ----------------------------
-- Table structure for attributesetinstance
-- ----------------------------
DROP TABLE IF EXISTS `attributesetinstance`;
CREATE TABLE `attributesetinstance` (
  `ID` varchar(256) NOT NULL,
  `ATTRIBUTESET_ID` varchar(256) NOT NULL,
  `DESCRIPTION` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ATTSETINST_SET` (`ATTRIBUTESET_ID`),
  CONSTRAINT `ATTSETINST_SET` FOREIGN KEY (`ATTRIBUTESET_ID`) REFERENCES `attributeset` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attributesetinstance
-- ----------------------------

-- ----------------------------
-- Table structure for attributeuse
-- ----------------------------
DROP TABLE IF EXISTS `attributeuse`;
CREATE TABLE `attributeuse` (
  `ID` varchar(256) NOT NULL,
  `ATTRIBUTESET_ID` varchar(256) NOT NULL,
  `ATTRIBUTE_ID` varchar(256) NOT NULL,
  `LINENO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ATTUSE_LINE` (`ATTRIBUTESET_ID`,`LINENO`),
  KEY `ATTUSE_ATT` (`ATTRIBUTE_ID`),
  CONSTRAINT `ATTUSE_SET` FOREIGN KEY (`ATTRIBUTESET_ID`) REFERENCES `attributeset` (`ID`) ON DELETE CASCADE,
  CONSTRAINT `ATTUSE_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attributeuse
-- ----------------------------

-- ----------------------------
-- Table structure for attributevalue
-- ----------------------------
DROP TABLE IF EXISTS `attributevalue`;
CREATE TABLE `attributevalue` (
  `ID` varchar(256) NOT NULL,
  `ATTRIBUTE_ID` varchar(256) NOT NULL,
  `VALUE` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ATTVAL_ATT` (`ATTRIBUTE_ID`),
  CONSTRAINT `ATTVAL_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of attributevalue
-- ----------------------------

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  `PARENTID` varchar(256) DEFAULT NULL,
  `IMAGE` blob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('63ce6431-fdac-4b02-b698-b8b25af0889b', 'goodcate2', null, null);
INSERT INTO `categories` VALUES ('a9f00177-3f68-41f9-bbc5-0b30c201f18d', 'goodcate1', null, null);

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `ID` varchar(256) NOT NULL,
  `SEARCHKEY` varchar(1024) DEFAULT NULL,
  `TAXID` varchar(1024) DEFAULT NULL,
  `NAME` varchar(1024) NOT NULL,
  `TAXCATEGORY` varchar(256) DEFAULT NULL,
  `CARD` varchar(1024) DEFAULT NULL,
  `MAXDEBT` double DEFAULT '0',
  `ADDRESS` varchar(1024) DEFAULT NULL,
  `ADDRESS2` varchar(1024) DEFAULT NULL,
  `POSTAL` varchar(1024) DEFAULT NULL,
  `CITY` varchar(1024) DEFAULT NULL,
  `REGION` varchar(1024) DEFAULT NULL,
  `COUNTRY` varchar(1024) DEFAULT NULL,
  `FIRSTNAME` varchar(1024) DEFAULT NULL,
  `LASTNAME` varchar(1024) DEFAULT NULL,
  `EMAIL` varchar(1024) DEFAULT NULL,
  `PHONE` varchar(1024) DEFAULT NULL,
  `PHONE2` varchar(1024) DEFAULT NULL,
  `FAX` varchar(1024) DEFAULT NULL,
  `NOTES` varchar(1024) DEFAULT NULL,
  `VISIBLE` smallint(6) NOT NULL DEFAULT '1',
  `CURDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CURDEBT` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customers
-- ----------------------------

-- ----------------------------
-- Table structure for people
-- ----------------------------
DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  `APPPASSWORD` varchar(1024) DEFAULT NULL,
  `CARD` varchar(1024) DEFAULT NULL,
  `ROLE` varchar(256) NOT NULL,
  `VISIBLE` smallint(6) NOT NULL,
  `IMAGE` blob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of people
-- ----------------------------
INSERT INTO `people` VALUES ('0', 'Administrator', null, null, '0', '1', null);
INSERT INTO `people` VALUES ('1', 'Manager', null, null, '1', '1', null);
INSERT INTO `people` VALUES ('2', 'Employee', null, null, '2', '1', null);
INSERT INTO `people` VALUES ('3', 'Guest', null, null, '3', '1', null);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ID` varchar(256) NOT NULL,
  `REFERENCE` varchar(1024) NOT NULL,
  `CODE` varchar(1024) NOT NULL,
  `CODETYPE` varchar(1024) DEFAULT NULL,
  `NAME` varchar(1024) NOT NULL,
  `PRICEBUY` double NOT NULL,
  `PRICESELL` double NOT NULL,
  `CATEGORY` varchar(256) NOT NULL,
  `TAXCAT` varchar(256) NOT NULL,
  `ATTRIBUTESET_ID` varchar(256) DEFAULT NULL,
  `STOCKCOST` double DEFAULT NULL,
  `STOCKVOLUME` double DEFAULT NULL,
  `IMAGE` blob,
  `ISCOM` smallint(6) NOT NULL DEFAULT '0',
  `ISSCALE` smallint(6) NOT NULL DEFAULT '0',
  `ATTRIBUTES` blob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------

-- ----------------------------
-- Table structure for products_cat
-- ----------------------------
DROP TABLE IF EXISTS `products_cat`;
CREATE TABLE `products_cat` (
  `PRODUCT` varchar(256) NOT NULL,
  `CATORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRODUCT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products_cat
-- ----------------------------

-- ----------------------------
-- Table structure for products_com
-- ----------------------------
DROP TABLE IF EXISTS `products_com`;
CREATE TABLE `products_com` (
  `ID` varchar(256) NOT NULL,
  `PRODUCT` varchar(256) NOT NULL,
  `PRODUCT2` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PCOM_INX_PROD` (`PRODUCT`,`PRODUCT2`),
  KEY `PRODUCTS_COM_FK_2` (`PRODUCT2`),
  CONSTRAINT `PRODUCTS_COM_FK_1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`ID`),
  CONSTRAINT `PRODUCTS_COM_FK_2` FOREIGN KEY (`PRODUCT2`) REFERENCES `products` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products_com
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  `PERMISSIONS` blob,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------

-- ----------------------------
-- Table structure for taxcategories
-- ----------------------------
DROP TABLE IF EXISTS `taxcategories`;
CREATE TABLE `taxcategories` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of taxcategories
-- ----------------------------
INSERT INTO `taxcategories` VALUES ('000', 'Tax Exempt');
INSERT INTO `taxcategories` VALUES ('001', 'Tax Standard');

-- ----------------------------
-- Table structure for taxes
-- ----------------------------
DROP TABLE IF EXISTS `taxes`;
CREATE TABLE `taxes` (
  `ID` varchar(256) NOT NULL,
  `NAME` varchar(1024) NOT NULL,
  `VALIDFROM` timestamp NOT NULL DEFAULT '2001-01-01 00:00:00',
  `CATEGORY` varchar(256) NOT NULL,
  `CUSTCATEGORY` varchar(256) DEFAULT NULL,
  `PARENTID` varchar(256) DEFAULT NULL,
  `RATE` double NOT NULL,
  `RATECASCADE` smallint(6) NOT NULL DEFAULT '0',
  `RATEORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of taxes
-- ----------------------------
INSERT INTO `taxes` VALUES ('000', 'Tax Exempt', '2001-01-01 00:00:00', '000', null, null, '0', '0', null);
INSERT INTO `taxes` VALUES ('001', 'Tax Standard', '2001-01-01 00:00:00', '001', null, null, '0.1', '0', null);
