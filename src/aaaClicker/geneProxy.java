package aaaClicker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import aaaCfg.IocX;

import com.attilax.anno.Inj;
import com.attilax.io.filex;
import com.attilax.net.websitex;
import com.google.inject.Inject;

public class geneProxy {

	public static void main(String[] args) throws IOException {

		geneProxy gp = IocX.getBean(geneProxy.class);
		gp.start();
	}

	@Inject
	ListParser lstp;

	@Inject
	PerLineProcessor plp;

	private void start() {
		filex fx;
		try {
			fx = new filex("e:\\proxy3.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		List<String> urls = getUrls();
		for (String url : urls) {
			System.out.println(url);
			try {
				perUrl(fx, url);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		try {
			fx.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void perUrl(filex fx, String url) {
		String t = websitex.WebpageContent(url);
		List<String> prxys = lstp.getlist(t);
		for (String string : prxys) {
			System.out.println(string);
			try {
				plp.process(fx, string);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	private List<String> getUrls() {
		List<String> li = new ArrayList<String>();
		for (int i = 1; i < 10; i++) {
			String s = "http://www.kuaidaili.com/free/inha/@n/";
			s = s.replaceAll("@n", String.valueOf(i));
			li.add(s);
		}
		return li;
	}

}
