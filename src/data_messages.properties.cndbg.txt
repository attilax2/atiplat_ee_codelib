#.Openbravo的POS是一个触摸屏应用而设计的销售点。
#.
#.版权所有(C)2007 - 2009年Openbravo S.L.
#.http://sourceforge.net/projects/openbravopos
#.
#.本文件是Openbravo的POS的一部分。
#.
#.Openbravo的POS是免费软件：你可以根据自由软件基金会出版的第三版或更新版的GNU通用公共出版许可证条款，重新发布它和/或修改。
#.
#.我们希望Openbravo的POS的发布是有益的，但不提供任何担保（甚至没有默示的保证）其适销性或针对特定用途。
#.
#.更多细节请见GNU通用公共许可证的。
#.
#.您应该已经收到一份附在Openbravo的POS上的GNU通用公共许可证的副本。如果没有，参阅<http://www.gnu.org/licenses/>。


exception.noupdatecount=末能获取更新总计

exception.nodataset=数据集末定意

exception.nonegativelimits=负面的限制不允许的

exception.outofbounds=超出了指定范围

exception.noreadfile=读取指定文件错误

exception.nofinishedfile=指是文件不存在

exception.notnull=值不能为NULL

exception.noparamtype=参数类型不批配

qbf.none =无

qbf.null =为空

qbf.notnull =不为空

qbf.equals=等于

qbf.distinct=排除

qbf.greater=大于

qbf.less=小于

qbf.greaterequals=大于等于

qbf.lessequals=小于等于
label.findwhat=查找
label.where=哪里
label.match=批配
label.casesensitive=区分大小写
button.ok=确定
button.cancel=取消

title.find=查寻

list.startfield=开始字段

list.wholefield=整个字段

list.anypart=字段的任意部分

list.re=规则表大式
title.message=数据库信息
button.information=通知

message.norecord=没有找到指定的记录

message.nolistdata=没找到指定的列表数据

message.noreload=指定的列表数据无法加载

message.nomove=不能移动到其它记录

message.nosave=记录末保存

message.nodelete=不能设置册除标记

message.nonew=无法新建记录

sgn.danger=严重错误\:

sgn.warning=错误\:

sgn.caution=警告\:

sgn.notice=注意\:

sgn.important=重要\:

sgn.success=成功\:

sgn.unknown=末知的\:

exception.nocompare=未能对比记录

exception.nodelete=末能册除记录

exception.noupdate=末能列新记录

exception.noinsert=末能新增加记录

message.changeslost=当前修改末保存，是否放弃修改?

title.editor=编辑器信息

message.wannasave=退出前是否要保存修改?

label.imagefiles=图型文件

message.resizeimage=当前选择的图像态大，是否自动调整到合适的大小?

qbf.re=匹配值

label.sortby=排序

label.andby=和

message.nosort=排序条件不正确

caption.sort=排序

exception.parserconfig= XML分析出错. 请联系管理员

exception.xmlfile= XML文件格式不批配

exception.iofile= 文件读取错误