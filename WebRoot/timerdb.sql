/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : timerdb

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-07-08 11:21:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `timer_tab`
-- ----------------------------
DROP TABLE IF EXISTS `timer_tab`;
CREATE TABLE `timer_tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` varchar(11) NOT NULL,
  `timex` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of timer_tab
-- ----------------------------
INSERT INTO `timer_tab` VALUES ('1', '1小', '0');
INSERT INTO `timer_tab` VALUES ('2', '2小', '0');
INSERT INTO `timer_tab` VALUES ('3', '3小', '0');
INSERT INTO `timer_tab` VALUES ('4', '7小', '5240');
INSERT INTO `timer_tab` VALUES ('5', '8小', '0');
INSERT INTO `timer_tab` VALUES ('6', '11小', '0');
INSERT INTO `timer_tab` VALUES ('7', '15小', '0');
INSERT INTO `timer_tab` VALUES ('8', '18小', '0');
INSERT INTO `timer_tab` VALUES ('9', '5中', '1760');
INSERT INTO `timer_tab` VALUES ('10', '6中', '0');
INSERT INTO `timer_tab` VALUES ('11', '9中', '0');
INSERT INTO `timer_tab` VALUES ('12', '10中', '0');
INSERT INTO `timer_tab` VALUES ('13', '12中', '0');
INSERT INTO `timer_tab` VALUES ('14', '19中', '0');
INSERT INTO `timer_tab` VALUES ('15', '20中', '0');
INSERT INTO `timer_tab` VALUES ('16', '21中', '0');
INSERT INTO `timer_tab` VALUES ('17', '22中', '0');
INSERT INTO `timer_tab` VALUES ('18', '23中', '0');
INSERT INTO `timer_tab` VALUES ('19', '16大', '0');
INSERT INTO `timer_tab` VALUES ('20', '17大', '0');
