<?php
require_once(dirname(__FILE__).'/../lang/ArrayJava.php');

class String{
	public $str;
		Function   __construct($str) 
	  {
		  
		  $this->str=$str;
	  }
		Function length()
	{
	return strlen($this->str);	
	}
	//same with java
	Function lastIndexOf($findChar)
	{
	return strrpos	($this->str,$findChar);
	}
	//same with java 
	Function substring($start,$end=9999)
	{
		if($end==9999)
				return substr($this->str,$start);
			else
				return substr($this->str,$start,$end-$start);
	}
	Function contains($find)
	{
	return strstr	($this->str,$find);
	}

	Function split($splitor)
	{
	$a=	explode($splitor,$this->str);
	return new ArrayJava($a);

	}
	
}

/*$s="abcdefghijklmn";
$str=new String($s);
//echo $str->lastIndexOf("i");
//echo $str->substring(3); echo "----";
//echo $str->substring(3,7);
 
$fileName="d:\\a\b.jpg";
 $fileName=str_replace("\\","/",$fileName);
// echo $fileName;

$fileName=new String("name.jpg");
	$i = $fileName->lastIndexOf(".");echo "index:"+$i;
	
$extention = $fileName->substring($i+1); // --扩展名
echo $extention;*/