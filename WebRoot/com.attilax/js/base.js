$(function(){
	//列表界面 表格中的checkbox 全选 全不选
	$("#selectAll").click(function(){
		if($("#selectAll").attr("checked")==true){
			
			$("td.checkedClass input[type='checkbox']").each(function(){
				$(this).attr("checked",true);
			});
			
		}else{
			
			$("td.checkedClass input[type='checkbox']").each(function(){
				$(this).attr("checked",false);
			});
		}
	});
});
