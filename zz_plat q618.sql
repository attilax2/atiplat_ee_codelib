/*
Navicat MySQL Data Transfer

Source Server         : loc
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : zz_plat

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-06-18 23:43:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unionid` varchar(255) NOT NULL DEFAULT '',
  `promoter` bigint(20) unsigned NOT NULL DEFAULT '0',
  `plat` varchar(64) NOT NULL DEFAULT '',
  `totalRmb` int(10) unsigned NOT NULL DEFAULT '0',
  `createdTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `unionid` (`unionid`)
) ENGINE=InnoDB AUTO_INCREMENT=3004 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1000', 'C8-0A-A9-95-B4-65', '888', '', '0', '0000-00-00 00:00:00');
INSERT INTO `account` VALUES ('1001', '98-90-96-D0-11-94', '888', '', '0', '0000-00-00 00:00:00');
INSERT INTO `account` VALUES ('3002', '', '1000', '', '0', '2016-06-14 15:41:45');
INSERT INTO `account` VALUES ('3003', '', '1001', '', '0', '2016-06-14 23:32:38');

-- ----------------------------
-- Table structure for agent
-- ----------------------------
DROP TABLE IF EXISTS `agent`;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `wechat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agent
-- ----------------------------
INSERT INTO `agent` VALUES ('1', '888', 'ati', '0', '000', null, null, null);
INSERT INTO `agent` VALUES ('9', '1000', null, '888', '111111', null, null, null);
INSERT INTO `agent` VALUES ('12', '1001', null, '888', '111111', null, null, null);

-- ----------------------------
-- Table structure for promoter
-- ----------------------------
DROP TABLE IF EXISTS `promoter`;
CREATE TABLE `promoter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `createdTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promoter
-- ----------------------------

-- ----------------------------
-- Table structure for recharge
-- ----------------------------
DROP TABLE IF EXISTS `recharge`;
CREATE TABLE `recharge` (
  `orderid` varchar(120) NOT NULL COMMENT '订单ID',
  `accountId` bigint(20) unsigned NOT NULL,
  `platform` int(10) unsigned NOT NULL COMMENT '充值平台',
  `rmb` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`orderid`),
  KEY `accountId` (`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recharge
-- ----------------------------
INSERT INTO `recharge` VALUES ('1', '1000', '0', '2', '2016-06-09 17:04:26');
INSERT INTO `recharge` VALUES ('2', '3002', '0', '325', '2016-06-14 15:41:27');
INSERT INTO `recharge` VALUES ('3', '3003', '0', '326', '2016-06-14 23:33:38');
INSERT INTO `recharge` VALUES ('4', '1000', '0', '150', '2016-06-09 17:03:43');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `user_id` varchar(8) NOT NULL COMMENT '用户编号',
  `user_name` varchar(40) NOT NULL COMMENT '用户名',
  `account` varchar(40) NOT NULL COMMENT '登录帐户',
  `password` varchar(40) NOT NULL COMMENT '登录密码',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `user_type` varchar(2) DEFAULT '1' COMMENT '人员类型(1:客服账号;2:管理账号;3:内置账号;4：财务账号 5：物流账号)',
  `enabled` varchar(2) NOT NULL DEFAULT '1' COMMENT '启用状态',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `role_id` varchar(200) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('23946449', '超级管理员', 'admin', 'MoKM2eflo/Y=', '系统管理员账号', '3', '1', '2013-08-16 20:35:00', '57414897');
INSERT INTO `user_info` VALUES ('28486868', '客服小薇', 'kehu', 'byHQ+jOqWSc=', '新增客户账号', '1', '1', '2015-02-09 00:30:56', '92122707');
INSERT INTO `user_info` VALUES ('67091985', '财政部长', 'caiwu', 'RaFutWeh0ww=', '财政部长--财神爷', '4', '1', '2015-03-03 19:49:14', '31058276');
INSERT INTO `user_info` VALUES ('77954297', '开发人员', 'kaifa', 'RaFutWeh0ww=', '系统开发人员账号', '3', '0', '2013-08-16 20:30:00', '57414897');
INSERT INTO `user_info` VALUES ('84769616', '客服小强', 'kehu1', 'RaFutWeh0ww=', '客服账号', '1', '1', '2015-02-11 01:34:58', '92122707');

-- ----------------------------
-- View structure for acc_rechg
-- ----------------------------
DROP VIEW IF EXISTS `acc_rechg`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `acc_rechg` AS select * from recharge left join account on recharge.accountId=account.id ;
